(function(window){
    'use strict';
    function define_cas(){
        var casOnOffCheck = [];
        var casInitCount = -1;
        var cas = function(){
            console.log('Constructor');
            var cascallQueue, user, contentId, casInitObj, serviceInitialized, serviceRunning, currentCasObj, currentCasMethod, currentCasQueueId, isProdEnv, browserUrl, CAS_CONSTANTS, initCasService, isCasAvailable, chkCasCredential,
                checkCasAvailable, next, thiS, SETTINGS, fetchFromCas, resetAndDoNext, addOrUpdateToCas, casLocalInitCount, AIOTCAS;
            
            this.serviceInitialized = false;
            this.serviceRunning = false;
            this.currentCasObj = null;
            this.currentCasMethod = null;
            this.currentCasQueueId = null;
            this.browserUrl = window.location.href;
            this.isProdEnv = (this.browserUrl.indexOf('.thinkcentral')!==-1 || this.browserUrl.indexOf('.hrw')!==-1)?true:false;
            this.isCasAvailable = false;
            this.chkCasCredential = '';
            this.casLocalInitCount = casInitCount++;
            this.AIOTCAS = 0;
            thiS = this;

            this.SETTINGS = {//Static object, used accross the lib
                EVENT_SETTINGS: {
                    EVENT_TOPICS: {
                        RESPONSE_EVENT: "CAS Integration - Response !!",
                        REQUEST_EVENT: "CAS Integration - Request !!"
                    },
                    ACTIONS: {
                        ADD: "created",
                        UPDATE: "updated",
                        FETCH: "fetch",
                        DELETE: "remove"
                    },
                    SAVE_TYPES: {
                        SET: "set",
                        FILE: "file"
                    },
                    ANNOTATION_TYPES: {//Type of annotataions
                        "BOOKMARK": 1,
                        "HIGHLIGHT": 2,
                        "STATIC_NOTE": 3,
                        "INTERACTIVE_NOTE": 4,
                        "TEACHER_COMMENT": 7,
                        "AUTOMATED_BOOKMARK": 8,
                        "EBOOK_ANSWER": 9,
                        "STUDENT_COVER_NOTE": 10,
                        "TEACHER_COVER_NOTE": 11,
                        "GO_NOTE": 12,
                        "MAPS_WIZARD": 13,
                        "MAC_DPO": 14
                    },
                },
                METHOD_TYPES: {
                    SET: "set",//create/update annotation data
                    GET: "get",//get annotation data
                    DELETE: "remove",//remove Annotation_id
                    UPLOAD: "upload",//upload image amazon server
                    DOWNLOAD: "download",//download image link
                    UNLINK: "unlink",//delete image
                    SET_USER: "seuser",
                    GET_USER: "getuser"
                },
                RESULT_TYPES: {
                    SUCCESSS: "success",
                    FAIL: "fail"
                },
                IDS: {
                    CAS_QUEUE: "casqueue"//If multi CAS request, this this release one after other
                },
                CAS_FIELDS: {
                    DEFAULTS: {
                        "object_id": "",//Request uuid "String 32"
                        "content_id": "",//"String 32"
                    }
                }
            };
            this.CAS_CONSTANTS = {//CAS request static object
                "GO_NOTE_TYPE": 14,
                "DR_TYPE": 4,
                "UserDetails": {//Dafault user cookie
                    "users": ["NBTEACHER1", "NBSTUDENT1", "NBSTUDENT2"],
                    "NBTEACHER1": {
                        "token": "JWT eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJhdWQiOiJodHRwOi8vd3d3LmhtaGNvLmNvbSIsImlzcyI6Imh0dHBzOi8vbXkuaHJ3LmNvbSIsIm5iZiI6MTQ1MzM2MTA4MiwiZXhwIjoxNDU2ODE3MDgyLCJzdWIiOiJjbj1OQlRFQUNIRVIxLHVpZD1OQlRFQUNIRVIxLHVuaXF1ZUlkZW50aWZpZXI9TkJURUFDSEVSMSxvPXVuZGVmaW5lZCxkYz11bmRlZmluZWQsc3Q9Z2EiLCJQbGF0Zm9ybUlkIjoiSE1PRiIsImh0dHA6Ly93d3cuaW1zZ2xvYmFsLm9yZy9pbXNwdXJsL2xpcy92MS92b2NhYi9wZXJzb24iOlsiW0luc3RydWN0b3JdIiwiW0FkbWluaXN0cmF0b3JdIl19.XKBRj3gfmP1nT1Rox_NQQdRXDTSENUIa_fDJPzECnCM",
                        "role": "Instructor"
                    },
                    "NBSTUDENT1": {
                        "token": "JWT eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJhdWQiOiJodHRwOi8vd3d3LmhtaGNvLmNvbSIsImlzcyI6Imh0dHBzOi8vbXkuaHJ3LmNvbSIsIm5iZiI6MTQ1MzM2MTEyNCwiZXhwIjoxNDU2ODE3MTI0LCJzdWIiOiJjbj1OQlNUVURFTlQxLHVpZD1OQlNUVURFTlQxLHVuaXF1ZUlkZW50aWZpZXI9TkJTVFVERU5UMSxvPXVuZGVmaW5lZCxkYz11bmRlZmluZWQsc3Q9Z2EiLCJQbGF0Zm9ybUlkIjoiSE1PRiIsImh0dHA6Ly93d3cuaW1zZ2xvYmFsLm9yZy9pbXNwdXJsL2xpcy92MS92b2NhYi9wZXJzb24iOlsiW1N0dWRlbnRdIl19.nAKyKQUrXWF_PM1CRM0c0FnvYM6jvyBa4LFhYArLBN8",
                        "role": "Student"
                    },
                    "NBSTUDENT2": {
                        "token": "JWT eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJhdWQiOiJodHRwOi8vd3d3LmhtaGNvLmNvbSIsImlzcyI6Imh0dHBzOi8vbXktcmV2aWV3LWNlcnQuaHJ3LmNvbSIsIm5iZiI6MTQ3NDkwMTM4OCwiZXhwIjoxNTA2MDA1Mzg4LCJzdWIiOiJjbj1OQlNUVURFTlQyLHVpZD1OQlNUVURFTlQyLHVuaXF1ZUlkZW50aWZpZXI9TkJTVFVERU5UMixvPXVuZGVmaW5lZCxkYz11bmRlZmluZWQsc3Q9Z2EiLCJQbGF0Zm9ybUlkIjoiSE1PRiIsImh0dHA6Ly93d3cuaW1zZ2xvYmFsLm9yZy9pbXNwdXJsL2xpcy92MS92b2NhYi9wZXJzb24iOlsiW1N0dWRlbnRdIl19.3XUhyRQzQMLlIWKJTbosWDKrNzQ4qi7FV6kJDNLPcbM",
                        "role": "Student"
                    }
                },
                "fields": ['annotation_id', 'title', 'body_text', 'type_id', 'data', "content_id"],//valid object key for cas call
                "visibility": "MX",
                "contentId": "MX2018",//Here we are adding default content id
                "annotationServiceInit": {
                    "production": this.isProdEnv,
                    "server_url": "//dubv-dpqaweb01.dubeng.local/",
                    "cors": true,
                    "initDefaults": {
                        "clientId": "DLO",
                        "casV2":false,
                        "usePostTunneling": false,
                        "forceDefaultServiceSettings": false,
                        "nextLinkDomainStripping": true,
                        "requestTimeout": 20000
                    }
                }
            };
            this.getCookieByName = function(name){
                var value = "; " + document.cookie;
                var parts = value.split("; " + name + "=");
                if(parts.length == 2){
                    return parts.pop().split(";").shift();
                }
            }
            this.checkCasAvailable = function(callback){
                annService.testHeartbeat({successHandler: function(data) {
                    thiS.isCasAvailable = true;
                    callback.call();
                }, errorHandler: function(data) {
                    thiS.chkCasCredential = data;
                    thiS.isCasAvailable = false;
                    if(casOnOffCheck && casOnOffCheck[thiS.casLocalInitCount] && casOnOffCheck[thiS.casLocalInitCount].fail && typeof casOnOffCheck[thiS.casLocalInitCount].fail==='function'){
                        casOnOffCheck[thiS.casLocalInitCount].fail.call(null,'fail');
                    }
                    console.warn('cas is not available');
                }});
            }
            this.resetAndDoNext = function(){
                thiS.currentCasObj = null;
                thiS.currentCasMethod = null;
                thiS.currentCasQueueId = null;
                thiS.serviceRunning = false;
                thiS.next();
            }
            this.mergeObjects = function(obj_a,obj_b){
                for(var p in obj_a){
                    if(obj_a[p] === undefined || obj_a[p] === null){
                        delete obj_a[p];
                    }
                }
                for(var p in obj_b){
                    if(obj_b[p] === undefined || obj_b[p] === null){
                        delete obj_b[p]
                    }else{
                        obj_a[p] = obj_b[p];
                    }
                }
                return obj_a;
            }
            this.casCallbackHandler = function(resultType, action, result, forceFail){
                if(thiS.currentCasObj){
                    result = result && result instanceof Array && result.length == 1 ? result[0] : result;
                    if(resultType == thiS.SETTINGS.RESULT_TYPES.SUCCESSS && thiS.currentCasObj[resultType]){
                        thiS.currentCasObj[resultType]({ACTION: action, RESULT: result});
                    }else if(forceFail){
                        if(thiS.currentCasObj[resultType]){
                            thiS.currentCasObj[resultType]({ACTION: action, RESULT: result});
                        }
                    }else if(resultType == thiS.SETTINGS.RESULT_TYPES.FAIL){//if request is failed from CAS server
                        thiS.checkCasAvailable(function(result){
                            if(result){
                                if(thiS.currentCasObj[resultType]){
                                    thiS.currentCasObj[resultType]({ACTION: action, RESULT: result});
                                }
                            }else{
                                thiS.cascallQueue.add({
                                    method: thiS.currentCasMethod,
                                    data: thiS.currentCasObj
                                }, thiS.currentCasQueueId);
                            }
                            thiS.resetAndDoNext();//it will reset all param and go for next queue
                        });
                        return;
                    }
                }
                thiS.resetAndDoNext();
            }
            this.fetchFromCas = function(){
                thiS.serviceRunning = true;
                if(thiS.currentCasObj.data == null){
                    thiS.casCallbackHandler(thiS.SETTINGS.RESULT_TYPES.FAIL, thiS.SETTINGS.EVENT_SETTINGS.ACTIONS.FETCH, null);
                }else if(typeof(thiS.currentCasObj.data) === 'string'){//while we are trying search by title or desc
                    try{
                        annService.Annotations.findAllAnnotationsByText(thiS.currentCasObj.data).done(function(result){
                            thiS.casCallbackHandler(thiS.SETTINGS.RESULT_TYPES.SUCCESSS, thiS.SETTINGS.EVENT_SETTINGS.ACTIONS.FETCH, result);
                        }).fail(function() {
                            thiS.casCallbackHandler(thiS.SETTINGS.RESULT_TYPES.FAIL, thiS.SETTINGS.EVENT_SETTINGS.ACTIONS.FETCH, null);
                        }).run();
                    }catch(e){
                        thiS.casCallbackHandler(thiS.SETTINGS.RESULT_TYPES.FAIL, thiS.SETTINGS.EVENT_SETTINGS.ACTIONS.FETCH, null, true);
                    }
                }else if(typeof(thiS.currentCasObj.data) === 'number'){//while we are trying to search with number such as "Anotation_id"
                    try{
                        annService.Annotations.findOne(thiS.currentCasObj.data).done(function(result) {
                            thiS.AIOTCAS = thiS.currentCasObj.data;
                            thiS.casCallbackHandler(thiS.SETTINGS.RESULT_TYPES.SUCCESSS, thiS.SETTINGS.EVENT_SETTINGS.ACTIONS.FETCH, result);
                        }).fail(function() {
                            thiS.casCallbackHandler(thiS.SETTINGS.RESULT_TYPES.FAIL, thiS.SETTINGS.EVENT_SETTINGS.ACTIONS.FETCH, null);
                        }).run();
                    }catch(e){
                        thiS.casCallbackHandler(thiS.SETTINGS.RESULT_TYPES.FAIL, thiS.SETTINGS.EVENT_SETTINGS.ACTIONS.FETCH, null, true);
                    }
                } else if(typeof (thiS.currentCasObj.data) === 'object'){//while we trying to search with multiple param, "Title", "l3" & "l5"
                    try{
                        annService.Annotations.find(thiS.currentCasObj.data).done(function(result){
                            (result && result.ACTION && (result.ACTION==='fetch' || result.ACTION==='created') && result.RESULT && typeof result.RESULT==='object' && result.RESULT.annotation_id && typeof result.RESULT.annotation_id==='number')?thiS.AIOTCAS = result.RESULT.annotation_id:null;
                            thiS.casCallbackHandler(thiS.SETTINGS.RESULT_TYPES.SUCCESSS, thiS.SETTINGS.EVENT_SETTINGS.ACTIONS.FETCH, result);
                        }).fail(function(){
                            thiS.casCallbackHandler(thiS.SETTINGS.RESULT_TYPES.FAIL, thiS.SETTINGS.EVENT_SETTINGS.ACTIONS.FETCH, null);
                        }).run();
                    }catch(e){
                        thiS.casCallbackHandler(thiS.SETTINGS.RESULT_TYPES.FAIL, thiS.SETTINGS.EVENT_SETTINGS.ACTIONS.FETCH, null, true);
                    }
                }
            }
            this.addOrUpdateToCas = function(){
                var reqObj = {}; //By default both env are visible or when checkbox is checked
                thiS.serviceRunning = true;
                if(thiS.currentCasObj && thiS.currentCasObj.data && (thiS.currentCasObj.data.annotation_id === undefined || thiS.currentCasObj.data.annotation_id === null || thiS.currentCasObj.data.annotation_id === '')) {//Requeest for new creation
                    reqObj = {
                        content_id: thiS.CAS_CONSTANTS.contentId,
			            type_id: thiS.CAS_CONSTANTS.DR_TYPE
                    };
                    if((thiS.currentCasObj.data.content_id !== null) && (thiS.currentCasObj.data.content_id !== '') && (thiS.currentCasObj.data.content_id !== undefined)) {
                        reqObj.content_id = thiS.currentCasObj.data.content_id;
                    }
                    if((thiS.currentCasObj.data.object_id !== null) && (thiS.currentCasObj.data.object_id !== '') && (thiS.currentCasObj.data.object_id !== undefined)) {
                        reqObj.object_id = thiS.currentCasObj.data.object_id;
                    }
                    if((thiS.currentCasObj.data.type_id !== null) && (thiS.currentCasObj.data.type_id !== '') && (thiS.currentCasObj.data.type_id !== undefined)) {//if annotation type is come with request
                        reqObj.type_id = thiS.currentCasObj.data.type_id;
                    }
                    thiS.mergeObjects(reqObj,thiS.currentCasObj.data);
                    delete reqObj.annotation_id;
                    try{
                        annService.Annotations.create(reqObj).done(function(result){//Request for create new object with requested data
                            (result && result.ACTION && (result.ACTION==='fetch' || result.ACTION==='created') && result.RESULT && typeof result.RESULT==='object' && result.RESULT.annotation_id && typeof result.RESULT.annotation_id==='number')?thiS.AIOTCAS = result.RESULT.annotation_id:null;
                            thiS.casCallbackHandler(thiS.SETTINGS.RESULT_TYPES.SUCCESSS, thiS.SETTINGS.EVENT_SETTINGS.ACTIONS.ADD, result);
                        }).fail(function(result){
                            thiS.casCallbackHandler(thiS.SETTINGS.RESULT_TYPES.FAIL, thiS.SETTINGS.EVENT_SETTINGS.ACTIONS.ADD, result);
                        }).run();
                    }catch(e){
                        thiS.casCallbackHandler(thiS.SETTINGS.RESULT_TYPES.FAIL, thiS.SETTINGS.EVENT_SETTINGS.ACTIONS.ADD, null, true);
                    }
                } else {//If request for update annotation
                    var annotation_id = parseInt(thiS.currentCasObj.data.annotation_id);//get annotation id from Requested object (from widget)
                    delete thiS.currentCasObj.data.annotation_id;
                    thiS.mergeObjects(reqObj,thiS.currentCasObj.data);
                    delete reqObj.content_id;
                    delete reqObj.object_id;
                    delete reqObj.type_id;
                    delete reqObj.l0;//Code: API_ERR_QUERY_API_11 Message: Annotations: type_id field is not allowed in update operation, Help page: http://dubconf.hmhpub.com:8080/display/DP2/Error+handling#Errorhandling-API_ERR_QUERY_API_11
                    thiS.AIOTCAS = annotation_id;
                    try{
                        annService.Annotations.update(annotation_id, reqObj).done(function(result){//Request for create new object with requested data and annotation_id
                            thiS.casCallbackHandler(thiS.SETTINGS.RESULT_TYPES.SUCCESSS, thiS.SETTINGS.EVENT_SETTINGS.ACTIONS.UPDATE, null);
                        }).fail(function(){
                            thiS.casCallbackHandler(thiS.SETTINGS.RESULT_TYPES.FAIL, thiS.SETTINGS.EVENT_SETTINGS.ACTIONS.UPDATE, null, true);
                        }).run();
                    }catch(e){
                        thiS.casCallbackHandler(thiS.SETTINGS.RESULT_TYPES.FAIL, thiS.SETTINGS.EVENT_SETTINGS.ACTIONS.UPDATE, null, true);
                    }
                }
            }
            this.addOrUpdateFileToCas = function(){
                var reqObj = {},
                    fileName = '',
                    fileString = ''; //By default both env are visible or when checkbox is checked
                thiS.serviceRunning = true;
                if(thiS.currentCasObj && thiS.currentCasObj.data && (thiS.currentCasObj.data.annotation_id === undefined || thiS.currentCasObj.data.annotation_id === null || thiS.currentCasObj.data.annotation_id === '')) {//Requeest for new creation
                    reqObj = {
                        content_id: thiS.CAS_CONSTANTS.contentId,
			            type_id: thiS.CAS_CONSTANTS.DR_TYPE
                    };
                    if((thiS.currentCasObj.data.content_id !== null) && (thiS.currentCasObj.data.content_id !== '') && (thiS.currentCasObj.data.content_id !== undefined)) {
                        reqObj.content_id = thiS.currentCasObj.data.content_id;
                    }
                    if((thiS.currentCasObj.data.object_id !== null) && (thiS.currentCasObj.data.object_id !== '') && (thiS.currentCasObj.data.object_id !== undefined)) {
                        reqObj.object_id = thiS.currentCasObj.data.object_id;
                    }
                    if((thiS.currentCasObj.data.type_id !== null) && (thiS.currentCasObj.data.type_id !== '') && (thiS.currentCasObj.data.type_id !== undefined)) {//if annotation type is come with request
                        reqObj.type_id = thiS.currentCasObj.data.type_id;
                    }
                    if((thiS.currentCasObj.data.fileName !== null) && (thiS.currentCasObj.data.fileName !== '') && (thiS.currentCasObj.data.fileName !== undefined)){
                        fileName = thiS.currentCasObj.data.fileName;
                    }
                    if((thiS.currentCasObj.data.fileString !== null) && (thiS.currentCasObj.data.fileString !== '') && (thiS.currentCasObj.data.fileString !== undefined)){
                        fileString = thiS.currentCasObj.data.fileString;
                    }
                    thiS.mergeObjects(reqObj,thiS.currentCasObj.data);
                    delete reqObj.fileName;
                    delete reqObj.fileString;
                    delete reqObj.annotation_id;
                    var tempStore = thiS.currentCasObj;
                    try{
                        annService.Annotations.createWithFile(reqObj,fileName,fileString).done(function(result) {//Request for create new object with requested data
                            (result && result.ACTION && (result.ACTION==='updated' || result.ACTION==='fetch' || result.ACTION==='created') && result.RESULT && typeof result.RESULT==='object' && result.RESULT.annotation && result.RESULT.annotation.annotation_id && typeof result.RESULT.annotation.annotation_id==='number')?thiS.AIOTCAS = result.RESULT.annotation.annotation_id:null;
                            thiS.currentCasObj = tempStore;
                            thiS.casCallbackHandler(thiS.SETTINGS.RESULT_TYPES.SUCCESSS, thiS.SETTINGS.EVENT_SETTINGS.ACTIONS.ADD, result);
                        }).fail(function(result){
                            thiS.casCallbackHandler(thiS.SETTINGS.RESULT_TYPES.FAIL, thiS.SETTINGS.EVENT_SETTINGS.ACTIONS.ADD, result, true);
                        }).run();
                    }catch(e){
                        thiS.casCallbackHandler(thiS.SETTINGS.RESULT_TYPES.FAIL, thiS.SETTINGS.EVENT_SETTINGS.ACTIONS.ADD, null, true);
                    }
                } else {//If request for update annotation
                    var annotation_id = parseInt(thiS.currentCasObj.data.annotation_id);//get annotation id from Requested object (from widget)
		            if((thiS.currentCasObj.data.content_id !== null) && (thiS.currentCasObj.data.content_id !== '') && (thiS.currentCasObj.data.content_id !== undefined)) {
                        reqObj.content_id = thiS.currentCasObj.data.content_id;
                    }
                    if((thiS.currentCasObj.data.object_id !== null) && (thiS.currentCasObj.data.object_id !== '') && (thiS.currentCasObj.data.object_id !== undefined)) {
                        reqObj.object_id = thiS.currentCasObj.data.object_id;
                    }
                    if((thiS.currentCasObj.data.type_id !== null) && (thiS.currentCasObj.data.type_id !== '') && (thiS.currentCasObj.data.type_id !== undefined)) {//if annotation type is come with request
                        reqObj.type_id = thiS.currentCasObj.data.type_id;
                    }
                    if((thiS.currentCasObj.data.fileName !== null) && (thiS.currentCasObj.data.fileName !== '') && (thiS.currentCasObj.data.fileName !== undefined)){
                        fileName = thiS.currentCasObj.data.fileName;
                    }
                    if((thiS.currentCasObj.data.fileString !== null) && (thiS.currentCasObj.data.fileString !== '') && (thiS.currentCasObj.data.fileString !== undefined)){
                        fileString = thiS.currentCasObj.data.fileString;
                    }
                    thiS.mergeObjects(reqObj,thiS.currentCasObj.data);
                    delete reqObj.fileName;
                    delete reqObj.fileString;
                    delete reqObj.annotation_id;
                    delete reqObj.content_id;
                    delete reqObj.object_id;
                    delete reqObj.type_id;
                    thiS.AIOTCAS = annotation_id;
                    var tempStore = thiS.currentCasObj;
                    try{
                        annService.Annotations.updateWithFile(annotation_id,reqObj,fileName,fileString).done(function(result) {//Request for create new object with requested data and annotation_id
                            thiS.currentCasObj = tempStore;
                            thiS.casCallbackHandler(thiS.SETTINGS.RESULT_TYPES.SUCCESSS, thiS.SETTINGS.EVENT_SETTINGS.ACTIONS.UPDATE, result);
                        }).fail(function(result) {
                            thiS.casCallbackHandler(thiS.SETTINGS.RESULT_TYPES.FAIL, thiS.SETTINGS.EVENT_SETTINGS.ACTIONS.UPDATE, result, true);
                        }).run();
                    }catch(e){
                        thiS.casCallbackHandler(thiS.SETTINGS.RESULT_TYPES.FAIL, thiS.SETTINGS.EVENT_SETTINGS.ACTIONS.UPDATE, null, true);
                    }
                }
            }
            this.getFileFromCas = function(){
                if ((thiS.currentCasObj.data.annotation_id !== null) && (thiS.currentCasObj.data.annotation_id !== '') && (thiS.currentCasObj.data.annotation_id !== undefined)){
                    try{
                        annService.Annotations.getFileUrl(thiS.currentCasObj.data.annotation_id).done(function(result) {//request for delete annothation with annotation_id as parameter
                            var result = result;
                            var url = result.signed_url;
                            var xhttp = new XMLHttpRequest();
                            result['fileString'] = '';
                            xhttp.onreadystatechange = function(){
                                if (this.readyState===4 && this.status===200){
                                    var fileString = this.responseText;
                                    result['fileString'] = fileString;
                                    thiS.casCallbackHandler(thiS.SETTINGS.RESULT_TYPES.SUCCESSS, thiS.SETTINGS.EVENT_SETTINGS.ACTIONS.FETCH, result);
                                }
                            };
                            xhttp.open("GET", url, true);
                            xhttp.send();
                        }).fail(function() {
                            thiS.casCallbackHandler(thiS.SETTINGS.RESULT_TYPES.FAIL, thiS.SETTINGS.EVENT_SETTINGS.ACTIONS.FETCH, null, true);
                        }).run();
                    }catch(e){
                        thiS.casCallbackHandler(thiS.SETTINGS.RESULT_TYPES.FAIL, thiS.SETTINGS.EVENT_SETTINGS.ACTIONS.FETCH, null, true);
                    }
                }else{
                    thiS.casCallbackHandler(thiS.SETTINGS.RESULT_TYPES.FAIL, thiS.SETTINGS.EVENT_SETTINGS.ACTIONS.FETCH, null, true);
                }
            }
            this.deleteCasObj = function(){
                if ((thiS.currentCasObj.data.annotation_id !== null) && (thiS.currentCasObj.data.annotation_id !== '') && (thiS.currentCasObj.data.annotation_id !== undefined)){
                    if(thiS.currentCasObj.data.withFile){
                        try{
                            annService.Annotations.removeWithFile(thiS.currentCasObj.data.annotation_id).done(function(result){
                                thiS.casCallbackHandler(thiS.SETTINGS.RESULT_TYPES.SUCCESSS, thiS.SETTINGS.EVENT_SETTINGS.ACTIONS.DELETE, result);
                            }).fail(function(err, addData){
                                thiS.casCallbackHandler(thiS.SETTINGS.RESULT_TYPES.FAIL, thiS.SETTINGS.EVENT_SETTINGS.ACTIONS.DELETE, null, true);
                            }).run();
                        }catch(e){
                            thiS.casCallbackHandler(thiS.SETTINGS.RESULT_TYPES.FAIL, thiS.SETTINGS.EVENT_SETTINGS.ACTIONS.DELETE, null, true);
                        }
                    }else{
                        try{
                            annService.Annotations.remove(thiS.currentCasObj.data.annotation_id).done(function(result) {//request for delete annothation with annotation_id as parameter
                                thiS.casCallbackHandler(thiS.SETTINGS.RESULT_TYPES.SUCCESSS, thiS.SETTINGS.EVENT_SETTINGS.ACTIONS.DELETE, result);
                            }).fail(function(result) {
                                annService.Annotations.removeWithFile(thiS.currentCasObj.data.annotation_id).done(function(result){
                                    thiS.casCallbackHandler(thiS.SETTINGS.RESULT_TYPES.SUCCESSS, thiS.SETTINGS.EVENT_SETTINGS.ACTIONS.DELETE, result);
                                }).fail(function(err, addData){
                                    thiS.casCallbackHandler(thiS.SETTINGS.RESULT_TYPES.FAIL, thiS.SETTINGS.EVENT_SETTINGS.ACTIONS.DELETE, null, true);
                                }).run();
                            }).run();
                        }catch(e){
                            thiS.casCallbackHandler(thiS.SETTINGS.RESULT_TYPES.FAIL, thiS.SETTINGS.EVENT_SETTINGS.ACTIONS.DELETE, null, true);
                        }
                    }
                }else{
                    thiS.casCallbackHandler(thiS.SETTINGS.RESULT_TYPES.FAIL, thiS.SETTINGS.EVENT_SETTINGS.ACTIONS.DELETE, null, true);
                }
            }
            this.getUserDetails = function(){
                if (typeof(thiS.currentCasObj) === 'object'){
                    var result;
                    try{
                        result = annService.getToken();
                        if(typeof(result) === 'string'){
                            thiS.casCallbackHandler(thiS.SETTINGS.RESULT_TYPES.SUCCESSS, thiS.SETTINGS.EVENT_SETTINGS.ACTIONS.FETCH, result);
                        }else{
                            thiS.casCallbackHandler(thiS.SETTINGS.RESULT_TYPES.FAIL, thiS.SETTINGS.EVENT_SETTINGS.ACTIONS.FETCH, null);
                        }
                    }catch(e){
                        thiS.casCallbackHandler(thiS.SETTINGS.RESULT_TYPES.FAIL, thiS.SETTINGS.EVENT_SETTINGS.ACTIONS.FETCH, null, true);
                    }
                }else{
                    thiS.casCallbackHandler(thiS.SETTINGS.RESULT_TYPES.FAIL, thiS.SETTINGS.EVENT_SETTINGS.ACTIONS.FETCH, null);
                }
            }
            this.setUserDetails = function(){
                if (typeof(thiS.currentCasObj) === 'object'){
                    var cookieName;
                    var cookieData;
                    var token;
                    try{
                        if(thiS.currentCasObj && thiS.currentCasObj.data && thiS.currentCasObj.data.cookieName){
                            cookieName = thiS.currentCasObj.data.cookieName;
                            cookieData = String(thiS.getCookieByName(cookieName));
                        }else if(thiS.currentCasObj && thiS.currentCasObj.data && thiS.currentCasObj.data.token && typeof token==='string'){
                            token = thiS.currentCasObj.data.token;
                            cookieData = token;
                        }
                        annService.setToken(cookieData);
                        var result = annService.getToken();
                        if(typeof(result) === 'string'){
                            thiS.casCallbackHandler(thiS.SETTINGS.RESULT_TYPES.SUCCESSS, thiS.SETTINGS.EVENT_SETTINGS.ACTIONS.SET, result);
                        }else{
                            thiS.casCallbackHandler(thiS.SETTINGS.RESULT_TYPES.FAIL, thiS.SETTINGS.EVENT_SETTINGS.ACTIONS.SET, null);
                        }
                    }catch(e){
                        thiS.casCallbackHandler(thiS.SETTINGS.RESULT_TYPES.FAIL, thiS.SETTINGS.EVENT_SETTINGS.ACTIONS.SET, null, true);
                    }
                }else{
                    thiS.casCallbackHandler(thiS.SETTINGS.RESULT_TYPES.FAIL, thiS.SETTINGS.EVENT_SETTINGS.ACTIONS.SET, null);
                }
            }
            this.next = function(){
                if(thiS.serviceRunning){
                    return;
                }else if(!thiS.isCasAvailable){
                    thiS.serviceRunning = true;
                    thiS.checkCasAvailable(function(result){
                        thiS.serviceRunning = false;
                        if(result){
                            thiS.next();
                        }
                    });
                    return;
                }
                if(thiS.cascallQueue.hasNext()){//Checking any request is there to process next
                    var obj = thiS.cascallQueue.next();
                    thiS.currentCasMethod = obj.method;
                    thiS.currentCasQueueId = obj.queueid
                    if(obj.method === thiS.SETTINGS.METHOD_TYPES.GET){//If fetch request
                        thiS.currentCasObj = obj.data;//Set Fetch Object request
                        thiS.fetchFromCas();//Call for fetching 
                    }else if(obj.method === thiS.SETTINGS.METHOD_TYPES.SET){//Request for create/update
                        thiS.currentCasObj = obj.data;
                        thiS.addOrUpdateToCas();
                    }else if(obj.method === thiS.SETTINGS.METHOD_TYPES.UPLOAD){//Request for create/update large data
                        thiS.currentCasObj = obj.data;
                        thiS.addOrUpdateFileToCas();
                    }else if(obj.method === thiS.SETTINGS.METHOD_TYPES.DOWNLOAD){//Request for download large data
                        thiS.currentCasObj = obj.data;
                        thiS.getFileFromCas();
                    }else if(obj.method === thiS.SETTINGS.METHOD_TYPES.DELETE){//Request for remove from CAS
                        thiS.currentCasObj = obj.data;
                        thiS.deleteCasObj();
                    }else if(obj.method === thiS.SETTINGS.METHOD_TYPES.GET_USER){//Request for Get User Details
                        thiS.currentCasObj = obj.data;
                        thiS.getUserDetails();
                    }else if(obj.method === thiS.SETTINGS.METHOD_TYPES.SET_USER){//Request for Set User Details
                        thiS.currentCasObj = obj.data;
                        thiS.setUserDetails();
                    }
                }
            }
            this.initCasService = function(){
                if(this.serviceInitialized===undefined){
                    if(casOnOffCheck && casOnOffCheck[this.casLocalInitCount] && casOnOffCheck[this.casLocalInitCount].fail && typeof casOnOffCheck[this.casLocalInitCount].fail==='function'){
                        casOnOffCheck[this.casLocalInitCount].fail.call(null,'fail');
                    }
                    console.warn('cas is not available');
                    return;
                }
                if(!this.serviceInitialized && !this.serviceRunning){
                    this.serviceRunning = true;
                    var thiS = this;
                    annService.init(this.casInitObj, (function(){
                        thiS.isCasAvailable = true;
                        thiS.serviceInitialized = true;
                        thiS.serviceRunning = false;
                        thiS.next();
                        console.info('cas available');
                        if(casOnOffCheck && casOnOffCheck[thiS.casLocalInitCount] && casOnOffCheck[thiS.casLocalInitCount].success && typeof casOnOffCheck[thiS.casLocalInitCount].success==='function'){
                            casOnOffCheck[thiS.casLocalInitCount].success.call(null,'success');
                        }
                    }), function(e){
                        thiS.isCasAvailable = false;
                        thiS.serviceInitialized = false;
                        thiS.serviceRunning = false;
                        if(thiS.chkCasCredential !== 'HTTP request failed'){//If CAS credential fail
                            setTimeout(function(){
                                thiS.checkCasAvailable(thiS.initCasService);
                            }, 2000);
                        }
                    });
                    return false;
                }
                return true;
            }
        };
        cas.prototype.init = function(obj){
            casOnOffCheck[this.casLocalInitCount] = obj;
            console.info('cas initialized');
            this.user = 'NBSTUDENT2';//Default user for CAS call
            this.casInitObj = this.CAS_CONSTANTS.annotationServiceInit.initDefaults;
            if(this.CAS_CONSTANTS.annotationServiceInit.production){
                this.casInitObj["serverURL"] = '/';
                this.casInitObj["token"] = '';
                this.casInitObj["cors"] = false;
            }else{
                this.casInitObj["serverURL"] = this.CAS_CONSTANTS.annotationServiceInit.server_url;
                this.casInitObj["token"] = this.CAS_CONSTANTS.UserDetails[this.user].token;
                this.casInitObj["cors"] = this.CAS_CONSTANTS.annotationServiceInit.cors;
            }
            this.cascallQueue = Queue(this.SETTINGS.IDS.CAS_QUEUE);//Initailizing QUEUE Object
            if(this.initCasService()){//If CAS service is initalize
                if (!this.serviceRunning) {//check if CAS is not running
                    this.next();//Run the next item in the QUEUE
                }
            };
        }
        cas.prototype.fetch = function(obj){
            if(obj===undefined || obj===null){
                obj = {};
                obj['data'] = {};
            }else if(obj['data']===undefined || obj['data']===null){
                obj['data'] = {};
            }
            if(typeof(obj)==='object'){
                this.cascallQueue.add({//Adding to CAS request Queue
                    method: "get",//Setting valid data from Static object
                    data: obj//set the requested object
                });
                if(this.initCasService()){//If CAS service is initalize
                    if (!this.serviceRunning) {//check if CAS is not running
                        this.next();//Run the next item in the QUEUE
                    }
                };
            }else{
                console.log('set callback parameter as get.fetch({success:function(res){console.log(res)}})');
            }
        }
        cas.prototype.get = function(obj){
            if(typeof(obj)==='object'){
                this.cascallQueue.add({//Adding to CAS request Queue
                    method: "get",//Setting valid data from Static object
                    data: obj//set the requested object
                });
                if(this.initCasService()){//If CAS service is initalize
                    if (!this.serviceRunning) {//check if CAS is not running
                        this.next();//Run the next item in the QUEUE
                    }
                };
            }else{
                console.log('set data and callback parameter as cas.get({data:{annotation_id:xxxxxx},success:function(res){console.log(res)}}) and data parameter should not be \'undefined\'');
                console.log('set data and callback parameter as cas.get({data:{data:"unique_id"},success:function(res){console.log(res)}}) and data parameter should not be \'undefined\'');
            }
        }
        cas.prototype.set = function(obj){
            if(typeof(obj)==='object'){
                this.cascallQueue.add({//Adding to CAS request Queue
                    method: "set",//Setting valid data from Static object
                    data: obj//set the requested object
                });
                if(this.initCasService()){//If CAS service is initalize
                    if (!this.serviceRunning) {//check if CAS is not running
                        this.next();//Run the next item in the QUEUE
                    }
                };
            }else{
                console.log('set data and callback parameter as cas.set({data:{annotation_id:xxxxxx,body_text:"string Object"},success:function(res){console.log(res)}}) (Note: give id to update)');
                console.log('set data and callback parameter as cas.set({data:{data:"unique_id",body_text:"string Object"},success:function(res){console.log(res)}}) (Note: remove id for new)');
            }
        }
        cas.prototype.upload = function(obj){
            if(typeof(obj)==='object'){
                this.cascallQueue.add({//Adding to CAS request Queue
                    method: "upload",//Setting valid data from Static object
                    data: obj//set the requested object
                });
                if(this.initCasService()){//If CAS service is initalize
                    if (!this.serviceRunning) {//check if CAS is not running
                        this.next();//Run the next item in the QUEUE
                    }
                };
            }else{
                console.log('set data and callback parameter as cas.upload({data:{annotation_id:xxxxxx,fileName:******.json,fileString:"stringData of file"},success:function(res){console.log(res)}}) (Note: give id to update)');
                console.log('set data and callback parameter as cas.upload({data:{data:"new value string",fileName:******.json,fileString:"stringData of file"},success:function(res){console.log(res)}}) (Note: remove id for new)');
            }
        }
        cas.prototype.download = function(obj){
            if(typeof(obj)==='object'){
                this.cascallQueue.add({//Adding to CAS request Queue
                    method: "download",//Setting valid data from Static object
                    data: obj//set the requested object
                });
                if(this.initCasService()){//If CAS service is initalize
                    if (!this.serviceRunning) {//check if CAS is not running
                        this.next();//Run the next item in the QUEUE
                    }
                };
            }else{
                console.log('set data and callback parameter as cas.download({data:{annotation_id:xxxxxx},success:function(res){console.log(res)}}) (Notebook: give id to update)');
            }
        }
        cas.prototype.delete = function(obj){
            if(obj && typeof obj==='number'){
                obj = {data:{annotation_id:obj}}
            }
            if(typeof(obj)==='object'){
                this.cascallQueue.add({//Adding to CAS request Queue
                    method: "remove",//Setting valid data from Static object
                    data: obj//set the requested object
                });
                if(this.initCasService()){//If CAS service is initalize
                    if (!this.serviceRunning) {//check if CAS is not running
                        this.next();//Run the next item in the QUEUE
                    }
                };
            }else{
                console.log('set data and callback parameter as cas.delete({data:{annotation_id:xxxxxx},success:function(res){console.log(res)}})');
            }
        }
        cas.prototype.getUserInfo = function(obj){
            if(typeof(obj)==='object'){
                this.cascallQueue.add({//Adding to CAS request Queue
                    method: "getuser",//Setting valid data from Static object
                    data: obj//set the requested object
                });
                if(this.initCasService()){//If CAS service is initalize
                    if (!this.serviceRunning) {//check if CAS is not running
                        this.next();//Run the next item in the QUEUE
                    }
                };
            }else{
                console.log('set data and callback parameter as cas.getUserInfo({data:{},success:function(res:any){ console.log(res)},fail:function(res:any){console.log(res)}})');
            }
        }
        cas.prototype.setUserInfo = function(obj){
            if(typeof(obj)==='object'){
                this.cascallQueue.add({//Adding to CAS request Queue
                    method: "setuser",//Setting valid data from Static object
                    data: obj//set the requested object
                });
                if(this.initCasService()){//If CAS service is initalize
                    if (!this.serviceRunning) {//check if CAS is not running
                        this.next();//Run the next item in the QUEUE
                    }
                };
            }else{
                console.log('set data and callback parameter as cas.setUserInfo({data:{cookieName:"nameofthecookie"},success:function(res:any){ console.log(res)},fail:function(res:any){console.log(res)}})');
            }
        }
        cas.prototype.showAnnotationId = function(password){
            (password===72779073)?console.info(this.AIOTCAS):console.error('password is wrong');
        }
        return cas;
    }
    if(typeof(cas) === 'undefined'){
        window.cas = define_cas();
    }else{
        console.log("cas already defined.");
    }
}(window));