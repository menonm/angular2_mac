/**
 * @author indecomm
 * @license Copyright HMH
 */

/**
 * <<<<<<<<<<<>Description<>>>>>>>>>>>
 * Data Persistence Object (dpo)
 * This namespace completely cover the data persistance object.(using to store the data retrive and update from the dp)
 */

/**
 * Represents the namespace DPO
 * @namespace DPO
 */
export namespace DPO {
    /**
     * Represents the structure of ElementData
     * @interface IElementData
     */
    export interface IElementData{
        elmType:string;
        elmClass:string;
        elmContent:string;
        elmSubData:Array<string>
    }
    /**
     * Represents the structure of AppInfo
     * @interface IAppInfo
     */
    export interface IAppInfo{
        isbn:string;
        appShortName:string;
    }
    /**
     * Represents the structure of UserInfo
     * @interface IAppInfo
     */
    export interface IUserInfo{
        userId:string;
    }
    /**
     * Represents the structure of PageData
     * @interface IAppInfo
     */
    export interface IPageData{
        elmId:string;
        elmData:IElementData;
    }
    /**
     * Represents the structure of PageInfo
     * @interface IAppInfo
     */
    export interface IPageInfo{
        pageName:string;
        pageData:Array<IPageData>;
    }
    /**
     * Represents the structure of DPOBaseObject
     * @interface IAppInfo
     */
    export interface IDPOBaseObject{
        userInfo: IUserInfo;
        appInfo: IAppInfo;
        pageInfo: Array<IPageInfo>;
    }
    /**
     * Represents the public variable used for storing and updating the data by the dp (data persistence)
     * @var DPOBaseObject
     */
    export var DPOBaseObject:IDPOBaseObject = {
        userInfo:{/** User Info detail */
            userId: "",/** User's unique ID */
        },
        appInfo:{/** App Info detail */
            isbn:"",/** Unique isbn for app */
            appShortName:""/** short form of name for the application */
        },
        pageInfo:[/** Individaul Page data-info */
            {
                pageName:"",/** Unique page name to identify the page */
                pageData:[/** All data belongs to the sigle page stored separatly */
                    {
                        elmId:"",/** Id of a input field element */
                        elmData:{/** mandatory data of the element */
                            elmType:"",/** element type means, checkbox, drop down, radio button and etc */
                            elmClass:"",/** element's class attribute */
                            elmContent:"",/** element's inner text */
                            elmSubData:[]
                        }
                    }
                ]
            }
        ]
    }
}

