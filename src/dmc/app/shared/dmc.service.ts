import { Injectable } from '@angular/core';
import {Http, Response, HTTP_PROVIDERS} from '@angular/http';
import 'rxjs/Rx';
import {Observable} from 'rxjs/Observable';


@Injectable()

export class DmcService {
    filteredArray: any = [];
    grade: string;
    url: string;

    /**
     * intialize http service
     * @constructor
     * @param {http} get and intialize http service
     */
    constructor(public http: Http) {
        // // this.grade = document.getElementById("dmc-main-container").getAttribute("localgrade");
        this.url = "assets/data/";
      //this.grade="g4"
      //  this.url = "assets/data/"  + this.grade + "/" ;
    }
    /**
     * get and return json data
     * @method getData
     * @param {filename} get file Name
     * return data
     */
    getData(filename: string) {

        return this.http.get(this.url + filename + '.json').map((res: Response) => res.json());
    }


    /**
        * get and return multiple json data
        * @method getBulkData
        * @param {filename} get file Name
        * return data
        */
    getBulkData(filename: any) {
        let observableBatch: any = [];
        filename.forEach((componentarray: any, key: any) => {
            observableBatch.push(this.http.get(this.url + componentarray + '.json').map((res: Response) => res.json()));
        });
        return Observable.forkJoin(observableBatch);
    }



}

