import { DPO } from './dpo'
import { DPService } from './dp.service';

declare var jQuery:any
export namespace DPOService{
    /**
     * Represents IDataPersistence interface
     * @interface IDataPersistence
     */
    export interface IDataPersistenceObject{
        initDPO(obj:any):void;
        setPageData(obj:any, callback?:Function):void;
        getPageData(obj:any, callback?:Function):void;
    }

    /**
     * Represents class DataPersistence
     * @class DataPersistence
     * @implements Interfaces.IDataPersistence
     */
    export class DataPersistenceObjectService implements IDataPersistenceObject{
        pageName:string;
        urlParam:any;
        dpoEnable:boolean;

        constructor(allow?:boolean){
            this.dpoEnable = (typeof allow==='undefined')?true:allow;
        }

        initDPO(obj:any={}){
            if(this.dpoEnable){
                clearInterval(gObj.pageReadClock);
                var thiS = this;
                this.pageName = '';
                this.urlParam = {};
                obj = (obj===undefined)?{}:obj;
                if(obj && obj.pageName && typeof obj.pageName==='string'){
                    this.pageName = obj.pageName;
                }else{
                    this.pageName = obj.pageName = this.getPageName(window.location.href);
                }
                if(obj && obj.urlParam && typeof obj.urlParam==='string'){
                    this.urlParam = obj.urlParam;
                }else{
                    this.urlParam = this.getUrlParam(window.location.href);
                }
                console.log('pageName =',this.pageName,'\nurlParam =',this.urlParam);
                gObj.finalEntry = false;
                gObj.pageLoaded = false;
                var checkDataLoad = setInterval(function(){
                    if(gObj.dataLoaded){
                        clearInterval(checkDataLoad);
                        thiS.setPageData(obj, thiS.update);
                    }else{
                        if(jQuery('.loading-div').hasClass('display-none')){
                            jQuery('.loading-div').removeClass('display-none');
                        }
                    }
                },100);
            }else{
                jQuery('.loading-div').addClass('display-none');
            }
        }
        private getUrlParam(url:string){
            var query:string = String(url);
            if(query.indexOf("?")!==-1){
                query = query.substr(query.indexOf("?") + 1);
                var result:any = {};
                query.split("&").forEach(function(part){
                    var item:any = part.split("=");
                    result[item[0]] = decodeURIComponent(item[1]);
                });
                return result;
            }else{
                return null;
            }
        }
        private getPageName(url:string){
            var index = url.lastIndexOf("/") + 1;
            var filenameWithExtension = url.substr(index);
            var filename = filenameWithExtension.split(".")[0];
            if(filename.indexOf('?')!==-1){
                filename = filename.substring(0,filename.indexOf('?'));
            }
            return filename;
        }
        private fillIndividualInputField(elmId:any,elmData:any){
            var elmId = elmId;
            var elmType = elmData['elmType'];
            var elmClass= elmData['elmClass'];
            var elmContent = elmData['elmContent'];
            var elmSubData = elmData['elmSubData'];
            if(elmType &&  elmType.indexOf('dropdown')!==-1 && elmType.indexOf('false')===-1){
                jQuery('#'+elmId).text(elmContent);
                if(elmSubData && elmSubData.length && elmSubData.length>0){
                    jQuery('#'+elmId).parent().next().children().each(function(index, value){
                        var attributeClass:string;
                        if(elmSubData[index]){
                            attributeClass = String(elmSubData[index]);
                            jQuery(this).attr('class',attributeClass);
                        }else{
                            console.log('value for the '+index+' is \'undefined\'');
                        }
                    });
                }else{
                    //console.error('length of subdata is \'0\'');
                }
            }else if(elmType &&  elmType.indexOf('checkbox')!==-1 && elmType.indexOf('false')===-1){
                jQuery('#'+elmId).attr('class',elmClass);
            }else if(elmType &&  elmType.indexOf('accordion')!==-1 && elmType.indexOf('false')===-1){
                jQuery('#'+elmId).attr('class',elmClass);
            }else if(elmType &&  elmType.indexOf('list')!==-1 && elmType.indexOf('false')===-1){
                //jQuery('#'+elmId).attr('class',elmClass);
            }else{
                //console.error('element type not predefined');
            }
        }
        private loadInputFieldData(pageData:any){
            for(var prop in pageData){
                this.fillIndividualInputField(prop, pageData[prop]);
            }
        }
        private removeIndividualDataIfFieldGotRemoved(elmId:any, elmData:any){
            var elmId = elmId;
            var elmType = elmData['elmType'];
            var elmClass = elmData['elmClass'];
            var elmContent = elmData['elmContent'];
            var elmSubData = elmData['elmSubData'];
            var valueRemoved = false;

            /*if(elmType &&  elmType.indexOf('dropdown')!==-1 && elmType.indexOf('false')===-1){
                jQuery('#'+elmId).text(elmContent);
                if(elmSubData && elmSubData.length && elmSubData.length>0){
                    jQuery('#'+elmId).parent().next().children().each(function(index, value){
                        var attributeClass:string;
                        if(elmSubData[index]){
                            attributeClass = String(elmSubData[index]);
                            jQuery(this).attr('class',attributeClass);
                        }else{
                            console.log('value for the '+index+' is \'undefined\'');
                        }
                    });
                }else{
                    //console.error('length of subdata is \'0\'');
                }
            }else if(elmType &&  elmType.indexOf('checkbox')!==-1 && elmType.indexOf('false')===-1){
                //jQuery('#'+elmId).attr('class',elmClass);
            }else if(elmType &&  elmType.indexOf('accordion')!==-1 && elmType.indexOf('false')===-1){
                //jQuery('#'+elmId).attr('class',elmClass);
            }else if(elmType &&  elmType.indexOf('list')!==-1 && elmType.indexOf('false')===-1){
                //jQuery('#'+elmId).attr('class',elmClass);
            }else{
                //console.error('element type not predefined');
            }*/
            if(elmType &&  elmType.indexOf('list')!==-1 && elmType.indexOf('false')===-1){
                //console.log('elmId:',elmId,' elmClass:',elmClass,' elmContent:',elmContent,' elmSubData:',elmSubData);
                if(jQuery('#'+elmId) && String(jQuery('#'+elmId).length) && jQuery('#'+elmId).length===0){
                    valueRemoved = true;
                }
            }
            return valueRemoved;
        }
        private removeDataIfFieldRemoved(pageData:any, pageName:string, callback:Function){
            var valueRemoved:boolean = false;
            var isRemoved:boolean = false;
            var removeObjList:any = [];
            for(var prop in pageData){
                isRemoved = this.removeIndividualDataIfFieldGotRemoved(prop, JSON.parse(JSON.stringify(pageData[prop])));
                if(isRemoved){
                    removeObjList.push(prop);
                }
            }
            if(removeObjList && removeObjList.length>0){
                console.log('need to remove', removeObjList);
                console.log('before',pageData);
                for(var i=0;i<removeObjList.length;i++){
                    delete pageData[removeObjList[i]];
                }
                console.log('after',pageData);
                callback.call(this, {pageData:JSON.parse(JSON.stringify(pageData)), pageName:pageName});
            }
        }
        setPageData(obj:any, callback:Function){
            clearInterval(gObj.pageReadClock);
            var thiS = this;
            var clockCount = 0;
            var pageName:string = (obj && obj.pageName && typeof obj.pageName==='string' && obj.pageName!=='')?obj.pageName:this.getPageName(window.location.href);
            var pageData:any;
            var loadClock = setInterval(function(){
                clockCount++;
                if(jQuery('.loading-div').hasClass('display-none')){
                    jQuery('.loading-div').removeClass('display-none');
                }
                if(clockCount >= 100 && gObj.dataLoaded){
                    clearInterval(loadClock);
                    if(gObj && gObj.dpoData && gObj.dpoData.pageInfo && gObj.dpoData.pageInfo[pageName]){
                        pageData = JSON.parse(JSON.stringify(gObj.dpoData.pageInfo[pageName]));
                        thiS.loadInputFieldData(JSON.parse(JSON.stringify(pageData)));
                    }else{
                        pageData = JSON.parse(JSON.stringify(thiS.getAllInputFieldDataAsObject().presentData));
                        gObj.dpoData.pageInfo[pageName] = JSON.parse(JSON.stringify(pageData));
                    }
                    gObj.pageLoaded = true;
                    obj.pageData = JSON.parse(JSON.stringify(pageData));
                    if(callback && typeof callback==='function'){
                        callback.call(thiS, {pageData:JSON.parse(JSON.stringify(pageData)), pageName:pageName});
                    }
                    if(obj && obj.ready && typeof obj.ready==='function'){
                        obj.ready.call(thiS, JSON.parse(JSON.stringify({pageData:JSON.parse(JSON.stringify(pageData)), pageName:pageName})));
                    }
                    thiS.getPageData(obj, thiS.update);
                }
            },10);
        }
        getAllInputFieldDataAsObject(previousData:any={},presentData?:any):any{
            previousData = JSON.parse(JSON.stringify(previousData));
            presentData = (typeof presentData==='object')?presentData:JSON.parse(JSON.stringify(previousData));
            var valueModified = false;
            jQuery('[data-dpo]').each(function(){
                var elmId:string = String(jQuery(this).attr('id'));
                var elmType:string = String(jQuery(this).attr('data-dpo'));
                var elmClass:string = String(jQuery(this).attr('class'));
                var elmContent:string = String(jQuery(this).text());
                var elmSubData:any = [];

                if(elmType==='dropdown'){
                    jQuery('#'+elmId).parent().next().children().each(function(){
                        var attributeClass:string = String(jQuery(this).attr('class'));
                        elmSubData.push(attributeClass);
                    });
                }
                
                if((previousData && previousData[elmId] && previousData[elmId]['elmType']    && previousData[elmId]['elmType']!==elmType) ||
                    (previousData && previousData[elmId] && previousData[elmId]['elmClass']   && previousData[elmId]['elmClass']!==elmClass)||
                    (previousData && previousData[elmId] && previousData[elmId]['elmContent'] && previousData[elmId]['elmContent']!==elmContent)){
                    presentData[elmId] = {elmType:"",elmClass:"",elmContent:""};
                    presentData[elmId]['elmType'] = elmType;
                    presentData[elmId]['elmClass'] = elmClass;
                    presentData[elmId]['elmContent'] = elmContent;
                    presentData[elmId]['elmSubData'] = elmSubData;
                    valueModified = true;
                }else if(previousData && previousData[elmId]===undefined){
                    presentData[elmId] = {elmType:"",elmClass:"",elmContent:""};
                    presentData[elmId]['elmType'] = elmType;
                    presentData[elmId]['elmClass'] = elmClass;
                    presentData[elmId]['elmContent'] = elmContent;
                    presentData[elmId]['elmSubData'] = elmSubData;
                    valueModified = true;
                }
            });
            return {presentData:JSON.parse(JSON.stringify(presentData)), valueModified:valueModified};
        }
        getPageData(obj:any, callback:Function){
            clearInterval(gObj.pageReadClock);
            var thiS = this;
            var previousData:any = {};
            var presentData:any = {};
            var pageName:any = (obj && obj.pageName && typeof obj.pageName==='string' && obj.pageName!=='')?obj.pageName:this.getPageName(window.location.href);
            var pageData:any = {};
            var dataAndModify:any = {};
            var valueModified:boolean = false;
            if(obj.pageData){
                pageData = JSON.parse(JSON.stringify(obj.pageData));
            }else{
                pageData = {};
            }

            gObj.pageReadClock = setInterval(function(){
                previousData = JSON.parse(JSON.stringify(pageData));
                dataAndModify = thiS.getAllInputFieldDataAsObject(previousData);
                presentData = JSON.parse(JSON.stringify(dataAndModify.presentData));
                valueModified = dataAndModify.valueModified;
                if(valueModified){
                    pageData = JSON.parse(JSON.stringify(presentData));
                    if(callback && typeof callback==='function'){
                        callback.call(thiS, {pageData:JSON.parse(JSON.stringify(pageData)), pageName:pageName});
                    }
                }
                thiS.removeDataIfFieldRemoved(pageData, pageName, callback);
            },1000);

            jQuery('.loading-div').addClass('display-none');
        }
        /*private removeDeletedData(PageData:any, callback?:Function){
            this.removeDataIfFieldRemoved(PageData, callback);
        }*/
        private update(obj:any){
            var pageName:any = String(obj.pageName);
            gObj.dpoData.pageInfo[pageName] = JSON.parse(JSON.stringify(obj.pageData));
            jQuery('#update-all-field').trigger('change', JSON.parse(JSON.stringify(gObj.dpoData)));
        }
    }
    /**
     * Represents the global variable to communicate between classes
     * @var gObj
     */
    export var gObj:any = {
        dpoData:{userInfo:{userId:""},appInfo:{isbn:"",appShortName:""},pageInfo:{}},/** To Store the DPO data */
        pageReadClock:{},/** Clock object to access the pageloading and data rendering to the page */
        finalEntry:false,/** check of modify of the input field in a page */
        dataLoaded:false,/** To confirm to other classes that data is loaded from the Data Persistace */
        pageLoaded:false,/** To confirm to page Loaded*/
        dpoGlbService:{}
    };
}