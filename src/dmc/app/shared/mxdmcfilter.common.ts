    declare var jQuery:any;
export namespace DmcFilterComponent {


  export class filter {
    /**
	 * variable to store user value
	 */
    user: string;
    /**
* variable to store dropdown show hide option
*/
    DropdownShow: string;

    /**
* open new tab
* @method enabledisablecontainer
* @param {user} user is student/teacher
*/
    enableDisableContainer(user: string) {
      jQuery("." + user + "-container").removeClass("display-none");
    }

    /**
* open new tab
* @method openInNewTab
* @param {url} url to open in new tab
*/
    openInNewTab(url: string, user?: string, type?: string) {
      var scope = this;
      //statements suspected to throw exception.
      jQuery.get(url).done(function () {

        if (type == "games") {
          var gamesWindow = window.open(url, '_blank');
        } else {
          var win = window.open(url, '_blank');
        }
      }).fail(function () {
        jQuery("#popup").removeClass("display-none");
        jQuery(".popup-div").removeClass("display-none");
        user == "teacher" ? jQuery(".popup-div .popup-message").text("Sorry, this resource could not be launched. Please contact HMH Technical Support.") : jQuery(".popup-div .popup-message").text("Sorry this resource could not be launched. Please let your teacher know.");
        jQuery(".listing-container").addClass("disable-pointer-events");
        jQuery(".footer").addClass("disable-pointer-events");
      });

    }

    /**
       * close 404 popup
       * @method closePopup
       */
    closePopup() {
      jQuery(".popup,.moreInfo-popup").addClass("display-none");
      jQuery(".popup-div,.moreInfo-popup-div").addClass("display-none");
      jQuery(".listing-container").removeClass("disable-pointer-events");
      jQuery(".footer").removeClass("disable-pointer-events");
    }

    /**
          * show more info
          * @method showMoreInfo
          * @param {moreInfo} more info text
          * @param {MoreInfoSecond} more info text for games boards
          */
    showMoreInfo(moreInfo: string, MoreInfoSecond?: string) {
      jQuery("#moreInfo").removeClass("display-none");
      jQuery(".moreInfo-popup-div").removeClass("display-none");
      jQuery(".moreInfo-popup-div .moreInfo-popup-message").html(moreInfo);
      MoreInfoSecond !== undefined ? jQuery(".moreInfo-popup-div .moreInfo-popup-games").html(MoreInfoSecond) : '';
      jQuery(".listing-container").addClass("disable-pointer-events");
      jQuery(".footer").addClass("disable-pointer-events");
    }

    /**
       * open new popup
       * @method openInNewPopUp
       * @param {url} url to load in popup
       */
    openInNewPopUp(url: string) {
      var hostnameProtocol = window.location.origin;
      var navigationUrl = hostnameProtocol + url;
      // var navigationUrl=url;
      jQuery("#modal").removeClass("display-none");
      document.getElementById('content').setAttribute('src', navigationUrl);
    }


    /**
* dropdownclick
* @method drpDownArrowClick
* @param {id} which dropdown to be clicked
*/
    drpDownArrowClick(id: any) {
      if (jQuery("#" + id).hasClass("tile-dropdown-clicked") == true) {
        jQuery("#" + id).removeClass("tile-dropdown-clicked");
        jQuery("#" + id).prev().addClass("display-none");
        jQuery("#" + id).prev().prev().removeClass("display-none");
      } else {
        jQuery("#" + id).addClass("tile-dropdown-clicked");
        jQuery("#" + id).prev().removeClass("display-none");
        jQuery("#" + id).prev().prev().addClass("display-none");
      }
    }

    /**
      * toggle drop down
      * @method toggleDropDown
      * @param {event} click event
      * @param {DropdownShow} class name to remove or add
      */

    toggleDropDown(DropdownShow: string, DropdownHide: string) {
      if (jQuery("#" + DropdownShow).hasClass("show") == true) {
        jQuery("#" + DropdownShow).removeClass("show");
        jQuery("#" + DropdownShow).siblings().removeClass("dropdown-clicked")
      } else {
        jQuery("#" + DropdownShow).addClass("show");
        jQuery("#" + DropdownShow).siblings().addClass("dropdown-clicked")
      }
      jQuery("#" + DropdownHide).removeClass("show");
      jQuery("#" + DropdownHide).siblings().removeClass("dropdown-clicked");
    }


    /**
      * reset All filters
      * @method resetAllFilters
      * 
      */

    resetAllFilters() {
      jQuery(".dropdown-content").removeClass('show');
      jQuery(".dropdown-arrow").removeClass('dropdown-up-arrow').addClass('dropdown-down-arrow');
      jQuery('.dropbtn').removeClass('dropdown-clicked');
      jQuery(".checkboxGroupElem").removeClass("checkbox-clicked");
      jQuery(".unit-filter-dDown_content").removeClass("unit-clicked");
    }


    /**
  * enabling and disabling checkbox
  * @method checkBoxEnableDisableEvent
  *  * @param {id} which checkbox to be enabled/disabled
  */

    checkBoxEnableDisableEvent(id: any) {
      if (jQuery("#" + id).hasClass("checkbox-clicked") == false) {
        jQuery("#" + id).addClass("checkbox-clicked");
      } else {
        jQuery("#" + id).removeClass("checkbox-clicked")
      }
    }

    /**
  * get the user
  * @method getUser
  */
    getUser() {
      let user = document.getElementById("dmc-main-container").getAttribute("name");
      return user;
    }


    /**
  * get the event type 
  * @method     getEventType() {
  */
    getEventType() {
      let eventType = (('ontouchstart' in window) || (Object(window).DocumentTouch && document instanceof Object(window).DocumentTouch)) ? 'touchstart' : 'mousedown';
      return eventType;
    }


    /**
* check for is mobile function
* @method isMobile
*/
    isMobile() {
      let isMobile = false
      if (window.innerWidth <= 767) {
        isMobile = true
      } else {
        isMobile = false
      }
      return isMobile;
    }
    
     isOrientation() {
      let potrait = false
      if (window.innerWidth <= 480) {
        potrait = true
      } else {
        potrait = false
      }
      return potrait;
    }

  }
}



