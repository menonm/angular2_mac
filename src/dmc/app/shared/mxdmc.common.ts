export namespace MXDmcCommon {

    export namespace Interfaces {

        export interface IUserRegistionInfo {
            firstname: string;
            middlename: string;
            lastname: string;
        }

        export interface IUser {
            UserRegistionInfo: IUserRegistionInfo
        }

        export interface IPlayer extends IUser {
            id: string

        }

        export interface IDmcService {
            preloadAllOfflineRessources(url: Array<string>, assetType: string): void;

            preloadAllOnlineRessources(url: Array<string>): void;

            HandleError(): void
        }

        export interface IActionResultMessage {
            key: string,
            value: string,
        }

        export interface IActionResult {
            pass: boolean,
            withWarning: boolean,
            messages: Array<IActionResultMessage>
        }


        export interface IDmcBase {
            // delete(itemId: Array<string>): IActionResult
        }

        export enum AnimationType {
            
        }
    }

    export namespace Types {

        export class Player implements Interfaces.IPlayer {
            UserRegistionInfo: Interfaces.IUserRegistionInfo;
            id: string
            constructor() {

            }
        }



        export class ActionResult implements Interfaces.IActionResult {
            pass: boolean;
            withWarning: boolean;
            messages: Array<Interfaces.IActionResultMessage>
            constructor() { }

        }

        export abstract class DMCBase implements Interfaces.IDmcBase {
            service: Services.DmcService;
            count: number = 0;
            isBrowserIE: boolean;
            eventType: any;

            constructor(private gameService: Services.DmcService, private playerService: Services.PlayerService) {
                this.service = new Services.DmcService();
                this.isBrowserIE = (window.navigator.userAgent.indexOf('Trident/') == -1) ? false : true;
                this.eventType = (('ontouchstart' in window) || (Object(window).DocumentTouch && document instanceof Object(window).DocumentTouch)) ? 'touchstart' : 'mousedown';
            }

            displayErrorMessage(value: string, errorMessageWindow: any) {
            }


            enableDisableControls(controlId: Array<string>, styleClass: string, flag: boolean, textClass: string) {

            }

            handleError() {

            }
        }

    }

    export namespace Services {


        export class DmcService implements Interfaces.IDmcService {
            constructor() {
            }


            preloadAllOfflineRessources(urls: Array<string>, assetType: string) {

            }

            preloadAllOnlineRessources(url: Array<string>) {

            }


            HandleError() {

            }

        }

        export class PlayerService {
            getCurrentPlayer(): Interfaces.IPlayer {
                return new Types.Player();
            }
        }



    }

}