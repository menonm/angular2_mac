import { provideRouter, RouterConfig } from '@angular/router';

import { DmcLandingComponent } from './component/Landing/dmc.landing.component';
import { DmcResultsComponent } from './component/SearchResults/dmc.results.component';
import { DmcGamesComponent } from './component/Games/dmc.games.component';
import { DmcPracticeComponent } from './component/Practice/dmc.practice.component';
import { DmcMathReaderComponent } from './component/MathReaders/dmc.mathreader.component';
import { DmcRtiComponent } from './component/Rti/dmc.rti.component';
import { DmcChallengeComponent } from './component/Challenge/dmc.challenge.component';
import { DmcReteachComponent } from './component/Reteach/dmc.reteach.component';
import {DmcPlanninGuideComponent}from './component/PlanningGuide/dmc.planninguide.component';
import {DmcLessonChecksomponent}from './component/LessonChecks/dmc.lessonchecks.component';
import {DmcFluencyBuildersComponent}from './component/FluencyBuilders/dmc.fluencybuilders.component';
import {DmcActivityCardsComponent}from './component/ActivityCards/dmc.activitycards.component';
import { DmcInquiryBasedTasksComponent } from './component/InquiryBasedTasks/dmc.inquirybasedtasks.component';
/**
 * import { RouterModule, Routes } from '@angular/router';
 * intialize routing path
 * 
 * 
 */

export const routes: RouterConfig = [
 
  { path: 'landing', component: DmcLandingComponent },
   { path: 'lessonchecks', component: DmcLessonChecksomponent },
   { path: 'landing', component: DmcLandingComponent },
  { path: 'results', component: DmcResultsComponent },
  { path: 'games', component: DmcGamesComponent },
  { path: 'challenge', component: DmcChallengeComponent },
  { path: 'practice', component: DmcPracticeComponent },
  { path: 'reteach', component: DmcReteachComponent },
  { path: 'mathreader', component: DmcMathReaderComponent },
  { path: 'rti', component: DmcRtiComponent },
  { path: 'activitycards', component: DmcActivityCardsComponent },
    { path: 'fluencybuilders', component: DmcFluencyBuildersComponent },
  { path: 'inquirybasedtasks', component: DmcInquiryBasedTasksComponent },  
  { path: '**', redirectTo:'landing',pathMatch:'full'}
];





export const APP_ROUTER_PROVIDERS = [
  provideRouter(routes)
];






