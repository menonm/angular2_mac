import { TestComponentBuilder } from '@angular/compiler/testing';
import { DmcFooterComponent } from '../Footer/dmc.footer.component';
import { DmcService} from '../../shared/dmc.service';
import { ROUTER_DIRECTIVES, Router, ActivatedRoute, UrlPathWithParams, RouterLink  } from '@angular/router';
import {Http, Response, HTTP_PROVIDERS} from '@angular/http';

import {
    beforeEachProviders,
    async,
    describe,
    expect,
    inject,
    it
} from '@angular/core/testing';
import {DmcFluencyBuildersComponent} from './dmc.fluencybuilders.component';


export function main() {
    
    let moreInfo = {
        "previous_text": "unit1", "next_text": "unit1", "items_text": "unit1", "fluency_checks": "unit1", "fluency_practice": "unit1",
        "type_text": "unit1"
    }
    
    let commonvalues=
    [{
         "color": "unit1", "moreInfo": "unit1", "title": "unit1", "header_background_color": "unit1",
        "tile_color": "unit1", "tile_border_color": "unit1"
    }]

    let textValue = "unit 1";
    let desc = "standardsdesc";
    let url = "post url"
    let unit = "unit1";
    let length = 10;
    let lesson="Lesson1"

    let dmcfluency = new DmcFluencyBuildersComponent(null, null, null);
    describe('dmcfluencyComponent', () => {
        it('assign json value', () => {
            const result:any = dmcfluency.assignJson(null, moreInfo,commonvalues);
            for (var i = 0; i < 13; i++) {
                expect(result[i]).toEqual("unit1");
            }
        })
    });


    describe('clickHoverEvent', () => {
        it('click hover events', () => {
            const result = dmcfluency.clickHoverEvent(null, "unit-clicked", textValue);
            expect(textValue).toEqual("unit 1");
        })
    });





    describe('setItemsCount', () => {
        it('set items count', () => {
            const result = dmcfluency.setItemsCount(length);
            expect(result).toEqual(10);
        })
    });
    
    
    describe('changePage', () => {
        it('click changePage', () => {
            const result = dmcfluency.changePage(null,"next");
            expect(result).toEqual("next");
        })
    });

    
    
   

    
  



}
