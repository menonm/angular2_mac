import { Component } from '@angular/core';
import { DmcService } from '../../shared/dmc.service';
import { ROUTER_DIRECTIVES } from '@angular/router';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { DmcFooterComponent } from '../Footer/dmc.footer.component';
import { DmcHeaderComponent } from '../Header/dmc.header.component';
import {TruncatePipe} from '../../shared/truncatepipe';
import { DmcFilterComponent } from '../../shared/mxdmcfilter.common';

import { DPOService } from '../../shared/dpo.service';
import { DmcPopupComponent } from '../Popup/dmc.popup.component';
import { DmcWideTileComponent } from '../WideTile/dmc.widetile.component';
import { DmcFilterByComponent } from '../FilterBy/dmc.filterby.component';

declare var jQuery: any;
@Component({
  moduleId: module.id,
  selector: 'games',
  templateUrl: './dmc.fluencybuilders.component.html',
  pipes: [TruncatePipe],
  directives: [ROUTER_DIRECTIVES, DmcFooterComponent, DmcHeaderComponent, DmcPopupComponent, DmcWideTileComponent, DmcFilterByComponent],
  providers: [DmcService],
})

export class DmcFluencyBuildersComponent {
  /**
	 * Object to store current page title
	 */
  filter: any = {};
  /**
  * Variable to store url value
  */
  url: any;
  /**
 * Variable to store user value
 */
  user: string;
  /**
 * Variable to event type 
 */
  eventType: any;
  /**
  * Array to store all common landing values
  */
  landingJson: any = {};
  /**
  * Array to store all units 
  */
  title: string;
  /**
 * Array to store no of fluency items
 */
  itemsFluency: number;
  /**
  * Array to store path value
  */
  path: string;
  /**
  * instance of filter component file 
  */
  commonClass: DmcFilterComponent.filter;
  /**
  * variable to store current page count
  */
  pageCount: number = 1;
  /**
	 * variable to store  page start count
	 */
  startCount: number = 1;
  /**
	 * variable to store total page count
	 */
  totalCount: number;
  /**
	 * variable to store current page id
	 */
  currentPageId: number = 0;
  /**
  * variable to store math value
  */
  Math: any;
  /**
  * variable to store fluency value
  */

  fluency: any = [];
  /**
  * variable to store fluency original value
  */
  fluencyOriginal: any = [];
  /**
  * variable to store fluency check original value
  */
  fluencyCheckOriginal: any = [];
  /**
	 * variable to store fluency check  values
	 */
  fluencyCheck: any = [];
  /**
	 * variable to store fluency json data  
	 */
  fluencyData: any;
  /**
  * variable to store common json value
  */
  commonJson: any = {};
  /**
  * variable to store fluency pratcice value
  */
  fluencyPractice: any = [];
  /**
  * variable to store truncateValue value
  */
  truncateValue: number;
  /**
	 * variable to store fluency pratcice original value
	 */
  fluencyPracticeOriginal: any;
  /**
	 * variable to store moreinfo value
	 */
  moreInfo: string;
  /**
   * Added for color configurable
  */
  /**
  * variable to store commonValues
  */
  commonValues: any = {};
  /**
  * variable to store tile  color
  */
  tileColor: string;
  /**
  * variable to store on tile border color
  */
  tileBorderColor: string;
  /**
  * variable to store currentRouteKey
  */
  currentRouteKey: string;
  /**
  * variable to store tilecopy
  */
  titleCopy: string;
  /**
	 * variable to store background color
	 */
  backgroundColor: string;
  /**
  * variable to store tile  color
  */
  tile_Color: string;
  /**
	 * variable to store previous text
	 */
  previous_text: string;
  /**
	 * variable to store next text
	 */
  next_text: string;
  /**
	 * variable to store items text
	 */
  items_text: string;
  /**
  * variable to store fluency checks text
  */
  fluency_checks: string;
  /**
  * variable to store fluency practice text
  *  */
  fluency_practice: string;
  /**
  * variable to store type text
  *  */
  type_text: string;
  /**
   * get dmc service and routing service
   * @constructor 
   * @param {_dmcService} get _dmcService
   * @param {router} get routing service
   */
  constructor(public _dmcService: DmcService, private router: Router, private route: ActivatedRoute) {
    this.path = document.getElementById("dmc-main-container").getAttribute("path");
    this.commonClass = new DmcFilterComponent.filter();
    this.user = this.commonClass.getUser();
    this.eventType = this.commonClass.getEventType();
    Object(document).self = this;
    Object(window).self = this;
    this.Math = Math;
    this.url = Object(router).currentRouterState.snapshot.url;
    this.currentRouteKey = Object(route).url._value[0].path;
    this.user == "student" ? this.title = "Fluency Practice" : "";
    this.fluencyData = jQuery('body').data();
    this.truncateValue = jQuery(window).width() <= 1024 ? jQuery(window).width() <= 961 ? 63 : 76 : jQuery(window).width() <= 1281 ? 110 : 115;
    this.assignJson(this.fluencyData[4], this.fluencyData[12], this.fluencyData[17]);
  }


  /**
 * @method ngAfterViewInit
 * 
 */
  ngAfterViewInit() {
    this.user == "student" ? jQuery(".main-container").addClass("student-main") : jQuery(".main-container").addClass("teacher-main");
    this.user == "student" ? jQuery(".student-container").removeClass("display-none") : jQuery(".teacher-container").removeClass("display-none");

    DPOService.gObj.dpoGlbService.initDPO({
      ready: function (obj: any) {
        console.log('DPO Service is ready for currentpage  ', obj.pageData);
        // CheckBox DropDown checkbox-clicked 
        jQuery(".checkbox-clicked").each(function () {
          jQuery(this).removeClass('checkbox-clicked');
          jQuery(this).trigger('click')
        });
      }
    });

    this.filter["fluency"].length <= 20 ? jQuery(".next").addClass("disabled") : jQuery(".next").removeClass("disabled");
    this.moreInfo != undefined && this.moreInfo != "" ? jQuery(".more-info-wrapper").removeClass("display-none") : "";
  }


  /**
   * close drop down click on document click
   * @method documentBodyClick
   * @param {event}  click event
   */
  documentBodyClick(event: Event) {
    if (jQuery(event.srcElement)[0].className != "dropdown-text" && jQuery(event.srcElement)[0].className != "dropbtn dropdown-clicked" && jQuery(event.srcElement)[0].className != "filter-Text" && jQuery(event.srcElement)[0].className != "dropdown-arrow dropdown-down-arrow") {
      if (jQuery(".dropbtn").hasClass("dropdown-clicked") == true) {
        jQuery(".dropdown-content").removeClass("show");
        jQuery(".dropbtn").addClass("hover").removeClass("dropdown-clicked");
      }
      Object(document).removeEventListener(Object(event.currentTarget).self.eventType, Object(event.currentTarget).self.documentBodyClick, false);
    }
  }
  /**
    * assign json data to an object
    * @method assignJson
    * @param {data} data from json file
    */
  assignJson(data: any, moreInfo: any, commonValues: any) {
    this.previous_text = moreInfo.previous_text;
    this.next_text = moreInfo.next_text;
    this.items_text = moreInfo.items_text;
    this.fluency_checks = moreInfo.fluency_checks;
    this.fluency_practice = moreInfo.fluency_practice;
    this.type_text = moreInfo.type_text;

    this.commonValues = this.user == "student" ? commonValues.student : commonValues.teacher;
    for (var i = 0; i < this.commonValues.length; i++) {
      if (this.commonValues[i].routeKey == this.currentRouteKey) {
        this.tileColor = this.commonValues[i].color;
        this.moreInfo = this.commonValues[i].moreInfo;
        this.title = this.commonValues[i].title;
        this.titleCopy = this.title;
        this.backgroundColor = this.commonValues[i].header_background_color;
        this.tile_Color = this.commonValues[i].tile_color;
        this.tileBorderColor = this.commonValues[i].tile_border_color;

      }
    }

    if (data != undefined) {
      jQuery(".loading-div").addClass("display-none");
    }
    if (this.user == "student") {
      this.fluency = data.student.slice();
    } else {
      this.fluency = data.teacher[0].libraryItems.slice();
      for (var i = 0; i < this.fluency.length; i++) {
        if ((this.fluency[i].HMH_ID).indexOf('FLUC') == -1) {
          this.fluencyPractice.push(this.fluency[i])
        } else {
          this.fluencyCheck.push(this.fluency[i])
        }
      }
      this.fluencyCheckOriginal = this.fluencyCheck.slice();
      this.fluencyPracticeOriginal = this.fluencyPractice.slice()
    }

    this.filter["fluency"] = this.fluency.slice();
    this.filter["fluencyOriginal"] = this.fluency.slice();
    this.setItemsCount(this.filter["fluency"].length);
    this.commonJson = moreInfo;
    this.landingJson = commonValues;
  }


  /**
    * click on tile
    * @method tileClickEvent
    * @param {event}click event
    * @param {url} url to open in new tab
    */
  tileClickEventStudent(event: Event, url: string) {
    let relativeUrl = "/wwtb/api/viewer.pl?uid=demo_user&cid=1&wid=" + url + "&wftype=19";
    // var iOS = !!navigator.platform && /iPad/.test(navigator.platform);
    // iOS == true ? relativeUrl = relativeUrl.replace('=', '') : '';
    this.commonClass.openInNewTab(relativeUrl, this.user, "");
  }


  /**
    * click on tile
    * @method tileClickEvent
    * @param {event}click event
    * @param {url} url to open in new tab
    */
  tileClickEvent(event: Event) {

    let url = Object(event).value;
    let relativeUrl = "/wwtb/api/viewer.pl?uid=demo_user&cid=1&wid=" + url + "&wftype=19";
    //   var iOS = !!navigator.platform && /iPad/.test(navigator.platform);
    //  iOS == true ? relativeUrl = relativeUrl.replace('=', '') : '';
    this.commonClass.openInNewTab(relativeUrl, this.user, "");

  }

  /**
   * click on teacher resources
   * @method clickInactive
   * @param {event} clickInactive
   * @param {id} element id 
   */
  clickInactive(event: Event, id: string) {
    this.user == "student" ? jQuery("#" + id).removeClass("tilesActive") : jQuery("#" + id).removeClass("teacherTileActive");
  }
  /**
   * click on teacher resources
   * @method clickActive
   * @param {event} click active
   * @param {id} element id 
   */
  clickActive(event: Event, id: string) {
    this.user == "student" ? jQuery("#" + id).addClass("tilesActive") : jQuery("#" + id).addClass("teacherTileActive");
  }


  /**
 * close popup and audio
 * @method close
 */
  close() {
    jQuery("#modal").addClass("display-none");
    document.getElementById('content').setAttribute('src', 'about:blank');
    var vid = jQuery('.inner_div').find('iframe').contents().find('#trailer').get(0);
    vid == undefined ? '' : vid.remove();

  }
  /**
  * checkBox hover
  * @method clickHoverEvent
  * @param {event} click event
  * @param {value} class name
  */
  clickHoverEvent(event: Event, value: string, textValue: string) {
    if (jQuery(event.currentTarget).hasClass(value) == false) {
      jQuery(event.currentTarget).removeClass("hover").addClass(value);
      if (value == "unit-clicked") {
        jQuery("#unit-text").text(textValue);
      }
      jQuery(event.currentTarget).removeClass("unselected")
    }
  }

  /**
   * reset filetrs 
   * @method resetFilters
   */
  resetFilters() {
    this.commonClass.isMobile() ? jQuery(".reset-container").removeClass("hover") : "";
    jQuery(".checkboxGroupElem").removeClass('checkbox-clicked');
    jQuery(".wide-tile-games").removeClass("display-none");
    this.filter["fluency"] = this.filter["fluencyOriginal"];
    this.setItemsCount(this.filter["fluency"].length);
  }

  /**
   * checkbox click event
   * @method checkBoxEvent
   * @param {event} click event
   */
  checkBoxEvent(event: Event) {
    this.commonClass.checkBoxEnableDisableEvent(jQuery(event.currentTarget).attr("id"));
    let currentId = jQuery(".checkboxGroupElem").filter('.checkbox-clicked').attr("id");
    if (jQuery(".checkboxGroupElem").filter('.checkbox-clicked').length == 1) {
      this.filter["fluency"] = [];
      this.filter["fluency"] = currentId == "checks" ? this.fluencyCheckOriginal : this.fluencyPracticeOriginal;
    } else {
      this.filter["fluency"] = this.filter["fluencyOriginal"];
    }
    this.setItemsCount(this.filter["fluency"].length);
    this.totalCount = this.pageCount > 1 ? this.filter["fluency"].length > 20 ? 20 : this.filter["fluency"].length : this.filter["fluency"].length;
    this.setDefault();
  }


  dropdownArrowClick(event: Event) {
    this.commonClass.drpDownArrowClick(jQuery(event.currentTarget).attr("id"));
  }


  /**
   * click on tile
   * @method tilesClickEvent
   * @param {routekey} routekey to navigate
   */
  tilesClickEvent(routekey: any) {
    this.router.navigate([routekey]);
  }



  /**
   * set the count
   * @method setItemsCount
   * @param {itemLength} length of item
   */

  setItemsCount(itemLength: number) {
    this.itemsFluency = itemLength;
    let addedLength: any = this.filter["fluency"].length;
    this.pageCount = Math.floor(addedLength / 20);
    addedLength != this.pageCount ? this.pageCount = this.pageCount + 1 : "";
    this.totalCount = this.pageCount > 1 ? addedLength > 20 ? 20 : addedLength : addedLength;
    addedLength <= 20 ? jQuery(".next").addClass("disabled") : jQuery(".next").removeClass("disabled");
    this.pageCount == 1 || this.currentPageId == 0 ? jQuery(".prev").addClass("disabled") : jQuery(".prev").removeClass("disabled");
  }


  /**
   * set the count
   * @method setDefault
   */

  setDefault() {
    this.pageCount = 1;
    let addedLength: any = this.filter["fluency"].length;
    this.pageCount = Math.floor(addedLength / 20);
    addedLength / 20 != this.pageCount ? this.pageCount = this.pageCount + 1 : "";
    this.startCount = 1;
    this.currentPageId = 0;
    this.totalCount = this.pageCount > 1 ? addedLength > 20 ? 20 : addedLength : addedLength;
    jQuery(".prev").addClass("disabled");
    jQuery(".wide-tile-games-container wide-tile .wideTileWrapper ").children().addClass("display-none");
    jQuery(".Page0").removeClass("display-none");
  }


  /**
   * set the changePage
   * @method changePage
   */

  changePage(event: Event, operation: String) {
    event.stopPropagation();
    this.currentPageId = jQuery(".content-container .wide-tile").not(".display-none").attr("class").split("Page")[1];
    jQuery(".prev,.next").removeClass("disabled");
    if (operation == "next") {
      jQuery(".next").hasClass("disabled") == false ? this.currentPageId++ : "";
      this.currentPageId == this.pageCount - 1 ? jQuery(".next").addClass("disabled") : "";
      this.startCount = this.totalCount + 1
    }
    else {
      this.currentPageId == 1 ? jQuery(".prev").addClass("disabled") : "";
      jQuery(".prev").hasClass("disabled") == false ? this.currentPageId-- : this.currentPageId = 0;
      this.totalCount = this.startCount - 1
    }
    jQuery(".content-container .wide-tile").not(".display-none").addClass("display-none");
    jQuery(".Page" + this.currentPageId).removeClass("display-none");
    if (operation == "next") {
      this.totalCount = this.totalCount + jQuery(".wide-tile-games-container.content-container wide-tile .wideTileWrapper").children().not(".display-none").length;
    } else {
      this.startCount = this.totalCount + 1 - jQuery(".wide-tile-games-container.content-container wide-tile .wideTileWrapper").children().not(".display-none").length;
    }
  }


}



