import { Component } from '@angular/core';
import { DmcService } from '../../shared/dmc.service';
import { ROUTER_DIRECTIVES, Router, ActivatedRoute, Params } from '@angular/router';
import { DmcFooterComponent } from '../Footer/dmc.footer.component';
declare var jQuery: any;
@Component({
  moduleId: module.id,
  selector: 'resource-page',
  templateUrl: './dmc.resources.component.html',
  directives: [ROUTER_DIRECTIVES, DmcFooterComponent],
  providers: [DmcService]
})

export class DmcResourceListingComponent {
  /**
 * variable to store route maps
 */
  routemaps: any;
  /**
 * variable to store jsonobject
 */
  jsonobject: any;
  /**
 * variable to store error messages
 */
  errorMessage: any;

  /**
   * get dmc service and routing service
   * @constructor 
   * @param {_dmcService} get _dmcService
   * @param {router} get routing service
   */
  constructor(public _dmcService: DmcService, private router: Router) {
    this._dmcService.getData("resources").subscribe(
      data => {
        this.assignJson(data);
      },
      error => this.errorMessage = error
    );
  }

  /**
   * assign json data to an object
   * @method assignJson
   * @param {data} data from json file
   */
  assignJson(data: any) {
    if (data != undefined) {
      jQuery(".loading-div").addClass("display-none");
    }
    this.jsonobject = data.teacher[0].libraryItems;
  }
  /**
   * open new tab
   * @method openInNewTab
   * @param {url} url to open in new tab
   */
  openInNewTab(url: string) {
    var win = window.open(url, '_blank');
  }
  /**
   * open on tile click event
   * @method tileClickEvent
   * @param {event} click event
   * @param {routekey} get route key to navigate
   */
  tileClickEvent(event: Event, routekey: any) {
    if (jQuery(event.currentTarget).hasClass("tile-disable") == false) {
      this.router.navigate([routekey]);
    }
  }
}