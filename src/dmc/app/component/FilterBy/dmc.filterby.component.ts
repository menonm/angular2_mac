import { Component, Pipe, Input, Output, EventEmitter } from '@angular/core';
import { DmcService } from '../../shared/dmc.service';




@Component({
  moduleId: module.id,
  selector: 'filterby',
  templateUrl: './dmc.filterby.component.html',
  providers: [DmcService]
})

export class DmcFilterByComponent {
  /**
  * Variable to store input type of landing json 
  */
  @Input() landingJson: any;
  /**
  * Variable to store data style
  */
  @Input() dataStyle: string;
  /**
* Variable to store show resources text value
*/
  @Input() showResources: string;
  /**
* Variable to store emiiting value of changeFilterModes
*/
  @Output() changeFilterModes: EventEmitter<any> = new EventEmitter();
  /**
* Variable to store emiiting value of resetFilter
*/
  @Output() resetFilter: EventEmitter<any> = new EventEmitter();
  /**
* Variable to store emiiting value of resetStandardFilter
*/
  @Output() resetStandardFilter: EventEmitter<any> = new EventEmitter();
  constructor() {
  }


  /**
     * change the mode from content and standards
     * @method changeFilterMode
     * @param {mode1} content
     * * @param {mode2} standards
     */
  changeFilterMode(mode1: string, mode2: string) {
    this.changeFilterModes.emit({
      value: [mode1, mode2]
    })
  }

  /**
   * resetFilters
   * @method resetFilters
   */

  resetFilters() {
    this.resetFilter.emit({});
  }

  /**
    * resetStandardsFilters
    * @method resetStandardsFilters
    */
  resetStandardsFilters() {
    this.resetStandardFilter.emit({});
  }

}