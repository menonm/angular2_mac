import { TestComponentBuilder } from '@angular/compiler/testing';
import { DmcFooterComponent } from '../Footer/dmc.footer.component';
import { DmcService} from '../../shared/dmc.service';
import { ROUTER_DIRECTIVES, Router, ActivatedRoute, UrlPathWithParams, RouterLink  } from '@angular/router';
import {Http, Response, HTTP_PROVIDERS} from '@angular/http';

import {
    beforeEachProviders,
    async,
    describe,
    expect,
    inject,
    it
} from '@angular/core/testing';
import {DmcResultsComponent} from './dmc.results.component';


export function main() {
    let data = [null,null,null,null,null,null,null,null,null,null,null,null,{ "filter_text": "unit1", "reset_text": "unit1", "show_resources_for": "unit1" , "previous_text": "unit1", 
    "next_text": "unit1" ,"items_text": "unit1", "results_not_found": "unit1", "worksheet_text": "unit1", "select_unit_text": "unit1", "select_lesson_text": "unit1","select_bigidea_text": "unit1" }];

    let textValue = "unit 1";
    let desc = "standardsdesc";
    let url = "post url"
    let unit = "unit1";
    let length = 10;
    let lesson="Lesson1"

    let dmcresults = new DmcResultsComponent(null, null);
    describe('dmcresultsComponent', () => {
        it('assign json value', () => {
            const result:any = dmcresults.assignJson(data);
            for (var i = 0; i < 14; i++) {
                expect(result[i]).toEqual("unit1");
            }
        })
    });


    describe('clickHoverEvent', () => {
        it('click hover events', () => {
            const result = dmcresults.clickHoverEvent(null, "unit-clicked", textValue);
            expect(textValue).toEqual("unit 1");
        })
    });




    describe('setItemsCount', () => {
        it('set items count', () => {
            const result = dmcresults.setItemsCount(length);
            expect(result).toEqual(10);
        })
    });
    
    
    

 describe('changePage', () => {
        it('click changePage', () => {
            const result = dmcresults.changePage(null,"next");
            expect(result).toEqual("next");
        })
    });



 describe('setDataCheckBox', () => {
        it('click setDataCheckBox', () => {
            const result:any = dmcresults.setDataCheckBox(null,"next","next");
            expect(result[0]).toEqual("next");
             expect(result[1]).toEqual("next");
        })
    });



 describe('setData', () => {
        it('click setDataCheckBox', () => {
            const result:any = dmcresults.setData("next","next");
            expect(result[0]).toEqual("next");
             expect(result[1]).toEqual("next");
        })
    });

    
    



}
