import { Component, Pipe } from '@angular/core';
import { DmcService } from '../../shared/dmc.service';
import { ROUTER_DIRECTIVES } from '@angular/router';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { DmcFooterComponent } from '../Footer/dmc.footer.component';
import { DmcHeaderComponent } from '../Header/dmc.header.component';
import { DmcFilterComponent } from '../../shared/mxdmcfilter.common';
import {TruncatePipe} from '../../shared/truncatepipe';
import { DPOService } from '../../shared/dpo.service';

declare var jQuery:any
@Component({
  moduleId: module.id,
  selector: 'result-component',
  templateUrl: './dmc.results.component.html',
  pipes: [TruncatePipe],
  directives: [ROUTER_DIRECTIVES, DmcFooterComponent, DmcHeaderComponent],
  providers: [DmcService]

})
export class DmcResultsComponent {
  /**
 * Array to store all units 
 */
  title: string = "Search Results";
  /**
     * Variable to store url value
     */
  url: any;
  /**
	 * Variable to event type 
	 */
  eventType: any;
  /**
  * Variable to store teacher library items 
  */
  displayContent: any = [];
  /**
 * Variable to store dropdown items
 */
  listItems: any;
  /**
 * Variable to store data
 */
  data: any;
  /**
 * Object to store current page title
 */
  filter: any = {};
  /**
  * instance of filter component file 
  */
  commonClass: DmcFilterComponent.filter;
  /**
* variable to store challenge data
*/
  challenge: any;
  /**
* variable to store result data
*/
  resultData: any;
  /**
* variable to store filtered data
*/
  filteredData: any;
  /**
* variable to store challenge original json object
*/
  challengeoriginal: any;
  /**
* variable to store challenge json object
*/
  challengeJson: any = {};
  /**
* variable to store reteach json object
*/
  reteachJson: any = {};
  /**
* variable to store practice json object
*/
  practiceJson: any = {};
  /**
* variable to store lesson checks json object
*/
  lessoncheckjson: any = {};
  /**
* variable to store rti json object
*/
  rtiJson: any = {};
  /**
* variable to store activity cards json object
*/
  activitycardsJson: any = {};
  /**
* variable to store mathreader json object
*/
  mathreaderJson: any = {};

  /**
 * variable to store inquiry cards json object
 */
  inquirycardsJson: any = {}
  /**
 * Variable to store user value
 */
  user: string;
  /**
  * variable to store results data
  */
  resultsData: any = {};
  /**
	 * variable to store unit value
	 */
  unitValue: string;

  /**
 * variable to store lesson value
 */
  lessonValue: string;

  /**
 * variable to values of checkboxes
 */
  checkBoxTempArrays: any = [];
  /**
	 * variable to store units 
	 */
  unitArray: any = [];
  /**
  * variable to store lessons per unit value
  */
  unitLesson: any = {};
  /**
  * variable to store total page count
  */
  totalCount: number;
  /**
 * variable to store  page start count
 */
  startCount: number = 1;
  /**
  * variable to store no of items
  */
  items: number;
  /**
* variable to store math value
*/
  Math: any;
  /**
 * variable to store disabled array
 */
  disabledArray: any = [];
  /**
* variable to store current page id
*/
  currentPageId: number = 0;
  /**
 * variable to store current page count
 */
  pageCount: number = 1;
  /**
 * variable to store truncateValue value
 */
  truncateValue: number;
  /**
* variable to store teacher landing data
*/
  teacherLandingData: any;
  /**
* variable to store filter text
*/
  filter_text: string;
  /**
* variable to store reset text
*/
  reset_text: string;
  /**
* variable to store show resources for  text
*/
  show_resources_for: string;
  /**
 * variable to store previous text
 */
  previous_text: string;
  /**
* variable to store next text
*/
  next_text: string;
  /**
	 * variable to store items text
	 */
  items_text: string;
  /**
 * variable to store results not found
 */
  results_not_found: string;
  /**
  * variable to store worksheet text
  */
  worksheet_text: string;
  /**
	 * variable to store select unit text
	 */
  select_unit_text: string;
  /**
  * variable to store select lesson text
  */
  select_lesson_text: string;
  /**
	 * variable to store select big idea text
	 */
  select_bigidea_text: string;
   /**
	 * variable to store type text
	 */
  type_text:string;
  /**
   * get dmc service and routing service
   * @constructor 
   * @param {_dmcService} get _dmcService
   * @param {router} get routing service
   */
  constructor(public _dmcService: DmcService, private routers: Router) {
    Object(document).self = this;
    Object(window).self = this;
    this.Math = Math;
    this.commonClass = new DmcFilterComponent.filter();
    this.user = this.commonClass.getUser();
    this.eventType = this.commonClass.getEventType();
    this.url = Object(routers).currentRouterState.snapshot.url;
    this.unitValue = document.getElementById("dmc-main-container").getAttribute("unit");
    this.lessonValue = document.getElementById("dmc-main-container").getAttribute("lesson");
    this.truncateValue = jQuery(window).width() <= 1024 ? jQuery(window).width() <= 961 ? 63 : 76 : jQuery(window).width() <= 1281 ? 110 : 115;
    this.resultData = jQuery('body').data();
    this.assignJson(this.resultData);

  }

  /**
    * set lesson drodown from landing
    * @method setLessonDropdown
    * @param {key} lesson text
    */
  setLessonDropdown(key: string) {
    var unitText = key.split(" ").join("").trim();
    this.listItems = "";
    for (var i = 0; i < this.unitLesson[unitText].length; i++) {
      this.listItems += "  <div  class='dDown_content  hover lesson-dDown_content' id=ert" + i + " (click)='lessonDropDownEvent($event)'><div class='dropdown-text' id=lesson" + i + ">" + this.unitLesson[unitText][i] + "</div></div>";
      jQuery("#LessonDropdown").html(this.listItems);
    }
    var classNames = document.getElementsByClassName("lesson-dDown_content");
    var self = this;
    jQuery("#LessonDropdown").data("self", this);
    for (var i = 0; i < classNames.length; i++) {
      classNames[i].addEventListener(this.eventType, this.lessonDropDownEvent, false);
    }
    jQuery("#LessonDropdown").data("self", this)
  }



  /**
    * ngAfterViewInit
    */
  ngAfterViewInit() {
    this.user == "student" ? jQuery(".main-container").addClass("student-main") : jQuery(".main-container").addClass("teacher-main");
    this.setLessonDropdown(this.unitValue);
    jQuery("#unit-text").text(this.unitValue);
    jQuery("#lesson-text").text(this.lessonValue);
    jQuery("#unit-text").addClass("value-selected");
    this.lessonValue == "Lessons All" ? "" : jQuery("#lesson-text").addClass("value-selected");
    let dropdownText = jQuery(".unit-filter-dDown_content .dropdown-text");
    for (var i = 0; i < dropdownText.length; i++) {
      if (dropdownText[i].innerText == jQuery("#unit-text").text().trim()) {
        jQuery("#" + dropdownText[i].getAttribute("id")).parent().addClass("unit-clicked");
      }
    }

    let lessonText = jQuery(".lesson-dDown_content .dropdown-text");
    for (var i = 0; i < lessonText.length; i++) {
      if (lessonText[i].innerText == jQuery("#lesson-text").text().trim()) {
        jQuery("#" + lessonText[i].getAttribute("id")).parent().addClass("lesson-clicked");
      }
    }
    DPOService.gObj.dpoGlbService.initDPO({
      ready: function (obj: any) {
        // CheckBox DropDown checkbox-clicked 
        jQuery(".checkbox-clicked").each(function () {
          jQuery(this).removeClass('checkbox-clicked');
          jQuery(this).trigger('click')
        });

      }
    });
  }


  /**
    * ngAfterViewChecked
    */
  ngAfterViewChecked(): void {
    this.setColor()
  }


  /**
    * close drop down click on document click
    * @method documentBodyClick
    * @param {event}  click event
    */
  documentBodyClick(event: Event) {
    if (jQuery(".main-container").hasClass("search-results")) {
      let className = jQuery(event.target).attr("class");
      let id = jQuery(event.target).attr("id");
      if (className != "dropdown-text" && className != "dropdown-arrow dropdown-down-arrow" && id != "dropdown1" && id != "dropdown2" && id != "lesson-text" && id != "unit-text") {
        if (jQuery(".dropbtn").hasClass("dropdown-clicked") == true) {
          jQuery(".dropdown-content").removeClass("show");
          jQuery(".dropbtn").addClass("hover").removeClass("dropdown-clicked");
        }
        Object(document).removeEventListener(Object(event.currentTarget).self.eventType, Object(event.currentTarget).self.documentBodyClick, false);
      }
    }
  }

  /**
     * set tile border color
     * @method setColor
     */

  setColor() {
    for (let r = 0; r < this.teacherLandingData.length; r++) {
      let tileName = "";
      switch (this.teacherLandingData[r].routeKey) {
        case 'practice':
          tileName = "Practice"
          break;
        case 'reteach':
          tileName = "Reteach"
          break;
        case 'challenge':
          tileName = "Challenge"
          break;
        case 'rti':
          tileName = "RtI"
          break;
        case 'fluencybuilders':
          tileName = "Check"
          break;
        case 'games':
          tileName = "Games"
          break;
        case 'lessonchecks':
          tileName = "Checks"
          break;
        case 'activitycards':
          tileName = "Cards"
          break;
        case 'mathreader':
          tileName = "Readers"
          break;
        case 'inquirybasedtasks':
          tileName = "Tasks"
          break;

      }

      jQuery("." + tileName + "-tile").css("border-left", "20px solid " + this.teacherLandingData[r].color);
      this.teacherLandingData[r].routeKey == "inquirybasedtasks" ? jQuery(".master-tile").css("border-left", "20px solid " + this.teacherLandingData[r].black_line_color) : "";




    }

  }


  /**
 * close popup and audio
 * @method close
 */
  close() {
    jQuery("#modal").addClass("display-none");
    var vid = jQuery('.inner_div').find('iframe').contents().find('#trailer').get(0);
    vid == undefined ? '' : vid.remove();
  }

  /**
     * click on tile
     * @method tilesClickEvent
     * @param {routekey} routekey to navigate
     */


  tileClickEvent(event: Event, url: string, hmhid?: string, component?: string) {
    if (component == "Fluency") {
      url = "/wwtb/api/viewer.pl?uid=demo_user&cid=1&wid=" + hmhid + "&wftype=19";
    }
    if (url.indexOf("games") > -1) {
      this.commonClass.openInNewTab(url, "", "games");
    }
    else {
      var iOS = !!navigator.platform && /iPad/.test(navigator.platform);
      iOS == true ? url = url.replace('=', '') : '';
      this.commonClass.openInNewTab(url, this.user, '');
    }
  }

  /**
   * open new popup
   * @method openInNewPopUp
   * @param {url} url to load in popup
   */
  openInNewPopUp(url: string) {
    var hostnameProtocol = window.location.origin;
    var navigationUrl = hostnameProtocol + url;
    jQuery("#modal").removeClass("display-none");
    document.getElementById('content').setAttribute('src', navigationUrl);
  }


  /**
     * dropDownArrow click
     * @method dropdownArrowClick
     * @param {event} click event
     */
  dropdownArrowClick(event: Event) {
    this.commonClass.drpDownArrowClick(jQuery(event.currentTarget).attr("id"));
  }


  /**
     * assign json data to an object
     * @method assignJson
     * @param {data} data from json file
     */
  assignJson(data: any) {

    let challenge = {}

    this.filter_text = data[12].filter_text;
    this.reset_text = data[12].reset_text;
    this.show_resources_for = data[12].show_resources_for;
    this.previous_text = data[12].previous_text;
    this.next_text = data[12].next_text;
    this.items_text = data[12].items_text;
    this.results_not_found = data[12].results_not_found;
    this.worksheet_text = data[12].worksheet_text;
    this.select_unit_text = data[12].select_unit_text;
    this.select_lesson_text = data[12].select_lesson_text;
    this.select_bigidea_text = data[12].select_bigidea_text;
    this.type_text= data[12].type_text;



    let obj: any = [];
    let lessonObj: any = [];
    var pattunit = /U\d+/;
    var pattlesson = /L\d+/;
    var pattdigit = /\d+/;
    this.unitArray = data[17].unitArray;
    this.unitLesson = data[17].unitLesson;
    this.teacherLandingData = data[17].teacher.slice();
    let disableArray: any = []
    this.filter["rti"] = data[3].teacher[0].libraryItems.slice();
    this.filter["reteach"] = data[1].teacher[0].libraryItems.slice();
    this.filter["practice"] = data[0].teacher[0].libraryItems.slice();
    this.filter["challenge"] = data[2].teacher[0].libraryItems.slice();
    this.filter["games"] = data[5].teacher[0].libraryItems.slice();
    this.filter["fluencybuilders"] = data[4].teacher[0].libraryItems.slice();
    this.filter["mathreader"] = data[8].teacher[0].libraryItems.slice();
    this.filter["lessonchecks"] = data[6].teacher[0].libraryItems.slice();
    this.filter["activitycards"] = data[7].teacher[0].libraryItems.slice();
    this.filter["inquirybasedtasks"] = data[10].teacher[0].libraryItems.slice();
    let tempOriginalData: any = [];
    for (let r = 0; r < this.teacherLandingData.length; r++) {
      if (this.teacherLandingData[r].enable == true && this.teacherLandingData[r].remove == "no" && this.teacherLandingData[r].results_title.length > 0) {
        tempOriginalData.length == 0 ? tempOriginalData = data[r].teacher[0].libraryItems.slice() : tempOriginalData.push.apply(tempOriginalData, data[r].teacher[0].libraryItems.slice());
        if (this.teacherLandingData[r].routeKey == "practice" || this.teacherLandingData[r].routeKey == "challenge" || this.teacherLandingData[r].routeKey == "reteach") {
          tempOriginalData.push.apply(tempOriginalData, data[r].student.slice());
        }
      } else {
        this.disabledArray.push(this.teacherLandingData[r].routeKey)
      }
    }

    this.filter["originalObject"] = tempOriginalData.slice();



    // merging teacher and student array values into displayContent
    this.displayContent = data[2].teacher[0].libraryItems.slice();
    this.displayContent = data[2].teacher[0].libraryItems.slice();
    jQuery.merge(this.displayContent, data[2].student.slice());
    this.filter["challenge"] = this.displayContent.slice();

    //challenge loop
    for (let i = 0; i < this.displayContent.length; i++) {

      if (pattunit.test(this.displayContent[i].HMH_ID)) {
        if (("Unit" + parseInt(pattdigit.exec(pattunit.exec(this.displayContent[i].HMH_ID)[0])[0]) in this.challengeJson) == false) {
          obj = [];
          obj.push(this.displayContent[i]);
          let keyValue = "Lesson" + parseInt(pattdigit.exec(pattlesson.exec(this.displayContent[i].HMH_ID)[0])[0]);
          this.challengeJson["Unit" + parseInt(pattdigit.exec(pattunit.exec(this.displayContent[i].HMH_ID)[0])[0])] = {};
          this.challengeJson["Unit" + parseInt(pattdigit.exec(pattunit.exec(this.displayContent[i].HMH_ID)[0])[0])].object = obj
          obj = [];
          obj.push(this.displayContent[i]);
          this.challengeJson["Unit" + parseInt(pattdigit.exec(pattunit.exec(this.displayContent[i].HMH_ID)[0])[0])][keyValue] = obj

        } else {
          obj = []
          lessonObj = [];
          let keyValue = "Lesson" + parseInt(pattdigit.exec(pattlesson.exec(this.displayContent[i].HMH_ID)[0])[0]);
          obj = this.challengeJson["Unit" + parseInt(pattdigit.exec(pattunit.exec(this.displayContent[i].HMH_ID)[0])[0])].object;
          lessonObj = this.challengeJson["Unit" + parseInt(pattdigit.exec(pattunit.exec(this.displayContent[i].HMH_ID)[0])[0])][keyValue];
          obj.push(this.displayContent[i]);
          lessonObj == undefined ? lessonObj = [] : "";
          lessonObj == undefined ? lessonObj.push(this.displayContent[i]) : "";
          this.challengeJson["Unit" + parseInt(pattdigit.exec(pattunit.exec(this.displayContent[i].HMH_ID)[0])[0])].object = obj
          obj = [];
          obj = this.challengeJson["Unit" + parseInt(pattdigit.exec(pattunit.exec(this.displayContent[i].HMH_ID)[0])[0])][keyValue];
          obj == undefined ? obj = [] : obj;
          obj.push(this.displayContent[i]);
          this.challengeJson["Unit" + parseInt(pattdigit.exec(pattunit.exec(this.displayContent[i].HMH_ID)[0])[0])][keyValue] = obj;
        }
      }
    }



    this.displayContent = [];
    this.filter["reteach"] = data[1].teacher[0].libraryItems.slice();
    this.displayContent = data[1].teacher[0].libraryItems.slice();
    jQuery.merge(this.displayContent, data[1].student.slice());
    this.filter["reteach"] = this.displayContent.slice();

    //challenge loop
    for (let i = 0; i < this.displayContent.length; i++) {

      if (pattunit.test(this.displayContent[i].HMH_ID)) {
        if (("Unit" + parseInt(pattdigit.exec(pattunit.exec(this.displayContent[i].HMH_ID)[0])[0]) in this.reteachJson) == false) {
          obj = [];
          obj.push(this.displayContent[i]);
          let keyValue = "Lesson" + parseInt(pattdigit.exec(pattlesson.exec(this.displayContent[i].HMH_ID)[0])[0]);
          this.reteachJson["Unit" + parseInt(pattdigit.exec(pattunit.exec(this.displayContent[i].HMH_ID)[0])[0])] = {};
          this.reteachJson["Unit" + parseInt(pattdigit.exec(pattunit.exec(this.displayContent[i].HMH_ID)[0])[0])].object = obj
          obj = [];
          obj.push(this.displayContent[i]);
          this.reteachJson["Unit" + parseInt(pattdigit.exec(pattunit.exec(this.displayContent[i].HMH_ID)[0])[0])][keyValue] = obj

        } else {
          obj = []
          obj = this.reteachJson["Unit" + parseInt(pattdigit.exec(pattunit.exec(this.displayContent[i].HMH_ID)[0])[0])].object;
          obj.push(this.displayContent[i]);
          let keyValue = "Lesson" + parseInt(pattdigit.exec(pattlesson.exec(this.displayContent[i].HMH_ID)[0])[0]);
          this.reteachJson["Unit" + parseInt(pattdigit.exec(pattunit.exec(this.displayContent[i].HMH_ID)[0])[0])].object = obj
          obj = [];
          obj = this.reteachJson["Unit" + parseInt(pattdigit.exec(pattunit.exec(this.displayContent[i].HMH_ID)[0])[0])][keyValue];
          obj == undefined ? obj = [] : obj;
          obj.push(this.displayContent[i]);
          this.reteachJson["Unit" + parseInt(pattdigit.exec(pattunit.exec(this.displayContent[i].HMH_ID)[0])[0])][keyValue] = obj
        }
      }
    }



    this.displayContent = [];
    this.filter["practice"] = data[0].teacher[0].libraryItems.slice();
    this.displayContent = data[0].teacher[0].libraryItems.slice();
    jQuery.merge(this.displayContent, data[0].student.slice());
    this.filter["practice"] = this.displayContent.slice();

    //challenge loop
    for (let i = 0; i < this.displayContent.length; i++) {

      if (pattunit.test(this.displayContent[i].HMH_ID)) {
        if (("Unit" + parseInt(pattdigit.exec(pattunit.exec(this.displayContent[i].HMH_ID)[0])[0]) in this.practiceJson) == false) {
          obj = [];
          obj.push(this.displayContent[i]);
          let keyValue = "Lesson" + parseInt(pattdigit.exec(pattlesson.exec(this.displayContent[i].HMH_ID)[0])[0]);
          this.practiceJson["Unit" + parseInt(pattdigit.exec(pattunit.exec(this.displayContent[i].HMH_ID)[0])[0])] = {};
          this.practiceJson["Unit" + parseInt(pattdigit.exec(pattunit.exec(this.displayContent[i].HMH_ID)[0])[0])].object = obj
          obj = [];
          obj.push(this.displayContent[i]);
          this.practiceJson["Unit" + parseInt(pattdigit.exec(pattunit.exec(this.displayContent[i].HMH_ID)[0])[0])][keyValue] = obj

        } else {
          obj = []
          obj = this.practiceJson["Unit" + parseInt(pattdigit.exec(pattunit.exec(this.displayContent[i].HMH_ID)[0])[0])].object;

          obj.push(this.displayContent[i]);
          let keyValue = "Lesson" + parseInt(pattdigit.exec(pattlesson.exec(this.displayContent[i].HMH_ID)[0])[0]);
          this.practiceJson["Unit" + parseInt(pattdigit.exec(pattunit.exec(this.displayContent[i].HMH_ID)[0])[0])].object = obj
          obj = [];
          obj = this.practiceJson["Unit" + parseInt(pattdigit.exec(pattunit.exec(this.displayContent[i].HMH_ID)[0])[0])][keyValue];
          obj == undefined ? obj = [] : obj;
          obj.push(this.displayContent[i]);
          this.practiceJson["Unit" + parseInt(pattdigit.exec(pattunit.exec(this.displayContent[i].HMH_ID)[0])[0])][keyValue] = obj;
        }
      }
    }




    this.displayContent = [];
    this.filter["lessonchecks"] = data[6].teacher[0].libraryItems.slice();
    this.displayContent = data[6].teacher[0].libraryItems.slice();
    //challenge loop
    for (let i = 0; i < this.displayContent.length; i++) {

      if (pattunit.test(this.displayContent[i].HMH_ID)) {
        if (("Unit" + parseInt(pattdigit.exec(pattunit.exec(this.displayContent[i].HMH_ID)[0])[0]) in this.lessoncheckjson) == false) {
          obj = [];
          obj.push(this.displayContent[i]);
          let keyValue = "Lesson" + parseInt(pattdigit.exec(pattlesson.exec(this.displayContent[i].HMH_ID)[0])[0]);
          this.lessoncheckjson["Unit" + parseInt(pattdigit.exec(pattunit.exec(this.displayContent[i].HMH_ID)[0])[0])] = {};
          this.lessoncheckjson["Unit" + parseInt(pattdigit.exec(pattunit.exec(this.displayContent[i].HMH_ID)[0])[0])].object = obj
          obj = [];
          obj.push(this.displayContent[i]);
          this.lessoncheckjson["Unit" + parseInt(pattdigit.exec(pattunit.exec(this.displayContent[i].HMH_ID)[0])[0])][keyValue] = obj

        } else {
          obj = []
          obj = this.lessoncheckjson["Unit" + parseInt(pattdigit.exec(pattunit.exec(this.displayContent[i].HMH_ID)[0])[0])].object;

          obj.push(this.displayContent[i]);
          let keyValue = "Lesson" + parseInt(pattdigit.exec(pattlesson.exec(this.displayContent[i].HMH_ID)[0])[0]);
          this.lessoncheckjson["Unit" + parseInt(pattdigit.exec(pattunit.exec(this.displayContent[i].HMH_ID)[0])[0])].object = obj
          obj = [];
          obj = this.lessoncheckjson["Unit" + parseInt(pattdigit.exec(pattunit.exec(this.displayContent[i].HMH_ID)[0])[0])][keyValue];
          obj == undefined ? obj = [] : obj;
          obj.push(this.displayContent[i]);
          this.lessoncheckjson["Unit" + parseInt(pattdigit.exec(pattunit.exec(this.displayContent[i].HMH_ID)[0])[0])][keyValue] = obj;
        }
      }
    }


    this.displayContent = data[7].teacher[0].libraryItems;
    //activitycardsloop
    for (let i = 0; i < this.displayContent.length; i++) {
      if (pattunit.test(this.displayContent[i].HMH_ID)) {
        if (("Unit" + parseInt(pattdigit.exec(pattunit.exec(this.displayContent[i].HMH_ID)[0])[0]) in this.activitycardsJson) == false) {
          obj = [];
          obj.push(this.displayContent[i]);
          this.activitycardsJson["Unit" + parseInt(pattdigit.exec(pattunit.exec(this.displayContent[i].HMH_ID)[0])[0])] = { object: obj };
        } else {
          obj = [];
          obj = this.activitycardsJson["Unit" + parseInt(pattdigit.exec(pattunit.exec(this.displayContent[i].HMH_ID)[0])[0])].object;
          obj.push(this.displayContent[i]);
          this.activitycardsJson["Unit" + parseInt(pattdigit.exec(pattunit.exec(this.displayContent[i].HMH_ID)[0])[0])] = { object: obj };
        }
      }
    }

    this.displayContent = data[8].teacher[0].libraryItems;
    //mathreaderloop
    for (let i = 0; i < this.displayContent.length; i++) {
      if (pattunit.test(this.displayContent[i].HMH_ID)) {
        if (("Unit" + parseInt(pattdigit.exec(pattunit.exec(this.displayContent[i].HMH_ID)[0])[0]) in this.mathreaderJson) == false) {
          obj = [];
          obj.push(this.displayContent[i]);

          this.mathreaderJson["Unit" + parseInt(pattdigit.exec(pattunit.exec(this.displayContent[i].HMH_ID)[0])[0])] = { object: obj };
        } else {
          obj = [];
          obj = this.mathreaderJson["Unit" + parseInt(pattdigit.exec(pattunit.exec(this.displayContent[i].HMH_ID)[0])[0])].object;
          obj.push(this.displayContent[i]);
          this.mathreaderJson["Unit" + parseInt(pattdigit.exec(pattunit.exec(this.displayContent[i].HMH_ID)[0])[0])] = { object: obj };
        }
      }
    }



    this.displayContent = data[10].teacher[0].libraryItems;
    //inquirycards
    for (let i = 0; i < this.displayContent.length; i++) {
      if (pattunit.test(this.displayContent[i].HMH_ID)) {
        if (("Unit" + parseInt(pattdigit.exec(pattunit.exec(this.displayContent[i].HMH_ID)[0])[0]) in this.inquirycardsJson) == false) {
          obj = [];
          obj.push(this.displayContent[i]);

          this.inquirycardsJson["Unit" + parseInt(pattdigit.exec(pattunit.exec(this.displayContent[i].HMH_ID)[0])[0])] = { object: obj };
        } else {
          obj = [];
          obj = this.inquirycardsJson["Unit" + parseInt(pattdigit.exec(pattunit.exec(this.displayContent[i].HMH_ID)[0])[0])].object;
          obj.push(this.displayContent[i]);
          this.inquirycardsJson["Unit" + parseInt(pattdigit.exec(pattunit.exec(this.displayContent[i].HMH_ID)[0])[0])] = { object: obj };
        }
      }
    }

    this.displayContent = data[3].teacher[0].libraryItems;
    //rtuloop
    for (let i = 0; i < this.displayContent.length; i++) {
      if (pattunit.test(this.displayContent[i].HMH_ID)) {
        if (("Unit" + parseInt(pattdigit.exec(pattunit.exec(this.displayContent[i].HMH_ID)[0])[0]) in this.rtiJson) == false) {
          obj = [];
          obj.push(this.displayContent[i]);
          this.rtiJson["Unit" + parseInt(pattdigit.exec(pattunit.exec(this.displayContent[i].HMH_ID)[0])[0])] = { object: obj };
        } else {
          obj = [];
          obj = this.rtiJson["Unit" + parseInt(pattdigit.exec(pattunit.exec(this.displayContent[i].HMH_ID)[0])[0])].object;
          obj.push(this.displayContent[i]);
          this.rtiJson["Unit" + parseInt(pattdigit.exec(pattunit.exec(this.displayContent[i].HMH_ID)[0])[0])] = { object: obj };
        }
      }
    }


    this.resultsData = { "practice": jQuery.extend({}, this.practiceJson), "reteach": jQuery.extend({}, this.reteachJson), "challenge": jQuery.extend({}, this.challengeJson), "rti": jQuery.extend({}, this.rtiJson), "fluency": data[4].teacher[0].libraryItems.slice(), "games": data[5].teacher[0].libraryItems.slice(), "lessoncheck": jQuery.extend({}, this.lessoncheckjson), "activitycards": jQuery.extend({}, this.activitycardsJson), "mathreader": jQuery.extend({}, this.mathreaderJson), "inquirycards": jQuery.extend({}, this.inquirycardsJson) };
    this.setData(this.unitValue, this.lessonValue);
    // this.setColor()
  }




  /**
       * set data based on landing page filtering
       * @method setData
       * @param {unitValue} unit from landing page
       * @param {lessonValue} lesson value from landing page
       */
  setData(unitValue: string, lessonValue: string) {
    this.filteredData = [];
    let tempData: any = [];
    var results = jQuery.extend(true, {}, this.resultsData);
    tempData.length = 0;


    if (results["practice"][unitValue.split(" ").join("").trim()] != undefined) {

      lessonValue == "Lessons All" && jQuery.inArray("practice", this.disabledArray) == -1 ? tempData = results["practice"][unitValue.split(" ").join("").trim()].object : "";
      if (lessonValue != "Lessons All" && results["practice"][unitValue.split(" ").join("").trim()][lessonValue.split(" ").join("").trim()] != undefined && jQuery.inArray("practice", this.disabledArray) == -1) {
        tempData.push(results["practice"][unitValue.split(" ").join("").trim()][lessonValue.split(" ").join("").trim()][0])
        tempData.push(results["practice"][unitValue.split(" ").join("").trim()][lessonValue.split(" ").join("").trim()][1])
      }
      if ((tempData.length == 0 || tempData.length == undefined) && results["reteach"][unitValue.split(" ").join("").trim()] != undefined && jQuery.inArray("reteach", this.disabledArray) == -1) {
        lessonValue == "Lessons All" ? tempData = results["reteach"][unitValue.split(" ").join("").trim()].object : "";
        if (lessonValue != "Lessons All" && results["reteach"][unitValue.split(" ").join("").trim()][lessonValue.split(" ").join("").trim()] != undefined) {
          tempData.push(results["reteach"][unitValue.split(" ").join("").trim()][lessonValue.split(" ").join("").trim()][0])
          tempData.push(results["reteach"][unitValue.split(" ").join("").trim()][lessonValue.split(" ").join("").trim()][1])
        }
        if ((tempData.length == 0 || tempData.length == undefined) && results["challenge"][unitValue.split(" ").join("").trim()] != undefined && jQuery.inArray("challenge", this.disabledArray) == -1) {
          lessonValue == "Lessons All" ? tempData = results["challenge"][unitValue.split(" ").join("").trim()].object : "";
          if (lessonValue != "Lessons All" && results["challenge"][unitValue.split(" ").join("").trim()][lessonValue.split(" ").join("").trim()] != undefined) {
            tempData.push(results["challenge"][unitValue.split(" ").join("").trim()][lessonValue.split(" ").join("").trim()][0])
            tempData.push(results["challenge"][unitValue.split(" ").join("").trim()][lessonValue.split(" ").join("").trim()][1])
          }

          if ((tempData.length == 0 || tempData.length == undefined) && results["rti"][unitValue.split(" ").join("").trim()] != 0 && jQuery.inArray("rti", this.disabledArray) == -1) {
            tempData = results["rti"][unitValue.split(" ").join("").trim()].object;
          }
          if ((tempData.length == 0 || tempData.length == undefined) && jQuery.inArray("fluencybuilders", this.disabledArray) == -1) {
            tempData = results["fluency"]
          }
        }

      }
    }

    /*  else if (results["reteach"][unitValue.split(" ").join("").trim()] != undefined) {
        lessonValue == "Lessons All" ? tempData = results["reteach"][unitValue.split(" ").join("").trim()].object : "";
        if (lessonValue != "Lessons All" && results["reteach"][unitValue.split(" ").join("").trim()][lessonValue.split(" ").join("").trim()] != undefined) {
          tempData.push(results["reteach"][unitValue.split(" ").join("").trim()][lessonValue.split(" ").join("").trim()][0])
          tempData.push(results["reteach"][unitValue.split(" ").join("").trim()][lessonValue.split(" ").join("").trim()][1])
        }
        if ((tempData.length == 0 || tempData.length == undefined) && results["challenge"][unitValue.split(" ").join("").trim()] != undefined) {
          lessonValue == "Lessons All" ? tempData = results["challenge"][unitValue.split(" ").join("").trim()].object : results["challenge"][unitValue.split(" ").join("").trim()][lessonValue.split(" ").join("").trim()] != undefined ? tempData = results["challenge"][unitValue.split(" ").join("").trim()][lessonValue.split(" ").join("").trim()] : tempData = [];
  
          if ((tempData.length == 0 || tempData.length == undefined) && results["rti"][unitValue.split(" ").join("").trim()] != 0) {
            tempData = results["rti"][unitValue.split(" ").join("").trim()].object;
          }
          if ((tempData.length == 0 || tempData.length == undefined)) {
            tempData = results["fluency"]
          }
        }
      }
      else if (results["challenge"][unitValue.split(" ").join("").trim()] != undefined) {
        lessonValue == "Lessons All" ? tempData = results["challenge"][unitValue.split(" ").join("").trim()].object : "";
        if (lessonValue != "Lessons All" && results["challenge"][unitValue.split(" ").join("").trim()][lessonValue.split(" ").join("").trim()] != undefined) {
          tempData.push(results["challenge"][unitValue.split(" ").join("").trim()][lessonValue.split(" ").join("").trim()][0])
          tempData.push(results["challenge"][unitValue.split(" ").join("").trim()][lessonValue.split(" ").join("").trim()][1])
        }
        if ((tempData.length == 0 || tempData.length == undefined) && results["rti"][unitValue.split(" ").join("").trim()] != 0) {
          tempData = results["rti"][unitValue.split(" ").join("").trim()].object;
        }
  
        if ((tempData.length == 0 || tempData.length == undefined)) {
          tempData = results["fluency"]
        }
  
      }
      else if ((tempData.length == 0 || tempData.length == undefined) && results["rti"][unitValue.split(" ").join("").trim()] != undefined) {
        tempData = results["rti"][unitValue.split(" ").join("").trim()].object;
      }
      else {
        tempData = results["fluency"]
      }*/



    //reteach
    results["practice"][unitValue.split(" ").join("").trim()] != undefined && results["reteach"][unitValue.split(" ").join("").trim()] != undefined && lessonValue == "Lessons All" && jQuery.inArray("reteach", this.disabledArray) == -1 ?
      tempData.push.apply(tempData, results["reteach"][unitValue.split(" ").join("").trim()].object) : "";
    if (results["practice"][unitValue.split(" ").join("").trim()] != undefined && results["reteach"][unitValue.split(" ").join("").trim()] != undefined && lessonValue != "Lessons All" && results["reteach"][unitValue.split(" ").join("").trim()][lessonValue.split(" ").join("").trim()] != undefined && jQuery.inArray("reteach", this.disabledArray) == -1) {
      tempData.push(results["reteach"][unitValue.split(" ").join("").trim()][lessonValue.split(" ").join("").trim()][0]);
      tempData.push(results["reteach"][unitValue.split(" ").join("").trim()][lessonValue.split(" ").join("").trim()][1]);
    }

    //challenge
    results["reteach"][unitValue.split(" ").join("").trim()] != undefined && results["challenge"][unitValue.split(" ").join("").trim()] != undefined && lessonValue == "Lessons All" && jQuery.inArray("challenge", this.disabledArray) == -1 ? tempData.push.apply(tempData, results["challenge"][unitValue.split(" ").join("").trim()].object) : "";
    if (results["reteach"][unitValue.split(" ").join("").trim()] != undefined && results["challenge"][unitValue.split(" ").join("").trim()] != undefined && lessonValue != "Lessons All" && results["challenge"][unitValue.split(" ").join("").trim()][lessonValue.split(" ").join("").trim()] != undefined && jQuery.inArray("challenge", this.disabledArray) == -1) {
      tempData.push(results["challenge"][unitValue.split(" ").join("").trim()][lessonValue.split(" ").join("").trim()][0])
      tempData.push(results["challenge"][unitValue.split(" ").join("").trim()][lessonValue.split(" ").join("").trim()][1])
    }


    //rti,fluency,games
    results["challenge"][unitValue.split(" ").join("").trim()] != undefined && results["rti"][unitValue.split(" ").join("").trim()] != undefined && jQuery.inArray("rti", this.disabledArray) == -1 ? tempData.push.apply(tempData, results["rti"][unitValue.split(" ").join("").trim()].object) : "";
    results["rti"][unitValue.split(" ").join("").trim()] != undefined && results["fluency"] != undefined && jQuery.inArray("fluencybuilders", this.disabledArray) == -1 ? tempData.push.apply(tempData, results["fluency"]) : "";
    results["fluency"] != undefined && results["games"] != undefined && jQuery.inArray("games", this.disabledArray) == -1 ? tempData.push.apply(tempData, results["games"]) : "";

    //lessoncheck
    results["lessoncheck"][unitValue.split(" ").join("").trim()] != undefined && lessonValue == "Lessons All" && jQuery.inArray("lessonchecks", this.disabledArray) == -1 ? tempData.push.apply(tempData, results["lessoncheck"][unitValue.split(" ").join("").trim()].object) : "";
    if (results["lessoncheck"][unitValue.split(" ").join("").trim()] != undefined && lessonValue != "Lessons All" && results["lessoncheck"][unitValue.split(" ").join("").trim()][lessonValue.split(" ").join("").trim()] != undefined && jQuery.inArray("lessonchecks", this.disabledArray) == -1) {
      tempData.push(results["lessoncheck"][unitValue.split(" ").join("").trim()][lessonValue.split(" ").join("").trim()][0])
    }

    results["activitycards"][unitValue.split(" ").join("").trim()] != undefined ? tempData.push.apply(tempData, results["activitycards"][unitValue.split(" ").join("").trim()].object) : "";

    results["mathreader"][unitValue.split(" ").join("").trim()] != undefined ? tempData.push.apply(tempData, results["mathreader"][unitValue.split(" ").join("").trim()].object) : "";
    results["inquirycards"][unitValue.split(" ").join("").trim()] != undefined ? tempData.push.apply(tempData, results["inquirycards"][unitValue.split(" ").join("").trim()].object) : "";



    this.filteredData = tempData.slice();
    this.setItemsCount(this.filteredData.length);
    this.pageCount = Math.floor(this.filteredData.length / 20);
    this.filteredData.length / 20 != this.pageCount ? this.pageCount = this.pageCount + 1 : "";
    this.totalCount = this.pageCount > 1 ? this.filteredData.length > 20 ? 20 : this.filteredData.length : this.filteredData.length;
    this.currentPageId = 0;
    tempData.length = 0;
    //this.setColor();
  }


  /**
   * toggle drop down
   * @method toggleDropDown
   * @param {event} click event
   * @param {DropdownShow} class name to remove or add
   */
  toggleDropDown(event: Event, DropdownShow: string, DropdownHide: string) {
    this.commonClass.toggleDropDown(DropdownShow, DropdownHide);
    if (jQuery("#" + DropdownShow).siblings().hasClass("dropdown-clicked") == true) {
      Object(document).addEventListener(this.eventType, this.documentBodyClick, true);
    }
  }



  /**
  * dropDownEvent click
  * @method unitDropDownEvent
  * @param {event} click event
  */
  unitDropDownEvent(event: Event) {
    jQuery(".landing-dropdown-content").children().removeClass("dropdown-clicked");
    jQuery(".landing-dDown_content").removeClass("unit-clicked");
    this.clickHoverEvent(event, "unit-clicked", jQuery(event.currentTarget)[0].innerText);
  }

  /**
 * dropDownEvent click
 * @method lessonDropDownEvent
 * @param {event} click event
 */
  lessonDropDownEvent(event: Event) {
    jQuery(".lesson-dDown_content").removeClass("lesson-clicked");
    jQuery(event.currentTarget).addClass("lesson-clicked");
    jQuery("#lesson-text").text(Object(jQuery(event.currentTarget).context).innerText);
    jQuery("#LessonDropdown").removeClass("show");
    jQuery("#LessonDropdown").siblings().removeClass("dropdown-clicked");
    jQuery("#lesson-text").addClass("value-selected");
    var scope = jQuery("#LessonDropdown").data("self");
    jQuery(".checkboxGroupElem").filter('.checkbox-clicked').length >= 1 ? scope.setDataCheckBox(scope.checkBoxTempArrays, jQuery("#unit-text").text(), jQuery("#lesson-text").text()) : scope.setData(jQuery("#unit-text").text(), jQuery("#lesson-text").text());
    scope.setItemsCount(scope.filteredData.length);
    scope.setDefault();
  }




  /**
   * drodown events
   * @method clickHoverEvent
   * @param {event} click event
   * @param {value} dropdown id value
   * @param {textValue} dropdown selected value
   */
  clickHoverEvent(event: Event, value: string, textValue: string) {
    jQuery(".unit-filter-dDown_content").removeClass("unit-clicked");
    jQuery("#lesson-text").removeClass("value-selected");
    if (jQuery(event.currentTarget).hasClass(value) == false) {
      jQuery(event.currentTarget).removeClass("hover").addClass(value);
      if (value == "unit-clicked") {
        jQuery("#lesson-text").text("");
        jQuery("#unit-text").text(textValue);
        jQuery("#UnitDropdown").removeClass("show");
        jQuery(".lesson-filter-dropdown ").removeClass("disabled-dropdown");
        jQuery("#UnitDropdown").siblings().removeClass("dropdown-clicked");
        jQuery("#unit-text").addClass("value-selected");
        this.listItems = "";
        this.setLessonDropdown(textValue);
        jQuery("#lesson-text").text("Lessons All");
        jQuery(".checkboxGroupElem").filter('.checkbox-clicked').length >= 1 ? this.setDataCheckBox(this.checkBoxTempArrays, jQuery("#unit-text").text(), jQuery("#lesson-text").text()) : this.setData(jQuery("#unit-text").text(), jQuery("#lesson-text").text());
      }
    } else {
      jQuery(event.currentTarget).removeClass(value);
    }
    this.setItemsCount(this.filteredData.length);
    this.setDefault();
  }

  /**
     * reset filetrs 
     * @method resetFilters
     */
  resetFilters() {
    this.commonClass.resetAllFilters();
    jQuery(".checkboxGroupElem").removeClass('checkbox-clicked');
    jQuery("#unit-text").text("Select Unit");
    jQuery("#lesson-text").text("Select Lesson");
    jQuery("#unit-text,#lesson-text").removeClass("value-selected");
    jQuery(".lesson-filter-dropdown").addClass("disabled-dropdown");
    jQuery(".dDown_content").removeClass("unit-clicked");
    console.log("kkkkkkkkkkkk", this.filter["originalObject"])
    this.filteredData = this.filter["originalObject"].slice();
    this.setItemsCount(this.filteredData.length);
    this.setDefault();
  }


  /**
    * checkBoxEvent
    * @method setDataCheckBox
    */
  setDataCheckBox(ids: any, unitValue: string, lessonValue: string) {
    let filterChallenge: any = {};
    let filterReteach: any = {};
    let filterPractice: any = {};
    let filterRti: any = {};
    let filterGames: any = {};
    let filterInquiry: any = {};
    let filterReader: any = {};
    let filterFluency: any = {};
    let filterLessoncheck: any = {};
    let filterActivitycards: any = {};
    this.filteredData = {};
    let tempDataFilter: any = [];
    var results = jQuery.extend(true, {}, this.resultsData)

    for (var i = 0; i < ids.length; i++) {
      if (results["practice"][unitValue.split(" ").join("").trim()] != undefined && ids[i] == "practice" && jQuery.inArray("practice", this.disabledArray) == -1) {
        lessonValue == "Lessons All" ? tempDataFilter = results["practice"][unitValue.split(" ").join("").trim()].object : "";
        if (lessonValue != "Lessons All" && results["practice"][unitValue.split(" ").join("").trim()][lessonValue.split(" ").join("").trim()] != undefined) {
          tempDataFilter.push(results["practice"][unitValue.split(" ").join("").trim()][lessonValue.split(" ").join("").trim()][0])
          tempDataFilter.push(results["practice"][unitValue.split(" ").join("").trim()][lessonValue.split(" ").join("").trim()][1])
        }
        filterPractice = tempDataFilter.length != 0 ? tempDataFilter.slice() : {};

        if (tempDataFilter.length == 0 && results["reteach"][unitValue.split(" ").join("").trim()] != undefined && ids[i] == "reteach" && jQuery.inArray("reteach", this.disabledArray) == -1) {
          lessonValue == "Lessons All" ? tempDataFilter = results["reteach"][unitValue.split(" ").join("").trim()].object : "";
          if (lessonValue != "Lessons All" && results["reteach"][unitValue.split(" ").join("").trim()][lessonValue.split(" ").join("").trim()] != undefined) {
            tempDataFilter.push(results["reteach"][unitValue.split(" ").join("").trim()][lessonValue.split(" ").join("").trim()][0])
            tempDataFilter.push(results["reteach"][unitValue.split(" ").join("").trim()][lessonValue.split(" ").join("").trim()][1])
          }
          filterReteach = tempDataFilter.slice();
        }
        else if (tempDataFilter.length == 0 && results["challenge"][unitValue.split(" ").join("").trim()] != undefined && ids[i] == "challenge" && jQuery.inArray("challenge", this.disabledArray) == -1) {
          lessonValue == "Lessons All" ? tempDataFilter = results["challenge"][unitValue.split(" ").join("").trim()].object : "";
          if (lessonValue != "Lessons All" && results["challenge"][unitValue.split(" ").join("").trim()][lessonValue.split(" ").join("").trim()] != undefined) {
            tempDataFilter.push(results["challenge"][unitValue.split(" ").join("").trim()][lessonValue.split(" ").join("").trim()][0])
            tempDataFilter.push(results["challenge"][unitValue.split(" ").join("").trim()][lessonValue.split(" ").join("").trim()][1])
          }
          filterChallenge = tempDataFilter.slice();
        }
        else if (tempDataFilter.length == 0 && results["rti"][unitValue.split(" ").join("").trim()] != 0 && ids[i] == "rti" && jQuery.inArray("rti", this.disabledArray) == -1) {
          tempDataFilter = results["rti"][unitValue.split(" ").join("").trim()].object;
          filterRti = tempDataFilter.slice();
        }

        else if (tempDataFilter.length == 0 && ids[i] == "fluencybuilders" && jQuery.inArray("fluencybuilders", this.disabledArray) == -1) {
          tempDataFilter = results["fluency"];
          filterFluency = tempDataFilter.slice();
        }
      }
      else if (tempDataFilter.length == 0 && results["reteach"][unitValue.split(" ").join("").trim()] != undefined && ids[i] == "reteach" && jQuery.inArray("reteach", this.disabledArray) == -1) {
        lessonValue == "Lessons All" ? tempDataFilter = results["reteach"][unitValue.split(" ").join("").trim()].object : "";
        if (lessonValue != "Lessons All" && results["reteach"][unitValue.split(" ").join("").trim()][lessonValue.split(" ").join("").trim()] != undefined) {
          tempDataFilter.push(results["reteach"][unitValue.split(" ").join("").trim()][lessonValue.split(" ").join("").trim()][0])
          tempDataFilter.push(results["reteach"][unitValue.split(" ").join("").trim()][lessonValue.split(" ").join("").trim()][1])
        }
        filterReteach = tempDataFilter.length != 0 ? tempDataFilter.slice() : {};

        if (tempDataFilter.length == 0 && results["challenge"][unitValue.split(" ").join("").trim()] != undefined && ids[i] == "challenge" && jQuery.inArray("challenge", this.disabledArray) == -1) {
          lessonValue == "Lessons All" ? tempDataFilter = results["challenge"][unitValue.split(" ").join("").trim()].object : "";
          if (lessonValue != "Lessons All" && results["challenge"][unitValue.split(" ").join("").trim()][lessonValue.split(" ").join("").trim()] != undefined) {
            tempDataFilter.push(results["challenge"][unitValue.split(" ").join("").trim()][lessonValue.split(" ").join("").trim()][0])
            tempDataFilter.push(results["challenge"][unitValue.split(" ").join("").trim()][lessonValue.split(" ").join("").trim()][1])
          }
          filterChallenge = tempDataFilter.slice();
        }
        else if (tempDataFilter.length == 0 && results["rti"][unitValue.split(" ").join("").trim()] != 0 && ids[i] == "rti" && jQuery.inArray("rti", this.disabledArray) == -1) {
          tempDataFilter = results["rti"][unitValue.split(" ").join("").trim()].object;
          filterRti = tempDataFilter.slice();
        }
        else if (tempDataFilter.length == 0 && ids[i] == "fluencybuilders" && jQuery.inArray("fluencybuilders", this.disabledArray) == -1) {
          tempDataFilter = results["fluency"];
          filterFluency = tempDataFilter.slice();
        }
      }
      else if (tempDataFilter.length == 0 && results["challenge"][unitValue.split(" ").join("").trim()] != undefined && ids[i] == "challenge" && jQuery.inArray("challenge", this.disabledArray) == -1) {
        lessonValue == "Lessons All" ? tempDataFilter = results["challenge"][unitValue.split(" ").join("").trim()].object : "";
        if (lessonValue != "Lessons All" && results["challenge"][unitValue.split(" ").join("").trim()][lessonValue.split(" ").join("").trim()] != undefined) {
          tempDataFilter.push(results["challenge"][unitValue.split(" ").join("").trim()][lessonValue.split(" ").join("").trim()][0])
          tempDataFilter.push(results["challenge"][unitValue.split(" ").join("").trim()][lessonValue.split(" ").join("").trim()][1])
        }

        filterChallenge = tempDataFilter.length != 0 ? tempDataFilter.slice() : {};

        if (tempDataFilter.length == 0 && results["rti"][unitValue.split(" ").join("").trim()] != undefined && ids[i] == "rti" && jQuery.inArray("rti", this.disabledArray) == -1) {
          tempDataFilter = results["rti"][unitValue.split(" ").join("").trim()].object;
          filterRti = tempDataFilter.slice();
        }

        if (tempDataFilter.length == 0 && ids[i] == "fluencybuilders" && jQuery.inArray("fluencybuilders", this.disabledArray) == -1) {
          tempDataFilter = results["fluency"];
          filterFluency = tempDataFilter.slice();
        }
      }

      else if (tempDataFilter.length == 0 && results["rti"][unitValue.split(" ").join("").trim()] != undefined && ids[i] == "rti" && jQuery.inArray("rti", this.disabledArray) == -1) {
        tempDataFilter = results["rti"][unitValue.split(" ").join("").trim()].object;
        filterRti = tempDataFilter.slice();
      }
      else if (tempDataFilter.length == 0 && results["fluency"] && ids[i] == "fluencybuilders" && jQuery.inArray("fluencybuilders", this.disabledArray) == -1) {
        tempDataFilter = results["fluency"]
        filterFluency = tempDataFilter.slice();
      }
      else if (tempDataFilter.length == 0 && results["games"] && ids[i] == "games" && jQuery.inArray("games", this.disabledArray) == -1) {
        tempDataFilter = results["games"]
        filterGames = tempDataFilter.slice();
      }
      else if (tempDataFilter.length == 0 && results["lessoncheck"][unitValue.split(" ").join("").trim()] != undefined && ids[i] == "lessonchecks" && jQuery.inArray("lessonchecks", this.disabledArray) == -1) {
        lessonValue == "Lessons All" ? tempDataFilter = results["lessoncheck"][unitValue.split(" ").join("").trim()].object : "";
        if (lessonValue != "Lessons All" && results["lessoncheck"][unitValue.split(" ").join("").trim()][lessonValue.split(" ").join("").trim()] != undefined) {
          tempDataFilter.push(results["lessoncheck"][unitValue.split(" ").join("").trim()][lessonValue.split(" ").join("").trim()][0])
        }

        filterLessoncheck = tempDataFilter.length != 0 ? tempDataFilter.slice() : {};
      }

      else if (tempDataFilter.length == 0 && results["activitycards"][unitValue.split(" ").join("").trim()] != undefined && ids[i] == "activitycards" && jQuery.inArray("activitycards", this.disabledArray) == -1) {
        tempDataFilter = results["activitycards"][unitValue.split(" ").join("").trim()].object;
        filterActivitycards = tempDataFilter.slice();
      }

      else if (tempDataFilter.length == 0 && results["mathreader"][unitValue.split(" ").join("").trim()] != undefined && ids[i] == "mathreader" && jQuery.inArray("mathreader", this.disabledArray) == -1) {
        tempDataFilter = results["mathreader"][unitValue.split(" ").join("").trim()].object;
        filterReader = tempDataFilter.slice();
      }
      else if (tempDataFilter.length == 0 && results["inquirycards"][unitValue.split(" ").join("").trim()] != undefined && ids[i] == "inquirybasedtasks" && jQuery.inArray("inquirybasedtasks", this.disabledArray) == -1) {
        tempDataFilter = results["inquirycards"][unitValue.split(" ").join("").trim()].object;
        filterInquiry = tempDataFilter.slice();
      }
      if (filterPractice.length != undefined && results["reteach"][unitValue.split(" ").join("").trim()] != undefined && ids[i] == "reteach" && jQuery.inArray("reteach", this.disabledArray) == -1) {
        lessonValue == "Lessons All" ? tempDataFilter.push.apply(tempDataFilter, results["reteach"][unitValue.split(" ").join("").trim()].object) : "";
        if (lessonValue != "Lessons All" && results["reteach"][unitValue.split(" ").join("").trim()][lessonValue.split(" ").join("").trim()] != undefined) {
          tempDataFilter.push(results["reteach"][unitValue.split(" ").join("").trim()][lessonValue.split(" ").join("").trim()][0])
          tempDataFilter.push(results["reteach"][unitValue.split(" ").join("").trim()][lessonValue.split(" ").join("").trim()][1])
        }
      }

      if ((filterPractice.length != undefined || filterReteach.length != undefined) && results["challenge"][unitValue.split(" ").join("").trim()] != undefined && ids[i] == "challenge" && jQuery.inArray("challenge", this.disabledArray) == -1) {
        lessonValue == "Lessons All" ? tempDataFilter.push.apply(tempDataFilter, results["challenge"][unitValue.split(" ").join("").trim()].object) : "";
        if (lessonValue != "Lessons All" && results["challenge"][unitValue.split(" ").join("").trim()][lessonValue.split(" ").join("").trim()] != undefined) {
          tempDataFilter.push(results["challenge"][unitValue.split(" ").join("").trim()][lessonValue.split(" ").join("").trim()][0])
          tempDataFilter.push(results["challenge"][unitValue.split(" ").join("").trim()][lessonValue.split(" ").join("").trim()][1])
        }
      }


      (filterPractice.length != undefined || filterReteach.length != undefined || filterChallenge.length != undefined) && ids[i] == "rti" && jQuery.inArray("rti", this.disabledArray) == -1 ? tempDataFilter.push.apply(tempDataFilter, results["rti"][unitValue.split(" ").join("").trim()].object) : "";

      (filterPractice.length != undefined || filterReteach.length != undefined || filterChallenge.length != undefined || filterRti.length != undefined) && ids[i] == "fluencybuilders" && jQuery.inArray("fluencybuilders", this.disabledArray) == -1 ? tempDataFilter.push.apply(tempDataFilter, results["fluency"]) : "";
      (filterPractice.length != undefined || filterReteach.length != undefined || filterChallenge.length != undefined || filterRti.length != undefined || filterFluency.length != undefined) && ids[i] == "games" && jQuery.inArray("fluencybuilders", this.disabledArray) == -1 ? tempDataFilter.push.apply(tempDataFilter, results["games"]) : "";



      if ((filterPractice.length != undefined || filterReteach.length != undefined || filterChallenge.length != undefined || filterRti.length != undefined || filterFluency.length != undefined || filterGames.length != undefined) && ids[i] == "lessonchecks" && jQuery.inArray("lessonchecks", this.disabledArray) == -1) {
        lessonValue == "Lessons All" ? tempDataFilter.push.apply(tempDataFilter, results["lessoncheck"][unitValue.split(" ").join("").trim()].object) : "";
        if (lessonValue != "Lessons All" && results["lessoncheck"][unitValue.split(" ").join("").trim()][lessonValue.split(" ").join("").trim()] != undefined) {
          tempDataFilter.push(results["lessoncheck"][unitValue.split(" ").join("").trim()][lessonValue.split(" ").join("").trim()][0])
        }
      }

      (filterPractice.length != undefined || filterReteach.length != undefined || filterChallenge.length != undefined || filterRti.length != undefined || filterFluency.length != undefined || filterGames.length != undefined || filterLessoncheck.length != undefined) && ids[i] == "activitycards" && jQuery.inArray("activitycards", this.disabledArray) == -1 ? tempDataFilter.push.apply(tempDataFilter, results["activitycards"][unitValue.split(" ").join("").trim()].object) : "";


      (filterPractice.length != undefined || filterReteach.length != undefined || filterChallenge.length != undefined || filterRti.length != undefined || filterFluency.length != undefined || filterGames.length != undefined || filterLessoncheck.length != undefined) && ids[i] == "mathreader" && jQuery.inArray("mathreader", this.disabledArray) == -1 ? tempDataFilter.push.apply(tempDataFilter, results["mathreader"][unitValue.split(" ").join("").trim()].object) : "";

      (filterPractice.length != undefined || filterReteach.length != undefined || filterChallenge.length != undefined || filterRti.length != undefined || filterFluency.length != undefined || filterGames.length != undefined || filterLessoncheck.length != undefined || filterReader.length != undefined) && ids[i] == "inquirybasedtasks" && jQuery.inArray("inquirybasedtasks", this.disabledArray) == -1 ? tempDataFilter.push.apply(tempDataFilter, results["inquirycards"][unitValue.split(" ").join("").trim()].object) : "";

      this.filteredData = tempDataFilter.slice();
    }

    this.setItemsCount(this.filteredData.length);
    this.pageCount = Math.floor(this.filteredData.length / 20);
    this.filteredData.length / 20 != this.pageCount ? this.pageCount = this.pageCount + 1 : "";
    this.totalCount = this.pageCount > 1 ? this.filteredData.length > 20 ? 20 : this.filteredData.length : this.filteredData.length;
    this.currentPageId = 0;
    tempDataFilter.length = 0;
  }



  /**
    * checkBoxEvent
    * @method checkBoxEvent
    */

  checkBoxEvent(event: Event) {
    this.commonClass.checkBoxEnableDisableEvent(jQuery(event.currentTarget).attr("id"));
    let tempArray: any = {};
    var self = this;
    var count = 0;
    let checkBoxTempArray: any = [];
    this.checkBoxTempArrays = [];

    jQuery(".checkbox-clicked").each(function () {
      let id = jQuery(this).attr("id");
      if (jQuery("#unit-text").text() == "Select Unit") {
        if (count == 0) {
          tempArray = self.filter[id].slice();
        } else {
          tempArray.push.apply(tempArray, self.filter[id].slice());
        }
        checkBoxTempArray[count] = id;
      } else {

        checkBoxTempArray[count] = id;
      }
      count++;
    });
    this.checkBoxTempArrays = checkBoxTempArray.slice();

    if (jQuery("#unit-text").text() != "Select Unit" && jQuery(".checkboxGroupElem").filter('.checkbox-clicked').length >= 1) {
      this.setDataCheckBox(checkBoxTempArray, jQuery("#unit-text").text(), jQuery("#lesson-text").text())
    } else if (jQuery("#unit-text").text() == "Select Unit" && jQuery(".checkboxGroupElem").filter('.checkbox-clicked').length >= 1) {
      this.filteredData = {};
      this.filteredData = tempArray;
    } else if (jQuery("#unit-text").text() == "Select Unit" && jQuery(".checkboxGroupElem").filter('.checkbox-clicked').length == 0) {
      this.filteredData = {};
      this.filteredData = this.filter["originalObject"].slice();;
    }
    else {
      this.setData(jQuery("#unit-text").text(), jQuery("#lesson-text").text());

    }
    this.setItemsCount(this.filteredData.length);
    this.setDefault();

  }


  /**
    * getUnit
    * @method getLesson
    */

  getUnit(id: any) {
    let pattunittReg = /U\d+/;
    let pattdigitReg = /\d+/;
    let result: any;
    let unitNo = pattunittReg.exec(id) != null ? pattdigitReg.exec(pattunittReg.exec(id)[0])[0] : "";
    if (unitNo != null && unitNo.match("^0")) {
      unitNo = unitNo.slice(1);
      result = "Unit " + unitNo;
    } else {
      result = "Unit " + unitNo;
    }
    return result;
  }


  /**
    * getLesson
    * @method getLesson
    */
  getLesson(id: any) {
    let pattlessonReg = /L\d+/;
    let pattdigitReg = /\d+/;
    let result: any;
    let lessonNo = pattlessonReg.exec(id) != null ? pattdigitReg.exec(pattlessonReg.exec(id)[0])[0] : "";
    if (lessonNo != null && lessonNo.match("^0")) {
      lessonNo = lessonNo.slice(1);
      result = "Lesson " + lessonNo;
    } else {
      result = "Lesson "
    }
    return "Lesson " + lessonNo;
  }


  /**
 * change page event
 * @method changePage
 */
  changePage(event: Event, operation: String) {
    event.stopPropagation();
    this.currentPageId = parseInt(jQuery(".goresult-wide-tile").not(".display-none").attr("class").split("Page")[1]);
    jQuery(".prev,.next").removeClass("disabled");
    if (operation == "next") {
      jQuery(".next").hasClass("disabled") == false ? this.currentPageId++ : "";
      this.currentPageId == this.pageCount - 1 ? jQuery(".next").addClass("disabled") : "";
      this.startCount = this.totalCount + 1
    }
    else {
      this.currentPageId == 1 ? jQuery(".prev").addClass("disabled") : "";
      jQuery(".prev").hasClass("disabled") == false ? this.currentPageId-- : this.currentPageId = 0;
      this.totalCount = this.startCount - 1
    }
    jQuery(".goresult-wide-tile").not(".display-none").addClass("display-none");
    jQuery(".Page" + this.currentPageId).removeClass("display-none");
    if (operation == "next") {
      this.totalCount = this.totalCount + jQuery(".wide-tile-games-container").children().not(".display-none").length;
    } else {
      this.startCount = this.totalCount + 1 - jQuery(".wide-tile-games-container").children().not(".display-none").length
    }
  }

  /**
 * set default items
 * @method setDefault
 */

  setDefault() {
    this.pageCount = 1;
    this.pageCount = Math.floor(this.filteredData.length / 20);
    this.filteredData.length / 20 != this.pageCount ? this.pageCount = this.pageCount + 1 : "";
    this.startCount = 1;
    this.currentPageId = 0;
    this.totalCount = this.pageCount > 1 ? this.filteredData.length > 20 ? 20 : this.filteredData.length : this.filteredData.length;
    jQuery(".prev").addClass("disabled");
    //   jQuery(".wide-tile-games-container").children().addClass("display-none");
    jQuery(".Page0").removeClass("display-none");
    if (this.filteredData.length == 0) {
      jQuery(".results-not-found").removeClass("display-none");
      this.startCount = 0;
    } else {
      jQuery(".results-not-found").addClass("display-none");
    }
  }


  /**
   * set no of items
   * @method setItemsCount
   *  * @param {itemLength} indicates number of items
   */

  setItemsCount(itemLength: number) {
    this.items = itemLength;
    this.items == 0 ? jQuery(".results-not-found").removeClass("display-none") : jQuery(".results-not-found").addClass("display-none");
    this.pageCount = Math.floor(this.filteredData.length / 20);
    this.filteredData.length / 20 != this.pageCount ? this.pageCount = this.pageCount + 1 : "";
    this.totalCount = this.pageCount > 1 ? this.filteredData.length > 20 ? 20 : this.filteredData.length : this.filteredData.length;
    this.filteredData.length <= 20 ? jQuery(".next").addClass("disabled") : jQuery(".next").removeClass("disabled");
    this.pageCount == 1 || this.currentPageId == 0 ? jQuery(".prev").addClass("disabled") : jQuery(".prev").removeClass("disabled");
  }
}


