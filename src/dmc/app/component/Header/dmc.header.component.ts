import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { ROUTER_DIRECTIVES } from '@angular/router';

declare var jQuery: any;
@Component({
  moduleId: module.id,
  selector: 'header-component',
  templateUrl: './dmc.header.component.html',
  directives: [ROUTER_DIRECTIVES],

})


export class DmcHeaderComponent {
  /**
  * variable to store pageTitle value
  */
  @Input()
  pageTitle: string;
  /**
* variable to store background value
*/
  @Input()
  background: string;
  /**
* variable to store unit title value
*/
  @Input()
  unitTitle: string;
  /**
* variable to store lesson title value
*/
  @Input()
  lessonTitle: string;

  /**
* variable to store path value
*/
  path: string;
  /**
* event emitter that handles event place item 
*/

  @Output() placeItem: EventEmitter<any> = new EventEmitter();
  /**
* event emitter that handles unit item 
*/

  @Output() unitItem: EventEmitter<any> = new EventEmitter();
  /**
* event emitter that handles more info
*/

  @Output() showMoreInfo: EventEmitter<any> = new EventEmitter();

  /**
* variable to store param id value
*/
  paramId: any;
  /**
  * Variable to store user value
  */
  user: string;
  /**
  * Variable to store math activity center value
  */
  math_activity_center: string;
  /**
* Variable to store more info text
*/
  more_info_text: string;


/**
   * after view initialisation
   * @method ngAfterViewInit
   */
  ngAfterViewInit() {
    this.user = document.getElementById("dmc-main-container").getAttribute("name");
    if (this.user == "student") {
      jQuery(".topnav-hr-rule").addClass("display-none");
      jQuery(".topnav-row").addClass("student-toprow")
      jQuery(".listing-head").addClass("student-heading-main");
    } else {
      jQuery(".topnav-row").removeAttr("style");
    }

  }
  
  /**
   * constructor
   */
  constructor(private router: Router) {
    let headerData = jQuery('body').data();
    this.math_activity_center = headerData[12].math_activity_center;
    this.more_info_text = headerData[12].more_info_text;
    Object(document).self = this;
    Object(window).self = this;
    this.path = document.getElementById("dmc-main-container").getAttribute("path");
  }
  
  
  /**
   * title click event
   * @method ngAfterViewInit
   */
  titleClick(event: Event) {
    this.placeItem.emit({});
  }
  
    
   /**
   * click on unit tile
   * @method unitClick
   * @param {event} eventype
   */
  
  unitClick(event: Event) {
    this.unitItem.emit({ value: [this.unitTitle] });
  }
  
  
   /**
   * click on more info
   * @method moreInfoClick
   * @param {event} eventype
   */
  moreInfoClick(event: Event) {
    this.showMoreInfo.emit({});
  }
}


