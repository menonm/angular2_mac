import { TestComponentBuilder } from '@angular/compiler/testing';
import { DmcFooterComponent } from '../Footer/dmc.footer.component';
import { DmcService} from '../../shared/dmc.service';
import { ROUTER_DIRECTIVES, Router, ActivatedRoute, UrlPathWithParams, RouterLink  } from '@angular/router';
import {Http, Response, HTTP_PROVIDERS} from '@angular/http';

import {
    beforeEachProviders,
    async,
    describe,
    expect,
    inject,
    it
} from '@angular/core/testing';
import {DmcPlanninGuideComponent} from './dmc.planninguide.component';


export function main() {
    

    
    let data=[null,{"Center_Planning_Guide":"unit1"},,null,null,null,null,null,null,null,null,null,{"items_text":"unit1","unit_text":"unit1"},null,null,null,null,]
    
    let textValue = "unit 1";
    let desc = "standardsdesc";
    let url = "post url"
    let unit = "unit1";
    let length = 10;
    let lesson="Lesson1"

    let dmcplanninguide = new DmcPlanninGuideComponent(null, null);
    describe('dmcplanninguideComponent', () => {
        it('assign json value', () => {
            const result:any = dmcplanninguide.assignJson(data);
                expect(result[0]).toEqual("unit1");
            
        })
    });
    


   





    describe('setItemsCount', () => {
        it('set items count', () => {
            const result = dmcplanninguide.setItemsCount(length);
            expect(result).toEqual(10);
        })
    });
    
    
    


 describe('changePage', () => {
        it('click changePage', () => {
            const result = dmcplanninguide.changePage(null,"next");
            expect(result).toEqual("next");
        })
    });



  

    

    
    
    

    
  



}
