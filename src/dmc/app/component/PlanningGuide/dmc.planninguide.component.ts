import { Component, Pipe } from '@angular/core';
import { DmcService } from '../../shared/dmc.service';
import { ROUTER_DIRECTIVES } from '@angular/router';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { DmcFooterComponent } from '../Footer/dmc.footer.component';
import { DmcHeaderComponent } from '../Header/dmc.header.component';
import {TruncatePipe} from '../../shared/truncatepipe';
import { DmcFilterComponent } from '../../shared/mxdmcfilter.common';

declare var jQuery: any;
@Component({
  moduleId: module.id,
  selector: 'planningguide',
  templateUrl: './dmc.planninguide.component.html',
  pipes: [TruncatePipe],
  directives: [ROUTER_DIRECTIVES, DmcFooterComponent, DmcHeaderComponent],
  providers: [DmcService],
})




export class DmcPlanninGuideComponent {
  /**
 * Variable to store user value
 */
  user: string;
  /**
 * Variable to store teacher library items 
 */
  displayContent: any = [];
  /**
* Variable to store url value
*/
  url: any;
  /**
 * Variable to store no of items in planning guide
 */
  itemsPlanning: number;
  /**
* Variable to event type 
*/
  eventType: any;
  /**
 * Object to store current page title
 */
  filter: any = {};
  /**
  * variable to store title
  */
  title: string = "Math Activity Center Planning Guide";
  /**
  * variable to store background color
  */
  backgroundColor: string = "#08A1B3";
  /**
	 * instance of filter component file 
	 */
  commonClass: DmcFilterComponent.filter;
  /**
  * variable to store json  value
  */
  planningData: any;
  /**
  * variable to store math value
  */
  Math: any;
  /**
	 * variable to store current page count
	 */
  pageCount: number = 1;
  /**
  * variable to store total page count
  */
  totalCount: number;
  /**
	 * variable to store  page start count
	 */
  startCount: number = 1;
  /**
  * variable to store current page id
  */
  currentPageId: number = 0;
  /**
  * variable to store truncateValue value
  */
  truncateValue: number;
  /**
	 * variable to store moreinfo value
	 */
  moreInfo: string;

  /**
   * get dmc service and routing service
   * @constructor 
   * @param {_dmcService} get _dmcService
   * @param {router} get routing service
   */
  constructor(public _dmcService: DmcService, private router: Router) {
    Object(document).self = this;
    Object(window).self = this;
    this.commonClass = new DmcFilterComponent.filter();
    this.Math = Math;
    this.user = this.commonClass.getUser();
    this.eventType = this.commonClass.getEventType();
    this.url = Object(router).currentRouterState.snapshot.url;
    this.planningData = jQuery('body').data();
    this.truncateValue = jQuery(window).width() <= 1024 ? jQuery(window).width() <= 961 ? 40 : 50 : 65;
    this.assignJson([this.planningData[9], this.planningData[12]])
  }

  /**
    * ngAfterViewInit
    */
  ngAfterViewInit() {
    this.commonClass.enableDisableContainer(this.user);
    this.user == "student" ? jQuery(".main-container").addClass("student-main") : jQuery(".main-container").addClass("teacher-main");
    let addedLength: any = parseInt(this.filter["planningguideleft"].length) + parseInt(this.filter["planningguideright"].length);
    addedLength < 20 ? jQuery(".next").addClass("disabled") : jQuery(".next").removeClass("disabled");

  }

  /**
   * assign json data to an object
   * @method assignJson
   * @param {data} data from json file
   */
  assignJson(data: any) {
    let itemLength = data[0].teacher[0].libraryItems.length;
    let remainingItems = itemLength - Math.round(itemLength / 2);
    this.filter["planningguide"] = data[0].teacher[0].libraryItems;
    this.filter["planningguideleft"] = data[0].teacher[0].libraryItems.slice(0, Math.round(itemLength / 2));
    this.filter["planningguideright"] = this.filter["planningguide"].slice(-remainingItems);
    let addedLength: any = parseInt(this.filter["planningguideleft"].length) + parseInt(this.filter["planningguideright"].length);
    this.setItemsCount(addedLength);
    this.pageCount = Math.floor(addedLength / 20);
    addedLength / 20 != this.pageCount ? this.pageCount = this.pageCount + 1 : "";
    this.totalCount = this.pageCount > 1 ? addedLength > 20 ? 20 : addedLength : addedLength;
    this.currentPageId = 0;
    this.moreInfo = data[1].Center_Planning_Guide;
  }

  /**
 * assign json data to an object
 * @method dropdownArrowClick
 * @param {event} event from event
 */

  dropdownArrowClick(event: Event) {
    this.commonClass.drpDownArrowClick(jQuery(event.currentTarget).attr("id"));
  }

  /**
   * click on tile
   * @method tilesClickEvent
   * @param {routekey} routekey to navigate
   */
  tileClickEvent(event: Event, url: string) {
    var iOS = !!navigator.platform && /iPad/.test(navigator.platform);
    iOS == true ? url = url.replace('=', '') : '';
    this.commonClass.openInNewTab(url, this.user, '');
  }

  /**
   * click on tile
   * @method setItemsCount
   * @param {itemLength} itemLength  no of items
   */

  setItemsCount(itemLength: number) {
    this.itemsPlanning = itemLength;
    let addedLength: any = parseInt(this.filter["planningguideleft"].length) + parseInt(this.filter["planningguideright"].length);
    this.pageCount = Math.floor(addedLength / 20);
    addedLength != this.pageCount ? this.pageCount = this.pageCount + 1 : "";
    this.totalCount = this.pageCount > 1 ? addedLength > 20 ? 20 : addedLength : addedLength;
    addedLength < 20 ? jQuery(".next").addClass("disabled") : jQuery(".next").removeClass("disabled");
    this.pageCount == 1 || this.currentPageId == 0 ? jQuery(".prev").addClass("disabled") : jQuery(".prev").removeClass("disabled");
  }


  /**
 * set default values
 * @method setDefault
 */

  setDefault() {
    this.pageCount = 1;
    let addedLength: any = parseInt(this.filter["planningguideleft"].length) + parseInt(this.filter["planningguideright"].length);
    this.pageCount = Math.floor(addedLength / 20);
    addedLength / 20 != this.pageCount ? this.pageCount = this.pageCount + 1 : "";
    this.startCount = 1;
    this.currentPageId = 0;
    this.totalCount = this.pageCount > 1 ? addedLength > 20 ? 20 : addedLength : addedLength;
    jQuery(".prev").addClass("disabled");
    jQuery(".planning-guide-left-container").children().addClass("display-none");
    jQuery(".planning-guide-right-container").children().addClass("display-none");
    jQuery(".Page0").removeClass("display-none");
  }

  /**
 * pagination part
 * @method changePage
* @param {event} event type  
* @param {operation} operation type  
 * 
 */
  changePage(event: Event, operation: String) {
    event.stopPropagation();
    this.currentPageId = jQuery(".wide-tile-planning").not(".display-none").attr("class").split("Page")[1];
    jQuery(".prev,.next").removeClass("disabled");
    if (operation == "next") {
      jQuery(".next").hasClass("disabled") == false ? this.currentPageId++ : "";
      this.currentPageId == this.pageCount - 1 ? jQuery(".next").addClass("disabled") : "";
      this.startCount = this.totalCount + 1
    }
    else {
      this.currentPageId == 1 ? jQuery(".prev").addClass("disabled") : "";
      jQuery(".prev").hasClass("disabled") == false ? this.currentPageId-- : this.currentPageId = 0;
      this.totalCount = this.startCount - 1
    }
    jQuery(".wide-tile-planning").not(".display-none").addClass("display-none");
    jQuery(".Page" + this.currentPageId).removeClass("display-none");

    if (operation == "next") {
      this.totalCount = this.totalCount + parseInt(jQuery(".wide-tile-planning").not(".display-none").length);
    } else {
      this.startCount = this.totalCount + 1 - parseInt(jQuery(".wide-tile-planning").not(".display-none").length);
    }
  }

}