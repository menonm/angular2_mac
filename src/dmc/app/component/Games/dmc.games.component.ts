import { Component } from '@angular/core';
import { DmcService } from '../../shared/dmc.service';
import { ROUTER_DIRECTIVES } from '@angular/router';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { DmcFooterComponent } from '../Footer/dmc.footer.component';
import { DmcHeaderComponent } from '../Header/dmc.header.component';
import {TruncatePipe} from '../../shared/truncatepipe';
import { DmcFilterComponent } from '../../shared/mxdmcfilter.common';

import { DPOService } from '../../shared/dpo.service';
import { DmcPopupComponent } from '../Popup/dmc.popup.component';
import { DmcWideTileComponent } from '../WideTile/dmc.widetile.component';
import { DmcFilterByComponent } from '../FilterBy/dmc.filterby.component';

declare var jQuery: any;
declare var PerfectScrollbar: any;
@Component({
  moduleId: module.id,
  selector: 'games',
  templateUrl: './dmc.games.component.html',
  pipes: [TruncatePipe],
  directives: [ROUTER_DIRECTIVES, DmcFooterComponent, DmcHeaderComponent, DmcPopupComponent, DmcWideTileComponent, DmcFilterByComponent],
  providers: [DmcService],
})

export class DmcGamesComponent {
  /**
    * Object to store current page title
    */
  filter: any = {};
  /**
  * Variable to store url value
  */
  url: any;
  /**
  * Variable to store user value
  */
  user: string;
  /**
  * Variable to event type 
  */
  eventType: any;
  /**
  * Array to store all units 
  */
  title: string;
  /**
	 * Array to store thumbnail images
	 */
  thumbnailsImages: any = [];
  /**
 * Variable to path url
 */
  path: string;
  /**
  * instance of filter component file 
  */
  commonClass: DmcFilterComponent.filter;
  /**
  * variable to store current page count
  */
  pageCount: number = 1;
  /**
	 * variable to store  page start count
	 */
  startCount: number = 1;
  /**
  * variable to store total page count
  */
  totalCount: number;
  /**
	 * variable to store current page id
	 */
  currentPageId: number = 0;
  /**
  * variable to store math value
  */
  Math: any;
  /**
	 * variable to store no of games
	 */
  itemsGames: number;
  /**
	 * variable to store all games value
	 */
  games: any = [];
  /**
	 * variable to store all games boards value
	 */
  gameboards: any = [];
  /**
  * variable to store all gamesOriginal value
  */
  gamesOriginal: any = [];
  /**
  * variable to store all games value
  */
  gamesData: any;
  /**
  * variable to store truncateValue value
  */
  truncateValue: number;
  /**
  * variable to store games more info text
  */
  gamesMoreInfo: string;
  /**
	 * variable to store games user guide pdf url
	 */
  userGuidePdfUrl: string;
  /**
	 * variable to store games boards more info
	 */
  gameBoardsMoreInfo: string;
  /**
  * variable to store more info games title
  */
  moreInfoGamesTitle: string = "Interactive Games";
  /**
  * variable to store more info games boards title
  */
  moreInfoGameBoardsTitle: string = "Game Boards";

  /**
   * Added for color configurable
  */
  /**
  * variable to store commonValues
  */
  commonValues: any = {};
  /**
	 * variable to store tile  color
	 */
  tileColor: string;
  /**
  * variable to store on tile border color
  */
  tileBorderColor: string;
  /**
 * variable to store common value
 */
  landingJson: any = {};
  /**
  * variable to store tile  color
  */
  tile_Color: string;
  /**
  * variable to store tile  color
  */
  landingTileColor: string;
  /**
  * variable to store currentRouteKey
  */
  currentRouteKey: string;
  /**
  * variable to store tilecopy
  */
  titleCopy: string;
  /**
  * variable to store background color
  */
  backgroundColor: string;
  /**
  * variable to store blackline master color
  */
  blackLineColor: string;
  /* added for correlation chart */

  /**
	 * variable to store corelation items
	 */
  correleationItems: any = [];
  /**
	 * variable to store chartItemsData
	 */
  chartItemsData: any = {};
  /**
	 * variable to store levels array
	 */
  levelsArray: any = [];
  /**
	 * variable to store idsArray
	 */
  idsArray: any = [];
  /**
  * variable to store titlesArray
  */
  titlesArray: any = [];
  /**
  * variable to store mainLessonsArray
  */
  mainLessonsArray: any = [];
  /**
	 * variable to store items text
	 */
  items_text: string;
  /**
* variable to store edition text
*/
  edition_text: string;
  /**
	 * variable to store games text
	 */
  games_text: string;
  /**
	 * variable to store games boards text
	 */
  gameboards_text: string;
  /**
	 * variable to store lesson corelation text
	 */
  lesson_corelation_text: string;
  /**
     * variable to user guide text 
     */
  user_guide_text: string;
  /**
     * variable to corelation chart text
     */
  corelation_chart_text: string;
  /**
     * variable to close text 
     */
  close_text: string;
  /**
     * variable to print text 
     */
  print_text: string;
  /**
     * variable to games user guide url text 
     */
  games_userguide_url: string;
  /**
     * variable to additionandsubtraction_text
     */
  additionandsubtraction_text: string;
  /**
     * variable to lesson text
     */
  lesson_text: string;
  /**
     * variable to games title text 
     */
  game_title: string;
  /**
     * variable to level  text 
     */
  level_text: string;
  /**
     * variable to lesson title  text 
     */
  lesson_title_text: string;
  /**
     * variable to type text 
     */
  type_text: string;
  /**
     * variable to game array 
     */
  gameArray: any = { "POG": "Addition and Subtraction", "SG": "Number Relationships", "FM": "Operations", "CG": "The Real Numbers" }
  /**
   * get dmc service and routing service
   * @constructor 
   * @param {_dmcService} get _dmcService
   * @param {router} get routing service
   */
  constructor(public _dmcService: DmcService, private router: Router, private route: ActivatedRoute) {
    this.path = document.getElementById("dmc-main-container").getAttribute("path");
    this.commonClass = new DmcFilterComponent.filter();
    this.user = this.commonClass.getUser();
    this.eventType = this.commonClass.getEventType();
    Object(document).self = this;
    Object(window).self = this;
    this.Math = Math;
    this.url = Object(router).currentRouterState.snapshot.url;
    this.currentRouteKey = Object(route).url._value[0].path;
    this.gamesData = jQuery('body').data();
    this.truncateValue = jQuery(window).width() <= 1024 ? jQuery(window).width() <= 961 ? 63 : 80 : 120;
    this.assignJson(this.gamesData);
  }



  /**
 * @method ngAfterViewInit
 * 
 */
  ngAfterViewInit() {
    this.user == "student" ? jQuery(".main-container").addClass("student-main") : jQuery(".main-container").addClass("teacher-main");
    this.user == "student" ? jQuery(".student-container").removeClass("display-none") : jQuery(".teacher-container").removeClass("display-none");

    DPOService.gObj.dpoGlbService.initDPO({
      ready: function (obj: any) {

        // CheckBox DropDown checkbox-clicked 
        jQuery(".checkbox-clicked").each(function () {
          jQuery(this).removeClass('checkbox-clicked');
          jQuery(this).trigger('click')
        });
      }
    });



    if (jQuery.inArray("POG", this.idsArray) != -1) {
      jQuery(".game-title-col,.game-title-div").addClass("display-none");
      jQuery(".level-title-col").addClass("lowergrade-level-title-col");
      jQuery(".game-level-div,.lesson-title-div,.ids,.lessonrow").addClass("lowergrade-levels");
      jQuery(".level-col").addClass("lowergrade-level-col");
      jQuery(".titles").addClass("lowergrade-titles");
      jQuery(".lesson-title").addClass("lowergrade-lesson-title");
      jQuery(".table-headings").addClass("lowerlevels-headings");
      jQuery(".row-wrapper").addClass("lowergrade-row-wrapper");
      jQuery(".game-title").removeClass("display-none");
    }

    this.gamesMoreInfo != undefined && this.gamesMoreInfo != "" ? jQuery(".more-info-wrapper").removeClass("display-none") : "";

  }




  /**
   * close drop down click on document click
   * @method documentBodyClick
   * @param {event}  click event
   */
  documentBodyClick(event: Event) {
    if (jQuery(event.srcElement)[0].className != "dropdown-text" && jQuery(event.srcElement)[0].className != "dropbtn dropdown-clicked" && jQuery(event.srcElement)[0].className != "filter-Text" && jQuery(event.srcElement)[0].className != "dropdown-arrow dropdown-down-arrow") {
      if (jQuery(".dropbtn").hasClass("dropdown-clicked") == true) {
        jQuery(".dropdown-content").removeClass("show");
        jQuery(".dropbtn").addClass("hover").removeClass("dropdown-clicked");
      }
      Object(document).removeEventListener(Object(event.currentTarget).self.eventType, Object(event.currentTarget).self.documentBodyClick, false);
    }
  }
  /**
    * assign json data to an object
    * @method assignJson
    * @param {data} data from json file
    */
  assignJson(data: any) {
    this.commonValues = this.user == "student" ? data[17].student : data[17].teacher;
    this.landingJson = data[17];
    this.userGuidePdfUrl = data[12].games_user_guide_url;
    this.items_text = data[12].items_text;
    this.edition_text = data[12].edition_text;
    this.games_text = data[12].games_text;
    this.gameboards_text = data[12].gameboards_text;
    this.lesson_corelation_text = data[12].lesson_corelation_text;
    this.user_guide_text = data[12].user_guide_text;
    this.type_text = data[12].type_text;
    this.corelation_chart_text = data[12].corelation_chart_text;
    this.close_text = data[12].close_text;
    this.print_text = data[12].print_text;
    this.additionandsubtraction_text = data[12].additionandsubtraction_text;
    this.lesson_text = data[12].lesson_text;
    this.games_userguide_url = data[12].games_user_guide_url;
    this.game_title = data[12].game_title;
    this.level_text = data[12].level_text;
    this.lesson_title_text = data[12].lesson_title_text;

    for (var i = 0; i < this.commonValues.length; i++) {
      if (this.commonValues[i].routeKey == this.currentRouteKey) {
        this.tileColor = this.commonValues[i].color;
        this.landingTileColor = this.commonValues[i].color;
        this.blackLineColor = this.commonValues[i].black_line_color;
        this.title = this.commonValues[i].title;
        this.titleCopy = this.title;
        this.tile_Color = this.commonValues[i].tile_color;
        this.backgroundColor = this.commonValues[i].header_background_color;
        if (this.commonValues[i].moreInfo) {
          this.gamesMoreInfo = this.commonValues[i].moreInfo.split("<gameboards>")[0];
          this.gameBoardsMoreInfo = this.commonValues[i].moreInfo.split("<gameboards>")[1];
        }

      }
    }
    if (data != undefined) {
      jQuery(".loading-div").addClass("display-none");
    }
    if (this.user == "student") {
      this.games = data[5].student.slice();

    } else {
      this.games = data[5].teacher[0].libraryItems.slice();
      this.gamesOriginal = data[5].teacher[0].libraryItems.slice();
      this.gameboards = data[13].teacher[0].libraryItems;
      jQuery.merge(this.games, this.gameboards);
      this.correleationItems = data[16].levelData;
    }

    this.filter["games"] = this.games.slice();
    this.filter["gamesOriginal"] = this.games.slice();
    this.thumbnailsImages = data[14].thumbanailsimages;
    this.setItemsCount(this.filter["games"].length);

    let chartItems = this.correleationItems.slice();
    for (var i = 0; i < chartItems.length; i++) {
      let lessonsString = chartItems[i].lessons.slice();
      if (lessonsString != undefined) {
        var myarray = lessonsString.split(', ');
        for (var f = 0; f < myarray.length; f++) {
          if (jQuery.inArray(myarray[f], this.mainLessonsArray) == -1) {
            this.mainLessonsArray.push(myarray[f]);
            this.mainLessonsArray.sort();
          }
        }
      }
    }

    this.mainLessonsArray.sort(function (a: any, b: any) {
      let firstnumber = a.split("-")[0];
      let secondnumber = b.split("-")[0];
      let returnValue: any;
      if (secondnumber == firstnumber) {
        returnValue = parseFloat(a.split("-").join("")) - parseFloat(b.split("-").join(""))
      } else {
        returnValue = firstnumber - secondnumber;
      }
      return returnValue
    });


    for (var i = 0; i < this.mainLessonsArray.length; i++) {
      this.levelsArray = [];
      this.idsArray = [];
      this.titlesArray = [];
      for (var j = 0; j < chartItems.length; j++) {
        var myLessonsArray = chartItems[j].lessons.split(', ');
        console.log(chartItems[j].id.trim() == "POG")

        if (jQuery.inArray(this.mainLessonsArray[i], myLessonsArray) != -1) {
          this.levelsArray.push(chartItems[j].level);
          this.idsArray.push(chartItems[j].id);
          this.titlesArray.push(chartItems[j].title);
        }
      }
      this.chartItemsData[this.mainLessonsArray[i]] = { levels: this.levelsArray, ids: this.idsArray, titles: this.titlesArray }

    }
  }




  /**
    * click on tile
    * @method tileClickEvent
    * @param {event}click event
    * @param {url} url to open in new tab
    */
  tileClickEvent(event: Event) {
    let url = Object(event).value;
    let type = "";
    url.indexOf("games") != -1 ? type = "games" : type = "Game Boards";
    this.tileEvent(url, type)
  }


  /**
    * click on tile
    * @method tileClickEvent
    * @param {event}click event
    * @param {url} url to open in new tab
    */

  tileClickEventStudent(event: Event, url: string, type: string) {
    this.tileEvent(url, type)
  }


  /**
    * click on tile
    * @method tileClickEvent
    * @param {event}click event
    * @param {url} url to open in new tab
    */

  tileEvent(url: string, type: string) {

    if (type != "Game Boards") {
      this.commonClass.openInNewTab(url, "", type);
    } else {
      jQuery('.wide-tile').removeClass("tile-clicked").addClass("hover");
      var iOS = !!navigator.platform && /iPad/.test(navigator.platform);
      iOS == true ? url = url.replace('=', '') : '';
      this.commonClass.openInNewTab(url, this.user, '');
    }

  }

  /**
   * click on teacher resources
   * @method clickInactive
   * @param {event} clickInactive
   * @param {id} element id 
   */
  clickInactive(event: Event, id: string) {
    this.user == "student" ? jQuery("#" + id).removeClass("tilesActive") : jQuery("#" + id).removeClass("teacherTileActive");
  }
  /**
   * click on teacher resources
   * @method clickActive
   * @param {event} click active
   * @param {id} element id 
   */
  clickActive(event: Event, id: string) {
    this.user == "student" ? jQuery("#" + id).addClass("tilesActive") : jQuery("#" + id).addClass("teacherTileActive");
  }


  /**
 * close popup and audio
 * @method close
 */
  close() {
    jQuery("#modal").addClass("display-none");
    document.getElementById('content').setAttribute('src', 'about:blank');
    var vid = jQuery('.inner_div').find('iframe').contents().find('#trailer').get(0);
    vid == undefined ? '' : vid.remove();

  }
  /**
  * checkBox hover
  * @method clickHoverEvent
  * @param {event} click event
  * @param {value} class name
  */
  clickHoverEvent(event: Event, value: string, textValue: string) {
    if (jQuery(event.currentTarget).hasClass(value) == false) {
      jQuery(event.currentTarget).removeClass("hover").addClass(value);
      if (value == "unit-clicked") {
        jQuery("#unit-text").text(textValue);
      }
      jQuery(event.currentTarget).removeClass("unselected")
    }
  }

  /**
 * setDefault
 * @method setDefault
 */

  setDefault() {
    this.pageCount = 1;
    this.pageCount = Math.floor(this.filter["games"].length / 20);
    this.filter["games"].length / 20 != this.pageCount ? this.pageCount = this.pageCount + 1 : "";
    this.startCount = 1;
    this.currentPageId = 0;
    this.totalCount = this.pageCount > 1 ? this.filter["games"].length > 20 ? 20 : this.filter["games"].length : this.filter["games"].length;
    jQuery(".prev").addClass("disabled");
    jQuery(".wide-tile-games-container wide-tile .wideTileWrapper ").children().addClass("display-none");
    jQuery(".Page0").removeClass("display-none");

  }
  /**
   * reset filetrs 
   * @method resetFilters
   */
  resetFilters() {
    this.commonClass.isMobile() ? jQuery(".reset-container").removeClass("hover") : "";
    jQuery(".checkboxGroupElem").removeClass('checkbox-clicked');
    jQuery(".wide-tile-games").removeClass("display-none");
    this.filter["games"] = this.filter["gamesOriginal"];
    this.setItemsCount(this.filter["games"].length);
  }




  /**
   * checkbox click event
   * @method checkBoxEvent
   * @param {event} click event
   */
  checkBoxEvent(event: Event) {
    this.commonClass.checkBoxEnableDisableEvent(jQuery(event.currentTarget).attr("id"));
    let currentId = jQuery(".checkboxGroupElem").filter('.checkbox-clicked').attr("id");
    if (jQuery(".checkboxGroupElem").filter('.checkbox-clicked').length == 1) {
      this.filter["games"] = [];
      this.filter["games"] = currentId == "gamescheckbox" ? this.gamesOriginal : this.gameboards;
    } else {
      this.filter["games"] = this.filter["gamesOriginal"];
    }
    this.setItemsCount(this.filter["games"].length);
  }


  dropdownArrowClick(event: Event) {
    this.commonClass.drpDownArrowClick(jQuery(event.currentTarget).attr("id"));
  }


  /**
   * click on tile
   * @method tilesClickEvent
   * @param {routekey} routekey to navigate
   */
  tilesClickEvent(routekey: any) {
    this.router.navigate([routekey]);
  }

  /**
   * setting total tiles count
   * @method setItemsCount
   * @param {gamesLength} length of games
   * @param {gamesBoardLength} length of gamesBoardLength optional parameter
   */
  setItemsCount(gamesLength: number, gamesBoardLength?: any) {
    gamesBoardLength != undefined ? this.itemsGames = parseInt(gamesLength + gamesBoardLength) : this.itemsGames = gamesLength;
  }


  /**
   * opens correlation chart popup
   * @method openCorrelationChart   
   */
  openCorrelationChart(event: Event) {
    jQuery(".correlation_chart_popup").removeClass("display-none")
    var container = document.getElementById('corelation-container');
    PerfectScrollbar.initialize(container);
  }

  /**
   * close correlation chart popup
   * @method closeChart   
   */
  closeChart() {
    jQuery('.correlation_chart_popup').addClass("display-none");
  }

  openGamesChart(event: Event) {
    this.commonClass.openInNewTab(this.games_userguide_url, this.user, '');
  }



  /**
   * print page
   * @method printPage   
   */

  printPage() {
    window.print();
  }
}



