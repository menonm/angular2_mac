import { TestComponentBuilder } from '@angular/compiler/testing';
import { DmcFooterComponent } from '../Footer/dmc.footer.component';
import { DmcService} from '../../shared/dmc.service';
import { ROUTER_DIRECTIVES, Router, ActivatedRoute, UrlPathWithParams, RouterLink  } from '@angular/router';
import {Http, Response, HTTP_PROVIDERS} from '@angular/http';

import {
    beforeEachProviders,
    async,
    describe,
    expect,
    inject,
    it
} from '@angular/core/testing';
import {DmcGamesComponent} from './dmc.games.component';


export function main() {
    
       
    
    let moreInfo = [null,null,null,null,null,null,null,null,null,null,
        {"games_user_guide_url": "unit1", "items_text": "unit1", "edition_text": "unit1", "games_text": "unit1", "gameboards_text": "unit1",
        "lesson_corelation_text": "unit1", "user_guide_text": "unit1", "type_text": "unit1", "corelation_chart_text": "unit1", "close_text": "unit1",
         "print_text": "unit1", "additionandsubtraction_text": "unit1", "lesson_text": "unit1", 
         "game_title": "unit1","level_text": "unit1","lesson_title_text": "unit1"}]
    
    
  

    let textValue = "unit 1";
    let desc = "standardsdesc";
    let url = "post url"
    let unit = "unit1";
    let gamesLength = 10;
    let gamesBoards=10;
    
    let lesson="Lesson1"

    let dmcgames = new DmcGamesComponent(null, null, null);
    describe('dmcgamesComponent', () => {
        it('assign json value', () => {
            const result:any = dmcgames.assignJson(moreInfo);
            for (var i = 0; i < 13; i++) {
                expect(result[i]).toEqual("unit1");
            }
        })
    });


    describe('clickHoverEvent', () => {
        it('click hover events', () => {
            const result = dmcgames.clickHoverEvent(null, "unit-clicked", textValue);
            expect(textValue).toEqual("unit 1");
        })
    });





    describe('setItemsCount', () => {
        it('set items count', () => {
            const result = dmcgames.setItemsCount(gamesLength,gamesBoards);
               for (var i = 0; i < 2; i++) {
            expect(result[i]).toEqual(10);
               }
        })
    });
    
 describe('tileEvent', () => {
        it('click hover events', () => {
            const result = dmcgames.tileEvent(url,null);
            expect(url).toEqual("post url");
        })
    });
    
    
   

    
  



}
