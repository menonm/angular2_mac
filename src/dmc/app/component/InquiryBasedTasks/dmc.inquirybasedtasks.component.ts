import { Component, Pipe } from '@angular/core';
import { DmcService } from '../../shared/dmc.service';
import { ROUTER_DIRECTIVES } from '@angular/router';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { DmcFooterComponent } from '../Footer/dmc.footer.component';
import { DmcHeaderComponent } from '../Header/dmc.header.component';
import {TruncatePipe} from '../../shared/truncatepipe';
import { DmcFilterComponent } from '../../shared/mxdmcfilter.common';
import { DmcWideTileComponent } from '../WideTile/dmc.widetile.component';
import { DmcPopupComponent } from '../Popup/dmc.popup.component';

declare var jQuery: any;
@Component({
  moduleId: module.id,
  selector: 'inquirybasedtasks',
  templateUrl: './dmc.inquirybasedtasks.component.html',
  pipes: [TruncatePipe],
  directives: [ROUTER_DIRECTIVES, DmcFooterComponent, DmcHeaderComponent, DmcWideTileComponent, DmcPopupComponent],
  providers: [DmcService],
})




export class DmcInquiryBasedTasksComponent {
  /**
	 * Variable to store user value
	 */
  user: string;
  /**
  * Variable to store teacher library items 
  */
  displayContent: any = [];
  /**
 * Variable to store url value
 */
  url: any;
  itemCount: number;
  /**
  * Variable to event type 
  */
  eventType: any;
  /**
  * Object to store current page title
  */
  filter: any = {};
  /**
	 * Array to store all units 
	 */
  title: string;
  /**
  * variable to store background color
  */
  backgroundColor: string = "#7ec3f8";
  /**
  * variable to store path  value
  */
  path: string;
  /**
  * instance of filter component file 
  */
  commonClass: DmcFilterComponent.filter;
  /**
	 * variable to store json data
	 */
  mathreaderData: any;
  /**
	 * variable to store truncateValue value
	 */
  truncateValue: number;
  /**
  * variable to store moreinfo value
  */
  moreInfo: string;
  /**
  * variable to store blackline color
  */
  blackLineColor: string;
  /**
 * variable to store items text
 */
  items_text: string;
  /**
   * Added for color configurable
  */
  /**
	 * variable to store commonValues
	 */
  commonValues: any = {};
  /**
  * variable to store tile  color
  */
  tileColor: string;
  /**
	 * variable to store on tile border color
	 */
  studentTileBorderColor: string;
  /**
 * variable to store currentRouteKey
 */
  currentRouteKey: string;

  /**
   * get dmc service and routing service
   * @constructor 
   * @param {_dmcService} get _dmcService
   * @param {router} get routing service
   */
  constructor(public _dmcService: DmcService, private router: Router, private route: ActivatedRoute) {
    this.path = document.getElementById("dmc-main-container").getAttribute("path");
    this.commonClass = new DmcFilterComponent.filter();
    Object(document).self = this;
    Object(window).self = this;
    this.user = this.commonClass.getUser();
    this.eventType = this.commonClass.getEventType();
    this.url = Object(router).currentRouterState.snapshot.url;
    this.currentRouteKey = Object(route).url._value[0].path;
    this.mathreaderData = jQuery('body').data();
    this.truncateValue = jQuery(window).width() <= 1024 ? jQuery(window).width() <= 961 ? 40 : 50 : 65;
    this.assignJson(this.mathreaderData[10], this.mathreaderData[12], this.mathreaderData[17])
  }


  /**
    * ngAfterViewInit
    */

  ngAfterViewInit() {
    jQuery(".main-container").addClass("teacher-main");
    this.commonClass.enableDisableContainer(this.user);
    this.moreInfo != undefined && this.moreInfo != "" ? jQuery(".more-info-wrapper").removeClass("display-none") : "";
  }

  /**
   * assign json data to an object
   * @method assignJson
   * @param {data} data from json file
   */
  assignJson(data: any, moreInfo: any, commonValues: any) {
    let itemLength = data.teacher[0].libraryItems.length;
    let remainingItems = itemLength - Math.round(itemLength / 2);
    this.items_text = moreInfo.items_text;

    this.filter["indepthTasks"] = data.teacher[0].libraryItems;
    this.filter["indepthTasksleft"] = data.teacher[0].libraryItems.slice(0, Math.round(itemLength / 2));
    this.filter["indepthTasksright"] = this.filter["indepthTasks"].slice(-remainingItems);
    this.itemCount = this.filter["indepthTasks"].length;
    this.commonValues = this.user == "student" ? commonValues.student : commonValues.teacher;
    for (var i = 0; i < this.commonValues.length; i++) {
      if (this.commonValues[i].routeKey == this.currentRouteKey) {
        this.tileColor = this.commonValues[i].color;
        this.blackLineColor = this.commonValues[i].black_line_color;
        this.moreInfo = this.commonValues[i].moreInfo;
        this.title = this.commonValues[i].title;
      }
    }

  }


  /**
     * click on dropdown event
     * @method dropdownArrowClick
     * @param {event} eventype
     */
  dropdownArrowClick(event: Event) {
    this.commonClass.drpDownArrowClick(jQuery(event.currentTarget).attr("id"));
  }


  /**
   * click on tile
   * @method tilesClickEvent
   * @param {routekey} routekey to navigate
   */
  tileClickEvent(event: Event) {
    let url = Object(event).value;
    jQuery('.wide-tile').removeClass("tile-clicked").addClass("hover");
    this.commonClass.openInNewTab(url, this.user, '');
  }

  /**
   * click on teacher resources
   * @method clickInactive
   * @param {event} clickInactive
   * @param {id} element id 
   */
  clickInactive(event: Event, id: string) {
    jQuery("#" + id).removeClass("teacherTileActive");
  }
  /**
   * click on teacher resources
   * @method clickActive
   * @param {event} click active
   * @param {id} element id 
   */
  clickActive(event: Event, id: string) {
    jQuery("#" + id).addClass("teacherTileActive");
  }
}
