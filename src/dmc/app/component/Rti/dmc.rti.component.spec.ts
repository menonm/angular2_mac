import { TestComponentBuilder } from '@angular/compiler/testing';
import { DmcFooterComponent } from '../Footer/dmc.footer.component';
import { DmcService} from '../../shared/dmc.service';
import { ROUTER_DIRECTIVES, Router, ActivatedRoute, UrlPathWithParams, RouterLink  } from '@angular/router';
import {Http, Response, HTTP_PROVIDERS} from '@angular/http';

import {
    beforeEachProviders,
    async,
    describe,
    expect,
    inject,
    it
} from '@angular/core/testing';
import {DmcRtiComponent} from './dmc.rti.component';


export function main() {
    let moreInfo = {
        "teacher_text": "unit1", "student_text": "unit1", "previous_text": "unit1", "next_text": "unit1", "select_unit_text": "unit1",
        "select_lesson_text": "unit1", "select_bigidea_text": "unit1", "tier1_text": "unit1", "tier2_text": "unit1", "tier3_text": "unit1"
    
    }

    let textValue = "unit 1";
    let desc = "standardsdesc";
    let url = "post url"
    let unit = "unit1";
    let length = 10;
    let lesson="Lesson1"

    let dmcrti = new DmcRtiComponent(null, null, null);
    describe('dmcrtiComponent', () => {
        it('assign json value', () => {
            const result:any = dmcrti.assignJson(null, moreInfo,  null);
            for (var i = 0; i < 14; i++) {
                expect(result[i]).toEqual("unit1");
            }
        })
    });


    describe('clickHoverEvent', () => {
        it('click hover events', () => {
            const result = dmcrti.clickHoverEvent(null, "unit-clicked", textValue);
            expect(textValue).toEqual("unit 1");
        })
    });




    describe('setItemsCount', () => {
        it('set items count', () => {
            const result = dmcrti.setItemsCount(length);
            expect(result).toEqual(10);
        })
    });
    
    
    



}
