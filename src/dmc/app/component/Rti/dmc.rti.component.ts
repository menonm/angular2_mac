import { Component, Pipe } from '@angular/core';
import { DmcService } from '../../shared/dmc.service';
import { ROUTER_DIRECTIVES } from '@angular/router';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { DmcFooterComponent } from '../Footer/dmc.footer.component';
import { DmcHeaderComponent } from '../Header/dmc.header.component';
import { DmcFilterComponent } from '../../shared/mxdmcfilter.common';
import {TruncatePipe} from '../../shared/truncatepipe';
import { DPOService } from '../../shared/dpo.service';
import { DmcPopupComponent } from '../Popup/dmc.popup.component';
import { DmcWideTileComponent } from '../WideTile/dmc.widetile.component';
import { DmcFilterByComponent } from '../FilterBy/dmc.filterby.component';

declare var jQuery: any;
@Component({
  moduleId: module.id,
  selector: 'rti',
  templateUrl: './dmc.rti.component.html',
  pipes: [TruncatePipe],
  directives: [ROUTER_DIRECTIVES, DmcFooterComponent, DmcHeaderComponent, DmcPopupComponent, DmcWideTileComponent, DmcFilterByComponent],
  providers: [DmcService]
})


export class DmcRtiComponent {
  /**
	 * Variable to store user value
	 */
  user: string;
  /**
  * Variable to store teacher library items 
  */
  displayContent: any = [];
  filteredArray: any = [];
  /**
* Variable to store url value
*/
  url: any;
  /**
  * Variable to event type 
  */
  eventType: any;
  /**
 * Array to store all bigidea dropdown items 
 */
  lessons: any = [];
  /**
  * Array to store all lessons dropdown items
  */
  units: any = [];
  /**
	 * Variable to store dropdown items
	 */
  listItems: any = [];
  /**
	 * Object for temporary storage
	 */
  obj: any = [];
  /**
	 * Object that holds complete info
	 */
  unitLessonData: any = {};
  /**
 * Array to store unit value
 */
  unitValue: number;
  /**
 * Array to store all units 
 */
  filter: any = {};
  /**
	 * Object to store current page title
	 */
  title: string = "RtI";
  /**
  * variable to store current page count
  */
  pageCount: number = 1;
  /**
  * variable to store total page count
  */
  totalCount: number;
  /**
 * variable to store common value
 */
  landingJson: any = {};
  /**
	 * variable to store  page start count
	 */
  startCount: number = 1;
  /**
	 * variable to store  no of items
	 */
  itemCount: number;
  /**
    * variable to store math value
    */
  Math: any;
  /**
 * instance of filter component file 
 */
  commonClass: DmcFilterComponent.filter;
  /**
  * variable to store rti data
  */
  rtiData: any = {};
  /**
* variable to store current page id
*/
  currentPageId: number = 0;
  /**
 * variable to store moreinfo value
 */
  moreInfo: string;
  /**
 * variable to store truncateValue value
 */
  truncateValue: number;
  /**
 * variable to store lessonCheck value
 */
  lessonCheck: boolean = true;

  /**
   * Added for color configurable
  */
  /**
* variable to store commonValues
*/
  commonValues: any = {};
  /**
 * variable to store tile  color
 */
  tileColor: string;
  /**
  * variable to store currentRouteKey
  */
  currentRouteKey: string;
  /**
  * variable to store select unit text
  */
  select_unit_text: string;
  /**
 * variable to store select lesson text
 */
  select_lesson_text: string;
  /**
  * variable to store select big idea text
  */
  select_bigidea_text: string;
  /**
 * variable to store previous text
 */
  previous_text: string;
  /**
* variable to store next text
*/
  next_text: string;
  /**
	 * variable to store teacher text
	 */
  teacher_text: string;
  /**
  * variable to store student text
  */
  student_text: string;
  /**
  * variable to store tier1 text
  */
  tier1_text: string;
  /**
* variable to store tier2 text
*/
  tier2_text: string;
  /**
* variable to store tier3 text
*/
  tier3_text: string;
    /**
* variable to store edition text
*/
  edition_text:string;
     /**
* variable to store rti user guide value
*/
  rti_user_guides:string
       /**
* variable to store rti user guide url value
*/
  rti_userguide_url:string
         /**
* variable to store text items
*/
  items_text:string;

  /**
   * get dmc service and routing service
   * @constructor 
   * @param {_dmcService} get _dmcService
   * @param {router} get routing service
   */
  constructor(public _dmcService: DmcService, private router: Router, private route: ActivatedRoute) {
    Object(document).self = this;
    Object(window).self = this;
    this.Math = Math;
    this.commonClass = new DmcFilterComponent.filter();
    this.user = this.commonClass.getUser();
    this.eventType = this.commonClass.getEventType();
    this.url = Object(router).currentRouterState.snapshot.url;
    this.currentRouteKey = Object(route).url._value[0].path;
    this.rtiData = jQuery('body').data();
    this.truncateValue = jQuery(window).width() <= 1024 ? jQuery(window).width() <= 961 ? 63 : 76 : jQuery(window).width() <= 1281 ? 110 : 115;
    this.assignJson(this.rtiData[3], this.rtiData[12], this.rtiData[17]);
  }


  /**
    * ngAfterViewInit
    */

  ngAfterViewInit() {
    this.commonClass.enableDisableContainer(this.user);

    this.user == "student" ? jQuery(".main-container").addClass("student-main") : jQuery(".main-container").addClass("teacher-main");
    let scope = this;
    DPOService.gObj.dpoGlbService.initDPO({
      ready: function (obj: any) {
        console.log('DPO Service is ready for currentpage  ', obj.pageData);
        let unitText = obj.pageData['unit-text-dropdown'].elmContent.trim();
        let lessonText = obj.pageData['lesson-text'].elmContent.trim();

        // Unit Text Val Dropdown
        if (obj.pageData['unit-text-dropdown'].elmContent.trim() !== "Select Unit") {

          jQuery('.dropdown-text').each(function () {
            let innerText = jQuery(this).text();
            if (innerText === unitText) {
              jQuery(this).trigger('click');
            }
          });

          // Lesson Dropdown
          if (obj.pageData['lesson-text'].elmContent.trim() !== "Select Big Idea") {
            jQuery('#LessonDropdown .dropdown-text').each(function () {
              let innerText = jQuery(this).text().trim();

              if (innerText === lessonText) {
                console.log(innerText);
                console.log('lesson', lessonText);
                jQuery("#lesson-text").attr('name', lessonText);
                console.log('this in console', this);
                jQuery(this).trigger('click');
                jQuery("#LessonDropdown").removeClass('lesson-clicked');
                jQuery(this.parentElement).addClass('lesson-clicked');
              }
            });

          }
        }

        // CheckBox DropDown checkbox-clicked 
        jQuery(".checkbox-clicked").each(function () {
          jQuery(this).removeClass('checkbox-clicked');
          jQuery(this).trigger('click')
        });

        /********************************************** */


      }
    });
    this.moreInfo != undefined && this.moreInfo != "" ? jQuery(".more-info-wrapper").removeClass("display-none") : "";
  }

  /**
   * assign json data to an object
   * @method assignJson
   * @param {data} data from json file
   */
  assignJson(data: any, moreInfo: any, commonValues: any) {
    //preparing filtering json 
    var pattunit = /U\d+/;
    var pattdigit = /\d+/;
    var pattlesson = /L\d+/;
    let tier1Value: any = [];
    let tier2Value: any = [];
    let tier1UnitValue: any = [];
    let tier2UnitValue: any = [];
    let tier3UnitValue: any = [];
    let tier3Value: any = [];



    this.teacher_text = moreInfo.teacher_text;
    this.student_text = moreInfo.student_text;
    this.previous_text = moreInfo.previous_text;
    this.next_text = moreInfo.next_text;
    this.select_unit_text = moreInfo.select_unit_text;
    this.select_lesson_text = moreInfo.select_lesson_text;
    this.select_bigidea_text = moreInfo.select_bigidea_text;
    this.tier1_text = moreInfo.tier1_text;
    this.tier2_text = moreInfo.tier2_text;
    this.tier3_text = moreInfo.tier3_text;
   this.items_text = moreInfo.items_text;
    this.edition_text=moreInfo.edition_text;
    this.rti_user_guides=moreInfo.rti_user_guide;
    this.rti_userguide_url=moreInfo.rti_userguide_url;

    this.filter["originaldata"] = data.teacher[0].libraryItems;
    this.filter["rti"] = data.teacher[0].libraryItems;
    this.displayContent = data.teacher[0].libraryItems;
    this.landingJson = commonValues;

    this.commonValues = this.user == "student" ? commonValues.student : commonValues.teacher;
    for (var i = 0; i < this.commonValues.length; i++) {
      if (this.commonValues[i].routeKey == this.currentRouteKey) {
        this.tileColor = this.commonValues[i].color;
        this.moreInfo = this.commonValues[i].moreInfo;
      }
    }
    console.log(this.currentRouteKey);

    for (var i = 0; i < this.displayContent.length; i++) {
      if (pattunit.test(this.displayContent[i].HMH_ID)) {
        this.filter["rti"][i].unit_type = "Unit " + parseInt(pattdigit.exec(pattunit.exec(data.teacher[0].libraryItems[i].HMH_ID)[0])[0]);
        this.filter["rti"][i].bigidea_type = "Big Idea " + this.displayContent[i].bigidea;

        //condition to save tier1 and tier2 value 
        let tierValue = data.teacher[0].libraryItems[i].display_title.split(":")[0].split("Interactive")[0].trim().toLowerCase().split(' ').join('');
        console.log(tierValue)
        if (tierValue == "tier1") {
          tier1Value.push(data.teacher[0].libraryItems[i]);
        } else if (tierValue == "tier2") {
          tier2Value.push(data.teacher[0].libraryItems[i]);
        } else if (tierValue == "tier3") {
          tier3Value.push(data.teacher[0].libraryItems[i]);
        }

        if (("Unit" + parseInt(pattdigit.exec(pattunit.exec(data.teacher[0].libraryItems[i].HMH_ID)[0])[0]) in this.unitLessonData) == false) {

          //clearing all array values before each new unit 
          this.lessons = [];
          this.obj = [];
          tier1UnitValue = [];
          tier2UnitValue = [];
          tier3UnitValue = [];

          //condition to push lesson, obj, tier values per unit
          this.lessons.push(this.displayContent[i].bigidea);
          this.obj.push(this.displayContent[i]);
          if (tierValue == "tier1") {
            tier1UnitValue.push(data.teacher[0].libraryItems[i]);
          } else if (tierValue == "tier2") {
            tier2UnitValue.push(data.teacher[0].libraryItems[i]);
          } else if (tierValue == "tier3") {
            tier3UnitValue.push(data.teacher[0].libraryItems[i]);
          }

          //unit lesson data json store details per unit
          this.unitLessonData["Unit" + parseInt(pattdigit.exec(pattunit.exec(data.teacher[0].libraryItems[i].HMH_ID)[0])[0])] = { lesson: this.lessons.sort(), obj: this.obj, tier1: tier1UnitValue, tier2: tier2UnitValue, tier3: tier3UnitValue }

          //units is an array for the dropdown
          this.units.push("Unit " + parseInt(pattdigit.exec(pattunit.exec(this.displayContent[i].HMH_ID)[0])[0]));
        } else {

          //clearing all array values before entering the existing  unit value 
          this.lessons = [];
          this.obj = [];
          tier1UnitValue = [];
          tier2UnitValue = [];

          //condition to fill existing data of that particular unit in array 
          tier1UnitValue = this.unitLessonData["Unit" + parseInt(pattdigit.exec(pattunit.exec(data.teacher[0].libraryItems[i].HMH_ID)[0])[0])].tier1;
          tier2UnitValue = this.unitLessonData["Unit" + parseInt(pattdigit.exec(pattunit.exec(data.teacher[0].libraryItems[i].HMH_ID)[0])[0])].tier2;
          tier3UnitValue = this.unitLessonData["Unit" + parseInt(pattdigit.exec(pattunit.exec(data.teacher[0].libraryItems[i].HMH_ID)[0])[0])].tier3;
          this.lessons = this.unitLessonData["Unit" + parseInt(pattdigit.exec(pattunit.exec(data.teacher[0].libraryItems[i].HMH_ID)[0])[0])].lesson;
          this.obj = this.unitLessonData["Unit" + parseInt(pattdigit.exec(pattunit.exec(data.teacher[0].libraryItems[i].HMH_ID)[0])[0])].obj;

          //condition to fill new data of that unit
          if (tierValue == "tier1") {
            tier1UnitValue.push(data.teacher[0].libraryItems[i]);
          } else if (tierValue == "tier2") {
            tier2UnitValue.push(data.teacher[0].libraryItems[i]);
          }
          else if (tierValue == "tier3") {
            tier3UnitValue.push(data.teacher[0].libraryItems[i]);
          }
          if (jQuery.inArray(this.displayContent[i].bigidea, this.lessons) == -1) {
            this.lessons.push(this.displayContent[i].bigidea);
          }
          this.obj.push(this.displayContent[i]);
          this.unitLessonData["Unit" + parseInt(pattdigit.exec(pattunit.exec(data.teacher[0].libraryItems[i].HMH_ID)[0])[0])] = { lesson: this.lessons.sort(), obj: this.obj, tier1: tier1UnitValue, tier2: tier2UnitValue, tier3: tier3UnitValue }
        }
      }
      console.log(this.unitLessonData)
    }
    // this.moreInfo = moreInfo.RtI;
    this.filter["tier1"] = tier1Value;
    this.filter["tier2"] = tier2Value;
    this.filter["tier3"] = tier3Value;
    this.filter["lessondata"] = this.unitLessonData;
    this.setItemsCount(this.filter["rti"].length);
    this.pageCount = Math.floor(this.filter["rti"].length / 20);
    this.filter["rti"].length / 20 != this.pageCount ? this.pageCount = this.pageCount + 1 : "";
    this.totalCount = this.pageCount > 1 ? this.filter["rti"].length > 20 ? 20 : this.filter["rti"].length : this.filter["rti"].length;

  }




  /**
   * close drop down click on document click
   * @method documentBodyClick
   * @param {event}  click event
   */
  documentBodyClick(event: Event) {
    if (jQuery(".main-container").hasClass("rti")) {
      let className = jQuery(event.target).attr("class");
      let id = jQuery(event.target).attr("id");
      if (className != "dropdown-text" && className != "dropdown-arrow dropdown-down-arrow" && id != "dropdown1" && id != "dropdown2" && id != "lesson-text" && id != "unit-text-dropdown") {
        if (jQuery(".dropbtn").hasClass("dropdown-clicked") == true) {
          jQuery(".dropdown-content").removeClass("show");
          jQuery(".dropbtn").addClass("hover").removeClass("dropdown-clicked");
        }
        Object(document).removeEventListener(Object(event.currentTarget).self.eventType, Object(event.currentTarget).self.documentBodyClick, false);
      }
    }
  }


  /**
    * toggle drop down
    * @method toggleDropDown
    * @param {event} click event
    * @param {DropdownShow} class name to remove or add
    */

  toggleDropDown(event: Event, DropdownShow: string, DropdownHide: string) {
    this.commonClass.toggleDropDown(DropdownShow, DropdownHide);
    if (jQuery("#" + DropdownShow).siblings().hasClass("dropdown-clicked") == true) {
      Object(document).addEventListener(this.eventType, this.documentBodyClick, true);
    }
  }


  /**
    * checkbox click event
    * @method checkBoxEvent
    * @param {event} click event
    */

  checkBoxEvent(event: Event) {
    this.commonClass.checkBoxEnableDisableEvent(jQuery(event.currentTarget).attr("id"));

    let filterArray: any = [];
    let currentId = jQuery(".checkboxGroupElem").filter('.checkbox-clicked').attr("id");
    let filterValue = this.filter["lessondata"];
    let text = jQuery("#unit-text-dropdown").text().trim().split(" ").join("");
    let lessonText = jQuery("#lesson-text").text().trim();

    //filtering part of checkbox
    if (jQuery("#unit-text-dropdown").text() == ("Select Unit")) {
      if (jQuery(".checkboxGroupElem").filter('.checkbox-clicked').length == 1) {
        this.filter["rti"] = this.filter[currentId];
      } else if (jQuery(".checkboxGroupElem").filter('.checkbox-clicked').length > 1) {
        this.filter["rti"] = [];
        let tempOriginalDatas: any = [];
        let count = 0;
        let self = this;

        jQuery(".checkbox-clicked").each(function () {
          let id = jQuery(this).attr("id");
          count++;
          count == 1 ? tempOriginalDatas = self.filter[id].slice() : tempOriginalDatas.push.apply(tempOriginalDatas, self.filter[id].slice());
        });
        self.filter["rti"] = tempOriginalDatas.slice();


      } else {
        this.filter["rti"] = this.filter["originaldata"];
      }
    }
    else {
      if (jQuery("#lesson-text").text() == "Select Big Idea") {
        if (jQuery(".checkboxGroupElem").filter('.checkbox-clicked').length == 1) {
          this.filter["rti"] = filterValue[text][currentId]
        } else if (jQuery(".checkboxGroupElem").filter('.checkbox-clicked').length > 1) {
          this.filter["rti"] = [];
          let tempOriginalDatas: any = [];
          let count = 0;
          let self = this;

          jQuery(".checkbox-clicked").each(function () {
            let id = jQuery(this).attr("id");
            count++;
            count == 1 ? tempOriginalDatas = filterValue[text][id].slice() : tempOriginalDatas.push.apply(tempOriginalDatas, filterValue[text][id].slice());
          });
          self.filter["rti"] = tempOriginalDatas.slice();
        } else {
          this.filter["rti"] = filterValue[text]["obj"];
        }
      }
      else {
        if (jQuery(".checkboxGroupElem").filter('.checkbox-clicked').length >= 1) {
          let self = this;
          filterArray = [];
          jQuery(".checkbox-clicked").each(function () {
            let id = jQuery(this).attr("id");
            for (var l = 0; l < filterValue[text]["obj"].length; l++) {
              let tierValue = filterValue[text]["obj"][l].display_title.split(":")[0].split("Interactive")[0].trim().toLowerCase().split(' ').join('');
              console.log(tierValue)
              if (lessonText == filterValue[text].obj[l].bigidea && tierValue == id) {
                filterArray.push(filterValue[text]["obj"][l]);
              }
              self.filter["rti"] = filterArray;
            }
          });
        } else {
          this.filter["rti"] = this.filter["originaldata"];
        }
      }
    }


    this.setItemsCount(this.filter["rti"].length);
    this.totalCount = this.pageCount > 1 ? this.filter["rti"].length > 20 ? 20 : this.filter["rti"].length : this.filter["rti"].length;
    this.setDefault();

  }

 /**
     * rti user guide  click
     * @method dropdownArrowClick
     * @param {event} click event
     */
 openRtiUserGuide(event: Event) {
    this.commonClass.openInNewTab(this.rti_userguide_url, this.user, '');
  }


  /**
     * dropDownArrow click
     * @method dropdownArrowClick
     * @param {event} click event
     */
  dropdownArrowClick(event: Event) {
    this.commonClass.drpDownArrowClick(jQuery(event.currentTarget).attr("id"));
  }

  /**
     * dropDownEvent click
     * @method unitDropDownEvent
     * @param {event} click event
     */
  unitDropDownEvent(event: Event) {
    jQuery(".landing-dropdown-content").children().removeClass("dropdown-clicked");
    jQuery(".unit-filter-dDown_content").removeClass("unit-clicked");
    this.clickHoverEvent(event, "unit-clicked", jQuery(event.currentTarget)[0].innerText);

  }


  /**
     * checkBox hover
     * @method clickHoverEvent
     * @param {event} click event
     * @param {value} class name
     */
  clickHoverEvent(event: Event, value: string, textValue: string) {
    jQuery(".landing-dDown_content").removeClass("lesson-clicked");
    if (jQuery(event.currentTarget).hasClass(value) == false) {
      jQuery(event.currentTarget).removeClass("hover").addClass(value);
      if (value == "unit-clicked") {
        jQuery(".go-button").removeClass("disable-gobutton").removeAttr("disabled");
        jQuery("#lesson-text").text("");
        jQuery("#unit-text-dropdown").text(textValue);
        jQuery("#unit-text-dropdown").addClass("value-selected");
        jQuery(".lesson-filter-dropdown ").removeClass("disabled-dropdown");
        jQuery("#UnitDropdown").removeClass("show").siblings().removeClass("dropdown-clicked");
        jQuery("#lesson-text").removeClass("value-selected");
        this.listItems = "";
        for (var i = 0; i < this.unitLessonData[textValue.trim().split(" ").join("")].lesson.length; i++) {
          this.listItems += "  <div  class='dDown_content  hover lesson-dDown_content' id=ert" + i + " (click)='lessonDropDownEvent($event)'><div class='dropdown-text'>" + this.unitLessonData[textValue.trim().split(" ").join("")].lesson[i] + "</div></div>";
          jQuery("#LessonDropdown").html(this.listItems);
        }
        jQuery("#lesson-text").text("Select Big Idea");

        var classNames = document.getElementsByClassName("lesson-dDown_content");
        var self = this;
        jQuery(".lesson-dDown_content").data("self", this);
        for (var i = 0; i < classNames.length; i++) {
          classNames[i].addEventListener(this.eventType, this.lessonDropDownEvent, false);
        }

        let tempOriginalDatas: any = [];
        let count = 0;
        //let self=this;

        jQuery(".checkbox-clicked").each(function () {
          let currentId = jQuery(this).attr("id");
          count++;
          count == 1 ? tempOriginalDatas = self.unitLessonData[textValue.trim().split(" ").join("")][currentId].slice() : tempOriginalDatas.push.apply(tempOriginalDatas, self.unitLessonData[textValue.trim().split(" ").join("")][currentId].slice());
        });
        this.filter["rti"] = jQuery(".checkboxGroupElem").filter('.checkbox-clicked').length >= 1 ? tempOriginalDatas.slice() : this.unitLessonData[textValue.trim().split(" ").join("")].obj;
      }
    } else {
      jQuery(event.currentTarget).removeClass(value);
    }
    this.setItemsCount(this.filter["rti"].length);
    this.totalCount = this.pageCount > 1 ? this.filter["rti"].length > 20 ? 20 : this.filter["rti"].length : this.filter["rti"].length;
    this.setDefault();
  }


  /**
    * click on lessonDropDown
    * @method lessonDropDownEvent
    */

  lessonDropDownEvent(event: Event) {
    jQuery(".lesson-dDown_content").removeClass("lesson-clicked");
    jQuery(event.currentTarget).addClass("lesson-clicked");
    let LessonName = jQuery("#lesson-text").attr('name');
    if (LessonName != undefined && this.lessonCheck) {
      jQuery("#lesson-text").text(LessonName);
      this.lessonCheck = false;
    }
    else {
      jQuery("#lesson-text").text(jQuery(event.currentTarget).context.innerText);
    }

    jQuery("#lesson-text").addClass("value-selected");
    jQuery("#LessonDropdown").removeClass("show").siblings().removeClass("dropdown-clicked");
    // let tierValue = jQuery(".checkboxGroupElem").filter('.checkbox-clicked').length == 1 ? jQuery('.checkbox-clicked').attr("id") : "";



    let tempOriginalDatas: any = [];
    let count = 0;


    jQuery(".checkbox-clicked").each(function () {
      let tierValue = jQuery(this).attr("id");
      count++;
      count == 1 ? tempOriginalDatas = jQuery(".lesson-dDown_content").data("self").unitLessonData[jQuery('#unit-text-dropdown').text().trim().split(" ").join("")][tierValue].slice() : tempOriginalDatas.push.apply(tempOriginalDatas, jQuery(".lesson-dDown_content").data("self").unitLessonData[jQuery('#unit-text-dropdown').text().trim().split(" ").join("")][tierValue]);
    });
    let unitData = jQuery(".checkboxGroupElem").filter('.checkbox-clicked').length >= 1 ? tempOriginalDatas.slice() : jQuery(".lesson-dDown_content").data("self").unitLessonData[jQuery('#unit-text-dropdown').text().split(" ").join("").trim()]["obj"];



    let lessonText = jQuery("#lesson-text").text().trim();
    jQuery(".lesson-dDown_content").data("self").filter["rti"] = [];

    for (var l = 0; l < unitData.length; l++) {
      if (lessonText == unitData[l].bigidea) {
        jQuery(".lesson-dDown_content").data("self").filter["rti"].push(unitData[l]);
      }
    }
    jQuery(".lesson-dDown_content").data("self").setItemsCount(jQuery(".lesson-dDown_content").data("self").filter["rti"].length);
    jQuery(".lesson-dDown_content").data("self").setDefault();
  }


  /**
   * click on tile
   * @method tilesClickEvent
   * @param {routekey} routekey to navigate
   */
  tileClickEvent(event: Event) {
    //jQuery('.wide-tile').removeClass("tile-clicked").addClass("hover");
    let url = Object(event).value;
    this.commonClass.openInNewTab(url, this.user, '');
  }

  /**
   * click on reset filter
   * @method resetFilters
   */
  resetFilters() {
    this.commonClass.resetAllFilters();
    this.commonClass.isMobile() ? jQuery(".reset-container").removeClass("hover") : "";
    jQuery("#lesson-text").text("Select Big Idea");
    jQuery("#unit-text-dropdown").text("Select Unit");
    jQuery(".lesson-filter-dropdown ").addClass("disabled-dropdown");
    this.filter["rti"] = this.filter["originaldata"];
    this.setItemsCount(this.filter["rti"].length);
    jQuery(".customDropDown .filter-Text").removeClass("value-selected");
    this.setDefault();
  }

  /**
   * set no of items
   * @method setItemsCount
   *  * @param {itemLength} indicates number of items
   */
  setItemsCount(itemLength: number) {
    this.itemCount = itemLength;
    this.itemCount == 0 ? jQuery(".results-not-found").removeClass("display-none") : jQuery(".results-not-found").addClass("display-none");
    this.pageCount = Math.floor(this.filter["rti"].length / 20);
    this.filter["rti"].length / 20 != this.pageCount ? this.pageCount = this.pageCount + 1 : "";
    this.totalCount = this.pageCount > 1 ? this.filter["rti"].length > 20 ? 20 : this.filter["rti"].length : this.filter["rti"].length;
    this.filter["rti"].length <= 20 ? jQuery(".next").addClass("disabled") : jQuery(".next").removeClass("disabled");
    this.pageCount == 1 || this.currentPageId == 0 ? jQuery(".prev").addClass("disabled") : jQuery(".prev").removeClass("disabled");
  }

  /**
    * navigation
    * @method changePage
    *  * @param {event} event
    *   */

  changePage(event: Event, operation: String) {
    event.stopPropagation();
    this.currentPageId = jQuery(".content-container .wide-tile").not(".display-none").attr("class").split("Page")[1];
    jQuery(".prev,.next").removeClass("disabled");
    if (operation == "next") {
      jQuery(".next").hasClass("disabled") == false ? this.currentPageId++ : "";
      this.currentPageId == this.pageCount - 1 ? jQuery(".next").addClass("disabled") : "";
      this.startCount = this.totalCount + 1
    }
    else {
      this.currentPageId == 1 ? jQuery(".prev").addClass("disabled") : "";
      jQuery(".prev").hasClass("disabled") == false ? this.currentPageId-- : this.currentPageId = 0;
      this.totalCount = this.startCount - 1
    }
    jQuery(".content-container .wide-tile").not(".display-none").addClass("display-none");
    jQuery(".Page" + this.currentPageId).removeClass("display-none");


    if (operation == "next") {
      this.totalCount = this.totalCount + jQuery(".wide-tile-games-container.content-container wide-tile .wideTileWrapper").children().not(".display-none").length;
    } else {
      this.startCount = this.totalCount + 1 - jQuery(".wide-tile-games-container.content-container wide-tile .wideTileWrapper").children().not(".display-none").length;
    }

  }

  /**
    * setDefault
    * @method setDefault
    *   */

  setDefault() {
    this.pageCount = 1;
    let addedLength: any = this.filter["rti"].length;
    this.pageCount = Math.floor(addedLength / 20);
    addedLength / 20 != this.pageCount ? this.pageCount = this.pageCount + 1 : "";
    this.startCount = 1;
    this.currentPageId = 0;
    this.totalCount = this.pageCount > 1 ? addedLength > 20 ? 20 : addedLength : addedLength;
    jQuery(".prev").addClass("disabled");
    jQuery(".wide-tile-games-container wide-tile .wideTileWrapper").children().addClass("display-none");
    jQuery(".Page0").removeClass("display-none");
  }


}