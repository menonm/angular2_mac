import { Component, ElementRef, Renderer, Output, Input, OnInit, OnDestroy, Directive} from '@angular/core';
import { DmcService} from '../../shared/dmc.service';
import { ROUTER_DIRECTIVES, Router, ActivatedRoute, UrlPathWithParams, RouterLink  } from '@angular/router';
import { DmcFooterComponent } from '../Footer/dmc.footer.component';
import { Subject }           from 'rxjs/Subject';
import { Observable }        from 'rxjs/Observable';
import {Http, Response, HTTP_PROVIDERS} from '@angular/http';
import { DmcFilterComponent } from '../../shared/mxdmcfilter.common';
import { Ng2AutoCompleteModule } from 'ng2-auto-complete';
import { DPOService } from '../../shared/dpo.service';

declare var jQuery: any;

@Component({
  moduleId: module.id,
  selector: 'landing-page',
  templateUrl: './dmc.landing.component.html',
  directives: [ROUTER_DIRECTIVES, DmcFooterComponent, RouterLink],
  providers: [DmcService]

})

export class DmcLandingComponent {
  /**
	 * Variable to store user value
	 */
  user: string;
  /**
  * Variable to store routemaps
  */
  routemaps: any;
  /**
	 * Variable to store error messages
	 */
  errorMessage: any;
  /**
	 * Variable to store listing value
	 */
  listingval: string = "pmt";
  /**
  * Variable to store search content
  */
  searchContent: any = [];
  /**
  * Variable to store lesson array
  */
  lessonArray: any;
  /**
 * Variable to store list items
 */
  listItems: any;
  /**
	 * Variable to store unit text
	 */

  unit: any;
  /**
 * Variable to store lesson text
 */
  lesson: any;
  /**
* Variable to store event Type
*/
  eventType: string;
  /**
* Variable to store elementRef
*/
  elementRef: ElementRef;
  /**
	 * instance of filter component file 
	 */
  commonClass: DmcFilterComponent.filter;
  /**
* Variable to store path value
*/
  path: string;
  /**
  * Variable to store units value
  */
  unitArray: any = [];
  /**
* Variable to store lessons per unit value
*/
  unitLesson: any = {};
  /**
* Variable to store results data
*/
  resultData: any = {};
  /**
* Variable to store lesson name check
*/
  lessonNameCheck: boolean = true;
  /**
  * Variable to store landing data
  */
  landingData: any = {};
  /**
 * Variable to store show resources text
 */
  showresources: string;
  /**
  * Variable to store url value
  */
  url: string;
  /**
* Variable to store hmhtitle
*/
  hmhtitle: string;
  /**
* Variable to store landing title
*/
  landingtitle: string;
  /**
* Variable to store teacher logo
*/
  teacherlogo: string;
  /**
* Variable to store all tile lements
*/
  landingTiles: any = [];
  /**
* Variable to store selected unit text
*/
  selectUnitText: string;
  /**
 * Variable to store selected lesson text
 */
  selectLessonText: string;
  /**
* Variable to store comming soon  text
*/
  comingSoonText: string;
  /**
   * 
   *   /**
* Variable to store comming soon  text
*/
isMobile:boolean;
/**
   * 
   *   /**
   * 
   * 
   * 
   * get http service,_dmcService and routing services
   * @constructor
   * @param{http} get http service
   * @param {_dmcService} get _dmcService
   * @param {router} get router service
   * @param {routes} activate routes
   */
  constructor(public http: Http, public _dmcService: DmcService, private router: Router, public routes: ActivatedRoute) {
    Object(document).self = this;
    Object(window).self = this;
    this.path = document.getElementById("dmc-main-container").getAttribute("path");
    this.commonClass = new DmcFilterComponent.filter();
    this.eventType = this.commonClass.getEventType();
    this.user = this.commonClass.getUser();
    this.landingData = jQuery('body').data();
    this._dmcService.getBulkData(['common', 'planningguide', 'landing']).subscribe(
      data => { this.assignJson(data); }
    );
    this.isMobile=this.commonClass.isMobile();
  }

  /**
     * assign json data to an object
     * @method assignJson
     * @param {data} data from json file
     */
  assignJson(data: any) {
    this.unitArray = data[2].unitArray;
    this.unitLesson = data[2].unitLesson;
    this.listingval = data[2].student_header_logo;
    this.showresources = data[2].show_resources_for_content;
    this.url = data[1].teacher[0].libraryItems[0].relative_url;
    this.user == "student" ? this.landingTiles = data[2].student : this.landingTiles = data[2].teacher;
    this.user=="teacher"? jQuery(".landing-main").removeClass("display-none"):"";
    this.hmhtitle = data[2].hmh_title;
    this.landingtitle = data[2].landing_title;
    this.teacherlogo =  this.isMobile?data[2].teacher_mobile_header_logo:data[2].teacher_header_logo;
    this.selectUnitText = data[0].select_unit_text;
    this.selectLessonText = data[0].select_lesson_text;
    this.comingSoonText = data[0].comingsoon_text;

  }

  /**
   * close search result on document click
   * @method offClickHandler
   * @param {event} click event
   */
  offClickHandler(event: any) {
    jQuery('.data-ctrl').hide();
  }


  /**
   * close drop down click on document click
   * @method documentBodyClick
   * @param {event}  click event
   */
  documentBodyClick(event: Event) {
    if (jQuery(".main-container").hasClass("teacher-landing-page")) {

      let className = jQuery(event.target).attr("class");
      let id = jQuery(event.target).attr("id");
      if (className != "dropdown-text" && id != "UnitDropdown" && id != "LessonDropdown" && className != "dropdown-arrow dropdown-down-arrow " && id != "dropdown1" && id != "dropdown2" && id != "lesson-text" && id != "unit-text") {
        if (jQuery(".dropbtn").hasClass("dropdown-clicked") == true) {
          jQuery(".dropdown-content").removeClass("show");
          jQuery(".dropbtn").addClass("hover").removeClass("dropdown-clicked");
        }
        Object(document).removeEventListener(Object(event.currentTarget).self.eventType, Object(event.currentTarget).self.documentBodyClick, false);
      }
    }

  }


  /**
   * ngAfterViewInit
   */
  ngAfterViewInit() {
    navigator.userAgent.indexOf('Mac OS X') != -1 ? jQuery(".circleIcon").addClass("circle-mac") : "";
    if (this.user == "student") {
      jQuery(".teacher-tiles-container").addClass("display-none");
      jQuery(".student-tiles-container").removeClass("display-none");
      jQuery("#elem_student").removeClass("display-none");
      jQuery(".main-container").addClass("student-background");
      jQuery(".footer").addClass("student-footer");
      jQuery("#elem_teacher,.landing").addClass("display-none");
      jQuery(".edition").text("Student Resources");
      jQuery(".header-banner").addClass("student");
      jQuery(".header-dragon").removeClass("display-none");
    }
    else {
      jQuery(".teacher-tiles-container").removeClass("display-none");
      jQuery(".header-banner").removeClass("student");
      jQuery("#elem_teacher").removeClass("display-none");
      jQuery("#elem_student").addClass("display-none");
    }
    var slideWidth = jQuery('#slider ul li').width();
    jQuery('#slider ul').css({ marginLeft: - slideWidth });
    jQuery('#slider ul li:last-child').prependTo('#slider ul');
    let unitValue = document.getElementById("dmc-main-container").getAttribute("unit");
    let lessonValue = document.getElementById("dmc-main-container").getAttribute("lesson");



    DPOService.gObj.dpoGlbService.initDPO({
      ready: function (obj: any) {
        // console.log('DPO Service is ready for :' + obj.pageName);
        let unitText = obj.pageData['unit-text'].elmContent.trim();
        let lessonText = obj.pageData['lesson-text'].elmContent.trim();

        if (unitValue !== null) {
          unitText = unitValue.trim();
          lessonText = lessonValue.trim();
        }

        if (obj.pageData['unit-text'].elmContent.trim() !== "Units: All") {
          // Unit DropDown
          jQuery('.dropdown-text').each(function () {
            if (jQuery(this).text() === unitText) {
              jQuery(this).trigger('click');
            }
          });

          // Lesson DropDown
          if (obj.pageData['lesson-text'].elmContent.trim() !== "Lessons All") {
            jQuery('#LessonDropdown .dropdown-text').each(function () {
              if (jQuery(this).text() === lessonText) {
                jQuery("#lesson-text").attr('name', lessonText)
                jQuery(this).trigger('click');
              }
            });
          }

        }
      }
    });
  }


  /**
   * click on tiles
   * @param {routekey} routekey to navigate
   * @method tileClickEvent
   */

  tileClickEvent(routekey: any) {
    document.getElementById("dmc-main-container").setAttribute("unit", jQuery("#unit-text").text());
    document.getElementById("dmc-main-container").setAttribute("lesson", jQuery("#lesson-text").text());

    if (routekey == "planningguide") {

      this.commonClass.openInNewTab(this.url, this.user, '');
    } else {
      this.router.navigate([routekey]);
    }
  }
  
  /**
   * click on tiles
   * @param {routekey} routekey to navigate
   * @method openDropdownMobile
   */
  openDropdownMobile()
  {
    if(jQuery(".landing-arrow-mobile").hasClass("clicked")==true)
    {
      jQuery(".landing-arrow-mobile").removeClass("clicked");
      jQuery(".dropdown-menu-mobile").addClass("display-none");
    }else{
         jQuery(".landing-arrow-mobile").addClass("clicked");
       jQuery(".dropdown-menu-mobile").removeClass("display-none");
    }
   
  }
  

  /**
   * click on teacher resources
   * @method clickInactive
   * @param {event} clickInactive
   */
  clickInactive(event: Event) {
    this.user == "student" ? jQuery(event.currentTarget).removeClass("tilesActive") : jQuery(event.currentTarget).removeClass("teacherTileActive");
  }
  /**
   * click on teacher resources
   * @method clickActive
   * @param {event} click active
   */
  clickActive(event: Event) {
    this.user == "student" ? jQuery(event.currentTarget).addClass("tilesActive") : jQuery(event.currentTarget).addClass("teacherTileActive");
  }

  /**
   * toggle drop down
   * @method toggleDropDown
   * @param {event} click event
   * @param {DropdownShow} class name to remove or add
   */
  toggleDropDown(event: Event, DropdownShow: string, DropdownHide: string) {
    this.commonClass.toggleDropDown(DropdownShow, DropdownHide);
    if (jQuery(event.currentTarget).hasClass("dropdown-clicked") == true) {
      Object(document).addEventListener(this.eventType, this.documentBodyClick, true);
    }
  }



  /**
  * dropDownEvent click
  * @method unitDropDownEvent
  * @param {event} click event
  */
  unitDropDownEvent(event: Event) {
  
    jQuery(".landing-dropdown-content").children().removeClass("dropdown-clicked");
    jQuery(".landing-dDown_content").removeClass("unit-clicked");
    this.clickHoverEvent(event, "unit-clicked", jQuery(event.currentTarget)[0].innerText);
   

  }

  /**
 * dropDownEvent click
 * @method lessonDropDownEvent
 * @param {event} click event
 */
  lessonDropDownEvent(event: Event) {
    jQuery(".landing-dDown_content").removeClass("lesson-clicked");
    jQuery(event.currentTarget).addClass("lesson-clicked");

    let lessonTextName = jQuery("#lesson-text").attr('name');
    if (lessonTextName != undefined && this.lessonNameCheck) {
      jQuery("#lesson-text").text(lessonTextName);
      this.lessonNameCheck = false;
    }
    else {
      jQuery("#lesson-text").text(jQuery(event.currentTarget).context.innerText);
    }
    jQuery("#lesson-text").addClass("value-selected");
    jQuery("#LessonDropdown").removeClass("show");
    jQuery("#LessonDropdown").siblings().removeClass("dropdown-clicked");
  }
  /**
   * checkBox hover
   * @method clickHoverEvent
   * @param {event} click event
   * @param {value} class name
   * @param {textValue} dropdown value
   */
  clickHoverEvent(event: Event, value: string, textValue: string) {
    jQuery(".landing-dDown_content").removeClass("lesson-clicked");
    if (jQuery(event.currentTarget).hasClass(value) == false) {
      jQuery(event.currentTarget).removeClass("hover").addClass(value);
      if (value == "unit-clicked") {
        jQuery(".go-button").removeClass("disable-gobutton").removeAttr("disabled");
        jQuery("#lesson-text").text("");
        jQuery("#unit-text").text(textValue);
        jQuery("#unit-text").addClass("value-selected");
        jQuery("#lesson-text").removeClass("value-selected");
        jQuery("#UnitDropdown").removeClass("show").siblings().removeClass("dropdown-clicked");
        this.listItems = "";
        for (var i = 0; i < this.unitLesson[textValue.split(" ").join("").trim()].length; i++) {
          this.listItems += "  <div  class='dDown_content landing-dDown_content hover lesson-dDown_content dropdown-popup-content' id=ert" + i + " (click)='lessonDropDownEvent($event)'><div class='dropdown-text'>" + this.unitLesson[textValue.split(" ").join("").trim()][i] + "</div></div>";
          jQuery(".landing-lesson").html(this.listItems);
            jQuery(".dropdown-content-mobile-lesson").html(this.listItems);
        }
        jQuery(".landing-unit-lesson-dropdown").removeClass("disabled-dropdown");
        jQuery("#lesson-text").text("Lessons All");
        var classNames = document.getElementsByClassName("lesson-dDown_content");
        for (var i = 0; i < classNames.length; i++) {
          classNames[i].addEventListener('click', this.lessonDropDownEvent, false);
        }
      }
    } else {
      jQuery(event.currentTarget).removeClass(value);
    }
  }


}
