import { TestComponentBuilder } from '@angular/compiler/testing';
import { DmcFooterComponent } from '../Footer/dmc.footer.component';
import { DmcService} from '../../shared/dmc.service';
import { ROUTER_DIRECTIVES, Router, ActivatedRoute, UrlPathWithParams, RouterLink  } from '@angular/router';
import {Http, Response, HTTP_PROVIDERS} from '@angular/http';

import {
    beforeEachProviders,
    async,
    describe,
    expect,
    inject,
    it
} from '@angular/core/testing';
import {DmcLandingComponent} from './dmc.landing.component';


export function main() {
    let data = [{ "select_unit_text": "unit1", "select_lesson_text": "unit1", "comingsoon_text": "unit1" }, {}, { "unitArray": ["unit1", "unit2"], "unitLesson": ["unit1", "unit2"], "student_header_logo": "unit1", "show_resources_for_content": "unit1", "hmh_title": "unit1", "teacher_header_logo": "unit1", "landing_title": "unit1" }];
    
    let routekey="practice";
    let textValue="unit 1";
    
    let dmclanding = new DmcLandingComponent(null, null, null, null);
    describe('DmcLandingComponent', () => {
        it('assign json value', () => {
            const result = dmclanding.assignJson(data);
            expect(result[0]).toEqual(["unit1", "unit2"]);
            expect(result[1]).toEqual(["unit1", "unit2"]);
            expect(result[3]).toEqual("unit1");
            expect(result[4]).toEqual("unit1");
            expect(result[5]).toEqual("unit1");
            expect(result[6]).toEqual("unit1");
            expect(result[7]).toEqual("unit1");
            expect(result[8]).toEqual("unit1");
            expect(result[9]).toEqual("unit1");
        })
    });
    
    
      describe('tileClickEvent', () => {
        it('tile click events', () => {
            const result = dmclanding.tileClickEvent(routekey);
            expect(routekey).toEqual("practice");
        })
    });
    
     describe('clickHoverEvent', () => {
        it('click hover events', () => {
            const result = dmclanding.clickHoverEvent(null,"unit-clicked",textValue);
            expect(textValue).toEqual("unit 1");
        })
    });


}
