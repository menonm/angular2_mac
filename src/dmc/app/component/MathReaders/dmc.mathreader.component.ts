import { Component, Pipe } from '@angular/core';
import { DmcService } from '../../shared/dmc.service';
import { ROUTER_DIRECTIVES } from '@angular/router';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { DmcFooterComponent } from '../Footer/dmc.footer.component';
import { DmcHeaderComponent } from '../Header/dmc.header.component';
import {TruncatePipe} from '../../shared/truncatepipe';
import { DmcFilterComponent } from '../../shared/mxdmcfilter.common';
import { DmcWideTileComponent } from '../WideTile/dmc.widetile.component';
import { DmcPopupComponent } from '../Popup/dmc.popup.component';

declare var jQuery: any;
@Component({
  moduleId: module.id,
  selector: 'mathreader',
  templateUrl: './dmc.mathreader.component.html',
  pipes: [TruncatePipe],
  directives: [ROUTER_DIRECTIVES, DmcFooterComponent, DmcHeaderComponent, DmcPopupComponent, DmcWideTileComponent],
  providers: [DmcService],
})




export class DmcMathReaderComponent {
  /**
 * Variable to store user value
 */
  user: string;
  /**
  * Variable to store teacher library items 
  */
  displayContent: any = [];
  /**
 * Variable to store url value
 */
  url: any;
  /**
  * Variable to store mathreader length
  */
  itemCount: number;
  /**
 * Variable to event type 
 */
  eventType: any;
  /**
  * Object to store current page title
  */
  filter: any = {};
  /**
 * Array to store all units 
 */
  title: string;
  /**
	 * Array to store thumbnail images
	 */
  thumbnailsImages: any = [];
  /**
 * Variable to path value
 */
  path: string;
  /**
  * instance of filter component file 
  */
  commonClass: DmcFilterComponent.filter;
  /**
  * Variable to mathreader data
  */
  mathreaderData: any;
  /**
	 * variable to store truncateValue value
	 */
  truncateValue: number;
  /**
	 * variable to store moreinfo value
	 */
  moreInfo: string;

  /**
   * Added for color configurable
  */
  /**
 * variable to store commonValues
 */
  commonValues: any = {};
  /**
  * variable to store tile  color
  */
  tileColor: string;
  /**
  * variable to store tile  color
  */
  tile_Color: string;
  /**
  * variable to store on tile border color
  */
  studentTileBorderColor: string;
  /**
	 * variable to store landing tile color
	 */
  landingTileColor: string;
  /**
  * variable to store currentRouteKey
  */
  currentRouteKey: string;
  /**
 * variable to store tilecopy
 */
  titleCopy: string;
  /**
	 * variable to store background color
	 */
  backgroundColor: string;
  /**
	 * variable to store on tile border color
	 */
  tileBorderColor: string;
  /**
	 * variable to store items text
	 */
  items_text: string;
  /**
	 * variable to store  unit text
	 */
  unit_text: string;
    /**
	 * variable to store  mobile or not
	 */
  isMobile:boolean;
  
  
  isPotrait:boolean;

  /**
   * get dmc service and routing service
   * @constructor 
   * @param {_dmcService} get _dmcService
   * @param {router} get routing service
   */
  constructor(public _dmcService: DmcService, private router: Router, private route: ActivatedRoute) {
    this.path = document.getElementById("dmc-main-container").getAttribute("path");
    this.commonClass = new DmcFilterComponent.filter();
    Object(document).self = this;
    Object(window).self = this;
    this.user = this.commonClass.getUser();
    this.eventType = this.commonClass.getEventType();
    this.url = Object(router).currentRouterState.snapshot.url;
    this.currentRouteKey = Object(route).url._value[0].path;
    this.mathreaderData = jQuery('body').data();
    this.truncateValue = jQuery(window).width() <= 1024 ? jQuery(window).width() <= 961 ? 40 : 50 : 65;
    this.assignJson(this.mathreaderData);
    this.isMobile=this.commonClass.isMobile();
    this.isPotrait=this.commonClass.isOrientation();
   
  }


  /**
    * ngAfterViewInit
    */

  ngAfterViewInit() {
    this.user == "student" ? jQuery(".main-container").addClass("student-main") : jQuery(".main-container").addClass("teacher-main");
    this.commonClass.enableDisableContainer(this.user);
    this.moreInfo != undefined && this.moreInfo != "" ? jQuery(".more-info-wrapper").removeClass("display-none") : "";
  }

  /**
   * assign json data to an object
   * @method assignJson
   * @param {data} data from json file
   */
  assignJson(data: any) {
    this.items_text = data[12].items_text;
    this.unit_text = data[12].unit_text;
    let itemLength = this.user == "student" ? data[8].teacher[0].libraryItems.length : data[8].student.length;
    let remainingItems = itemLength - Math.round(itemLength / 2)
    if (this.user == "student") {
      this.filter["mathreader"] = data[8].student
      this.filter["mathreaderleft"] = data[8].student.slice(0, Math.round(itemLength / 2));
      this.filter["mathreaderright"] = this.filter["mathreader"].slice(-remainingItems);
    } else {
      this.filter["mathreader"] = data[8].teacher[0].libraryItems;
      this.filter["mathreaderleft"] = data[8].teacher[0].libraryItems.slice(0, Math.round(itemLength / 2));
      this.filter["mathreaderright"] = this.filter["mathreader"].slice(-remainingItems);
    }
    this.itemCount = this.filter["mathreader"].length;
    this.thumbnailsImages = data[11].thumbanailsimages;
    this.commonValues = this.user == "student" ? data[17].student : data[17].teacher;
    for (var i = 0; i < this.commonValues.length; i++) {
      if (this.commonValues[i].routeKey == this.currentRouteKey) {
        this.tileColor = this.commonValues[i].color;
        this.moreInfo = this.commonValues[i].moreInfo;
        this.title = this.commonValues[i].title;
        this.titleCopy = this.title;
        this.backgroundColor = this.commonValues[i].header_background_color;
        this.tile_Color = this.commonValues[i].tile_color;
        this.tileBorderColor = this.commonValues[i].tile_border_color;

      }
    }
  }


  /**
    * drop down arrow
    * @method dropdownArrowClick
    * @param {event} event type
      */

  dropdownArrowClick(event: Event) {
    this.commonClass.drpDownArrowClick(jQuery(event.currentTarget).attr("id"));
  }


  /**
   * click on tile
   * @method tilesClickEvent
   * @param {routekey} routekey to navigate
   */
  tileClickEvent(event: Event) {
    let url = Object(event).value;
    this.tileEvent(url)
  }




  /**
    * drop down arrow
    * @method tileClickEventStudent
    * @param {event} event type
    * @param {url} url type 
      */
  tileClickEventStudent(event: Event, url: string) {
    this.tileEvent(url)
  }


  /**
  * tile event 
  * @method tileEvent
  * @param {url} url type 
    */
  tileEvent(url: string) {
    jQuery('.wide-tile').removeClass("tile-clicked").addClass("hover");
    this.commonClass.openInNewTab(url, this.user, '');

  }




  /**
   * click on teacher resources
   * @method clickInactive
   * @param {event} clickInactive
   * @param {id} element id 
   */
  clickInactive(event: Event, id: string) {
    this.user == "student" ? jQuery("#" + id).removeClass("tilesActive") : jQuery("#" + id).removeClass("teacherTileActive");
  }
  /**
   * click on teacher resources
   * @method clickActive
   * @param {event} click active
   * @param {id} element id 
   */
  clickActive(event: Event, id: string) {
    this.user == "student" ? jQuery("#" + id).addClass("tilesActive") : jQuery("#" + id).addClass("teacherTileActive");
  }
}
