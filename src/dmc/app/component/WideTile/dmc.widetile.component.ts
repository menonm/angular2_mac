import { Component, Pipe, Input, Output, EventEmitter } from '@angular/core';
import { DmcService } from '../../shared/dmc.service';
import { DmcFilterComponent } from '../../shared/mxdmcfilter.common';
import {TruncatePipe} from '../../shared/truncatepipe';
declare var jQuery: any;
@Component({
  moduleId: module.id,
  selector: 'wide-tile',
  templateUrl: './dmc.widetile.component.html',
  pipes: [TruncatePipe],
  providers: [DmcService]
})


export class DmcWideTileComponent {
  /**
  * instance of filter component file 
  */
  commonClass: DmcFilterComponent.filter;
  /**
  * variable to store math value
  */
  Math: any;
  /**
	 * variable to store unit regex
	 */
  pattunit = /U\d+/;
  /**
  * variable to store lesson regex
  */
  pattlesson = /L\d+/;
  /**
  * variable to store digit regex
  */
  pattdigit = /\d+/;
  /**
 * variable to store truncateValue value
 */
  truncateValue: number;
  /**
     * variable input to store enableBreadcrumbUnit value
     */
  @Input() enableBreadcrumbUnit: string;
  /**
     * variable input to store enableBreadcrumbLesson value
     */
  @Input() enableBreadcrumbLesson: string;
  /**
	 * variable input to store enableBreadcrumbBigIdea value
	 */
  @Input() enableBreadcrumbBigIdea: string;
  /**
 * variable input to store borderColor value
 */
  @Input() borderColor: string;
  /**
* variable input to store blackLineColor value
*/
  @Input() blackLineColor: string;
  /**
* variable input to store filter value
*/
  @Input() filter: any;
  /**
* variable input to store searchList value
*/
  @Input() searchList: any = [];
  /**
* variable input to store tileType value
*/
  @Input() tileType: string;
  /**
* variable input to store dataStyle value
*/
  @Input() dataStyle: string;
  /**
* variable input to store dataStyle value
*/
  @Input() filterRight: any = [];
  /**
* variable input to store filterLeft value
*/
  @Input() filterLeft: any = [];
  /**
* variable input to store wideTileClicks value
*/
  @Output() wideTileClick: EventEmitter<any> = new EventEmitter();



  /**
    constructor
     */

  constructor() {
    this.commonClass = new DmcFilterComponent.filter();
    this.Math = Math;
  }



  /**
   * set no of items
   * @method ngAfterContentChecked 
   */

  ngAfterContentChecked() {

    if (this.dataStyle == 'column') {
      this.truncateValue = jQuery(window).width() <= 1024 ? jQuery(window).width() <= 961 ? 40 : 50 : jQuery(window).width() <= 1281 ? 62 : 65;
         if(jQuery(window).width() <= 768)
      {
         this.truncateValue = 85;
      }

    } else {

      this.truncateValue = jQuery(window).width() <= 1024 ?  jQuery(window).width() <= 961? 63 : 76 : jQuery(window).width() <= 1282 ? 105 : 115;
        if(jQuery(window).width() <= 768)
      {
         this.truncateValue = 90;
      }
      
      
    }
  }


  /**
     * getUnit
     * @method getUnit
     */
  getUnit(id: any) {
    let unitNo = this.pattdigit.exec(this.pattunit.exec(id)[0])[0];
    if (unitNo.match("^0")) {
      unitNo = unitNo.slice(1);
    }
    return "Unit " + unitNo;
  }


  /**
     * getLesson
     * @method getLesson
     */
  getLesson(id: any) {
    let lessonNo = this.pattdigit.exec(this.pattlesson.exec(id)[0])[0];
    if (lessonNo.match("^0")) {
      lessonNo = lessonNo.slice(1);
    }
    return "Lesson " + lessonNo;
  }


  /**
   * dropdownArrowClick
   * @method dropdownArrowClick
   */

  dropdownArrowClick(event: Event) {
    this.commonClass.drpDownArrowClick(jQuery(event.currentTarget).attr("id"));
  }


  /**
 * setBoolean
 * @method setBoolean
 */

  setBoolean(list: string, id: string) {
    if (list !== undefined) {
      let tempList = this.getData(list)
      if (jQuery.inArray(id, tempList) != -1) {
        return 1;
      } else {
        return 0;
      }
    } else {
      return 0;
    }

  }


  /**
 * getData
 * @method getData
 */

  getData(list: any) {
    let standards: any = [];
    let myarray = list.split(', ');
    for (var f = 0; f < myarray.length; f++) {
      standards.push(myarray[f]);
    }
    return standards
  }

  /**
* tileClickEvent
* @method tileClickEvent
*/

  tileClickEvent(url: string) {
    this.wideTileClick.emit({
      value: url
    })
  }
}