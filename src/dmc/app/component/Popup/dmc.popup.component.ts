import { Component, Pipe, Input, Output, EventEmitter } from '@angular/core';
import { DmcService } from '../../shared/dmc.service';
import { DmcFilterComponent } from '../../shared/mxdmcfilter.common';

@Component({
  moduleId: module.id,
  selector: 'dmc-popup',
  templateUrl: './dmc.popup.component.html',
  providers: [DmcService]
})


export class DmcPopupComponent {
  /**
  * instance to store filter component
  */
  commonClass: DmcFilterComponent.filter;
  /**
 * variable to store information Title
 */
  @Input() infoTitle: string;
  /**
* variable to store filter standard item  
*/
  @Input() filterStdItem: any;
  /**
* variable to store standard category
*/
  @Input() stdCategory: string;
  /**
* variable to store common value
*/

  @Input() common: string;
  /**
* variable to store more info games title
*/
  @Input() moreInfoGamesTitle: string;
  /**
* variable to store dataStyle
*/
  @Input() dataStyle: string;
  /**
* variable to store standards data
*/
  @Input() dataStandards: string;
  /**
* variable to store standards json
*/
  @Input() stdJson: string;
  /**
* variable to store event emitting hideStandardsPoup
*/
  @Output() hideStandardsPoup: EventEmitter<any> = new EventEmitter();

  /**
* variable to store event emitting showResultStandards
*/
  @Output() showResultStandards: EventEmitter<any> = new EventEmitter();
  /**
* variable to store event emitting removeSelectionStandards
*/

  @Output() removeSelectionStandards: EventEmitter<any> = new EventEmitter();
  /**
* variable to store event emitting removeselectStandardsList
*/
  @Output() removeselectStandardsList: EventEmitter<any> = new EventEmitter();

  /**
    * constructor
     */
  constructor(public _dmcService: DmcService) {
    this.commonClass = new DmcFilterComponent.filter();

  }

  /**
    * method to hide standards 
    * @method hideStandards
    */
  hideStandards() {
    this.hideStandardsPoup.emit({});
  }


  /**
    * method to show result
    * @method showResult
    */
  showResult() {
    this.showResultStandards.emit({});
  }


  /**
    * method to remove selection
    * @method removeSelection
    * @param {id} indicates the selected standard to be removed
    */
  removeSelection(id: any) {
    this.removeSelectionStandards.emit({ value: id });
  }



  /**
    * method to select standards
    * @method selectStandards
    *  @param {list} contains the standards to be removed
    */
  selectStandards(list: any) {
    this.removeselectStandardsList.emit({ value: list });
  }


}