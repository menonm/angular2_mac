import { Component, Pipe } from '@angular/core';
import { DmcService } from '../../shared/dmc.service';
import { ROUTER_DIRECTIVES } from '@angular/router';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { DmcFooterComponent } from '../Footer/dmc.footer.component';
import { DmcHeaderComponent } from '../Header/dmc.header.component';
import { DmcFilterComponent } from '../../shared/mxdmcfilter.common';
import {TruncatePipe} from '../../shared/truncatepipe';
import { DPOService } from '../../shared/dpo.service';
import { DmcPopupComponent } from '../Popup/dmc.popup.component';
import { DmcWideTileComponent } from '../WideTile/dmc.widetile.component';
import { DmcFilterByComponent } from '../FilterBy/dmc.filterby.component';

declare var jQuery: any;
declare var PerfectScrollbar: any;
@Component({
  moduleId: module.id,
  selector: 'challenge',
  templateUrl: './dmc.challenge.component.html',
  pipes: [TruncatePipe],
  directives: [ROUTER_DIRECTIVES, DmcFooterComponent, DmcHeaderComponent, DmcPopupComponent, DmcWideTileComponent, DmcFilterByComponent],
  providers: [DmcService]
})




export class DmcChallengeComponent {
  /**
  * Variable to store user value
  */
  user: string;
  /**
 * Variable to store teacher library items 
 */
  displayContent: any = [];
  /**
  * Variable to store student library items 
  */
  studentDisplayContent: any = [];
  /**
* Variable to store url value
*/
  url: any;
  /**
* Variable to event type 
*/
  eventType: any;
  /**
* Variable to store total number of items
*/
  itemsChallenge: number;
  /**
  * Variable to store dropdown items
  */
  listItems: any = [];
  /**
	 * Variable to store bigideas
	 */
  bigIdeaItems: any = [];
  /**
	 * Object to store challenge data
	 */

  filter: any = {};
  /**
	 * Object to store current page title
	 */
  title: string;
  /**
	 * Array to store all units 
	 */
  units: any = [];
  /**
 * Array to store all lessons dropdown items
 */
  lessons: any = [];
  /**
* Array to store all bigidea dropdown items 
*/
  bigIdea: any = [];
  /**
* Object for temporary storage
*/
  obj: any = [];
  /**
 * Object that holds complete info
 */
  unitLessonData: any = {};

  /**
 * arrays that holdsunit
 */
  unitData: any = [];

  /**
	 * variable to store number of items
	 */
  items: number;
  /**
	 * object  that hold all items of student 
	 */
  studentUnitLessonData: any = {};
  /**
 * instance of filter component file 
 */
  commonClass: DmcFilterComponent.filter;
  /**
	 * variable to store unit title
	 */
  tileTitle: any = "Unit";
  /**
 * variable to store changing filtered array
 */

  filterStdArray: any = [];
  /**
	 * variable to store old title
	 */
  oldTitle: string;
  /**
  * variable to store unit title
  */

  unitTitle: string;
  /**
 * variable to store lesson title
 */
  lessonTitle: string;
  /**
  * variable to store original units
  */
  unitsOriginal: any;
  /**
	 * variable to store relative urls
	 */
  urls: any = [];
  /**
	 * variable to store lesson dropdown object
	 */
  lessonData: any = [];
  /**
	 * variable to store values of landing page
	 */
  landingJson: any = {};
  /**
	 * variable to store current page count
	 */
  pageCount: number = 1;
  /**
	 * variable to store total page count
	 */
  totalCount: number;
  /**
	 * variable to store  page start count
	 */
  startCount: number = 1;
  /**
  * variable to store current page id
  */
  currentPageId: number = 0;
  /**
    * variable to keep copy of student data
    */
  studentDummyContent: any = [];
  /**
	 * variable to keep copy of teacher data
	 */
  teacherDummyContent: any = [];
  /**
	 * variable to store teacher content
	 */
  teacherOriginalContent: any = [];
  /**
  * variable to store math value
  */
  Math: any;
  /**
	 * variable to store challenge data
   * 
   *  */
  challengeData: any;
  /**
	 * variable to store moreinfo value
	 */
  moreInfo: string;
  /**
	 * variable to store standards value
	 */
  mainStandardsArray: any = [];
  /**
    * variable to store standards code value
    */
  standardCategory: any = ["CC", "SP", "G", "EE", "NS", "RP", "MD", "NF", "NBT", "OA", "MP"];

  /**
	 * variable to store standards data
	 */
  standardsData: any = [];

  /**
  * variable to store grouped standards
  */
  groupedStandards: any = [];

  /**
  * variable to store new standards
  */
  newStandards: any = [];

  /**
	 * variable to common json value
	 */
  commonJson: any = {};
  /**
	 * variable to store standards json value
	 */
  standardsJson: any = {};
  /**
   * Added for color configurable
  */
  /**
  * variable to store common values
  */
  commonValues: any = {};
  /**
	 * variable to store currentRouteKey value
	 */
  currentRouteKey: string;
  /**
	 * variable to store title
	 */
  titleCopy: string;
  /**
 * variable to store background color
 */
  backgroundColor: string;
  /**
* variable to store tileColor
*/
  tileColor: string;
  /**
	 * variable to store tileBorderColor
	 */
  tileBorderColor: string;
  /**
 * variable to store tile_Color
 */
  tile_Color: string;
  /**
   * Added For DP
   */
  /**
  * variable to store bigIdeaCheck value
  */
  bigIdeaCheck: boolean = true;
  /**
	 * variable to store lessonCheck value
	 */
  lessonCheck: boolean = true;
  /**
  * variable to store truncateValue value
  */
  truncateValue: number;
  /**
  * variable to store serach list value
  */
  searchList: any = [];

  /**
 * variable to store previous text
 */
  dropdownClicked: boolean = false;
  /**
	 * variable to store previous text
	 */
  previous_text: string;
  /**
 * variable to store next text
 */
  next_text: string;
  /**
  * variable to store items text
  */
  items_text: string;
  /**
	 * variable to store select new standards text
	 */
  select_new_standards: string;
  /**
	 * variable to store edition text
	 */
  edition_text: string;

  /**
  * variable to store standards text
  */
  standards_text: string;
  /**
	 * variable to store teacher text
	 */
  teacher_text: string;
  /**
  * variable to store student text
  */
  student_text: string;
  /**
* variable to store interactice comming soon text
*/
  interactive_comming_soon_text: string;
  /**
  * variable to results not found text
  */
  results_not_found: string;

  /**
  * variable to store worksheet text
  */
  worksheet_text: string;
  /**
	 * variable to store select unit text
	 */
  select_unit_text: string;
  /**
* variable to store select lesson text
*/
  select_lesson_text: string;
  /**
  * variable to store select big idea text
  */
  select_bigidea_text: string;


  /**
   * get dmc service and routing service
   * @constructor 
   * @param {_dmcService} get _dmcService
   * @param {router} get routing service
   */
  constructor(public _dmcService: DmcService, private router: Router, private route: ActivatedRoute) {
    Object(document).self = this;
    Object(window).self = this;
    this.commonClass = new DmcFilterComponent.filter();
    this.Math = Math;
    this.eventType = this.commonClass.getEventType();
    this.user = this.commonClass.getUser();
    this.user == "student" ? this.title = "Lesson Challenge" : "";
    this.url = Object(router).currentRouterState.snapshot.url;
    this.currentRouteKey = Object(route).url._value[0].path;
    this.challengeData = jQuery('body').data();
    this.truncateValue = jQuery(window).width() <= 1024 ? jQuery(window).width() <= 961 ? 63 : 76 : jQuery(window).width() <= 1281 ? 110 : 115;
    this.assignJson(this.challengeData[2], this.challengeData[12], this.challengeData[15], this.challengeData[17]);
  }


  /**
     * ngAfterViewInit
     */
  ngAfterViewInit() {
    this.commonClass.enableDisableContainer(this.user);
    this.user == "student" ? jQuery(".main-container").addClass("student-main") : jQuery(".main-container").addClass("teacher-main");
    let scope = this;
    DPOService.gObj.dpoGlbService.initDPO({
      ready: function (obj: any) {
        console.log('DPO Service is ready for currentpage  ', obj.pageData);

        let unitText = obj.pageData['unit-text-dropdown'].elmContent.trim();
        let bigIdeaText = obj.pageData['bigIdea-text'].elmContent.trim();
        let lessonText = obj.pageData['lesson-text'].elmContent.trim();
        scope.filterStdArray = [];
        scope.filterStdArray.length = 0;
        for (var key in obj.pageData) {
          if (key.indexOf('standards-list') >= 0) {
            scope.filterStdArray.push(obj.pageData[key].elmContent.trim())
          }
        }

        // Unit Text Val Dropdown
        if (obj.pageData['unit-text-dropdown'].elmContent.trim() !== "Select Unit") {

          jQuery('.dropdown-text').each(function () {
            let innerText = jQuery(this).text();
            if (innerText === unitText) {
              jQuery(this).trigger('click');
            }
          });

          // Big Data Dropdown
          if (obj.pageData['bigIdea-text'].elmContent.trim() !== "Select Big Idea") {
            jQuery('#BigIdeaDropdown .dropdown-text').each(function () {
              let innerText = jQuery(this).text();
              if (innerText === bigIdeaText) {
                jQuery("#bigIdea-text").attr('name', bigIdeaText)
                jQuery(this).trigger('click');
                jQuery("#BigIdeaDropdown").removeClass('lesson-clicked');
                jQuery(this.parentElement).addClass('lesson-clicked');
              }
            });
          }

          // Lesson Dropdown
          if (obj.pageData['lesson-text'].elmContent.trim() !== "Select Lesson") {
            jQuery('#LessonDropdown .dropdown-text').each(function () {
              let innerText = jQuery(this).text();

              if (innerText === lessonText) {
                jQuery("#lesson-text").attr('name', lessonText)
                jQuery(this).trigger('click');
                jQuery("#LessonDropdown").removeClass('lesson-clicked');
                jQuery(this.parentElement).addClass('lesson-clicked');

              }
            });

          }
        }

        // CheckBox DropDown checkbox-clicked 
        jQuery(".checkbox-clicked").each(function () {
          jQuery(this).removeClass('checkbox-clicked');
          jQuery(this).trigger('click')
        });

      }
    });
    this.moreInfo != undefined && this.moreInfo != "" ? jQuery(".more-info-wrapper").removeClass("display-none") : "";
  }

  /**
   * close drop down click on document click
   * @method documentBodyClick
   * @param {event}  click event
   */
  documentBodyClick(event: Event) {
    if (jQuery(".main-container").hasClass("challenge")) {
      let id = jQuery(event.target).attr("id");
      let className = jQuery(event.target).attr("class");

      if (className != "dropdown-text" && className != "dropdown-arrow dropdown-down-arrow" && id != "dropdown1" && id != "dropdown2" && id != "dropdown3" && id != "lesson-text" && id != "unit-text-dropdown" && id != "bigIdea-text") {
        if (jQuery(".dropbtn").hasClass("dropdown-clicked") == true) {
          jQuery(".dropdown-content").removeClass("show");
          jQuery(".dropbtn").addClass("hover").removeClass("dropdown-clicked");
        }
        Object(document).removeEventListener(Object(event.currentTarget).self.eventType, Object(event.currentTarget).self.documentBodyClick, false);
      }
    }
  }

  /**
   * assign json data to an object
   * @method assignJson
   * @param {data} data from json file
   */
  assignJson(data: any, moreInfo: any, standardsDescription: any, commonValues: any) {
    if (data != undefined) {
      jQuery(".loading-div").addClass("display-none");
    }
    var pattunit = /U\d+/;
    var pattlesson = /L\d+/;
    var pattdigit = /\d+/;
    let unitIndex: any = [];
    let lessonurls: any = [];
    let standards: any = [];



    this.studentDummyContent = data.student.slice();

    this.commonJson = moreInfo;

    this.previous_text = this.commonJson.previous_text;
    this.next_text = this.commonJson.next_text;
    this.items_text = this.commonJson.items_text;
    this.select_new_standards = this.commonJson.select_new_standards;
    this.edition_text = this.commonJson.edition_text;
    this.standards_text = this.commonJson.standards_text;
    this.teacher_text = this.commonJson.teacher_text;
    this.student_text = this.commonJson.student_text;
    this.interactive_comming_soon_text = this.commonJson.interactive_comming_soon_text;
    this.results_not_found = this.commonJson.results_not_found;
    this.worksheet_text = this.commonJson.worksheet_text;
    this.select_unit_text = this.commonJson.select_unit_text;
    this.select_lesson_text = this.commonJson.select_lesson_text;
    this.select_bigidea_text = this.commonJson.select_bigidea_text;
    this.standardsJson = standardsDescription;

    this.landingJson = commonValues;
    this.commonValues = this.user == "student" ? commonValues.student : commonValues.teacher;
    for (var i = 0; i < this.commonValues.length; i++) {
      if (this.commonValues[i].routeKey == this.currentRouteKey) {
        this.tileColor = this.commonValues[i].color;
        this.moreInfo = this.commonValues[i].moreInfo;
        this.title = this.commonValues[i].title;
        this.titleCopy = this.title;
        this.backgroundColor = this.commonValues[i].header_background_color;

        this.tileBorderColor = this.commonValues[i].tile_border_color;
        this.tile_Color = this.commonValues[i].tile_color;
      }
    }
    if (this.user == "teacher") {
      this.displayContent = data.teacher[0].libraryItems.slice();
      this.teacherOriginalContent = this.displayContent.slice();
      jQuery.merge(this.displayContent, this.studentDummyContent);
    } else {
      this.displayContent = this.studentDummyContent.slice();
    }

    //this.moreInfo = moreInfo.Practice_Reteach_Challenge;

    this.filter["challenge"] = this.displayContent.slice();
    this.filter["challengeOriginal"] = this.displayContent.slice();
    let dataPractice = this.displayContent.slice();
    //all units added
    //this.units.push("All Units");

    for (var i = 0; i < dataPractice.length; i++) {
      if (pattunit.test(dataPractice[i].HMH_ID)) {
        if (("Unit" + parseInt(pattdigit.exec(pattunit.exec(dataPractice[i].HMH_ID)[0])[0]) in this.unitLessonData) == false) {
          this.bigIdea = [];
          this.lessons = [];
          this.obj = [];
          standards = [];


          this.lessons.push("Lesson " + parseInt(pattdigit.exec(pattlesson.exec(dataPractice[i].HMH_ID)[0])[0]));
          this.bigIdea.push(dataPractice[i].bigidea);
          this.obj.push(dataPractice[i]);






          this.unitLessonData["Unit" + parseInt(pattdigit.exec(pattunit.exec(dataPractice[i].HMH_ID)[0])[0])] = { lesson: this.lessons.sort(), bigIdea: this.bigIdea.sort(), ustd: standards, obj: this.obj };
          this.units.push("Unit " + parseInt(pattdigit.exec(pattunit.exec(dataPractice[i].HMH_ID)[0])[0]));
          this.unitLessonData["Unit" + parseInt(pattdigit.exec(pattunit.exec(dataPractice[i].HMH_ID)[0])[0]) + "unitindex"] = i;
        } else {

          //clearing all array values before entering the existing  unit value 
          this.bigIdea = [];
          this.lessons = [];
          this.obj = [];
          standards = [];


          //condition to fill existing data of that particular unit in array 
          this.bigIdea = this.unitLessonData["Unit" + parseInt(pattdigit.exec(pattunit.exec(dataPractice[i].HMH_ID)[0])[0])].bigIdea;
          this.lessons = this.unitLessonData["Unit" + parseInt(pattdigit.exec(pattunit.exec(dataPractice[i].HMH_ID)[0])[0])].lesson;
          this.obj = this.unitLessonData["Unit" + parseInt(pattdigit.exec(pattunit.exec(dataPractice[i].HMH_ID)[0])[0])].obj;
          standards = this.unitLessonData["Unit" + parseInt(pattdigit.exec(pattunit.exec(dataPractice[i].HMH_ID)[0])[0])].ustd;
          let stdArray: any = [];

          jQuery.inArray(dataPractice[i].bigidea, this.bigIdea) == -1 ? this.bigIdea.push(dataPractice[i].bigidea) : "";
          jQuery.inArray("Lesson " + parseInt(pattdigit.exec(pattlesson.exec(dataPractice[i].HMH_ID)[0])[0]), this.lessons) == -1 ? this.lessons.push("Lesson " + parseInt(pattdigit.exec(pattlesson.exec(dataPractice[i].HMH_ID)[0])[0])) : "";
          this.obj.push(dataPractice[i]);
          this.unitLessonData["Unit" + parseInt(pattdigit.exec(pattunit.exec(dataPractice[i].HMH_ID)[0])[0])] = { lesson: this.lessons.sort(), bigIdea: this.bigIdea.sort(), ustd: standards, obj: this.obj }
        }
      }
      this.filter["teacherlessondata"] = this.unitLessonData;

    }

    for (var i = 0; i < this.mainStandardsArray.length; i++) {
      for (var j = 0; j < this.standardCategory.length; j++) {
        if (this.mainStandardsArray[i].indexOf(this.standardCategory[j]) != -1) {
          if ((this.standardCategory[j] in this.standardsData) == false) {
            this.groupedStandards = [];
            this.groupedStandards.push(this.mainStandardsArray[i]);
            this.standardsData[this.standardCategory[j]] = { standards: this.groupedStandards };
            this.newStandards.push(this.standardCategory[j]);
          } else {
            this.groupedStandards = [];
            this.groupedStandards = this.standardsData[this.standardCategory[j]].standards;
            jQuery.inArray(this.mainStandardsArray[i], this.groupedStandards) == -1 ? this.groupedStandards.push(this.mainStandardsArray[i]) : "";
            this.standardsData[this.standardCategory[j]] = { standards: this.groupedStandards };
          }
        }
      }
    }



    this.unitsOriginal = this.units;
    this.filter["studentUnitLessonData"] = this.studentUnitLessonData;
    this.setItemsCount(this.filter["challenge"].slice().length);
    this.pageCount = Math.floor(this.filter["challenge"].slice().length / 20);
    this.filter["challenge"].slice().length / 20 != this.pageCount ? this.pageCount = this.pageCount + 1 : "";
    this.totalCount = this.pageCount > 1 ? this.filter["challenge"].slice().length > 20 ? 20 : this.filter["challenge"].slice().length : this.filter["challenge"].slice().length;
  }

  /**
   * toggle drop down
   * @method toggleDropDown
   * @param {event} click event
   * @param {DropdownShow} class name to remove or add
   */
  toggleDropDown(event: Event, DropdownShow: string, DropdownHide: any) {
    this.toggleDropDownState(event, DropdownShow, DropdownHide);
  }

  /**
   * toggle drop down state change
   * @method toggleDropDownState
   * @param {event} click event
   * @param {DropdownShow} class name to remove or add
   */

  toggleDropDownState(event: Event, DropdownShow: string, DropdownHide: any) {
    if (jQuery("#" + DropdownShow).hasClass("show") == true) {
      jQuery("#" + DropdownShow).removeClass("show");
      jQuery("#" + DropdownShow).siblings().removeClass("dropdown-clicked")
    } else {
      jQuery("#" + DropdownShow).addClass("show");
      jQuery("#" + DropdownShow).siblings().addClass("dropdown-clicked")
    }
    for (var i = 0; i < DropdownHide.length; i++) {
      jQuery("#" + DropdownHide[i]).removeClass("show");
      jQuery("#" + DropdownHide[i]).siblings().removeClass("dropdown-clicked");
    }
    if (jQuery(event.currentTarget).hasClass("dropdown-clicked") == true) {
      Object(document).addEventListener(this.eventType, this.documentBodyClick, true);
    }
  }

  /**
* Unit dropDownEvent click
* @method unitDropDownEvent
* @param {event} click event
*/
  unitDropDownEvent(event: Event) {
    jQuery(".landing-dropdown-content").children().removeClass("dropdown-clicked");
    jQuery(".unit-filter-dDown_content").removeClass("unit-clicked");
    this.clickHoverEvent(event, "unit-clicked", jQuery(event.currentTarget)[0].innerText);

  }

  /**
* dropDownEvent click
* @method lessonDropDownEvent
* @param {event} click event
*/
  lessonDropDownEvent(event: Event) {
    jQuery(".lesson-dDown_content").removeClass("lesson-clicked");
    jQuery(event.currentTarget).addClass("lesson-clicked");
    let LessonName = jQuery("#lesson-text").attr('name');
    if (LessonName != undefined && this.lessonCheck) {
      jQuery("#lesson-text").text(LessonName);
      this.lessonCheck = false;
    }
    else {
      jQuery("#lesson-text").text(jQuery(event.currentTarget).context.innerText);
    }

    jQuery("#LessonDropdown").removeClass("show");
    jQuery("#LessonDropdown").siblings().removeClass("dropdown-clicked");

    let id = jQuery(event.currentTarget).attr("id");
    let bigIdeaText = jQuery('#bigIdea-text').text();
    let lessonData = jQuery("#LessonDropdown").data("self");
    let bigIdeaData = jQuery("#BigIdeaDropdown").data("self");
    jQuery("#lesson-text").addClass("value-selected");

    let unitText = jQuery('#unit-text-dropdown').text().split(" ").join("");
    let unitData = lessonData.unitLessonData[unitText.trim()];
    let studentUnitData = bigIdeaData.studentUnitLessonData[unitText.trim()];
    let lessonText = jQuery("#lesson-text").text().split(" ").join("");

    let index: any;
    let pdfType: string;

    let pattlesson = /L\d+/;
    let pattdigit = /\d+/;
    lessonData.filter["challenge"] = [];
    lessonData.filter["student"] = [];

    if (jQuery(".checkboxGroupElem").filter('.checkbox-clicked').length == 1) {
      pdfType = jQuery('.checkbox-clicked').attr("id")
    }



    if (pdfType != "student" || pdfType == undefined) {
      for (var l = 0; l < unitData.obj.length; l++) {
        let lesson = "Lesson" + parseInt(pattdigit.exec(pattlesson.exec(unitData.obj[l].HMH_ID)[0])[0]);

        if (bigIdeaText != "Select Big Idea" ? lessonText.trim() == lesson.trim() && jQuery('#bigIdea-text').text().split("Big Idea")[1].trim() == unitData.obj[l].bigidea.split("Big Idea")[1].trim() : lessonText.trim() == lesson.trim()) {
          unitData.obj[l].display_title.split(":")[0].trim() == "TE" ? lessonData.filter["challenge"].push(unitData.obj[l]) : "";
        }
      }
    }
    if (pdfType != "teacher" || pdfType == undefined) {
      for (var l = 0; l < unitData.obj.length; l++) {
        let lessonStudent = "Lesson" + parseInt(pattdigit.exec(pattlesson.exec(unitData.obj[l].HMH_ID)[0])[0]);
        if (bigIdeaText != "Select Big Idea" ? lessonText.trim() == lessonStudent.trim() && jQuery('#bigIdea-text').text().split("Big Idea")[1].trim() == unitData.obj[l].bigidea.split("Big Idea")[1].trim() : lessonText.trim() == lessonStudent.trim()) {
          unitData.obj[l].display_title.split(":")[0].trim() == "SE" ? lessonData.filter["challenge"].push(unitData.obj[l]) : "";
        }
      }
    }


    //added for standards
    //lessonData.filterStandards(jQuery("#unit-text-dropdown").text().trim().split(" ").join(""), jQuery('#bigIdea-text').text(), jQuery("#lesson-text").text(), lessonData.unitLessonData, "filtering")

    lessonData.setItemsCount(lessonData.filter["challenge"].length);
    lessonData.setDefault();
  }

  /**
* Big Idea dropDownEvent click
* @method bigIdeaDropDownEvent
* @param {event} click event
*/
  bigIdeaDropDownEvent(event: Event) {
    jQuery(".bigIdea-dDown_content").removeClass("lesson-clicked");
    jQuery(event.currentTarget).addClass("lesson-clicked");
    let bigIdeaName = jQuery("#bigIdea-text").attr('name');
    if (bigIdeaName != undefined && this.bigIdeaCheck) {
      jQuery("#bigIdea-text").text(bigIdeaName);
      this.bigIdeaCheck = false;
    }
    else {
      jQuery("#bigIdea-text").text(jQuery(event.currentTarget)[0].innerText);
    }

    jQuery("#BigIdeaDropdown").removeClass("show");
    jQuery("#BigIdeaDropdown").siblings().removeClass("dropdown-clicked");
    jQuery("#lesson-text").text("Select Lesson").removeClass("value-selected");;
    let id = jQuery(event.currentTarget).attr("id");
    let lessonData = jQuery("#LessonDropdown").data("self");
    let bigIdeaData = jQuery("#BigIdeaDropdown").data("self");
    jQuery("#bigIdea-text").addClass("value-selected");
    let unitText = jQuery('#unit-text-dropdown').text().split(" ").join("");
    let unitData = lessonData.unitLessonData[unitText.trim()];
    let studentUnitData = bigIdeaData.studentUnitLessonData[unitText.trim()];
    let bigDataText = jQuery("#bigIdea-text").text();
    let index: any;
    let pattlesson = /L\d+/;
    let pattdigit = /\d+/;
    jQuery("#LessonDropdown").empty();
    let listItems = "";
    let pdfType: string;
    bigIdeaData.filter["challenge"] = [];
    bigIdeaData.filter["student"] = [];

    if (jQuery(".checkboxGroupElem").filter('.checkbox-clicked').length == 1) {
      pdfType = jQuery('.checkbox-clicked').attr("id")
    }

    let lessonArray: any = [];
    for (var l = 0; l < unitData.obj.length; l++) {
      console.log(bigDataText.trim() == unitData.obj[l].bigidea)
      if (bigDataText.trim().split(" ").join("") == unitData.obj[l].bigidea.split(" ").join("")) {
        if (pdfType != "student" || pdfType == undefined) {
          unitData.obj[l].display_title.split(":")[0].trim() == "TE" ? bigIdeaData.filter["challenge"].push(unitData.obj[l]) : "";
        }
        jQuery.inArray(parseInt(pattdigit.exec(pattlesson.exec(unitData.obj[l].HMH_ID)[0])[0]), lessonArray) == -1 ? lessonArray.push(parseInt(pattdigit.exec(pattlesson.exec(unitData.obj[l].HMH_ID)[0])[0])) : "";
      }
    }
    for (var l = 0; l < lessonArray.length; l++) {
      listItems += "  <div  class='dDown_content  hover lesson-dDown_content' id=ert" + l + " (click)='lessonDropDownEvent($event)'><div class='dropdown-text'>" + "Lesson " + lessonArray[l] + "</div></div>";
      jQuery("#LessonDropdown").html(listItems);

    }

    var classNames = document.getElementsByClassName("lesson-dDown_content");
    for (var i = 0; i < classNames.length; i++) {
      classNames[i].addEventListener(lessonData.eventType, lessonData.lessonDropDownEvent, false);
    }


    if (pdfType != "teacher" || pdfType == undefined) {
      for (var l = 0; l < unitData.obj.length; l++) {
        console.log(bigDataText.trim().split(" ").join("") == unitData.obj[l].bigidea.split(" ").join(""))
        if (bigDataText.trim() == unitData.obj[l].bigidea) {
          unitData.obj[l].display_title.split(":")[0].trim() == "SE" ? bigIdeaData.filter["challenge"].push(unitData.obj[l]) : "";
        }
      }
    }

    //added for standards
    // bigIdeaData.filterStandards(jQuery("#unit-text-dropdown").text().trim().split(" ").join(""), jQuery('#bigIdea-text').text(), jQuery("#lesson-text").text(), bigIdeaData.unitLessonData, "filtering")


    lessonData.setItemsCount(bigIdeaData.filter["challenge"].length);
    lessonData.setDefault();
  }

  /**
  * dropDownEvent click
  * @method clickHoverEvent
  * @param {event} click event
  * @param {value} which dropdown clicked
  * @param {textValue} dropdown text 
  
  */
  clickHoverEvent(event: Event, value: string, textValue: string) {
    jQuery(".landing-dDown_content").removeClass("lesson-clicked");
    if (jQuery(event.currentTarget).hasClass(value) == false) {
      jQuery(event.currentTarget).removeClass("hover").addClass(value);
      //if (value == "unit-clicked" && textValue.trim()!="All Units") {
      if (value == "unit-clicked") {
        jQuery(".go-button").removeClass("disable-gobutton").removeAttr("disabled");
        jQuery("#lesson-text").text("");
        jQuery("#bigIdea-text").text("");
        jQuery("#unit-text-dropdown").text(textValue);
        jQuery(".lesson-filter-dropdown ").removeClass("disabled-dropdown");
        jQuery(".bigidea-filter-dropdown ").removeClass("disabled-dropdown");
        jQuery("#unit-text-dropdown").addClass("value-selected");
        jQuery("#UnitDropdown").removeClass("show").siblings().removeClass("dropdown-clicked");
        this.listItems = "";
        this.bigIdeaItems = "";
        var lessonText = textValue.split(" ").join("").trim();
        let data = this.unitLessonData[lessonText].lesson;
        let temArray = this.sort(data)

        for (var i = 0; i < temArray.length; i++) {
          this.listItems += "  <div  class='dDown_content  hover lesson-dDown_content' id=ert" + i + " (click)='lessonDropDownEvent($event)'><div class='dropdown-text'>Lesson " + temArray[i] + "</div></div>";
          jQuery("#LessonDropdown").html(this.listItems);
        }

        for (var i = 0; i < this.unitLessonData[lessonText].bigIdea.length; i++) {
          this.bigIdeaItems += "  <div  class='dDown_content  hover bigIdea-dDown_content' id=ert" + i + " (click)='bigIdeaDropDownEvent($event,)'><div class='dropdown-text'>" + this.unitLessonData[lessonText].bigIdea[i] + "</div></div>";
          jQuery("#BigIdeaDropdown").html(this.bigIdeaItems);
        }
        jQuery("#lesson-text").text("Select Lesson");
        jQuery("#bigIdea-text").text("Select Big Idea");
        jQuery("#lesson-text,#bigIdea-text").removeClass("value-selected");
        var classNames = document.getElementsByClassName("lesson-dDown_content");
        var bigIdeaClassName = document.getElementsByClassName("bigIdea-dDown_content");
        var self = this;
        let pdfType: any;
        jQuery("#BigIdeaDropdown").data("self", this);
        jQuery("#LessonDropdown").data("self", this);
        for (var i = 0; i < classNames.length; i++) {
          classNames[i].addEventListener(this.eventType, this.lessonDropDownEvent, false);
        }
        for (var i = 0; i < bigIdeaClassName.length; i++) {
          bigIdeaClassName[i].addEventListener(this.eventType, this.bigIdeaDropDownEvent, false);
        }

        this.filter["challenge"] = [];


        let TE: any = [];
        let SE: any = [];
        let filterValue = this.filter["teacherlessondata"];
        for (var i = 0; i < filterValue[textValue.trim().split(" ").join("")]["obj"].length; i++) {
          if (filterValue[textValue.trim().split(" ").join("")]["obj"][i].display_title.split(":")[0].trim() == "TE") {
            TE.push(filterValue[textValue.trim().split(" ").join("")]["obj"][i]);
          } else {
            SE.push(filterValue[textValue.trim().split(" ").join("")]["obj"][i]);
          }
        }
        if (jQuery(".checkboxGroupElem").filter('.checkbox-clicked').length == 1) {
          pdfType = jQuery('.checkbox-clicked').attr("id")
        }
        if (pdfType == undefined) {
          this.filter["challenge"] = this.unitLessonData[textValue.trim().split(" ").join("")].obj;
        }
        else if (pdfType == "teacher") {
          this.filter["challenge"] = TE;
        } else if (pdfType == "student") {
          this.filter["challenge"] = SE;
        }
      }
    }
    else {
      jQuery(event.currentTarget).removeClass(value);
    }
    this.setItemsCount(this.filter["challenge"].length);
    this.totalCount = this.pageCount > 1 ? this.filter["challenge"].length > 20 ? 20 : this.filter["challenge"].length : this.filter["challenge"].length;
    this.setDefault();
  }


  /**
   * sort the data
   * @method sort
   * @param {data} data to sort
   */

  sort(data: any) {
    let temArray: any = [];
    for (var j = 0; j < data.length; j++) {
      var lesson = parseInt(data[j].split("Lesson")[1].trim())
      temArray.push(lesson)
    }
    return temArray.sort(function (a: any, b: any) { return a - b });
  }

  /**
    * checkboxEvent click
    * @method checkBoxEvent
    * @param {event} click event
    */

  checkBoxEvent(event: Event) {
    this.commonClass.checkBoxEnableDisableEvent(jQuery(event.currentTarget).attr("id"));

    let filterArray: any = [];
    let currentId = jQuery(".checkboxGroupElem").filter('.checkbox-clicked').attr("id");
    let filterValue = this.filter["teacherlessondata"];
    let text = jQuery("#unit-text-dropdown").text().trim().split(" ").join("");
    let bigIdeaText = jQuery('#bigIdea-text').text();
    let lessonText = jQuery("#lesson-text").text();
    let filterData: any;
    let pattlesson = /L\d+/;
    let pattdigit = /\d+/;

    //filtering part of checkbox
    if (jQuery("#unit-text-dropdown").text() == ("Select Unit")) {
      if (jQuery(".checkboxGroupElem").filter('.checkbox-clicked').length == 1) {
        this.filter["challenge"] = [];
        this.filter["challenge"] = currentId == "teacher" ? this.teacherOriginalContent : this.studentDummyContent;
      } else {
        this.filter["challenge"] = this.filter["challengeOriginal"];
      }
    }
    else {
      let TE: any = [];
      let SE: any = [];

      for (var i = 0; i < filterValue[text]["obj"].length; i++) {
        if (filterValue[text]["obj"][i].display_title.split(":")[0].trim() == "TE") {
          TE.push(filterValue[text]["obj"][i]);
        } else {
          SE.push(filterValue[text]["obj"][i]);
        }
      }
      if (bigIdeaText == "Select Big Idea" && lessonText == "Select Lesson") {
        this.filter["challenge"] = [];
        if (jQuery(".checkboxGroupElem").filter('.checkbox-clicked').length == 1) {
          currentId == "teacher" ? this.filter["challenge"] = TE : this.filter["challenge"] = SE;
        } else {
          this.filter["challenge"] = this.filter["teacherlessondata"][text]["obj"];
        }
      }
      if (bigIdeaText != "Select Big Idea" || lessonText != "Select Lesson") {
        this.filter["challenge"] = [];
        if (jQuery(".checkboxGroupElem").filter('.checkbox-clicked').length == 1) {
          filterData = currentId == "teacher" ? TE : SE;
          for (let i = 0; i < filterData.length; i++) {
            let lesson = "Lesson " + parseInt(pattdigit.exec(pattlesson.exec(filterData[i].HMH_ID)[0])[0]);
            if (bigIdeaText != "Select Big Idea" && lessonText != "Select Lesson" ? jQuery('#bigIdea-text').text().trim() == this.filter["teacherlessondata"][text]["obj"][i].bigidea.trim() && lessonText.trim() == lesson : jQuery('#bigIdea-text').text().trim() == this.filter["teacherlessondata"][text]["obj"][i].bigidea.trim() || lessonText.trim() == lesson) {
              this.filter["challenge"].push(filterData[i]);
            }
          }
        } else {
          this.filter["challenge"] = [];
          for (let k = 0; k < this.filter["teacherlessondata"][text]["obj"].length; k++) {
            let lessonteacher = "Lesson " + parseInt(pattdigit.exec(pattlesson.exec(this.filter["teacherlessondata"][text]["obj"][k].HMH_ID)[0])[0]);
            if (bigIdeaText != "Select Big Idea" && lessonText != "Select Lesson" ? jQuery('#bigIdea-text').text().trim() == this.filter["teacherlessondata"][text]["obj"][k].bigidea.trim() && lessonText.trim() == lessonteacher : jQuery('#bigIdea-text').text().trim() == this.filter["teacherlessondata"][text]["obj"][k].bigidea.trim() || lessonText.trim() == lessonteacher) {
              this.filter["challenge"].push(this.filter["teacherlessondata"][text]["obj"][k])
            }
          }

        }
      }
    }
    this.setItemsCount(this.filter["challenge"].length);
    this.totalCount = this.pageCount > 1 ? this.filter["challenge"].length > 20 ? 20 : this.filter["challenge"].length : this.filter["challenge"].length;
    this.setDefault();
  }
  /**
  * standards checkbox event
  * @method checkBoxStandardEvent
  * @param {event} click event
  */
  checkBoxStandardEvent(event: Event) {
    this.commonClass.checkBoxEnableDisableEvent(jQuery(event.currentTarget).attr("id"));
  }


  /**
 * reset filetrs 
 * @method resetFilters
 *  @param {event} click event
 */
  resetFilters(event: Event) {
    this.commonClass.resetAllFilters();
    this.commonClass.isMobile() ? jQuery(".reset-container").removeClass("hover") : "";
    jQuery("#lesson-text").text("Select Lesson");
    jQuery("#bigIdea-text").text("Select Big Idea");
    jQuery(".customDropDown .filter-Text").removeClass("value-selected");
    jQuery("#unit-text-dropdown").text("Select Unit");
    jQuery(".lesson-filter-dropdown ").addClass("disabled-dropdown");
    jQuery(".bigidea-filter-dropdown ").addClass("disabled-dropdown");
    this.filter["challenge"] = this.filter["challengeOriginal"];
    this.setItemsCount(this.filter["challenge"].length);
    this.setDefault();
  }


  /**
   * click on tile
   * @method tilesClickEvent
   * @param {event} click event
   */

  tileClickEvent(event: Event) {
    let url = Object(event).value;
    this.tileEvent(url);
  }


  /**
   * tileclick event student
   * @method tileClickEventStudent
   * @param {event} mentions the event type
     * @param {url} mentions the url type
   */

  tileClickEventStudent(event: Event, url: string) {
    this.tileEvent(url);
  }



  /**
   * click on tile
   * @method tilesClickEvent
   * @param {event} click event
   */
  tileEvent(url: string) {
    if (this.user == "teacher") {
      var iOS = !!navigator.platform && /iPad/.test(navigator.platform);
      iOS == true ? url = url.replace('=', '') : '';
      this.commonClass.openInNewTab(url, this.user, '');
    } else {
      this.commonClass.openInNewTab(this.urls, this.user, '');
    }

  }


  /**
   * get unitfor student
   * @method getUnit
   * @param {id} unit
   */
  getUnit(id: any) {
    let pattunittReg = /U\d+/;
    let pattdigitReg = /\d+/;
    let unitNo = pattdigitReg.exec(pattunittReg.exec(id)[0])[0];
    if (unitNo.match("^0")) {
      unitNo = unitNo.slice(1);
    }
    return "Unit " + unitNo;
  }

  /**
   * get lesson for  student
   * @method getLesson
   * @param {id} lesson
   */
  getLesson(id: any) {
    let pattlessonReg = /L\d+/;
    let pattdigitReg = /\d+/;
    let lessonNo = pattdigitReg.exec(pattlessonReg.exec(id)[0])[0];
    if (lessonNo.match("^0")) {
      lessonNo = lessonNo.slice(1);
    }
    return "Lesson " + lessonNo;
  }

  /**
   * set count
   * @method setItemsCount
   * @param {itemLength} length of array
   */
  setItemsCount(itemLength: number) {
    this.itemsChallenge = itemLength;
    let addedLength: any = this.itemsChallenge;
    this.pageCount = Math.floor(addedLength / 20);
    addedLength != this.pageCount ? this.pageCount = this.pageCount + 1 : "";
    this.totalCount = this.pageCount > 1 ? addedLength > 20 ? 20 : addedLength : addedLength;
    addedLength <= 20 ? jQuery(".next").addClass("disabled") : jQuery(".next").removeClass("disabled");
    this.pageCount == 1 || this.currentPageId == 0 ? jQuery(".prev").addClass("disabled") : jQuery(".prev").removeClass("disabled");
  }


  /**
  * set default items count
  * @method setDefault
  */
  setDefault() {
    this.pageCount = 1;
    let addedLength: any = this.filter["challenge"].length;
    this.pageCount = Math.floor(addedLength / 20);
    addedLength / 20 != this.pageCount ? this.pageCount = this.pageCount + 1 : "";
    this.startCount = 1;
    this.currentPageId = 0;
    this.totalCount = this.pageCount > 1 ? addedLength > 20 ? 20 : addedLength : addedLength;
    jQuery(".prev").addClass("disabled");
    jQuery(".wide-tile-games-container.content-container wide-tile .wideTileWrapper").children().addClass("display-none");
    jQuery(".content-container .wide-tile.Page0").removeClass("display-none");
  }


  /**
   * click on tile
   * @method tilesClickEvent
   * @param {routekey} routekey to navigate
   */
  tilesClickEvent(routekey: any) {
    this.router.navigate([routekey]);
  }


  /**
   * tile click event of each tile
   * @method unitTileClickEvent
   * @param {event} mentions the event type
     * @param {key} mentions the key for which the vent should be split
   */
  unitTileClickEvent(event: Event, key: any) {
    this.oldTitle = this.title;
    this.lessonTitle = "Lesson Challenge";
    this.tileTitle = "Lesson";
    let lesson: any;

    if (key.match("^Lesson")) {
      this.title = "Lesson" + key.split("Lesson")[1];
      this.unitTitle = this.oldTitle;
      for (var j = 0; j < this.units.length; j++) {
        if (this.title.trim() == this.getLesson(this.lessonData.obj[j].HMH_ID).trim().split(" ").join("")) {
          this.urls = this.lessonData.obj[j].relative_url
          break;
        }
      }

      jQuery(".student-units-container").addClass("display-none");
      jQuery(".student-lessons-container").removeClass("display-none");
      jQuery(".breadcrumb-arrow.second,.unitTitle").attr("display", true);
      jQuery('.worksheet').attr('id', jQuery('.unit-tile-wrapper').attr('id'));
    }
    else {
      this.title = "Unit " + key.split("Unit")[1];
      this.unitTitle = this.title;
      this.lessonData = this.unitLessonData[key.trim().split(" ").join("")];
      let data = this.sort(this.lessonData.lesson);
      this.units = this.getLessonData(data);

      jQuery(".pageCurrTitle").attr("display", true);
      jQuery(".pageTitle").attr("display", false);
      jQuery(".breadcrumb-arrow.first").attr("display", true);
    }
  }

  /**
* getLessonData
* @method getLessonData
* @param {data} data
*/
  getLessonData(data: any) {
    let unitsArray: any = [];
    for (var j = 0; j < data.length; j++) {
      unitsArray.push("Lesson " + data[j])
    }
    return unitsArray

  }

  /**
 * clickOnItem
 * @method clickOnItem
 * @param {event} clickInactive
 */


  clickOnItem(event: Event) {
    this.units = [];
    this.units = this.unitsOriginal;
    this.lessonTitle = "";
    this.unitTitle = "";
    this.title = "Lesson Challenge";
    this.tileTitle = "Unit";
    jQuery(".breadcrumb-arrow,.student-heading").attr("display", false);
    jQuery(".pageTitle").attr("display", true);
    jQuery(".student-units-container").removeClass("display-none");
    jQuery(".student-lessons-container").addClass("display-none");
    jQuery(".lesson-tile").removeClass("set-lesson-tile-text");
  }

  /**
  * showUnitItem
  * @method showUnitItem
  * @param {event} clickInactive
  */

  showUnitItem(event: Event) {
    this.units = [];
    this.tileTitle = "Lesson";
    let unitDetails = this.unitLessonData[Object(event).value[0].trim().split(" ").join("")];
    let data = this.sort(unitDetails.lesson);
    this.units = this.getLessonData(data);
    this.title = Object(event).value[0].trim().split(" ").join("");
    let no = this.title.split("Unit")[1];
    this.title = "Unit " + no;
    jQuery(".student-units-container").removeClass("display-none");
    jQuery(".student-lessons-container").addClass("display-none");
    jQuery(".unitTitle,.breadcrumb-arrow.second,.pageTitle").attr("display", false);
    jQuery(".lesson-tile").addClass("set-lesson-tile-text");
  }


  /**
   * click on teacher resources
   * @method clickInactive
   * @param {event} clickInactive
   * @param {id} element id 
   */
  clickInactive(event: Event, id: string) {
    this.user == "student" ? jQuery("#" + id).removeClass("tilesActive") : jQuery("#" + id).removeClass("teacherTileActive");
  }
  /**
   * click on teacher resources
   * @method clickActive
   * @param {event} click active
   * @param {id} element id 
   */
  clickActive(event: Event, id: string) {
    this.user == "student" ? jQuery("#" + id).addClass("tilesActive") : jQuery("#" + id).addClass("teacherTileActive");
  }
  setId(value: any) {
    return value.trim().split(" ").join("");
  }


  /**
  * deafult function for navigation
  * @method changePage
  * @param {event} event
  * @param {operation} what operation it belongs
  * @param {filterMode} what filterMode it belongs
    */

  changePage(event: Event, operation: String, filterMode: String) {
    event.stopPropagation();
    this.currentPageId = jQuery(".content-container .wide-tile").not(".display-none").attr("class").split("Page")[1];
    jQuery(".prev,.next").removeClass("disabled");
    if (operation == "next") {
      jQuery(".next").hasClass("disabled") == false ? this.currentPageId++ : "";
      this.currentPageId == this.pageCount - 1 ? jQuery(".next").addClass("disabled") : "";
      this.startCount = this.totalCount + 1
    }
    else {
      this.currentPageId == 1 ? jQuery(".prev").addClass("disabled") : "";
      jQuery(".prev").hasClass("disabled") == false ? this.currentPageId-- : this.currentPageId = 0;
      this.totalCount = this.startCount - 1
    }
    jQuery(".content-container .wide-tile").not(".display-none").addClass("display-none");
    jQuery(".Page" + this.currentPageId).removeClass("display-none");
    if (operation == "next") {
      this.totalCount = this.totalCount + jQuery(".wide-tile-games-container.content-container wide-tile .wideTileWrapper").children().not(".display-none").length;
    } else {
      this.startCount = this.totalCount + 1 - jQuery(".wide-tile-games-container.content-container wide-tile .wideTileWrapper").children().not(".display-none").length;
    }
  }

}
