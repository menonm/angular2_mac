import { Component, Pipe } from '@angular/core';
import { DmcService } from '../../shared/dmc.service';
import { ROUTER_DIRECTIVES } from '@angular/router';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { DmcFooterComponent } from '../Footer/dmc.footer.component';
import { DmcHeaderComponent } from '../Header/dmc.header.component';
import { DmcFilterComponent } from '../../shared/mxdmcfilter.common';
import {TruncatePipe} from '../../shared/truncatepipe';
import { DPOService } from '../../shared/dpo.service';
import { DmcPopupComponent } from '../Popup/dmc.popup.component';
import { DmcWideTileComponent } from '../WideTile/dmc.widetile.component';
import { DmcFilterByComponent } from '../FilterBy/dmc.filterby.component';

declare var jQuery: any;
declare var PerfectScrollbar: any;
@Component({
  moduleId: module.id,
  selector: 'activitycards',
  templateUrl: './dmc.activitycards.component.html',
  pipes: [TruncatePipe],
  directives: [ROUTER_DIRECTIVES, DmcFooterComponent, DmcHeaderComponent, DmcPopupComponent, DmcWideTileComponent, DmcFilterByComponent],
  providers: [DmcService],
})


export class DmcActivityCardsComponent {
  /**
	 * Variable to store user value
	 */
  user: string;
  /**
	 * Variable to store teacher library items 
	 */
  displayContent: any = [];
  /**
	 * Variable to store student library items 
	 */
  studentDisplayContent: any = [];
  /**
 * Variable to store url value
 */
  url: any;
  /**
 * Variable to event type 
 */
  eventType: any;
  /**
* Variable to store total number of items
*/
  itemsChallenge: number;
  /**
	 * Variable to store dropdown items
	 */
  listItems: any = [];
  /**
  * Variable to store bigideas
  */
  bigIdeaItems: any = [];
  /**
	 * Object to store current page title
	 */
  filter: any = {};
  /**
	 * Array to store all units 
	 */
  title: string;
  /**
 * Array to store all lessons dropdown items
 */
  units: any = [];
  /**
 * Array to store all bigidea dropdown items 
 */
  lessons: any = [];
  /**
 * Object for temporary storage
 */
  bigIdea: any = [];
  /**
  * Object for temporary storage
  */
  obj: any = [];
  /**
  * Object that holds complete info
  */
  unitLessonData: any = {};
  /**
  * arrays that holdsunit
  */
  unitData: any = [];
  /**
  * variable to store standards current page id
  */
  standardsCurrentPageId: number = 0;
  /**
  * variable to store number of items
  */
  items: number;
  /**
	 * object  that hold all items of student 
	 */
  studentUnitLessonData: any = {};
  /**
  * instance of filter component file 
  */
  commonClass: DmcFilterComponent.filter;
  /**
 * variable to store unit title
 */
  tileTitle: any = "Unit";
  /**
 * variable to store units original data
 */
  unitsOriginal: any;
  /**
	 * variable to store relative urls
	 */
  urls: any = [];
  /**
	 * variable to store lesson dropdown object
	 */
  lessonData: any = [];
  /**
	 * variable to store current page count
	 */
  pageCount: number = 1;
  /**
	 * variable to store total page count
	 */
  totalCount: number;
  /**
	 * variable to store  page start count
	 */
  startCount: number = 1;
  /**
  * variable to store current page id
  */
  currentPageId: number = 0;
  /**
    * variable to keep copy of student data
    */
  studentDummyContent: any = [];
  /**
	 * variable to keep copy of teacher data
	 */
  teacherDummyContent: any = [];
  /**
  * variable to store teacher content
  */
  teacherOriginalContent: any = [];
  /**
  * variable to store math value
  */
  Math: any;
  /**
	 * variable to store activity data
   * 
   *  */
  activityData: any;
  /**
	 * variable to store moreinfo value
	 */
  moreInfo: string;
  /**
  * variable to store filtered old data
  */
  filteredOldArray: any = []
  /**
	 * variable to store intervention data
	 */
  interventionArray: any = [];
  /**
  * variable to store challenge data
  */
  challengeArray: any = [];
  /**
* variable to store onlevel  data
*/
  onLevelArray: any = [];
  /**
  * variable to store unit intervention
  */
  unitIntervention: any = [];
  /**
  * variable to store unit challenge
  */
  uitChallenge: any = [];
  /**
* variable to store unit challenge
*/
  unitc: any = [];
  /**
* variable to store unit onlevel
*/
  unitOnlevel: any = [];
  /**
  * variable to store standard category
  */
  standardCategory: any = [];
  /**
  * variable to store standard data
  */
  standardsData: any = [];
  /**
  * variable to store grouped standards
  */
  groupedStandards: any = [];
  /**
  * variable to store new standards
  */
  newStandards: any = [];
  /**
 * variable to store common json value
 */
  commonJson: any = {};
  /**
 * variable to store standard json value
 */
  standardsJson: any = [];
  /**
  * variable to store standard start count
  */
  standardsStartCount: any = 0;
  /**
  * variable to store standard total count
  */
  standardsTotalCount: any = 0;
  /**
	 * variable to store standard end count
	 */
  standardsEndCount: any = 0;
  /**
	 * variable to store standard page count
	 */
  standardsPageCount: any = 1;

  /**
   * Added For DP
   */
  /**
  * variable to store bigIdeaCheck value
  */
  bigIdeaCheck: boolean = true;
  /**
	 * variable to store lessonCheck value
	 */
  lessonCheck: boolean = true;
  /**
	 * variable to store truncateValue value
	 */
  truncateValue: number;
  /**
  * variable to store standards array
  */
  standardsArray: any = [];
  /**
  * variable to store popup array
  */
  popupArray: any = [];
  /**
	 * variable to store mainStandardsArray
	 */
  mainStandardsArray: any = [];
  /**
 * variable to store standards desc
 */
  standardsDesc: any = [];
  /**
   * Added for color configurable
  */
  /**
  * variable to store commonValues
  */
  commonValues: any = {};
  /**
* variable to store common value
*/
  landingJson: any = {};

  /**
	 * variable to store studentTileBorderColor
	 */
  studentTileBorderColor: string;
  /**
  * variable to store currentRouteKey
  */
  currentRouteKey: string;
  /**
  * variable to store tilecopy
  */
  titleCopy: string;
  /**
  * variable to store background color
  */
  backgroundColor: string;
  /**
	 * variable to store tile  color
	 */
  tileColor: string;
  /**
  * variable to store on tile border color
  */
  tileBorderColor: string;
  /**
	 * variable to store on filter array
	 */
  filterStdArray: any = [];
  /**
	 * variable to store on searchlist value
	 */
  searchList: any = [];
  /**
  * variable to store on dropdown status
  */
  dropdownClicked: boolean = false;
  /**
 * variable to store on intervention text
 */
  intervention_text: string;
  /**
 * variable to store on level text
 */
  onlevel_text: string;
  /**
 * variable to store challenge text
 */
  challenge_text: string;
  /**
	 * variable to store previous text
	 */
  previous_text: string;
  /**
 * variable to store next text
 */
  next_text: string;
  /**
    * variable to store items text
    */
  items_text: string;
  /**
  * variable to store select new standards text
  */
  select_new_standards: string;
  /**
  * variable to store edition text
  */
  edition_text: string;
  /**
     * variable to store standards text
     */
  standards_text: string;
  /**
     * variable to store teacher text
     */
  teacher_text: string;
  /**
    * variable to store student text
    */
  student_text: string;
  /**
    * variable to results not found text
    */
  results_not_found: string;
  /**
    * variable to store select unit text
    */
  select_unit_text: string;
  /**
	 * variable to store select lesson text
	 */
  select_lesson_text: string;
  /**
    * variable to store select big idea text
    */
  select_bigidea_text: string;
    /**
    * variable to store level text
    */
  level_text:string;



  /**
   * get dmc service and routing service
   * @constructor 
   * @param {_dmcService} get _dmcService
   * @param {router} get routing service
   */
  constructor(public _dmcService: DmcService, private router: Router, private route: ActivatedRoute) {
    Object(document).self = this;
    Object(window).self = this;
    this.commonClass = new DmcFilterComponent.filter();
    this.Math = Math;
    this.eventType = this.commonClass.getEventType();
    this.user = this.commonClass.getUser();
    this.url = Object(router).currentRouterState.snapshot.url;
    this.currentRouteKey = Object(route).url._value[0].path;
    this.activityData = jQuery('body').data();
    this.truncateValue = jQuery(window).width() <= 1024 ? jQuery(window).width() <= 961 ? 63 : 76 : jQuery(window).width() <= 1281 ? 110 : 115;
    this.assignJson(this.activityData[7], this.activityData[12], this.activityData[15], this.activityData[17]);
  }



  /**
     * ngAfterViewInit
     */
  ngAfterViewInit() {
    this.commonClass.enableDisableContainer(this.user);
    this.user == "student" ? jQuery(".main-container").addClass("student-main") : jQuery(".main-container").addClass("teacher-main");
    let scope = this;
    DPOService.gObj.dpoGlbService.initDPO({
      ready: function (obj: any) {
        console.log('DPO Service is ready for currentpage  ', obj.pageData);
        let unitText = obj.pageData['unit-text-dropdown'].elmContent.trim();
        let bigIdeaText = obj.pageData['bigIdea-text'].elmContent.trim();
        if (obj.pageData['unit-text-dropdown'].elmContent.trim() !== "Select Unit") {
          jQuery('.dropdown-text').each(function () {
            let innerText = jQuery(this).text();
            if (innerText === unitText) {
              jQuery(this).trigger('click');
            }
          });

          // Big Data Dropdown
          if (obj.pageData['bigIdea-text'].elmContent.trim() !== "Select Big Idea") {
            jQuery('#BigIdeaDropdown .dropdown-text').each(function () {
              let innerText = jQuery(this).text();
              if (innerText === bigIdeaText) {
                jQuery("#bigIdea-text").attr('name', bigIdeaText)
                jQuery(this).trigger('click');
                jQuery("#BigIdeaDropdown").removeClass('lesson-clicked');
                jQuery(this.parentElement).addClass('lesson-clicked');
              }
            });
          }
        }

        // CheckBox DropDown checkbox-clicked 
        jQuery(".checkbox-clicked").each(function () {
          jQuery(this).removeClass('checkbox-clicked');
          jQuery(this).trigger('click')
        });
      }
    });
    this.interventionArray.length == 0 ? jQuery("#intervention").parent().addClass("display-none") : "";
    this.moreInfo != undefined && this.moreInfo != "" ? jQuery(".more-info-wrapper").removeClass("display-none") : "";
  }

  /**
   * close drop down click on document click
   * @method documentBodyClick
   * @param {event}  click event
   */
  documentBodyClick(event: Event) {
    if (jQuery(".main-container").hasClass("activity-cards")) {
      let id = jQuery(event.target).attr("id");
      let className = jQuery(event.target).attr("class");
      if (className != "dropdown-text" && className != "dropdown-arrow dropdown-down-arrow" && id != "dropdown1" && id != "dropdown2" && id != "dropdown3" && id != "lesson-text" && id != "unit-text-dropdown" && id != "bigIdea-text") {
        if (jQuery(".dropbtn").hasClass("dropdown-clicked") == true) {
          jQuery(".dropdown-content").removeClass("show");
          jQuery(".dropbtn").addClass("hover").removeClass("dropdown-clicked");

        }
        Object(document).removeEventListener(Object(event.currentTarget).self.eventType, Object(event.currentTarget).self.documentBodyClick, false);
      }
    }
  }

  /**
   * assign json data to an object
   * @method assignJson
   * @param {data} data from json file
   */
  assignJson(data: any, moreInfo: any, standardsDescription: any, commonValues: any) {
    this.intervention_text = moreInfo.intervention_text;
    this.onlevel_text = moreInfo.onlevel_text;
    this.challenge_text = moreInfo.challenge_text;
    this.previous_text = moreInfo.previous_text;
    this.next_text = moreInfo.next_text;
    this.items_text = moreInfo.items_text;
    this.select_new_standards = moreInfo.select_new_standards;
    this.edition_text = moreInfo.edition_text;
    this.standards_text = moreInfo.standards_text;
    this.teacher_text = moreInfo.teacher_text;
    this.student_text = moreInfo.student_text;
    this.results_not_found = moreInfo.results_not_found;
    this.select_unit_text = moreInfo.select_unit_text;
    this.select_lesson_text = moreInfo.select_lesson_text;
    this.select_bigidea_text = moreInfo.select_bigidea_text;
    this.level_text=moreInfo.level_text


    this.landingJson = commonValues;
    this.commonValues = this.user == "student" ? commonValues.student : commonValues.teacher;

    for (var i = 0; i < this.commonValues.length; i++) {
      if (this.commonValues[i].routeKey == this.currentRouteKey) {
        this.tileColor = this.commonValues[i].color;
        this.moreInfo = this.commonValues[i].moreInfo;
        this.title = this.commonValues[i].title;
      }
    }
    if (data != undefined) {
      jQuery(".loading-div").addClass("display-none");
    }
    var pattunit = /U\d+/;
    var pattlesson = /L\d+/;
    var pattdigit = /\d+/;
    let unitIndex: any = [];
    let lessonurls: any = [];
    let standards: any = [];
    this.commonJson = moreInfo;
    this.standardsJson = standardsDescription;
    this.standardCategory = this.commonJson.standardCategory;
    this.displayContent = data.teacher[0].libraryItems.slice();
    this.teacherOriginalContent = this.displayContent.slice();
    jQuery.merge(this.displayContent, this.studentDummyContent);
    this.filter["activity"] = this.displayContent.slice();
    this.filter["activityOriginal"] = this.displayContent.slice();
    let dataPractice = this.displayContent.slice();

    for (var i = 0; i < dataPractice.length; i++) {
      if (pattunit.test(dataPractice[i].HMH_ID)) {
        if (("Unit" + parseInt(pattdigit.exec(pattunit.exec(dataPractice[i].HMH_ID)[0])[0]) in this.unitLessonData) == false) {
          this.bigIdea = [];
          this.obj = [];
          standards = [];
          if (i != 0) {
            this.unitIntervention = [];
            this.unitOnlevel = [];
            this.unitc.length = [];
            if (this.unitLessonData["Unit" + parseInt(pattdigit.exec(pattunit.exec(dataPractice[i].HMH_ID)[0])[0])] != undefined) {
              this.unitOnlevel = this.unitLessonData["Unit" + parseInt(pattdigit.exec(pattunit.exec(dataPractice[i].HMH_ID)[0])[0])].level;
              this.unitIntervention = this.unitLessonData["Unit" + parseInt(pattdigit.exec(pattunit.exec(dataPractice[i].HMH_ID)[0])[0])].intervention;
              this.unitc = this.unitLessonData["Unit" + parseInt(pattdigit.exec(pattunit.exec(dataPractice[i].HMH_ID)[0])[0])].challenge;
            }
          }
          if (dataPractice[i].relative_url.indexOf("Intervention") != -1) {
            this.unitIntervention = [];
            this.interventionArray.push(dataPractice[i])
            this.unitIntervention.push(dataPractice[i]);
          } else if (dataPractice[i].relative_url.indexOf("Challenge") != -1) {
            this.unitc = [];
            this.challengeArray.push(dataPractice[i]);
            this.unitc.push(dataPractice[i]);
          } else {
            this.unitOnlevel = [];
            this.onLevelArray.push(dataPractice[i]);
            this.unitOnlevel.push(dataPractice[i]);
          }

          this.bigIdea.push(dataPractice[i].bigidea);
          this.obj.push(dataPractice[i]);
          let standardsStrings = dataPractice[i].standards.slice();
          if (standardsStrings != undefined) {
            var myarray = standardsStrings.split(', ');
            for (var f = 0; f < myarray.length; f++) {
              standards.push(myarray[f]);
              if (jQuery.inArray(myarray[f], this.mainStandardsArray) == -1) {
                this.mainStandardsArray.push(myarray[f])
              }
            }
          }
          this.unitLessonData["Unit" + parseInt(pattdigit.exec(pattunit.exec(dataPractice[i].HMH_ID)[0])[0])] = { intervention: this.unitIntervention.slice(), challenge: this.unitc.slice(), level: this.unitOnlevel.slice(), bigIdea: this.bigIdea.sort(), ustd: standards, obj: this.obj };
          this.units.push("Unit " + parseInt(pattdigit.exec(pattunit.exec(dataPractice[i].HMH_ID)[0])[0]));
          this.unitLessonData["Unit" + parseInt(pattdigit.exec(pattunit.exec(dataPractice[i].HMH_ID)[0])[0]) + "unitindex"] = i;

        } else {

          //clearing all array values before entering the existing  unit value 
          this.bigIdea = [];
          this.obj = [];
          let stdArray: any = [];
          let standardsStrings = dataPractice[i].standards.slice();
          var myarray = standardsStrings.split(', ');

          if (this.unitLessonData["Unit" + parseInt(pattdigit.exec(pattunit.exec(dataPractice[i].HMH_ID)[0])[0])] != undefined) {
            this.unitOnlevel = this.unitLessonData["Unit" + parseInt(pattdigit.exec(pattunit.exec(dataPractice[i].HMH_ID)[0])[0])].level;
            this.unitIntervention = this.unitLessonData["Unit" + parseInt(pattdigit.exec(pattunit.exec(dataPractice[i].HMH_ID)[0])[0])].intervention;
            this.unitc = this.unitLessonData["Unit" + parseInt(pattdigit.exec(pattunit.exec(dataPractice[i].HMH_ID)[0])[0])].challenge;
          }
          if (dataPractice[i].relative_url.indexOf("Intervention") != -1) {
            this.unitIntervention = [];
            this.interventionArray.push(dataPractice[i]);
            this.unitIntervention = this.unitLessonData["Unit" + parseInt(pattdigit.exec(pattunit.exec(dataPractice[i].HMH_ID)[0])[0])].intervention;
            this.unitIntervention.push(dataPractice[i]);
          } else if (dataPractice[i].relative_url.indexOf("Challenge") != -1) {
            this.unitc = [];
            this.challengeArray.push(dataPractice[i]);
            this.unitc = this.unitLessonData["Unit" + parseInt(pattdigit.exec(pattunit.exec(dataPractice[i].HMH_ID)[0])[0])].challenge;
            this.unitc.push(dataPractice[i]);
          } else {
            this.unitOnlevel = [];
            this.onLevelArray.push(dataPractice[i]);
            this.unitOnlevel = this.unitLessonData["Unit" + parseInt(pattdigit.exec(pattunit.exec(dataPractice[i].HMH_ID)[0])[0])].level;
            this.unitOnlevel.push(dataPractice[i]);
          }
          for (var f = 0; f < myarray.length; f++) {
            stdArray.push(myarray[f])
            if (jQuery.inArray(myarray[f], this.mainStandardsArray) == -1) {
              this.mainStandardsArray.push(myarray[f])
            }
          }
          for (var j = 0; j < stdArray.length; j++) {
            if (jQuery.inArray(stdArray[j], standards) == -1) {
              standards.push(stdArray[j])
            }
          }

          //condition to fill existing data of that particular unit in array 

          this.bigIdea = this.unitLessonData["Unit" + parseInt(pattdigit.exec(pattunit.exec(dataPractice[i].HMH_ID)[0])[0])].bigIdea;
          this.obj = this.unitLessonData["Unit" + parseInt(pattdigit.exec(pattunit.exec(dataPractice[i].HMH_ID)[0])[0])].obj;
          standards = this.unitLessonData["Unit" + parseInt(pattdigit.exec(pattunit.exec(dataPractice[i].HMH_ID)[0])[0])].ustd;
          jQuery.inArray(dataPractice[i].bigidea, this.bigIdea) == -1 ? this.bigIdea.push(dataPractice[i].bigidea) : "";
          this.obj.push(dataPractice[i]);
          this.unitLessonData["Unit" + parseInt(pattdigit.exec(pattunit.exec(dataPractice[i].HMH_ID)[0])[0])] = { intervention: this.unitIntervention.slice(), challenge: this.unitc.slice(), level: this.unitOnlevel.slice(), bigIdea: this.bigIdea.sort(), ustd: standards, obj: this.obj }
        }
      }
      this.filter["teacherlessondata"] = this.unitLessonData;
    }
    this.standardsData = {};
    for (var j = 0; j < this.standardCategory.length; j++) {
      this.groupedStandards = [];
      for (var key in this.standardsJson) {
        if ((this.commonJson[this.standardCategory[j]] == this.standardsJson[key]["title"])) {
          this.groupedStandards.push(key);
          this.standardsData[this.standardCategory[j]] = { standards: this.groupedStandards };
          this.newStandards.push(this.standardCategory[j]);
        }
      }
    }
    this.unitsOriginal = this.units;
    this.filter["studentUnitLessonData"] = this.studentUnitLessonData;
    this.setItemsCount(this.filter["activity"].slice().length);
    this.pageCount = Math.floor(this.filter["activity"].slice().length / 20);
    this.filter["activity"].slice().length / 20 != this.pageCount ? this.pageCount = this.pageCount + 1 : "";
    this.totalCount = this.pageCount > 1 ? this.filter["activity"].slice().length > 20 ? 20 : this.filter["activity"].slice().length : this.filter["activity"].slice().length;
  }


  /**
   * toggle drop down
   * @method toggleDropDown
   * @param {event} click event
   * @param {DropdownShow} class name to remove or add
   */
  toggleDropDown(event: Event, DropdownShow: string, DropdownHide: any) {
    if (this.searchList.length != 0 && this.dropdownClicked == false) {
      jQuery("#" + DropdownShow).css('z-index', 99);
      jQuery(".popup,.popup-div").removeClass("display-none");
      jQuery(".popup-message").text("Changing any of the filters may impact your selected Standards.");
      this.commonClass.DropdownShow = DropdownShow;
      this.dropdownClicked = true;
    } else {
      this.toggleDropDownState(event, DropdownShow, DropdownHide);
    }
  }


  /**
   * toggle drop down state change
   * @method toggleDropDownState
   * @param {event} click event
   * @param {DropdownShow} class name to remove or add
   */

  toggleDropDownState(event: Event, DropdownShow: string, DropdownHide: any) {
    if (jQuery("#" + DropdownShow).hasClass("show") == true) {
      jQuery("#" + DropdownShow).removeClass("show");
      jQuery("#" + DropdownShow).siblings().removeClass("dropdown-clicked")
    } else {
      jQuery("#" + DropdownShow).addClass("show");
      jQuery("#" + DropdownShow).siblings().addClass("dropdown-clicked")
    }
    for (var i = 0; i < DropdownHide.length; i++) {
      jQuery("#" + DropdownHide[i]).removeClass("show");
      jQuery("#" + DropdownHide[i]).siblings().removeClass("dropdown-clicked");
    }
    if (jQuery(event.currentTarget).hasClass("dropdown-clicked") == true) {
      Object(document).addEventListener(this.eventType, this.documentBodyClick, true);
    }
  }

  /**
* Unit dropDownEvent click
* @method unitDropDownEvent
* @param {event} click event
*/
  unitDropDownEvent(event: Event) {
    jQuery(".landing-dropdown-content").children().removeClass("dropdown-clicked");
    jQuery(".unit-filter-dDown_content").removeClass("unit-clicked");
    this.clickHoverEvent(event, "unit-clicked", jQuery(event.currentTarget)[0].innerText);

  }

  /**
  * Big Idea dropDownEvent click
  * @method bigIdeaDropDownEvent
  * @param {event} click event
  */

  bigIdeaDropDownEvent(event: Event) {
    jQuery(".bigIdea-dDown_content").removeClass("lesson-clicked");
    jQuery(event.currentTarget).addClass("lesson-clicked");
    let bigIdeaName = jQuery("#bigIdea-text").attr('name');
    if (bigIdeaName != undefined && this.bigIdeaCheck) {
      jQuery("#bigIdea-text").text(bigIdeaName);
      this.bigIdeaCheck = false;
    }
    else {
      jQuery("#bigIdea-text").text(jQuery(event.currentTarget)[0].innerText);
    }
    jQuery("#BigIdeaDropdown").removeClass("show");
    jQuery("#BigIdeaDropdown").siblings().removeClass("dropdown-clicked");
    let bigIdeaData = jQuery("#BigIdeaDropdown").data("self");
    jQuery("#bigIdea-text").addClass("value-selected");
    let unitText = jQuery('#unit-text-dropdown').text().split(" ").join("");
    let unitData = bigIdeaData.unitLessonData[unitText.trim()];
    let bigDataText = jQuery("#bigIdea-text").text().trim();
    let checkboxType: string;
    bigIdeaData.filter["activity"] = [];
    for (var l = 0; l < unitData.obj.length; l++) {
      if (bigDataText.trim() == unitData.obj[l].bigidea) {
        bigIdeaData.filter["activity"].push(unitData.obj[l]);
      }
    }

    if (jQuery(".checkboxGroupElem").filter('.checkbox-clicked').length == 1) {
      checkboxType = jQuery('.checkbox-clicked').attr("id");
      this.setFilterData()
    }
    bigIdeaData.filterStandards(jQuery("#unit-text-dropdown").text().trim().split(" ").join(""), jQuery('#bigIdea-text').text(), jQuery("#lesson-text").text(), bigIdeaData.unitLessonData, "filtering")
    bigIdeaData.setItemsCount(bigIdeaData.filter["activity"].length);
    bigIdeaData.setDefault();
  }


  /**
  * dropDownEvent click
  * @method clickHoverEvent
  * @param {event} click event
  * @param {value} which dropdown clicked
  * @param {textValue} dropdown text 
  
  */
  clickHoverEvent(event: Event, value: string, textValue: string) {
    jQuery(".landing-dDown_content").removeClass("lesson-clicked");
    if (jQuery(event.currentTarget).hasClass(value) == false) {
      jQuery(event.currentTarget).removeClass("hover").addClass(value);
      if (value == "unit-clicked") {
        jQuery(".go-button").removeClass("disable-gobutton").removeAttr("disabled");
        jQuery("#bigIdea-text").text("");
        jQuery("#unit-text-dropdown").text(textValue);
        jQuery(".bigidea-filter-dropdown,.lesson-filter-dropdown ").removeClass("disabled-dropdown");
        jQuery("#unit-text-dropdown").addClass("value-selected");
        jQuery("#UnitDropdown").removeClass("show").siblings().removeClass("dropdown-clicked");
        this.bigIdeaItems = "";
        var lessonText = textValue.split(" ").join("").trim();

        for (var i = 0; i < this.unitLessonData[lessonText].bigIdea.length; i++) {
          this.bigIdeaItems += "  <div  class='dDown_content  hover bigIdea-dDown_content' id=ert" + i + " (click)='bigIdeaDropDownEvent($event,)'><div class='dropdown-text'>" + this.unitLessonData[lessonText].bigIdea[i] + "</div></div>";
          jQuery("#BigIdeaDropdown").html(this.bigIdeaItems);
        }
        jQuery("#bigIdea-text").text("Select Big Idea").removeClass("value-selected");
        var bigIdeaClassName = document.getElementsByClassName("bigIdea-dDown_content");
        var self = this;
        let checkboxType: any;
        let populateArray: any = [];
        jQuery("#BigIdeaDropdown").data("self", this);

        for (var i = 0; i < bigIdeaClassName.length; i++) {
          bigIdeaClassName[i].addEventListener(this.eventType, this.bigIdeaDropDownEvent, false);
        }

        let unitTexts = textValue.trim().split(" ").join("");
        this.filter["activity"] = [];
        let filterValue = this.filter["teacherlessondata"];
        this.filter["activity"] = filterValue[unitTexts]["obj"];

        if (jQuery(".checkboxGroupElem").filter('.checkbox-clicked').length == 1) {
          checkboxType = jQuery('.checkbox-clicked').attr("id");
          this.setFilterData()
        }

      }
    }
    else {
      jQuery(event.currentTarget).removeClass(value);
    }
    this.filterStandards(jQuery("#unit-text-dropdown").text().trim().split(" ").join(""), jQuery('#bigIdea-text').text(), jQuery("#lesson-text").text(), this.unitLessonData, "filtering")
    this.setItemsCount(this.filter["activity"].length);
    this.totalCount = this.pageCount > 1 ? this.filter["activity"].length > 20 ? 20 : this.filter["activity"].length : this.filter["activity"].length;
    this.setDefault();
  }



  /**
  * checkboxEvent click
  * @method checkBoxEvent
  * @param {event} click event
  */
  checkBoxEvent(event: Event) {
    this.commonClass.checkBoxEnableDisableEvent(jQuery(event.currentTarget).attr("id"));
    this.setFilterData()
    this.setItemsCount(this.filter["activity"].length);
    this.totalCount = this.pageCount > 1 ? this.filter["activity"].length > 20 ? 20 : this.filter["activity"].length : this.filter["activity"].length;
    this.setDefault();
  }



  /**
  * setting filtered data
  * @method setFilterData
  */
  setFilterData() {
    //filtering part of checkbox
    let text = jQuery("#unit-text-dropdown").text().trim().split(" ").join("");
    let filterValue = this.filter["teacherlessondata"];
    let bigideaDefaultText = jQuery('#bigIdea-text').text();
    let bigIdeaText = jQuery('#bigIdea-text').text().trim();

    if (bigideaDefaultText == ("Select Big Idea")) {
      if (jQuery(".checkboxGroupElem").filter('.checkbox-clicked').length >= 1) {
        this.filter["activity"] = [];
        let temporaryArray: any = [];
        let count = 0;
        let scope = this;
        jQuery(".checkboxGroupElem.checkbox-clicked").each(function () {
          count++;
          let populateArray: any = [];
          if (jQuery("#unit-text-dropdown").text() == ("Select Unit")) {
            populateArray = jQuery(this).attr("id") == "intervention" ? scope.interventionArray.slice() : jQuery(this).attr("id") == "challenge" ? scope.challengeArray.slice() : scope.onLevelArray.slice();
          } else {
            populateArray = jQuery(this).attr("id") == "intervention" ? filterValue[text]["intervention"] : jQuery(this).attr("id") == "challenge" ? filterValue[text]["challenge"] : filterValue[text]["level"];
          }
          count == 1 ? temporaryArray = populateArray.slice() : temporaryArray.push.apply(temporaryArray, populateArray.slice());
        });
        scope.filter["activity"] = temporaryArray.slice();

      } else {
        this.filter["activity"] = [];
        if (jQuery("#unit-text-dropdown").text() == ("Select Unit")) {
          this.filter["activity"] = this.filter["activityOriginal"];
        } else {
          let populateArray: any = [];
          populateArray = filterValue[text].obj;
          this.filter["activity"] = populateArray.slice();
        }
      }
    } else {
      if (jQuery(".checkboxGroupElem").filter('.checkbox-clicked').length >= 1) {
        this.filter["activity"] = [];
        let temoArray: any = [];
        let scope = this;
        jQuery(".checkboxGroupElem.checkbox-clicked").each(function () {

          let populateArrays: any = [];
          populateArrays = jQuery(this).attr("id") == "intervention" ? filterValue[text]["intervention"] : jQuery(this).attr("id") == "challenge" ? filterValue[text]["challenge"] : filterValue[text]["level"];
          for (let i = 0; i < populateArrays.length; i++) {
            if (populateArrays[i].bigidea == bigIdeaText) {
              temoArray.push((populateArrays[i]));
            }
          }
        });
        scope.filter["activity"] = temoArray.slice();
      } else {
        let populateArray: any = [];
        let temoArray: any = [];
        populateArray = filterValue[text].obj;
        for (let i = 0; i < populateArray.length; i++) {
          if (populateArray[i].bigidea == bigIdeaText) {
            temoArray.push((populateArray[i]))
          }
        }
        this.filter["activity"] = temoArray.slice();
      }
    }
  }




  /**
  * standards checkbox event
  * @method checkBoxStandardEvent
  * @param {event} click event
  */
  checkBoxStandardEvent(event: Event) {
    this.commonClass.checkBoxEnableDisableEvent(jQuery(event.currentTarget).attr("id"));
    this.setStandards();
  }

  /**
 * reset filetrs 
 * @method resetFilters
 *  @param {event} click event
 */
  resetFilters(event: Event) {
    this.commonClass.resetAllFilters();
    this.commonClass.isMobile() ? jQuery(".reset-container").removeClass("hover") : "";
    jQuery("#lesson-text").text("Select Lesson");
    jQuery("#bigIdea-text").text("Select Big Idea");
    jQuery(".customDropDown .filter-Text").removeClass("value-selected");
    jQuery("#unit-text-dropdown").text("Select Unit");
    jQuery(".lesson-filter-dropdown ").addClass("disabled-dropdown");
    jQuery(".bigidea-filter-dropdown ").addClass("disabled-dropdown");
    this.filter["activity"] = this.filter["activityOriginal"];
    this.setItemsCount(this.filter["activity"].length);
    this.setDefault();
  }

  /**
 * reset filetrs 
 * @method resetStandardsFilters
 *  @param {event} click event
 */
  resetStandardsFilters(event: Event) {
    this.filter["standards"] = [];
    jQuery(".checkboxGroupElemStandards").removeClass("checkbox-clicked");
    this.filterStdArray = [];
    this.searchList = [];
    this.setStandardsItemsCount(this.filter["standards"].length);
    this.standardsEndCount = this.standardsPageCount > 1 ? this.filter["standards"].length > 20 ? 20 : this.filter["standards"].length : this.filter["standards"].length;
    this.setStandardsDefault(this.filter["standards"].length);
    this.standardsStartCount = 0;
    jQuery(".results-not-found").removeClass("display-none");
  }

  /**
   * click on tile
   * @method tilesClickEvent
   * @param {event} click event
   */
  tileClickEvent(event: Event) {
    let url = Object(event).value;
    if (this.user == "teacher") {
      var iOS = !!navigator.platform && /iPad/.test(navigator.platform);
      iOS == true ? url = url.replace('=', '') : '';
      this.commonClass.openInNewTab(url, this.user, '');
    } else {
      this.commonClass.openInNewTab(this.urls, this.user, '');
    }
  }

  /**
   * get unitfor student
   * @method getUnit
   * @param {id} unit
   */

  getUnit(id: any) {
    let pattunittReg = /U\d+/;
    let pattdigitReg = /\d+/;
    let unitNo = pattdigitReg.exec(pattunittReg.exec(id)[0])[0];
    if (unitNo.match("^0")) {
      unitNo = unitNo.slice(1);
    }
    return "Unit " + unitNo;
  }

  /**
   * get lesson for  student
   * @method getLesson
   * @param {id} lesson
   */

  getLesson(id: any) {
    let pattlessonReg = /L\d+/;
    let pattdigitReg = /\d+/;
    let lessonNo = pattdigitReg.exec(pattlessonReg.exec(id)[0])[0];
    if (lessonNo.match("^0")) {
      lessonNo = lessonNo.slice(1);
    }
    return "Lesson " + lessonNo;
  }

  /**
   * set count
   * @method setItemsCount
   * @param {itemLength} length of array
   */

  setItemsCount(itemLength: number) {
    this.itemsChallenge = itemLength;
    let addedLength: any = this.filter["activity"].length;
    this.pageCount = Math.floor(addedLength / 20);
    addedLength != this.pageCount ? this.pageCount = this.pageCount + 1 : "";
    this.totalCount = this.pageCount > 1 ? addedLength > 20 ? 20 : addedLength : addedLength;
    addedLength <= 20 ? jQuery(".next").addClass("disabled") : jQuery(".next").removeClass("disabled");
    this.pageCount == 1 || this.currentPageId == 0 ? jQuery(".prev").addClass("disabled") : jQuery(".prev").removeClass("disabled");
  }

  /**
 * set standards items count
 * @method setStandardsItemsCount
 * @param {itemLength} length of array
 */

  setStandardsItemsCount(itemLength: number) {
    this.standardsTotalCount = itemLength;
    let addedLength: any = this.standardsTotalCount;
    this.standardsPageCount = Math.floor(addedLength / 20);
    addedLength != this.standardsPageCount ? this.standardsPageCount = this.standardsPageCount + 1 : "";
    this.standardsEndCount = this.standardsPageCount > 1 ? addedLength > 20 ? 20 : addedLength : addedLength;
    addedLength <= 20 ? jQuery(".next-standards").addClass("disabled") : jQuery(".next-standards").removeClass("disabled");
    this.standardsPageCount == 1 || this.standardsCurrentPageId == 0 ? jQuery(".prev-standards").addClass("disabled") : jQuery(".prev-standards").removeClass("disabled");
  }

  /**
  * set default items count
  * @method setDefault
  */

  setDefault() {
    this.pageCount = 1;
    let addedLength: any = this.filter["activity"].length;
    this.pageCount = Math.floor(addedLength / 20);
    addedLength / 20 != this.pageCount ? this.pageCount = this.pageCount + 1 : "";
    this.startCount = 1;
    this.currentPageId = 0;
    this.totalCount = this.pageCount > 1 ? addedLength > 20 ? 20 : addedLength : addedLength;
    jQuery(".prev").addClass("disabled");
    jQuery(".wide-tile-games-container.content-container wide-tile .wideTileWrapper").children().addClass("display-none");
    jQuery(".content-container .wide-tile.Page0").removeClass("display-none");
  }

  /**
* set standards items count
* @method changePage
* @param {event} event
* @param {operation} what operation whether it is next /previous
*/

  changePage(event: Event, operation: String) {
    event.stopPropagation();
    this.currentPageId = jQuery(".content-container .wide-tile").not(".display-none").attr("class").split("Page")[1];
    jQuery(".prev,.next").removeClass("disabled");
    if (operation == "next") {
      jQuery(".next").hasClass("disabled") == false ? this.currentPageId++ : "";
      this.currentPageId == this.pageCount - 1 ? jQuery(".next").addClass("disabled") : "";
      this.startCount = this.totalCount + 1
    }
    else {
      this.currentPageId == 1 ? jQuery(".prev").addClass("disabled") : "";
      jQuery(".prev").hasClass("disabled") == false ? this.currentPageId-- : this.currentPageId = 0;
      this.totalCount = this.startCount - 1
    }
    jQuery(".content-container .wide-tile").not(".display-none").addClass("display-none");
    jQuery(".Page" + this.currentPageId).removeClass("display-none");
    if (operation == "next") {
      this.totalCount = this.totalCount + jQuery(".wide-tile-games-container.content-container wide-tile .wideTileWrapper").children().not(".display-none").length;
    } else {
      this.startCount = this.totalCount + 1 - jQuery(".wide-tile-games-container.content-container wide-tile .wideTileWrapper").children().not(".display-none").length;
    }
  }


  /**
   * click on tile
   * @method tilesClickEvent
   * @param {routekey} routekey to navigate
   */
  tilesClickEvent(routekey: any) {
    this.router.navigate([routekey]);
  }



  /**
* set standards items count
* @method getLessonData
* @param {data} data
*/
  getLessonData(data: any) {
    let unitsArray: any = [];
    for (var j = 0; j < data.length; j++) {
      unitsArray.push("Lesson " + data[j])
    }
    return unitsArray
  }

  /**
   * click on teacher resources
   * @method clickInactive
   * @param {event} clickInactive
   * @param {id} element id 
   */
  clickInactive(event: Event, id: string) {
    this.user == "student" ? jQuery("#" + id).removeClass("tilesActive") : jQuery("#" + id).removeClass("teacherTileActive");
  }
  /**
   * click on teacher resources
   * @method clickActive
   * @param {event} click active
   * @param {id} element id 
   */
  clickActive(event: Event, id: string) {
    this.user == "student" ? jQuery("#" + id).addClass("tilesActive") : jQuery("#" + id).addClass("teacherTileActive");
  }


  /**
   * setting id
   * @method setId
   * @param {value} value of url
   */
  setId(value: any) {
    return value.trim().split(" ").join("");
  }


  /**
* deafult function to set count
* @method setStandardsDefault
  * @param {itemLength} length of an array
*/

  setStandardsDefault(itemLength: number) {
    this.standardsPageCount = 1;
    let addedLength: any = this.filter["standards"].length;
    this.standardsPageCount = Math.floor(addedLength / 20);
    addedLength / 20 != this.standardsPageCount ? this.standardsPageCount = this.standardsPageCount + 1 : "";
    this.standardsStartCount = 1;
    this.standardsCurrentPageId = 0;
    this.standardsEndCount = this.standardsPageCount > 1 ? addedLength > 20 ? 20 : addedLength : addedLength;
    addedLength <= 20 ? jQuery(".next-standards").addClass("disabled") : jQuery(".next-standards").removeClass("disabled");
    jQuery(".prev-standards").addClass("disabled");
    jQuery(".wide-tile-games-container.standards-container wide-tile .wideTileWrapper").children().addClass("display-none");
    jQuery(".standards-container .wide-tile.standardsPage0").removeClass("display-none");
  }

  /**
* changeFilterMode switching filter mode
* @method changeFilterMode
*/

  changeFilterMode(event: Event) {
    let select = Object(event).value[0];
    let deselect = Object(event).value[1];
    if (jQuery("#" + select).hasClass('selected') == false) {
      jQuery("#" + select).addClass('selected');
      jQuery("#" + deselect).removeClass('selected');
    }
    if (select == "contentMode") {
      jQuery(".customDropDown,.resources-filter,.content-container,.content-count-container").removeClass("display-none");
      jQuery(".standard-wrapper,.standards-container,.standards-count-container").addClass("display-none");
    } else {
      jQuery(".standard-wrapper,.standards-container,.standards-count-container").removeClass("display-none");
      jQuery(".customDropDown,.resources-filter,.content-container,.content-count-container,.filter-component-Text").addClass("display-none");
      this.openStandards(event);
    }
  }


  /**
* deafult function for navigation
* @method standardsChangePage
  * @param {event} event
  * @param {operation} what operation it belongs
 * @param {filterMode} what filterMode it belongs
    */

  standardsChangePage(event: Event, operation: String, filterMode: String) {
    event.stopPropagation();
    this.standardsCurrentPageId = jQuery(".standards-container .wide-tile").not(".display-none").attr("class").split("standardsPage")[1];
    jQuery(".prev-standards,.next-standards").removeClass("disabled");
    if (operation == "next") {
      jQuery(".next-standards").hasClass("disabled") == false ? this.standardsCurrentPageId++ : "";
      this.standardsCurrentPageId == this.standardsPageCount - 1 ? jQuery(".next-standards").addClass("disabled") : "";
      this.standardsStartCount = this.standardsEndCount + 1;
    }
    else {
      this.standardsCurrentPageId == 1 ? jQuery(".prev-standards").addClass("disabled") : "";
      jQuery(".prev-standards").hasClass("disabled") == false ? this.standardsCurrentPageId-- : this.standardsCurrentPageId = 0;
      this.standardsEndCount = this.standardsStartCount - 1;
    }
    jQuery(".standards-container .wide-tile").not(".display-none").addClass("display-none");
    jQuery(".standardsPage" + this.standardsCurrentPageId).removeClass("display-none");
    if (operation == "next") {
      this.standardsEndCount = this.standardsEndCount + jQuery(".wide-tile-games-container.standards-container wide-tile .wideTileWrapper").children().not(".display-none").length;
    } else {
      this.standardsStartCount = this.standardsEndCount + 1 - jQuery(".wide-tile-games-container.standards-container wide-tile .wideTileWrapper").children().not(".display-none").length;
    }
  }



  /**
* open standards poup
* @method openStandards
  * @param {event} event
    */

  openStandards(event: Event) {
    let text = jQuery("#unit-text-dropdown").text().trim().split(" ").join("");
    let bigIdeaText = jQuery('#bigIdea-text').text();
    let lessonText = jQuery("#lesson-text").text();
    let filterValue = this.unitLessonData;
    this.filterStandards(text, bigIdeaText, lessonText, filterValue, "openstandards");
    jQuery(".standards_popup").css({ display: 'block' });
    var container = document.getElementById('standards-main-container');
    this.searchList.length > 0 ? jQuery(".apply-button").removeClass("disabled-apply") : jQuery(".apply-button").addClass("disabled-apply");
    PerfectScrollbar.initialize(container);
  }


  /**
* filter standards based on grade and filter
* @method filterStandards
  * @param {type} whether standards in open state or not
    */

  filterStandards(text: string, bigIdeaText: string, lessonText: string, filterValue: any, type: string) {
    let stdArray: any = [];
    let dummyArray: any = [];

    for (var i = 0; i < this.newStandards.length; i++) {
      for (var j = 0; j < this.standardsData[this.newStandards[i]].standards.length; j++)
        dummyArray.push(this.standardsData[this.newStandards[i]].standards[j]);
    }

    stdArray = dummyArray.slice();
    let temArray: any = [];
    temArray = this.filterStdArray.slice();

    jQuery(".standards-content").removeClass("active");
    for (var e = 0; e < this.filterStdArray.length; e++) {
      if (jQuery.inArray(this.filterStdArray[e], stdArray) != -1) {
        if (type == "openstandards") {
          let index = stdArray.indexOf(this.filterStdArray[e]);
          let selectDiv = document.getElementById("id_" + dummyArray[index]);
          selectDiv.className = 'standards-content active';
        }
      } else {
        if (type != "openstandards") {
          let index = temArray.indexOf(this.filterStdArray[e]);
          temArray.splice(index, 1);
        }
      }
    }

    this.filterStdArray = [];
    this.searchList = [];
    this.filterStdArray = temArray.slice()
    this.searchList = temArray.slice();
    this.filteredOldArray = this.filterStdArray.slice();
  }



  /**
* select standards on popup
* @method selectStandards
  * @param {event} event
    */
  selectStandards(event: Event) {
    let id = Object(event).value;
    this.filterStdArray.indexOf(id);
    let selectDiv = document.getElementById("id_" + id);
    if (!this.filterStdArray.includes(id)) {
      if (this.filterStdArray.length >= 5) {
        jQuery(".popup,.popup-div").removeClass("display-none");
        jQuery(".popup-message").text("You may only select as many as five Standards at a time.")
        return;
      }
      selectDiv.className = 'standards-content active';
      this.filterStdArray.push(id);

    } else {
      var index = this.filterStdArray.indexOf(id);
      this.filterStdArray.splice(index, 1);
      selectDiv.className = 'standards-content';
      this.commonClass.closePopup();
    }
    jQuery(".apply-button").removeClass("disabled-apply");


  }

  /**
* show result on filter
* @method showResult
    */


  showResult() {
    this.filteredOldArray = [];
    this.filteredOldArray = this.filterStdArray.slice();
    this.setStandards();
    jQuery('.standards_popup').css({ display: "none" })
    this.commonClass.closePopup();
    this.searchList = this.filterStdArray.slice();
    this.filterStdArray.length == 0 || this.filter["standards"].length == 0 ? jQuery(".results-not-found").removeClass("display-none") : jQuery(".results-not-found").addClass("display-none");
    if (this.searchList.length == 0 || this.filter["standards"].length == 0) {
      this.filter["standards"] = []
      this.setStandardsItemsCount(this.filter["standards"].length);
      this.standardsEndCount = this.standardsPageCount > 1 ? this.filter["standards"].length > 20 ? 20 : this.filter["standards"].length : this.filter["standards"].length;
      this.setStandardsDefault(this.filter["standards"].length);
      this.standardsStartCount = 0;
      jQuery(".results-not-found").removeClass("display-none")
    }
  }


  /**
* delete and add filters
* @method removeFilters
* @param {id} standards code
  * @param {type} whther it is a popup or not
  */

  removeFilters(id: any, type: string) {
    let selectDiv = document.getElementById("standards-list-" + id)
    selectDiv.className = "standards-content";
    if (type != "popup") {
      var index = this.filterStdArray.indexOf(this.searchList[id]);
      this.filterStdArray.splice(index, 1);

      this.searchList = [];
      for (var i = 0; i < this.filterStdArray.length; i++) {
        this.searchList.push(this.filterStdArray[i]);
      }
    }
    this.filteredOldArray = this.searchList.slice();
    this.searchList.length == 0 ? "" : this.setStandards();
    if (this.searchList.length == 0 || this.filter["standards"].length == 0) {
      this.filter["standards"] = []
      this.setStandardsItemsCount(this.filter["standards"].length);
      this.standardsEndCount = this.standardsPageCount > 1 ? this.filter["standards"].length > 20 ? 20 : this.filter["standards"].length : this.filter["standards"].length;
      this.setStandardsDefault(this.filter["standards"].length);
      this.standardsStartCount = 0;
      jQuery(".results-not-found").removeClass("display-none")
    }
    this.filterStdArray.length == 0 || this.filter["standards"].length == 0 ? jQuery(".results-not-found").removeClass("display-none") : jQuery(".results-not-found").addClass("display-none");
  }


  /**
* common function to check the change
* @method setStandards
*/
  setStandards() {
    let tempStdArray: any = [];
    let filteredTempArray: any = [];
    let myStandards: any = [];
    let status = false;
    tempStdArray = this.filter["activityOriginal"].slice();
    this.filter["standards"] = [];
    let checkboxTempData: any = [];
    let TE: any = [];
    let SE: any = [];
    for (var j = 0; j < tempStdArray.length; j++) {
      var myarray = tempStdArray[j].standards.split(', ');
      myStandards = [];
      for (var f = 0; f < myarray.length; f++) {
        myStandards.push(myarray[f].split(" ").join("").trim());
      }
      for (var i = 0; i < this.filterStdArray.length; i++) {
        if (jQuery.inArray(this.filterStdArray[i].split(" ").join("").trim(), myStandards) != -1) {
          filteredTempArray.push(tempStdArray[j]);
          break;
        }
      }
    }
    checkboxTempData = filteredTempArray.slice();
    this.filter["standards"] = filteredTempArray.slice();
    this.setStandardsItemsCount(this.filter["standards"].length);
    this.standardsEndCount = this.standardsPageCount > 1 ? this.filter["standards"].length > 20 ? 20 : this.filter["standards"].length : this.filter["standards"].length;
    this.setStandardsDefault(this.filter["standards"].length);
    this.searchList.length == 0 || this.filter["standards"].length == 0 ? jQuery(".results-not-found").removeClass("display-none") : "";
  }



  /**
*remove selection from popup
* @method removeSelection
*/
  removeSelection(event: Event) {
    let id = Object(event).value;
    let selectDiv = document.getElementById("id_" + id)
    selectDiv.className = "standards-content";
    var index = this.filterStdArray.indexOf(id);
    this.filterStdArray.splice(index, 1);

  }

  /**
*remove hide standards
* @method hideStandardes
*/
  hideStandardes() {
    this.filterStdArray = [];
    this.filterStdArray = this.filteredOldArray.slice();
    this.searchList = [];
    this.searchList = this.filteredOldArray.slice();
    jQuery('.standards_popup').css({ display: "none" })
    jQuery(".listing-container").removeClass("disable-pointer-events");
    this.commonClass.closePopup();
    this.searchList.length == 0 ? jQuery(".results-not-found").removeClass("display-none") : "";
    this.searchList.length == 0 ? jQuery(".next-standards").addClass("disabled") : jQuery(".next-standards").removeClass("disabled");
  }

  /**
*remove hide standards
* @method hideStandardes
 */

  clearFilters() {
    for (var i = 0; i < this.filterStdArray.length; i++) {
      var selectDiv = document.getElementById("id_" + this.filterStdArray[i]);
      selectDiv.className = 'standards-content';
    }
    this.filterStdArray.length = 0;
    this.searchList.length = 0;
    this.searchList = [];
    jQuery('.standards_popup').css({ display: "none" })
    jQuery(".listing-container").removeClass("disable-pointer-events");
  }

  /**
*get standards data
* @method getData
*/

  getData(list: any) {
    let standards: any = [];
    let myarray = list.split(', ');
    for (var f = 0; f < myarray.length; f++) {
      standards.push(myarray[f]);
    }
    return standards
  }
}
