import { TestComponentBuilder } from '@angular/compiler/testing';
import { DmcFooterComponent } from '../Footer/dmc.footer.component';
import { DmcService} from '../../shared/dmc.service';
import { ROUTER_DIRECTIVES, Router, ActivatedRoute, UrlPathWithParams, RouterLink  } from '@angular/router';
import {Http, Response, HTTP_PROVIDERS} from '@angular/http';

import {
    beforeEachProviders,
    async,
    describe,
    expect,
    inject,
    it
} from '@angular/core/testing';
import {DmcActivityCardsComponent} from './dmc.activitycards.component';


export function main() {
    

    
    let moreInfo = {
        "intervention_text": "unit1", "onlevel_text": "unit1", "challenge_text": "unit1", "previous_text": "unit1", "next_text": "unit1",
        "items_text": "unit1", "select_new_standards": "unit1", "edition_text": "unit1", "standards_text": "unit1", 
        "teacher_text": "unit1", "student_text": "unit1", "results_not_found": "unit1", "select_unit_text": "unit1","select_lesson_text": "unit1","select_bigidea_text": "unit1"
    }

    let textValue = "unit 1";
    let desc = "standardsdesc";
    let url = "post url"
    let unit = "unit1";
    let length = 10;
    let lesson="Lesson1"

    let dmcchallenge = new DmcActivityCardsComponent(null, null, null);
    describe('dmcchallengeComponent', () => {
        it('assign json value', () => {
            const result:any = dmcchallenge.assignJson(null, moreInfo, desc, null);
            for (var i = 0; i < 13; i++) {
                expect(result[i]).toEqual("unit1");
            }
        })
    });


    describe('clickHoverEvent', () => {
        it('click hover events', () => {
            const result = dmcchallenge.clickHoverEvent(null, "unit-clicked", textValue);
            expect(textValue).toEqual("unit 1");
        })
    });





    describe('setItemsCount', () => {
        it('set items count', () => {
            const result = dmcchallenge.setItemsCount(length);
            expect(result).toEqual(10);
        })
    });
    
     describe('setStandardsItemsCount', () => {
        it('set setStandardsItemsCount count', () => {
            const result = dmcchallenge.setStandardsItemsCount(length);
            expect(result).toEqual(10);
        })
    });
    
    
     describe('changePage', () => {
        it('click changePage', () => {
            const result = dmcchallenge.changePage(null,"next");
            expect(result).toEqual("next");
        })
    });

    
    
    

    
  



}
