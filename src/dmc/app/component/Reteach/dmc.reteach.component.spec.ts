import { TestComponentBuilder } from '@angular/compiler/testing';
import { DmcFooterComponent } from '../Footer/dmc.footer.component';
import { DmcService} from '../../shared/dmc.service';
import { ROUTER_DIRECTIVES, Router, ActivatedRoute, UrlPathWithParams, RouterLink  } from '@angular/router';
import {Http, Response, HTTP_PROVIDERS} from '@angular/http';

import {
    beforeEachProviders,
    async,
    describe,
    expect,
    inject,
    it
} from '@angular/core/testing';
import {DmcReteachComponent} from './dmc.reteach.component';


export function main() {
    let moreInfo = {
        "previous_text": "unit1", "next_text": "unit1", "items_text": "unit1", "select_new_standards": "unit1", "edition_text": "unit1",
        "standards_text": "unit1", "teacher_text": "unit1", "student_text": "unit1", "interactive_comming_soon_text": "unit1", "results_not_found": "unit1",
        "worksheet_text": "unit1", "select_unit_text": "unit1", "select_lesson_text": "unit1", "select_bigidea_text": "unit1"
    }

    let textValue = "unit 1";
    let desc = "standardsdesc";
    let url = "post url"
    let unit = "unit1";
    let length = 10;
    let lesson="Lesson1"

    let dmcreteach = new DmcReteachComponent(null, null, null);
    describe('dmcreteachComponent', () => {
        it('assign json value', () => {
            const result = dmcreteach.assignJson(null, moreInfo, desc, null);
            for (var i = 0; i < 13; i++) {
                expect(result[i]).toEqual("unit1");
            }
        })
    });


    describe('clickHoverEvent', () => {
        it('click hover events', () => {
            const result = dmcreteach.clickHoverEvent(null, "unit-clicked", textValue);
            expect(textValue).toEqual("unit 1");
        })
    });





    describe('setItemsCount', () => {
        it('set items count', () => {
            const result = dmcreteach.setItemsCount(length);
            expect(result).toEqual(10);
        })
    });
    
    
      describe('setStandardsItemsCount', () => {
        it('set setStandardsItemsCount count', () => {
            const result = dmcreteach.setStandardsItemsCount(length);
            expect(result).toEqual(10);
        })
    });
    
      
      describe('standardsChangePage', () => {
        it('set standardsChangePage', () => {
            const result = dmcreteach.standardsChangePage(null,"next",null);
            expect(result).toEqual("next");
        })
    });
    
         describe('filterStandards', () => {
        it('set filterStandards', () => {
            const result = dmcreteach.filterStandards("","","",null,"openstandards");
            expect(result).toEqual("openstandards");
        })
    });
    
    



}
