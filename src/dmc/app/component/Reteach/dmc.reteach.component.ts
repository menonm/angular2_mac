import { Component, Pipe } from '@angular/core';
import { DmcService } from '../../shared/dmc.service';
import { ROUTER_DIRECTIVES } from '@angular/router';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { DmcFooterComponent } from '../Footer/dmc.footer.component';
import { DmcHeaderComponent } from '../Header/dmc.header.component';
import { DmcFilterComponent } from '../../shared/mxdmcfilter.common';
import {TruncatePipe} from '../../shared/truncatepipe';

import { DPOService } from '../../shared/dpo.service';
import { DmcPopupComponent } from '../Popup/dmc.popup.component';
import { DmcWideTileComponent } from '../WideTile/dmc.widetile.component';
import { DmcFilterByComponent } from '../FilterBy/dmc.filterby.component';

declare var jQuery: any;
declare var PerfectScrollbar: any;
@Component({
  moduleId: module.id,
  selector: 'reteach',
  templateUrl: './dmc.reteach.component.html',
  pipes: [TruncatePipe],
  directives: [ROUTER_DIRECTIVES, DmcFooterComponent, DmcHeaderComponent, DmcPopupComponent, DmcWideTileComponent, DmcFilterByComponent],
  providers: [DmcService]
})


export class DmcReteachComponent {
  /**
  * Variable to store user value
  */
  user: string;
  /**
	 * Variable to store teacher library items 
	 */
  displayContent: any = [];
  /**
  * Variable to store student library items 
  */
  studentDisplayContent: any = [];
  /**
* Variable to store url value
*/
  url: any;
  /**
* Variable to event type 
*/
  eventType: any;
  /**
 * Variable to store dropdown items
 */
  listItems: any = [];
  /**
* Variable to store bigideas
*/
  bigIdeaItems: any = [];
  /**
 * Object to store current page title
 */
  filter: any = {};
  /**
* Variable to store title
*/
  title: string;
  /**
	 * Array to store all units 
	 */
  units: any = [];
  /**
 * Array to store all lessons
 */
  lessons: any = [];
  /**
	 * Array to store all bigidea dropdown items 
	 */
  bigIdea: any = [];
  /**
* variable to store common value
*/
  landingJson: any = {};
  /**
* variable to storeall objects
*/
  obj: any = [];
  /**
* Object that holds complete info
*/
  unitLessonData: any = {};
  /**
* arrays that holdsunit
*/
  unitData: any = [];
  /**
  * variable to store reteach items
  */
  itemsReteach: number;
  /**
* instance of filter component file 
*/
  commonClass: DmcFilterComponent.filter;
  /**
* variable to store units original data
*/
  unitsOriginal: any;
  /**
 * variable to store current page count
 */
  pageCount: number = 1;
  /**
 * variable to store total page count
 */
  totalCount: number;
  /**
 * variable to store  page start count
 */
  startCount: number = 1;
  /**
* variable to store current page id
*/
  currentPageId: number = 0;
  /**
	 * variable to keep copy of student data
	 */
  studentDummyContent: any = [];
  /**
 * variable to keep copy of teacher data
 */
  teacherDummyContent: any = [];
  /**
* variable to store teacher content
*/
  teacherOriginalContent: any = [];
  /**
* variable to store math value
*/
  Math: any;
  /**
 * variable to store reteach data
 * 
 *  */
  challengeData: any;
  /**
 * variable to store truncateValue value
 */
  truncateValue: number;
  /**
 * variable to store moreinfo value
 */
  moreInfo: string;
  /**
* variable to store standards array
*/
  standardsArray: any = [];
  /**
 * variable to store popup array
 */

  popupArray: any = [];
  /**
* variable to store filtered old data
*/
  filteredOldArray: any = [];
  /**
* variable to store pattunit regex
*/
  pattunit = /U\d+/;
  /**
* variable to store patt lesson  regex
*/
  pattlesson = /L\d+/;
  /**
* variable to store patt digit  regex
*/
  pattdigit = /\d+/;
  /**
* variable to store on dropdown status
*/
  dropdownClicked: boolean = false;
  /**
 * variable to store mainStandardsArray
 */
  mainStandardsArray: any = [];
  /**
* variable to store standard category
*/
  standardCategory: any = ["CC", "SP", "G", "EE", "NS", "RP", "MD", "NF", "NBT", "OA", "MP"];
  /**
* variable to store standard data
*/
  standardsData: any = [];
  /**
 * variable to store grouped standards
 */
  groupedStandards: any = [];
  /**
* variable to store new standards
*/
  newStandards: any = [];
  /**
  * variable to store common json value
  */
  commonJson: any = {};
  /**
* variable to store standard json value
*/
  standardsJson: any = {};
  /**
* variable to store standard start count
*/
  standardsStartCount: any = 0;
  /**
* variable to store tilecopy
*/
  titleCopy: string;
  /**
 * variable to store background color
 */
  backgroundColor: string;
  /**
  * variable to store studentTileBorderColor
  */
  studentBorderColor: string;
  /**
  * variable to store on tile border color
  */
  tileBorderColor: string;
  /**
* variable to store standard total count
*/
  standardsTotalCount: any = 0;
  /**
  * variable to store standard end count
  */
  standardsEndCount: any = 0;
  /**
 * variable to store standard page count
 */
  standardsPageCount: any = 1;
  /**
* variable to store standards current page id
*/
  standardsCurrentPageId: number = 0;
  /**
 * variable to store select new standards text
 */
  select_new_standards: string;


  /** 
   * Added for color configurable
   */
  /**
  * variable to store commonValues
  */
  commonValues: any = {};
  /**
 * variable to store tile  color
 */
  tileColor: string;
  /**
* variable to store currentRouteKey
*/
  currentRouteKey: string;

  /**
   * Added For DP
   */
  /**
* variable to store bigIdeaCheck value
*/
  bigIdeaCheck: boolean = true;
  /**
 * variable to store lessonCheck value
 */
  lessonCheck: boolean = true;
  /**
 * object  that hold all items of student 
 */
  studentUnitLessonData: any = [];
  /**
 * variable to store on searchlist value
 */
  searchList: any = [];
  /**
 * variable to store on filter array
 */
  filterStdArray: any = [];
  /**
  * variable to store select unit text
  */
  select_unit_text: string;
  /**
 * variable to store select lesson text
 */
  select_lesson_text: string;
  /**
  * variable to store select big idea text
  */
  select_bigidea_text: string;
  /**
 * variable to store previous text
 */
  previous_text: string;
  /**
* variable to store next text
*/
  next_text: string;
  /**
* variable to store edition text
*/
  edition_text: string;
  /**
	 * variable to store standards text
	 */
  standards_text: string;
  /**
	 * variable to store teacher text
	 */
  teacher_text: string;
  /**
  * variable to store student text
  */
  student_text: string;
  /**
	 * variable to storeinteractive_comming_soon_text 
	 */
  interactive_comming_soon_text: string;
  /**
  * variable to results not found text
  */
  results_not_found: string;
           /**
* variable to store text items
*/
  items_text:string;
  /**
	 * variable to store worksheet text
	 */
  worksheet_text: string;
  /**
   * get dmc service and routing service
   * @constructor 
   * @param {_dmcService} get _dmcService
   * @param {router} get routing service
   */
  constructor(public _dmcService: DmcService, private router: Router, private route: ActivatedRoute) {
    Object(document).self = this;
    Object(window).self = this;
    this.commonClass = new DmcFilterComponent.filter();
    this.Math = Math;
    this.eventType = this.commonClass.getEventType();
    this.user = this.commonClass.getUser();
    this.user == "student" ? this.title = "Lesson Practice" : "";
    this.url = Object(router).currentRouterState.snapshot.url;
    this.currentRouteKey = Object(route).url._value[0].path;
    this.challengeData = jQuery('body').data();
    this.truncateValue = jQuery(window).width() <= 1024 ? jQuery(window).width() <= 961 ? 63 : 76 : jQuery(window).width() <= 1282 ? 100 : 115;
    this.assignJson(this.challengeData[1], this.challengeData[12], this.challengeData[15], this.challengeData[17]);
  }


  /**
     * ngAfterViewInit
     */
  ngAfterViewInit() {
    this.commonClass.enableDisableContainer(this.user);
    this.user == "student" ? jQuery(".main-container").addClass("student-main") : jQuery(".main-container").addClass("teacher-main");
    let scope = this;
    DPOService.gObj.dpoGlbService.initDPO({
      ready: function (obj: any) {
        console.log('DPO Service is ready for currentpage  ', obj.pageData);

        let unitText = obj.pageData['unit-text-dropdown'].elmContent.trim();
        let bigIdeaText = obj.pageData['bigIdea-text'].elmContent.trim();
        let lessonText = obj.pageData['lesson-text'].elmContent.trim();

        scope.filterStdArray = [];
        scope.filterStdArray.length = 0;
        for (var key in obj.pageData) {
          if (key.indexOf('standards-list') >= 0) {
            scope.filterStdArray.push(obj.pageData[key].elmContent.trim())
          }
        }
        scope.filterStdArray.length != 0 ? scope.showResult() : "";

        // Unit Text Val Dropdown
        if (obj.pageData['unit-text-dropdown'].elmContent.trim() !== "Select Unit") {

          jQuery('.dropdown-text').each(function () {
            let innerText = jQuery(this).text();
            if (innerText === unitText) {
              jQuery(this).trigger('click');
            }
          });

          // Big Data Dropdown
          if (obj.pageData['bigIdea-text'].elmContent.trim() !== "Select Big Idea") {
            jQuery('#BigIdeaDropdown .dropdown-text').each(function () {
              let innerText = jQuery(this).text();
              if (innerText === bigIdeaText) {
                jQuery("#bigIdea-text").attr('name', bigIdeaText)
                jQuery(this).trigger('click');
                jQuery("#BigIdeaDropdown").removeClass('lesson-clicked');
                jQuery(this.parentElement).addClass('lesson-clicked');
              }
            });
          }

          // Lesson Dropdown
          if (obj.pageData['lesson-text'].elmContent.trim() !== "Select Lesson") {
            jQuery('#LessonDropdown .dropdown-text').each(function () {
              let innerText = jQuery(this).text();

              if (innerText === lessonText) {
                jQuery("#lesson-text").attr('name', lessonText)
                jQuery(this).trigger('click');
                jQuery("#LessonDropdown").removeClass('lesson-clicked');
                jQuery(this.parentElement).addClass('lesson-clicked');

              }
            });

          }
        }

        // CheckBox DropDown checkbox-clicked 
        jQuery(".checkbox-clicked").each(function () {
          jQuery(this).removeClass('checkbox-clicked');
          jQuery(this).trigger('click')
        });

      }
    });

    this.moreInfo != undefined && this.moreInfo != "" ? jQuery(".more-info-wrapper").removeClass("display-none") : "";
  }

  /**
   * close drop down click on document click
   * @method documentBodyClick
   * @param {event}  click event
   */
  documentBodyClick(event: Event) {
    if (jQuery(".main-container").hasClass("reteach")) {
      let id = jQuery(event.target).attr("id");
      let className = jQuery(event.target).attr("class");
      if (className != "dropdown-text" && className != "dropdown-arrow dropdown-down-arrow" && id != "dropdown1" && id != "dropdown2" && id != "dropdown3" && id != "lesson-text" && id != "unit-text-dropdown" && id != "bigIdea-text") {
        if (jQuery(".dropbtn").hasClass("dropdown-clicked") == true) {
          jQuery(".dropdown-content").removeClass("show");
          jQuery(".dropbtn").addClass("hover").removeClass("dropdown-clicked");
        }
        Object(document).removeEventListener(Object(event.currentTarget).self.eventType, Object(event.currentTarget).self.documentBodyClick, false);
      }
    }
  }



  /**
   * assign json data to an object
   * @method assignJson
   * @param {data} data from json file
   */
  assignJson(data: any, moreInfo: any, standardsDescription: any, commonValues: any) {
    if (data != undefined) {
      jQuery(".loading-div").addClass("display-none");
    }
    let unitIndex: any = [];
    let lessonurls: any = [];
    let standards: any = [];
    this.studentDummyContent = data.student.slice();
    this.commonJson = moreInfo;
    this.select_new_standards = this.commonJson.select_new_standards;
    this.edition_text = this.commonJson.edition_text;
    this.standards_text = this.commonJson.standards_text;
    this.teacher_text = this.commonJson.teacher_text;
    this.student_text = this.commonJson.student_text;
    this.previous_text = this.commonJson.previous_text;
    this.items_text = this.commonJson.items_text;
    this.next_text = this.commonJson.next_text;
    this.select_unit_text = this.commonJson.select_unit_text;
    this.select_lesson_text = this.commonJson.select_lesson_text;
    this.select_bigidea_text = this.commonJson.select_bigidea_text;
    this.interactive_comming_soon_text = this.commonJson.interactive_comming_soon_text;
    this.results_not_found = this.commonJson.results_not_found;
    this.worksheet_text = this.commonJson.worksheet_text;
    this.standardsJson = standardsDescription;
    this.standardCategory = this.commonJson.standardCategory;
    this.commonValues = this.user == "student" ? commonValues.student : commonValues.teacher;
    this.landingJson = commonValues;



    for (var i = 0; i < this.commonValues.length; i++) {
      if (this.commonValues[i].routeKey == this.currentRouteKey) {
        this.tileColor = this.commonValues[i].color;
        this.moreInfo = this.commonValues[i].moreInfo;
        this.title = this.commonValues[i].title;
        this.titleCopy = this.title;
        this.backgroundColor = this.commonValues[i].header_background_color;
        this.tileBorderColor = this.commonValues[i].tile_border_color;
        this.studentBorderColor = this.commonValues[i].tile_color;
      }
    }
    if (this.user == "teacher") {
      this.displayContent = data.teacher[0].libraryItems.slice();
      this.teacherOriginalContent = this.displayContent.slice();
      jQuery.merge(this.displayContent, this.studentDummyContent);
    } else {
      this.displayContent = this.studentDummyContent.slice();
    }



    this.filter["challenge"] = this.displayContent.slice();
    this.filter["challengeOriginal"] = this.displayContent.slice();


    let dataPractice = this.displayContent.slice();
    for (var i = 0; i < dataPractice.length; i++) {
      if (this.pattunit.test(dataPractice[i].HMH_ID)) {
        if (("Unit" + parseInt(this.pattdigit.exec(this.pattunit.exec(dataPractice[i].HMH_ID)[0])[0]) in this.unitLessonData) == false) {
          this.bigIdea = [];
          this.lessons = [];
          this.obj = [];
          standards = [];
          let standardsStrings = dataPractice[i].standards.slice();
          if (standardsStrings != undefined) {
            var myarray = standardsStrings.split(', ');
            for (var f = 0; f < myarray.length; f++) {
              standards.push(myarray[f]);
              if (jQuery.inArray(myarray[f], this.mainStandardsArray) == -1) {
                this.mainStandardsArray.push(myarray[f])
              }
            }
          }
          this.lessons.push("Lesson " + parseInt(this.pattdigit.exec(this.pattlesson.exec(dataPractice[i].HMH_ID)[0])[0]));
          this.bigIdea.push(dataPractice[i].bigidea);
          this.obj.push(dataPractice[i]);
          this.unitLessonData["Unit" + parseInt(this.pattdigit.exec(this.pattunit.exec(dataPractice[i].HMH_ID)[0])[0])] = { lesson: this.lessons.sort(), bigIdea: this.bigIdea.sort(), ustd: standards, obj: this.obj };
          this.units.push("Unit " + parseInt(this.pattdigit.exec(this.pattunit.exec(dataPractice[i].HMH_ID)[0])[0]));
          this.unitLessonData["Unit" + parseInt(this.pattdigit.exec(this.pattunit.exec(dataPractice[i].HMH_ID)[0])[0]) + "unitindex"] = i;
        } else {

          //clearing all array values before entering the existing  unit value 
          this.bigIdea = [];
          this.lessons = [];
          this.obj = [];
          standards = [];

          standards = this.unitLessonData["Unit" + parseInt(this.pattdigit.exec(this.pattunit.exec(dataPractice[i].HMH_ID)[0])[0])].ustd;
          let stdArray: any = [];
          let standardsStrings = dataPractice[i].standards.slice();


          var myarray = standardsStrings.split(', ');
          for (var f = 0; f < myarray.length; f++) {
            stdArray.push(myarray[f])
            if (jQuery.inArray(myarray[f], this.mainStandardsArray) == -1) {
              this.mainStandardsArray.push(myarray[f])

            }
          }

          for (var j = 0; j < stdArray.length; j++) {
            if (jQuery.inArray(stdArray[j], standards) == -1) {
              standards.push(stdArray[j])
            }
          }
          //condition to fill existing data of that particular unit in array 
          this.bigIdea = this.unitLessonData["Unit" + parseInt(this.pattdigit.exec(this.pattunit.exec(dataPractice[i].HMH_ID)[0])[0])].bigIdea;
          this.lessons = this.unitLessonData["Unit" + parseInt(this.pattdigit.exec(this.pattunit.exec(dataPractice[i].HMH_ID)[0])[0])].lesson;
          this.obj = this.unitLessonData["Unit" + parseInt(this.pattdigit.exec(this.pattunit.exec(dataPractice[i].HMH_ID)[0])[0])].obj;

          jQuery.inArray(dataPractice[i].bigidea, this.bigIdea) == -1 ? this.bigIdea.push(dataPractice[i].bigidea) : "";
          jQuery.inArray("Lesson " + parseInt(this.pattdigit.exec(this.pattlesson.exec(dataPractice[i].HMH_ID)[0])[0]), this.lessons) == -1 ? this.lessons.push("Lesson " + parseInt(this.pattdigit.exec(this.pattlesson.exec(dataPractice[i].HMH_ID)[0])[0])) : "";
          // this.lessons.push("Lesson" + parseInt(pattdigit.exec(pattlesson.exec(this.displayContent[i].HMH_ID)[0])[0]));
          this.obj.push(dataPractice[i]);
          this.unitLessonData["Unit" + parseInt(this.pattdigit.exec(this.pattunit.exec(dataPractice[i].HMH_ID)[0])[0])] = { lesson: this.lessons.sort(), bigIdea: this.bigIdea.sort(), ustd: standards, obj: this.obj }
        }
      }
      this.filter["teacherlessondata"] = this.unitLessonData;

    }
    this.standardsData = {};
    for (var j = 0; j < this.standardCategory.length; j++) {
      this.groupedStandards = [];
      for (var key in this.standardsJson) {
        if ((this.commonJson[this.standardCategory[j]] == this.standardsJson[key]["title"])) {
          this.groupedStandards.push(key);
          this.standardsData[this.standardCategory[j]] = { standards: this.groupedStandards };
          this.newStandards.push(this.standardCategory[j]);
        }
      }
    }
    this.unitsOriginal = this.units;
    this.filter["studentUnitLessonData"] = this.studentUnitLessonData;
    this.setItemsCount(this.filter["challenge"].slice().length);
    this.pageCount = Math.floor(this.filter["challenge"].slice().length / 20);
    this.filter["challenge"].slice().length / 20 != this.pageCount ? this.pageCount = this.pageCount + 1 : "";
    this.totalCount = this.pageCount > 1 ? this.filter["challenge"].slice().length > 20 ? 20 : this.filter["challenge"].slice().length : this.filter["challenge"].slice().length;
  }


  /**
   * toggle drop down
   * @method toggleDropDown
   * @param {event} click event
   * @param {DropdownShow} class name to remove or add
   */
  toggleDropDown(event: Event, DropdownShow: string, DropdownHide: any) {
    this.toggleDropDownState(event, DropdownShow, DropdownHide);
  }


  /**
   * toggle drop down state change
   * @method toggleDropDownState
   * @param {event} click event
   * @param {DropdownShow} class name to remove or add
   */

  toggleDropDownState(event: Event, DropdownShow: string, DropdownHide: any) {
    if (jQuery("#" + DropdownShow).hasClass("show") == true) {
      jQuery("#" + DropdownShow).removeClass("show");
      jQuery("#" + DropdownShow).siblings().removeClass("dropdown-clicked")
    } else {
      jQuery("#" + DropdownShow).addClass("show");
      jQuery("#" + DropdownShow).siblings().addClass("dropdown-clicked")
    }
    for (var i = 0; i < DropdownHide.length; i++) {
      jQuery("#" + DropdownHide[i]).removeClass("show");
      jQuery("#" + DropdownHide[i]).siblings().removeClass("dropdown-clicked");
    }
    if (jQuery(event.currentTarget).hasClass("dropdown-clicked") == true) {
      Object(document).addEventListener(this.eventType, this.documentBodyClick, true);
    }
  }

  /**
* Unit dropDownEvent click
* @method unitDropDownEvent
* @param {event} click event
*/
  unitDropDownEvent(event: Event) {
    jQuery(".landing-dropdown-content").children().removeClass("dropdown-clicked");
    jQuery(".unit-filter-dDown_content").removeClass("unit-clicked");
    this.clickHoverEvent(event, "unit-clicked", jQuery(event.currentTarget)[0].innerText);

  }

  /**
  * dropDownEvent click
  * @method lessonDropDownEvent
  * @param {event} click event
  */
  lessonDropDownEvent(event: Event) {
    jQuery(".lesson-dDown_content").removeClass("lesson-clicked");
    jQuery(event.currentTarget).addClass("lesson-clicked");
    // jQuery("#lesson-text").text(jQuery(event.currentTarget).context.innerText);

    let LessonName = jQuery("#lesson-text").attr('name');
    if (LessonName != undefined && this.lessonCheck) {
      jQuery("#lesson-text").text(LessonName);
      this.lessonCheck = false;
    }
    else {
      jQuery("#lesson-text").text(jQuery(event.currentTarget).context.innerText);
    }
    jQuery("#LessonDropdown").removeClass("show");
    jQuery("#LessonDropdown").siblings().removeClass("dropdown-clicked");

    let id = jQuery(event.currentTarget).attr("id");
    let bigIdeaText = jQuery('#bigIdea-text').text();
    let lessonData = jQuery("#LessonDropdown").data("self");
    let bigIdeaData = jQuery("#BigIdeaDropdown").data("self");
    jQuery("#lesson-text").addClass("value-selected");

    let unitText = jQuery('#unit-text-dropdown').text().split(" ").join("");
    let unitData = lessonData.unitLessonData[unitText.trim()];
    let lessonText = jQuery("#lesson-text").text().split(" ").join("");
    let index: any;
    let pdfType: string;
    lessonData.filter["challenge"] = [];
    lessonData.filter["student"] = [];

    if (jQuery(".checkboxGroupElem").filter('.checkbox-clicked').length == 1) {
      pdfType = jQuery('.checkbox-clicked').attr("id")
    }

    if (pdfType != "student" || pdfType == undefined) {
      for (var l = 0; l < unitData.obj.length; l++) {
        let lesson = "Lesson" + parseInt(lessonData.pattdigit.exec(lessonData.pattlesson.exec(unitData.obj[l].HMH_ID)[0])[0]);
        if (bigIdeaText != "Select Big Idea" ? lessonText.trim() == lesson && jQuery('#bigIdea-text').text().split("Big Idea")[1].trim() == unitData.obj[l].bigidea.split("Big Idea")[1].trim() : lessonText.trim() == lesson) {
          unitData.obj[l].display_title.split(":")[0].trim() == "TE" ? lessonData.filter["challenge"].push(unitData.obj[l]) : "";
        }
      }
    }

    if (pdfType != "teacher" || pdfType == undefined) {
      for (var l = 0; l < unitData.obj.length; l++) {
        let lessonStudent = "Lesson" + parseInt(lessonData.pattdigit.exec(lessonData.pattlesson.exec(unitData.obj[l].HMH_ID)[0])[0]);
        if (bigIdeaText != "Select Big Idea" ? lessonText.trim() == lessonStudent && jQuery('#bigIdea-text').text().split("Big Idea")[1].trim() == unitData.obj[l].bigidea.split("Big Idea")[1].trim() : lessonText.trim() == lessonStudent) {
          unitData.obj[l].display_title.split(":")[0].trim() == "SE" ? lessonData.filter["challenge"].push(unitData.obj[l]) : "";
        }
      }
    }
    //added for standards
    lessonData.filterStandards(jQuery("#unit-text-dropdown").text().trim().split(" ").join(""), jQuery('#bigIdea-text').text(), jQuery("#lesson-text").text(), lessonData.unitLessonData, "filtering")
    lessonData.setItemsCount(lessonData.filter["challenge"].length);
    lessonData.setDefault();
  }

  /**
* Big Idea dropDownEvent click
* @method bigIdeaDropDownEvent
* @param {event} click event
*/
  bigIdeaDropDownEvent(event: Event) {
    jQuery(".bigIdea-dDown_content").removeClass("lesson-clicked");
    jQuery(event.currentTarget).addClass("lesson-clicked");
    // jQuery("#bigIdea-text").text(jQuery(event.currentTarget).context.innerText);
    let bigIdeaName = jQuery("#bigIdea-text").attr('name');
    if (bigIdeaName != undefined && this.bigIdeaCheck) {
      jQuery("#bigIdea-text").text(bigIdeaName);
      this.bigIdeaCheck = false;
    }
    else {
      jQuery("#bigIdea-text").text(jQuery(event.currentTarget)[0].innerText);
    }
    jQuery("#BigIdeaDropdown").removeClass("show");
    jQuery("#BigIdeaDropdown").siblings().removeClass("dropdown-clicked");
    jQuery("#lesson-text").text("Select Lesson").removeClass("value-selected");;
    let id = jQuery(event.currentTarget).attr("id");
    let lessonData = jQuery("#LessonDropdown").data("self");
    let bigIdeaData = jQuery("#BigIdeaDropdown").data("self");
    jQuery("#bigIdea-text").addClass("value-selected");
    let unitText = jQuery('#unit-text-dropdown').text().split(" ").join("");
    let unitData = lessonData.unitLessonData[unitText.trim()];
    let bigDataText = jQuery("#bigIdea-text").text();
    let index: any;
    jQuery("#LessonDropdown").empty();
    let listItems = "";
    let pdfType: string;
    bigIdeaData.filter["challenge"] = [];
    bigIdeaData.filter["student"] = [];

    if (jQuery(".checkboxGroupElem").filter('.checkbox-clicked').length == 1) {
      pdfType = jQuery('.checkbox-clicked').attr("id")
    }

    let lessonArray: any = [];
    for (var l = 0; l < unitData.obj.length; l++) {
      if (bigDataText.trim() == unitData.obj[l].bigidea) {
        if (pdfType != "student" || pdfType == undefined) {
          unitData.obj[l].display_title.split(":")[0].trim() == "TE" ? bigIdeaData.filter["challenge"].push(unitData.obj[l]) : "";
        }
        jQuery.inArray(parseInt(bigIdeaData.pattdigit.exec(bigIdeaData.pattlesson.exec(unitData.obj[l].HMH_ID)[0])[0]), lessonArray) == -1 ? lessonArray.push(parseInt(bigIdeaData.pattdigit.exec(bigIdeaData.pattlesson.exec(unitData.obj[l].HMH_ID)[0])[0])) : "";
      }
    }
    for (var l = 0; l < lessonArray.length; l++) {
      listItems += "  <div  class='dDown_content  hover lesson-dDown_content' id=ert" + l + " (click)='lessonDropDownEvent($event)'><div class='dropdown-text'>" + "Lesson " + lessonArray[l] + "</div></div>";
      jQuery("#LessonDropdown").html(listItems);

    }

    var classNames = document.getElementsByClassName("lesson-dDown_content");
    for (var i = 0; i < classNames.length; i++) {
      classNames[i].addEventListener(lessonData.eventType, lessonData.lessonDropDownEvent, false);
    }


    if (pdfType != "teacher" || pdfType == undefined) {
      for (var l = 0; l < unitData.obj.length; l++) {
        if (bigDataText.trim() == unitData.obj[l].bigidea) {
          unitData.obj[l].display_title.split(":")[0].trim() == "SE" ? bigIdeaData.filter["challenge"].push(unitData.obj[l]) : "";
        }
      }
    }
    //added for standards
    bigIdeaData.filterStandards(jQuery("#unit-text-dropdown").text().trim().split(" ").join(""), jQuery('#bigIdea-text').text(), jQuery("#lesson-text").text(), bigIdeaData.unitLessonData, "filtering")

    lessonData.setItemsCount(bigIdeaData.filter["challenge"].length);
    lessonData.setDefault();
  }

  /**
  * dropDownEvent click
  * @method clickHoverEvent
  * @param {event} click event
  * @param {value} which dropdown clicked
  * @param {textValue} dropdown text 
  
  */
  clickHoverEvent(event: Event, value: string, textValue: string) {
    jQuery(".landing-dDown_content").removeClass("lesson-clicked");
    if (jQuery(event.currentTarget).hasClass(value) == false) {
      jQuery(event.currentTarget).removeClass("hover").addClass(value);
      if (value == "unit-clicked") {
        jQuery(".go-button").removeClass("disable-gobutton").removeAttr("disabled");
        jQuery("#lesson-text").text("");
        jQuery("#bigIdea-text").text("");
        jQuery("#unit-text-dropdown").text(textValue);
        jQuery(".lesson-filter-dropdown ").removeClass("disabled-dropdown");
        jQuery(".bigidea-filter-dropdown ").removeClass("disabled-dropdown");
        jQuery("#unit-text-dropdown").addClass("value-selected");
        jQuery("#UnitDropdown").removeClass("show").siblings().removeClass("dropdown-clicked");
        this.listItems = "";
        this.bigIdeaItems = "";
        var lessonText = textValue.split(" ").join("").trim();

        let data = this.unitLessonData[lessonText].lesson;
        let temArray = this.sort(data)
        for (var i = 0; i < temArray.length; i++) {
          this.listItems += "  <div  class='dDown_content  hover lesson-dDown_content' id=ert" + i + " (click)='lessonDropDownEvent($event)'><div class='dropdown-text'>Lesson " + temArray[i] + "</div></div>";
          jQuery("#LessonDropdown").html(this.listItems);
        }

        for (var i = 0; i < this.unitLessonData[lessonText].bigIdea.length; i++) {
          this.bigIdeaItems += "  <div  class='dDown_content  hover bigIdea-dDown_content' id=ert" + i + " (click)='bigIdeaDropDownEvent($event,)'><div class='dropdown-text'>" + this.unitLessonData[lessonText].bigIdea[i] + "</div></div>";
          jQuery("#BigIdeaDropdown").html(this.bigIdeaItems);
        }
        jQuery("#lesson-text").text("Select Lesson");
        jQuery("#bigIdea-text").text("Select Big Idea");
        jQuery("#lesson-text,#bigIdea-text").removeClass("value-selected");
        var classNames = document.getElementsByClassName("lesson-dDown_content");
        var bigIdeaClassName = document.getElementsByClassName("bigIdea-dDown_content");
        var self = this;
        let pdfType: any;
        jQuery("#BigIdeaDropdown").data("self", this);
        jQuery("#LessonDropdown").data("self", this);
        for (var i = 0; i < classNames.length; i++) {
          classNames[i].addEventListener(this.eventType, this.lessonDropDownEvent, false);
        }
        for (var i = 0; i < bigIdeaClassName.length; i++) {
          bigIdeaClassName[i].addEventListener(this.eventType, this.bigIdeaDropDownEvent, false);
        }

        this.filter["challenge"] = [];

        let TE: any = [];
        let SE: any = [];
        let filterValue = this.filter["teacherlessondata"];
        for (var i = 0; i < filterValue[textValue.trim().split(" ").join("")]["obj"].length; i++) {
          if (filterValue[textValue.trim().split(" ").join("")]["obj"][i].display_title.split(":")[0].trim() == "TE") {
            TE.push(filterValue[textValue.trim().split(" ").join("")]["obj"][i]);
          } else {
            SE.push(filterValue[textValue.trim().split(" ").join("")]["obj"][i]);
          }
        }

        if (jQuery(".checkboxGroupElem").filter('.checkbox-clicked').length == 1) {
          pdfType = jQuery('.checkbox-clicked').attr("id")
        }
        if (pdfType == undefined) {
          this.filter["challenge"] = this.unitLessonData[textValue.trim().split(" ").join("")].obj;
        }
        else if (pdfType == "teacher") {
          this.filter["challenge"] = TE;
        } else if (pdfType == "student") {
          this.filter["challenge"] = SE;
        }
      }
    } else {
      jQuery(event.currentTarget).removeClass(value);
    }
    //added for standards
    this.filterStandards(jQuery("#unit-text-dropdown").text().trim().split(" ").join(""), jQuery('#bigIdea-text').text(), jQuery("#lesson-text").text(), this.unitLessonData, "filtering")

    this.setItemsCount(this.filter["challenge"].length);
    this.totalCount = this.pageCount > 1 ? this.filter["challenge"].length > 20 ? 20 : this.filter["challenge"].length : this.filter["challenge"].length;
    this.setDefault();
  }


  /**
    * checkboxEvent click
    * @method checkBoxEvent
    * @param {event} click event
    */

  checkBoxEvent(event: Event) {
    this.commonClass.checkBoxEnableDisableEvent(jQuery(event.currentTarget).attr("id"));
    let filterArray: any = [];
    let currentId = jQuery(".checkboxGroupElem").filter('.checkbox-clicked').attr("id");
    let filterValue = this.filter["teacherlessondata"];
    let text = jQuery("#unit-text-dropdown").text().trim().split(" ").join("");
    let bigIdeaText = jQuery('#bigIdea-text').text();
    let lessonText = jQuery("#lesson-text").text();
    let filterData: any;
    //filtering part of checkbox
    if (jQuery("#unit-text-dropdown").text() == ("Select Unit")) {
      if (jQuery(".checkboxGroupElem").filter('.checkbox-clicked').length == 1) {
        this.filter["challenge"] = [];
        this.filter["challenge"] = currentId == "teacher" ? this.teacherOriginalContent : this.studentDummyContent;
      } else {
        this.filter["challenge"] = this.filter["challengeOriginal"];
      }
    }
    else {
      let TE: any = [];
      let SE: any = [];

      for (var i = 0; i < filterValue[text]["obj"].length; i++) {
        if (filterValue[text]["obj"][i].display_title.split(":")[0].trim() == "TE") {
          TE.push(filterValue[text]["obj"][i]);
        } else {
          SE.push(filterValue[text]["obj"][i]);
        }
      }
      if (bigIdeaText == "Select Big Idea" && lessonText == "Select Lesson") {
        this.filter["challenge"] = [];
        if (jQuery(".checkboxGroupElem").filter('.checkbox-clicked').length == 1) {
          currentId == "teacher" ? this.filter["challenge"] = TE : this.filter["challenge"] = SE;
        } else {
          this.filter["challenge"] = this.filter["teacherlessondata"][text]["obj"];
        }
      }
      if (bigIdeaText != "Select Big Idea" || lessonText != "Select Lesson") {
        this.filter["challenge"] = [];
        if (jQuery(".checkboxGroupElem").filter('.checkbox-clicked').length == 1) {
          filterData = currentId == "teacher" ? TE : SE;
          for (let i = 0; i < filterData.length; i++) {
            let lesson = "Lesson " + parseInt(this.pattdigit.exec(this.pattlesson.exec(filterData[i].HMH_ID)[0])[0]);
            if (bigIdeaText != "Select Big Idea" && lessonText != "Select Lesson" ? jQuery('#bigIdea-text').text().trim() == this.filter["teacherlessondata"][text]["obj"][i].bigidea.trim() && lessonText.trim() == lesson : jQuery('#bigIdea-text').text().trim() == this.filter["teacherlessondata"][text]["obj"][i].bigidea.trim() || lessonText.trim() == lesson) {
              this.filter["challenge"].push(filterData[i]);
            }
          }
        } else {
          this.filter["challenge"] = [];
          for (let k = 0; k < this.filter["teacherlessondata"][text]["obj"].length; k++) {
            let lessonteacher = "Lesson " + parseInt(this.pattdigit.exec(this.pattlesson.exec(this.filter["teacherlessondata"][text]["obj"][k].HMH_ID)[0])[0]);
            if (bigIdeaText != "Select Big Idea" && lessonText != "Select Lesson" ? jQuery('#bigIdea-text').text().trim() == this.filter["teacherlessondata"][text]["obj"][k].bigidea.trim() && lessonText.trim() == lessonteacher : jQuery('#bigIdea-text').text().trim() == this.filter["teacherlessondata"][text]["obj"][k].bigidea.trim() || lessonText.trim() == lessonteacher) {
              this.filter["challenge"].push(this.filter["teacherlessondata"][text]["obj"][k])
            }
          }
        }
      }
    }
    this.setItemsCount(this.filter["challenge"].length);
    this.totalCount = this.pageCount > 1 ? this.filter["challenge"].length > 20 ? 20 : this.filter["challenge"].length : this.filter["challenge"].length;
    this.setDefault();
  }
  /**
  * standards checkbox event
  * @method checkBoxStandardEvent
  * @param {event} click event
  */
  checkBoxStandardEvent(event: Event) {
    this.commonClass.checkBoxEnableDisableEvent(jQuery(event.currentTarget).attr("id"));
    this.setStandards();
  }

  /**
 * reset filetrs 
 * @method resetFilters
 *  @param {event} click event
 */
  resetFilters(event: Event) {
    this.commonClass.resetAllFilters();
    this.commonClass.isMobile() ? jQuery(".reset-container").removeClass("hover") : "";
    jQuery("#lesson-text").text("Select Lesson");
    jQuery("#bigIdea-text").text("Select Big Idea");
    jQuery(".customDropDown .filter-Text").removeClass("value-selected");
    jQuery("#unit-text-dropdown").text("Select Unit");
    jQuery(".lesson-filter-dropdown ").addClass("disabled-dropdown");
    jQuery(".bigidea-filter-dropdown ").addClass("disabled-dropdown");
    this.filter["challenge"] = this.filter["challengeOriginal"];
    this.setItemsCount(this.filter["challenge"].length);
    this.setDefault();
  }
  /**
 * reset filetrs 
 * @method resetStandardsFilters
 *  @param {event} click event
 */
  resetStandardsFilters(event: Event) {
    this.filter["standards"] = [];
    jQuery(".checkboxGroupElemStandards").removeClass("checkbox-clicked");
    this.filterStdArray = [];
    this.searchList = [];
    this.setStandardsItemsCount(this.filter["standards"].length);
    this.standardsEndCount = this.standardsPageCount > 1 ? this.filter["standards"].length > 20 ? 20 : this.filter["standards"].length : this.filter["standards"].length;
    this.setStandardsDefault(this.filter["standards"].length);
    this.standardsStartCount = 0;
    jQuery(".results-not-found").removeClass("display-none");
  }


  /**
   * click on tile
   * @method tilesClickEvent
   * @param {event} click event
   */
  tileClickEvent(event: Event) {
    let url = Object(event).value;
    var iOS = !!navigator.platform && /iPad/.test(navigator.platform);
    iOS == true ? url = url.replace('=', '') : '';
    this.commonClass.openInNewTab(url, this.user, '');
  }

  /**
   * get unitfor student
   * @method getUnit
   * @param {id} unit
   */
  getUnit(id: any) {
    let unitNo = this.pattdigit.exec(this.pattunit.exec(id)[0])[0];
    if (unitNo.match("^0")) {
      unitNo = unitNo.slice(1);
    }
    return "Unit " + unitNo;
  }

  /**
   * get lesson for  student
   * @method getLesson
   * @param {id} lesson
   */
  getLesson(id: any) {
    let lessonNo = this.pattdigit.exec(this.pattlesson.exec(id)[0])[0];
    if (lessonNo.match("^0")) {
      lessonNo = lessonNo.slice(1);
    }
    return "Lesson " + lessonNo;
  }

  /**
   * set count
   * @method setItemsCount
   * @param {itemLength} length of array
   */
  setItemsCount(itemLength: number) {
    this.itemsReteach = itemLength;
    let addedLength: any = this.filter["challenge"].length;
    this.pageCount = Math.floor(addedLength / 20);
    addedLength != this.pageCount ? this.pageCount = this.pageCount + 1 : "";
    this.totalCount = this.pageCount > 1 ? addedLength > 20 ? 20 : addedLength : addedLength;
    addedLength <= 20 ? jQuery(".next").addClass("disabled") : jQuery(".next").removeClass("disabled");
    this.pageCount == 1 || this.currentPageId == 0 ? jQuery(".prev").addClass("disabled") : jQuery(".prev").removeClass("disabled");
  }
  /**
 * set standards items count
 * @method setStandardsItemsCount
 * @param {itemLength} length of array
 */

  setStandardsItemsCount(itemLength: number) {
    this.standardsTotalCount = itemLength;
    let addedLength: any = this.standardsTotalCount;
    this.standardsPageCount = Math.floor(addedLength / 20);
    addedLength != this.standardsPageCount ? this.standardsPageCount = this.standardsPageCount + 1 : "";
    this.standardsEndCount = this.standardsPageCount > 1 ? addedLength > 20 ? 20 : addedLength : addedLength;
    addedLength <= 20 ? jQuery(".next-standards").addClass("disabled") : jQuery(".next-standards").removeClass("disabled");
    this.standardsPageCount == 1 || this.standardsCurrentPageId == 0 ? jQuery(".prev-standards").addClass("disabled") : jQuery(".prev-standards").removeClass("disabled");
  }
  /**
  * set default items count
  * @method setDefault
  */
  setDefault() {
    this.pageCount = 1;
    let addedLength: any = this.filter["challenge"].length;
    this.pageCount = Math.floor(addedLength / 20);
    addedLength / 20 != this.pageCount ? this.pageCount = this.pageCount + 1 : "";
    this.startCount = 1;
    this.currentPageId = 0;
    this.totalCount = this.pageCount > 1 ? addedLength > 20 ? 20 : addedLength : addedLength;
    jQuery(".prev").addClass("disabled");
    jQuery(".wide-tile-games-container.content-container wide-tile .wideTileWrapper").children().addClass("display-none");
    jQuery(".content-container .wide-tile.Page0").removeClass("display-none");
  }

  /**
* set standards items count
* @method changePage
* @param {event} event
* @param {operation} what operation whether it is next /previous
*/

  changePage(event: Event, operation: String) {
    event.stopPropagation();
    this.currentPageId = jQuery(".content-container .wide-tile").not(".display-none").attr("class").split("Page")[1];
    jQuery(".prev,.next").removeClass("disabled");
    if (operation == "next") {
      jQuery(".next").hasClass("disabled") == false ? this.currentPageId++ : "";
      this.currentPageId == this.pageCount - 1 ? jQuery(".next").addClass("disabled") : "";
      this.startCount = this.totalCount + 1
    }
    else {
      this.currentPageId == 1 ? jQuery(".prev").addClass("disabled") : "";
      jQuery(".prev").hasClass("disabled") == false ? this.currentPageId-- : this.currentPageId = 0;
      this.totalCount = this.startCount - 1
    }
    jQuery(".content-container .wide-tile").not(".display-none").addClass("display-none");
    jQuery(".Page" + this.currentPageId).removeClass("display-none");
    if (operation == "next") {
      this.totalCount = this.totalCount + jQuery(".wide-tile-games-container.content-container wide-tile .wideTileWrapper").children().not(".display-none").length;
    } else {
      this.startCount = this.totalCount + 1 - jQuery(".wide-tile-games-container.content-container wide-tile .wideTileWrapper").children().not(".display-none").length;
    }
  }


  /**
   * click on tile
   * @method tilesClickEvent
   * @param {routekey} routekey to navigate
   */
  tilesClickEvent(routekey: any) {
    this.router.navigate([routekey]);
  }

  sort(data: any) {
    let temArray: any = [];
    for (var j = 0; j < data.length; j++) {
      var lesson = parseInt(data[j].split("Lesson")[1].trim());
      temArray.push(lesson);
    }
    return temArray.sort(function (a: any, b: any) { return a - b });
  }



  /**
   * click on teacher resources
   * @method clickInactive
   * @param {event} clickInactive
   * @param {id} element id 
   */
  clickInactive(event: Event, id: string) {
    this.user == "student" ? jQuery("#" + id).removeClass("tilesActive") : jQuery("#" + id).removeClass("teacherTileActive");
  }
  /**
   * click on teacher resources
   * @method clickActive
   * @param {event} click active
   * @param {id} element id 
   */
  clickActive(event: Event, id: string) {
    this.user == "student" ? jQuery("#" + id).addClass("tilesActive") : jQuery("#" + id).addClass("teacherTileActive");
  }
  setId(value: any) {
    return value.trim().split(" ").join("");
  }
  /**
  * deafult function to set count
  * @method setStandardsDefault
  * @param {itemLength} length of an array
  */
  setStandardsDefault(itemLength: number) {
    this.standardsPageCount = 1;
    let addedLength: any = this.filter["standards"].length;
    this.standardsPageCount = Math.floor(addedLength / 20);
    addedLength / 20 != this.standardsPageCount ? this.standardsPageCount = this.standardsPageCount + 1 : "";
    this.standardsStartCount = 1;
    this.standardsCurrentPageId = 0;
    this.standardsEndCount = this.standardsPageCount > 1 ? addedLength > 20 ? 20 : addedLength : addedLength;
    addedLength <= 20 ? jQuery(".next-standards").addClass("disabled") : jQuery(".next-standards").removeClass("disabled");
    jQuery(".prev-standards").addClass("disabled");
    jQuery(".wide-tile-games-container.standards-container wide-tile .wideTileWrapper").children().addClass("display-none");
    jQuery(".standards-container .wide-tile.standardsPage0").removeClass("display-none");
  }


  /**
* changeFilterMode switching filter mode
* @method changeFilterMode
*/

  changeFilterMode(event: Event) {
    let select = Object(event).value[0];
    let deselect = Object(event).value[1];
    if (jQuery("#" + select).hasClass('selected') == false) {
      jQuery("#" + select).addClass('selected');
      jQuery("#" + deselect).removeClass('selected');
    }

    if (select == "contentMode") {
      jQuery(".customDropDown,.resources-filter,.content-container,.content-count-container").removeClass("display-none");
      jQuery(".standard-wrapper,.standards-container,.standards-count-container").addClass("display-none");
    } else {
      jQuery(".standard-wrapper,.standards-container,.standards-count-container").removeClass("display-none");
      jQuery(".customDropDown,.resources-filter,.content-container,.content-count-container").addClass("display-none");
      this.openStandards(event);
    }
  }

  /**
  * deafult function for navigation
  * @method standardsChangePage
  * @param {event} event
  * @param {operation} what operation it belongs
  * @param {filterMode} what filterMode it belongs
    */

  standardsChangePage(event: Event, operation: String, filterMode: String) {
    event.stopPropagation();
    this.standardsCurrentPageId = jQuery(".standards-container .wide-tile").not(".display-none").attr("class").split("standardsPage")[1];
    jQuery(".prev-standards,.next-standards").removeClass("disabled");
    if (operation == "next") {
      jQuery(".next-standards").hasClass("disabled") == false ? this.standardsCurrentPageId++ : "";
      this.standardsCurrentPageId == this.standardsPageCount - 1 ? jQuery(".next-standards").addClass("disabled") : "";
      this.standardsStartCount = this.standardsEndCount + 1;
    }
    else {
      this.standardsCurrentPageId == 1 ? jQuery(".prev-standards").addClass("disabled") : "";
      jQuery(".prev-standards").hasClass("disabled") == false ? this.standardsCurrentPageId-- : this.standardsCurrentPageId = 0;
      this.standardsEndCount = this.standardsStartCount - 1;
    }
    jQuery(".standards-container .wide-tile").not(".display-none").addClass("display-none");
    jQuery(".standardsPage" + this.standardsCurrentPageId).removeClass("display-none");
    if (operation == "next") {
      this.standardsEndCount = this.standardsEndCount + jQuery(".wide-tile-games-container.standards-container wide-tile .wideTileWrapper").children().not(".display-none").length;
    } else {
      this.standardsStartCount = this.standardsEndCount + 1 - jQuery(".wide-tile-games-container.standards-container wide-tile .wideTileWrapper").children().not(".display-none").length;
    }
  }

  /**
  * open standards poup
  * @method openStandards
  * @param {event} event
    */
  openStandards(event: Event) {

    let text = jQuery("#unit-text-dropdown").text().trim().split(" ").join("");
    let bigIdeaText = jQuery('#bigIdea-text').text();
    let lessonText = jQuery("#lesson-text").text();
    let filterValue = this.unitLessonData;
    this.filterStandards(text, bigIdeaText, lessonText, filterValue, "openstandards");
    jQuery(".standards_popup").css({ display: 'block' });
    var container = document.getElementById('standards-main-container');
    this.searchList.length > 0 ? jQuery(".apply-button").removeClass("disabled-apply") : jQuery(".apply-button").addClass("disabled-apply");
    PerfectScrollbar.initialize(container);
  }
  /**
  * filter standards based on grade and filter
  * @method filterStandards
  * @param {type} whether standards in open state or not
    */

  filterStandards(text: string, bigIdeaText: string, lessonText: string, filterValue: any, type: string) {
    let stdArray: any = [];
    let dummyArray: any = [];

    for (var i = 0; i < this.newStandards.length; i++) {
      for (var j = 0; j < this.standardsData[this.newStandards[i]].standards.length; j++)
        dummyArray.push(this.standardsData[this.newStandards[i]].standards[j]);
    }

    stdArray = dummyArray.slice();
    let temArray: any = [];
    temArray = this.filterStdArray.slice();

    jQuery(".standards-content").removeClass("active");
    for (var e = 0; e < this.filterStdArray.length; e++) {
      if (jQuery.inArray(this.filterStdArray[e], stdArray) != -1) {
        if (type == "openstandards") {
          let index = stdArray.indexOf(this.filterStdArray[e]);
          let selectDiv = document.getElementById("id_" + dummyArray[index]);
          selectDiv.className = 'standards-content active';
        }
      } else {
        if (type != "openstandards") {
          let index = temArray.indexOf(this.filterStdArray[e]);
          temArray.splice(index, 1);
        }
      }
    }

    this.filterStdArray = [];
    this.searchList = [];
    this.filterStdArray = temArray.slice()
    this.searchList = temArray.slice();
    this.filteredOldArray = this.filterStdArray.slice();
  }



  /**
  * select standards on popup
  * @method selectStandards
  * @param {event} event
    */
  selectStandards(event: Event) {
    let id = Object(event).value;
    this.filterStdArray.indexOf(id);
    let selectDiv = document.getElementById("id_" + id);
    if (!this.filterStdArray.includes(id)) {
      if (this.filterStdArray.length >= 5) {
        jQuery(".popup,.popup-div").removeClass("display-none");
        jQuery(".popup-message").text("You may only select as many as five Standards at a time.")
        return;
      }
      selectDiv.className = 'standards-content active';
      this.filterStdArray.push(id);

    } else {
      var index = this.filterStdArray.indexOf(id);
      this.filterStdArray.splice(index, 1);
      selectDiv.className = 'standards-content';
      this.commonClass.closePopup();
    }
    jQuery(".apply-button").removeClass("disabled-apply");


  }

  /**
  * show result on filter
  * @method showResult
    */
  showResult() {
    this.filteredOldArray = [];
    this.filteredOldArray = this.filterStdArray.slice();
    this.setStandards();
    jQuery('.standards_popup').css({ display: "none" })
    this.commonClass.closePopup();
    this.searchList = this.filterStdArray.slice();
    this.filterStdArray.length == 0 || this.filter["standards"].length == 0 ? jQuery(".results-not-found").removeClass("display-none") : jQuery(".results-not-found").addClass("display-none");
    if (this.searchList.length == 0 || this.filter["standards"].length == 0) {
      this.filter["standards"] = []
      this.setStandardsItemsCount(this.filter["standards"].length);
      this.standardsEndCount = this.standardsPageCount > 1 ? this.filter["standards"].length > 20 ? 20 : this.filter["standards"].length : this.filter["standards"].length;
      this.setStandardsDefault(this.filter["standards"].length);
      this.standardsStartCount = 0;
      jQuery(".results-not-found").removeClass("display-none")
    }
  }
  /**
  * delete and add filters
  * @method removeFilters
  * @param {id} standards code
  * @param {type} whther it is a popup or not
  */

  removeFilters(id: any, type: string) {
    let selectDiv = document.getElementById("standards-list-" + id)
    selectDiv.className = "standards-content";
    if (type != "popup") {
      var index = this.filterStdArray.indexOf(this.searchList[id]);
      this.filterStdArray.splice(index, 1);

      this.searchList = [];
      for (var i = 0; i < this.filterStdArray.length; i++) {
        this.searchList.push(this.filterStdArray[i]);
      }
    }
    this.filteredOldArray = this.searchList.slice();
    this.searchList.length == 0 ? "" : this.setStandards();
    if (this.searchList.length == 0 || this.filter["standards"].length == 0) {
      this.filter["standards"] = []
      this.setStandardsItemsCount(this.filter["standards"].length);
      this.standardsEndCount = this.standardsPageCount > 1 ? this.filter["standards"].length > 20 ? 20 : this.filter["standards"].length : this.filter["standards"].length;
      this.setStandardsDefault(this.filter["standards"].length);
      this.standardsStartCount = 0;
      jQuery(".results-not-found").removeClass("display-none")
    }
    this.filterStdArray.length == 0 || this.filter["standards"].length == 0 ? jQuery(".results-not-found").removeClass("display-none") : jQuery(".results-not-found").addClass("display-none");
  }

  /**
  * common function to check the change
  * @method setStandards
  */
  setStandards() {
    let tempStdArray: any = [];
    let filteredTempArray: any = [];
    let myStandards: any = [];
    let status = false;
    tempStdArray = this.filter["challengeOriginal"].slice();
    this.filter["standards"] = [];
    let checkboxTempData: any = [];
    let TE: any = [];
    let SE: any = [];

    for (var j = 0; j < tempStdArray.length; j++) {
      var myarray = tempStdArray[j].standards.split(', ');
      myStandards = [];
      for (var f = 0; f < myarray.length; f++) {
        myStandards.push(myarray[f].split(" ").join("").trim());
      }
      for (var i = 0; i < this.filterStdArray.length; i++) {
        if (jQuery.inArray(this.filterStdArray[i].split(" ").join("").trim(), myStandards) != -1) {
          filteredTempArray.push(tempStdArray[j]);
          break;
        }
      }
    }
    checkboxTempData = filteredTempArray.slice();
    if (jQuery(".checkboxGroupElemStandards").filter('.checkbox-clicked').length == 1) {
      let currentId = jQuery(".checkboxGroupElemStandards").filter('.checkbox-clicked').attr("id");
      for (var i = 0; i < checkboxTempData.length; i++) {
        if (checkboxTempData[i].display_title.split(":")[0].trim() == "TE") {
          TE.push(checkboxTempData[i]);
        } else {
          SE.push(checkboxTempData[i]);
        }
      }
      this.filter["standards"] = currentId == "student-standards" ? SE : TE;
    } else {
      this.filter["standards"] = filteredTempArray.slice();
    }
    this.setStandardsItemsCount(this.filter["standards"].length);
    this.standardsEndCount = this.standardsPageCount > 1 ? this.filter["standards"].length > 20 ? 20 : this.filter["standards"].length : this.filter["standards"].length;
    this.setStandardsDefault(this.filter["standards"].length);
    this.searchList.length == 0 || this.filter["standards"].length == 0 ? jQuery(".results-not-found").removeClass("display-none") : "";
  }




  /**
  *remove selection from popup
  * @method removeSelection
  */
  removeSelection(event: Event) {
    let id = Object(event).value;
    let selectDiv = document.getElementById("id_" + id)
    selectDiv.className = "standards-content";

    var index = this.filterStdArray.indexOf(id);
    this.filterStdArray.splice(index, 1);

  }

  /**
  *remove hide standards
  * @method hideStandardes
  */
  hideStandardes() {
    this.filterStdArray = [];
    this.filterStdArray = this.filteredOldArray.slice();
    this.searchList = [];
    this.searchList = this.filteredOldArray.slice();

    jQuery('.standards_popup').css({ display: "none" })
    jQuery(".listing-container").removeClass("disable-pointer-events");
    this.commonClass.closePopup();
    this.searchList.length == 0 ? jQuery(".results-not-found").removeClass("display-none") : "";
    this.searchList.length == 0 ? jQuery(".next-standards").addClass("disabled") : jQuery(".next-standards").removeClass("disabled");
  }

  /**
  *remove hide standards
  * @method hideStandardes
  */
  clearFilters() {
    for (var i = 0; i < this.filterStdArray.length; i++) {
      var selectDiv = document.getElementById("id_" + this.filterStdArray[i]);
      selectDiv.className = 'standards-content';
    }
    this.filterStdArray.length = 0;
    this.searchList.length = 0;
    this.searchList = [];
    jQuery('.standards_popup').css({ display: "none" })
    jQuery(".listing-container").removeClass("disable-pointer-events");
  }
}
