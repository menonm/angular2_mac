import { TestComponentBuilder } from '@angular/compiler/testing';
import { DmcFooterComponent } from '../Footer/dmc.footer.component';
import { DmcService} from '../../shared/dmc.service';
import { ROUTER_DIRECTIVES, Router, ActivatedRoute, UrlPathWithParams, RouterLink  } from '@angular/router';
import {Http, Response, HTTP_PROVIDERS} from '@angular/http';

import {
    beforeEachProviders,
    async,
    describe,
    expect,
    inject,
    it
} from '@angular/core/testing';
import {DmcLessonChecksomponent} from './dmc.lessonchecks.component';


export function main() {
    

    
    let moreInfo = {
        "previous_text": "unit1", "next_text": "unit1", "items_text": "unit1", "select_new_standards": "unit1", "standards_text": "unit1",
        "results_not_found": "unit1", "select_unit_text": "unit1", "select_lesson_text": "unit1", "select_bigidea_text": "unit1" 
  
    }
    
     let commonvalues=
    [{
         "color": "unit1", "moreInfo": "unit1", "title": "unit1", "header_background_color": "unit1",
        "tile_color": "unit1", "tile_border_color": "unit1"
    }]

    
    let textValue = "unit 1";
    let desc = "standardsdesc";
    let url = "post url"
    let unit = "unit1";
    let length = 10;
    let lesson="Lesson1"

    let dmclessonchecks = new DmcLessonChecksomponent(null, null, null);
    describe('dmclessonchecksComponent', () => {
        it('assign json value', () => {
            const result:any = dmclessonchecks.assignJson(null, moreInfo, desc, commonvalues);
            for (var i = 0; i < 13; i++) {
                expect(result[i]).toEqual("unit1");
            }
        })
    });


    describe('clickHoverEvent', () => {
        it('click hover events', () => {
            const result = dmclessonchecks.clickHoverEvent(null, "unit-clicked", textValue);
            expect(textValue).toEqual("unit 1");
        })
    });



    describe('filterStandards', () => {
        it('set items count', () => {
            const result = dmclessonchecks.filterStandards("openstandards");
            expect(result).toEqual("openstandards");
        })
    });


    describe('setStandardsDefault', () => {
        it('set items count', () => {
            const result = dmclessonchecks.setStandardsDefault(length);
            expect(result).toEqual(10);
        })
    });
    
      describe('setItemsCount', () => {
        it('set items count', () => {
            const result = dmclessonchecks.setItemsCount(length);
            expect(result).toEqual(10);
        })
    });
    
     describe('setStandardsItemsCount', () => {
        it('set setStandardsItemsCount count', () => {
            const result = dmclessonchecks.setStandardsItemsCount(length);
            expect(result).toEqual(10);
        })
    });
    
    
     describe('changePage', () => {
        it('click changePage', () => {
            const result = dmclessonchecks.changePage(null,"next",null);
            expect(result).toEqual("next");
        })
    });

    
    
      describe('removeFilters', () => {
        it('click removeFilters', () => {
            const result = dmclessonchecks.removeFilters(null,"popup");
            expect(result).toEqual("popup");
        })
    });

    
    
    

    
  



}
