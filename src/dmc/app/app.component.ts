import { Component, ElementRef } from '@angular/core';
import { ROUTER_DIRECTIVES } from '@angular/router';
import { MXDmcCommon } from './shared/mxdmc.common';
import { provideRouter, RouterConfig } from '@angular/router';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { DmcService } from './shared/dmc.service';
import { DmcLandingComponent } from './component/Landing/dmc.landing.component';
import { DmcGamesComponent } from './component/Games/dmc.games.component';
import { DmcPracticeComponent } from './component/Practice/dmc.practice.component';
import { DmcFooterComponent } from './component/Footer/dmc.footer.component';
import { DmcResultsComponent } from './component/SearchResults/dmc.results.component';
import {DmcPlanninGuideComponent}from './component/PlanningGuide/dmc.planninguide.component';
import {DmcRtiComponent}from './component/Rti/dmc.rti.component';
import {DmcLessonChecksomponent}from './component/Lessonchecks/dmc.lessonchecks.component';
import { DmcFilterComponent } from './shared/mxdmcfilter.common';
import { DPO } from './shared/dpo';
import { DPOService } from './shared/dpo.service';
import { DPService } from './shared/dp.service';
declare var jQuery:any

@Component({
  moduleId: module.id,
  selector: 'mx-dmc',
  templateUrl: './app.component.html',
  directives: [DmcLandingComponent, DmcFooterComponent, DmcGamesComponent, ROUTER_DIRECTIVES, DmcRtiComponent, DmcResultsComponent],
  providers: [DmcService],

})




export class AppComponent extends MXDmcCommon.Types.DMCBase {
  /**
	 * variable to store user value
	 */
  user: string;
  /**
 * variable to store user grade value
 */
  grade: string = "default";
  /**
* variable to store local grade value
*/
  localgrade: string = "default";
  /**
* variable to store current page instance
*/
  currentInstance: any;
  /**
* variable to store common filter
*/
  commonFilter: any = {}
  /**
* variable to store path  value
*/
  path: string;
  /**
* variable to store filter component instance value
*/
  commonClass: DmcFilterComponent.filter;
  /**
* variable to store complete json  value
*/
  global: any = {};
  /**
* variable to render data value
*/
  data: any;
  /**
* variable to store isbn number
*/
  isbnNumber: number;
  /**
* variable to store dpservice instance
*/
  dpService: any = DPService.DataPersistence;
  /**
* variable to store dposervice instance
*/
  dpoService: any = DPOService.DataPersistenceObjectService;
  /**
   * intialize router
   * @constructor
   * @param {router} get routing service
   * @param {routes} activate routes
   */
  constructor(private router: Router, public _dmcService: DmcService, public routes: ActivatedRoute, elm: ElementRef) {
    super(null, null);
    this.currentInstance = this;
    this.path = elm.nativeElement.getAttribute('relativeUrl')
    this.commonClass = new DmcFilterComponent.filter();
    this._dmcService.getBulkData(['practice', 'reteach', 'challenge', 'rti', 'fluencybuilders', 'games', 'lessonchecks', 'activitycards', 'mathreader', 'planningguide', 'inquirybasedtasks', 'mathreaderthumbnails', 'common', 'gameboards', 'gamesthumbnails', 'standards', 'corelation', 'landing']).subscribe(
      data => { this.assignJson(data); }            // get data from prc.json
    );
    var el = document.getElementsByTagName('head');
    Object(el).self = this;
    Object(window).dp = this;

    jQuery('body').append('<input id="update-all-field" type="checkbox" style="display:none"/>');

    this.dpService = new DPService.DataPersistence();
    this.dpoService = new DPOService.DataPersistenceObjectService(true);//set value true to enable dpo
    DPOService.gObj.dpoGlbService = this.dpoService;
  }


  /**
   * assign json value
   * @method assignJson
   * @param {data} data value from json
   */
  assignJson(data: any) {
    if (data != undefined) {
      this.global = data;
      jQuery('body').data(this.global);

      if (this.user == "teacher") {
        this.isbnNumber = this.global[12].isbn_te;
      } else {
        this.isbnNumber = this.global[12].isbn_se;
      }
      //init  //casStorege,BrowserStorage,data,browserStorageType,

      this.dpService.initDP('mac' + this.user, this.isbnNumber, { casStorage: true, browserStorage: true, ready: this.onDPReady });
    }
  }

  /**
 * ngAfterViewInit
 */
  ngAfterViewInit() {
    if (window.location.href.indexOf("thinkcentral") > -1 || window.location.href.indexOf("lectora") > -1) {
      let locationValue = window.location.href.split("_center_")[1].split("/")[0].split("_")[0];

      let gradeValue = window.location.href.split("/gr")[1].split("/")[0].trim();

      if (locationValue.trim() == "te") {
        this.user = "teacher";
      } else {
        this.user = "student";
      }
      this.grade = "gr" + gradeValue;
    } else {
      var paramId = window.location.href.split("?")[1];
      if (paramId != undefined) {
        this.localgrade = paramId.split("grade=")[1];
        this.user = paramId != "" ? jQuery.trim((paramId.split("and")[0]).split("user=")[1].toLowerCase()) : "";
      } else {
        this.user = "teacher";
      }
    }
    document.getElementById("dmc-main-container").setAttribute("name", this.user);
    document.getElementById("dmc-main-container").setAttribute("grade", this.grade);
    document.getElementById("dmc-main-container").setAttribute("localgrade", this.localgrade);
    document.getElementById("dmc-main-container").setAttribute("path", this.path);

  }


  /**
   * Represents the call back for DP Ready
   * @method onDPReady
   * @param {any} getObj
   */
  onDPReady(getObj: any) {
    console.log(getObj.annotationId);
    var el = document.getElementsByTagName('head');
    var thiS = Object(el).self;
    if (typeof getObj.data === 'object' && getObj.data.pageInfo) {
      DPOService.gObj.dpoData = JSON.parse(JSON.stringify(getObj.data));
    } else {
      getObj.data = JSON.parse(JSON.stringify(DPOService.gObj.dpoData));
    }
    DPOService.gObj.dpoData = JSON.parse(JSON.stringify(getObj.data));
    DPOService.gObj.dataLoaded = true;

    jQuery('#update-all-field').delay(10).change(function (res: any, data: any) {
      Object(window).dp.dpService.storeData(data);
    });
  }
}